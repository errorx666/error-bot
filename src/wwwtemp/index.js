import { AmbientLight, CanvasTexture, DodecahedronGeometry, Mesh, SpotLight, LinearFilter, MeshStandardMaterial, PCFSoftShadowMap, PerspectiveCamera, PlaneGeometry, PointLight, Scene, Vector2, WebGLRenderer } from 'three';

import { EffectComposer } from 'three/examples/jsm/postprocessing/EffectComposer.js';
import { SSAARenderPass } from 'three/examples/jsm/postprocessing/SSAARenderPass.js';
import { SMAAPass } from 'three/examples/jsm/postprocessing/SMAAPass.js';

( async () => {
	const options = await self.getOptions?.() ?? {
		width: 640,
		height: 480,
		fov: 70,
		messages: Array.from( { length: 12 } ).map( ( _, i ) => `Face ${i}` )
	};
	const FOV = options.fov;
	const WIDTH = options.width;
	const HEIGHT = options.height;
	const ASPECT = WIDTH / HEIGHT;

	const contextOptions = {
		antialias: true,
		depth: true,
		premultipliedAlpha: true,
		stencil: true
	};
	const canvas3d = new OffscreenCanvas( WIDTH, HEIGHT );
	const context3d = canvas3d.getContext( 'webgl2', contextOptions );

	function getMap( str, fillStyle ) {
		const WIDTH = 512;
		const HEIGHT = 512;
		// const canvas2d = new OffscreenCanvas( WIDTH, HEIGHT );
		const canvas2d = document.createElement( 'canvas' );
		canvas2d.width = WIDTH;
		canvas2d.height = HEIGHT;
		document.body.appendChild( canvas2d );
		const c2d = canvas2d.getContext( '2d' );
		const lineHeight = HEIGHT * .125;
		c2d.font = `${lineHeight}px "Georgia"`;
		c2d.antialias = 'subpixel';
		c2d.quality = 'best';
		c2d.textDrawingMode = 'path';
		c2d.textBaseline = 'middle';
		c2d.imageSmoothingEnabled = true;
		c2d.imageSmoothingQuality = 'high';
		c2d.fillStyle = '#fff';
		c2d.fillRect( 0, 0, WIDTH, HEIGHT );
		c2d.fillStyle = fillStyle;
		c2d.textAlign = 'center';
		c2d.lineWidth = 2;
		c2d.strokeStyle = '#ccc';
		c2d.fillText( str, WIDTH * .5, HEIGHT * .5, WIDTH );
		c2d.shadowColor = '#000';
		c2d.shadowOffsetX = 0;
		c2d.shadowOffsetY = 0;
		c2d.shadowBlur = 2;
		c2d.strokeText( str, WIDTH * .5, HEIGHT * .5, WIDTH );
		const tex = new CanvasTexture( canvas2d );
		tex.minFilter = LinearFilter;
		tex.magFilter = LinearFilter;
		tex.anisotropy = 32;
		tex.generateMipmaps = false;
		return tex;
	}

	function getDodecahedron( messages ) {
		const geom = new DodecahedronGeometry( 1 );
		const h = 1 - ( ( 1 - Math.cos( 2 * Math.PI / 5 ) ) / ( 1 + Math.cos( Math.PI / 5 ) ) );

		for( let i = 0; i < 36; ++i ) {
			geom.faces[ i ].materialIndex = Math.floor( i / 3 );
		}
		geom.faceVertexUvs = [
			Array.from( { length: 12 } ).flatMap( () => ( [
				[ new Vector2( 1 / 5, 0 ), new Vector2( 4 / 5, 0 ), new Vector2( 0 / 1, h ) ],
				[ new Vector2( 4 / 5, 0 ), new Vector2( 1 / 1, h ), new Vector2( 0 / 1, h ) ],
				[ new Vector2( 1 / 1, h ), new Vector2( 1 / 2, 1 ), new Vector2( 0 / 1, h ) ]
			] ) )
		];
		geom.uvsNeedUpdate = true;
		const mesh = new Mesh( geom, messages.map( m => {
			return new MeshStandardMaterial( {
				color: 0xddddee,
				map: getMap( m, '#f99' ),
				metalness: .5,
				roughness: .6,
				bumpMap: getMap( m, '#000' ),
				bumpScale: 0.1
			} );
		} ) );
		return mesh;
	}

	function dispose( ...objects ) {
		const props = [ 'material', 'geometry' ];
		for( const object of objects ) {
			if( object == null ) continue;
			if( Array.isArray( object ) ) {
				dispose( ...object );
				continue;
			}
			if( typeof object.traverse === 'function' ) {
				object.traverse( o => {
					if( typeof o.dispose === 'function' ) o.dispose();
				} );
			} else if( typeof object.dispose === 'function' ) {
				object.dispose();
			}
			for( const prop of props ) {
				if( prop in object ) {
					dispose( object[ prop ] );
				}
			}
		}
	}

	const renderer = new WebGLRenderer( {
		canvas: canvas3d,
		context: context3d,
		precision: 'highp',
		...contextOptions
	} );
	renderer.setDrawingBufferSize( WIDTH, HEIGHT, 1 );

	renderer.shadowMap.enabled = true;
	renderer.shadowMap.type = PCFSoftShadowMap;
	renderer.shadowMap.autoUpdate = true;

	const scene = new Scene;
	scene.add( new AmbientLight( 0xffffff, 0.2 ) );
	const camera = new PerspectiveCamera( FOV, ASPECT, 0.1, 20 );
	scene.add( camera );

	const mesh = getDodecahedron( options.messages );
	mesh.castShadow = true;
	scene.add( mesh );
	mesh.rotation.set( Math.PI * 1, Math.PI * 1.5, Math.PI * 1 );

	camera.castShadow = true;
	camera.position.set( 0, .5, 2 );
	camera.lookAt( mesh.position );

	const light1 = new PointLight( 0xffffff, .2 );
	light1.shadow.mapSize.set( 2048, 2048 );
	light1.shadow.camera.near = 0.1;
	light1.shadow.camera.far = 20;
	light1.shadow.bias = 0.001;
	light1.shadow.radius = 6;
	light1.castShadow = true;
	camera.add( light1 );
	light1.position.set( 1, .2, 0 );

	const light2 = new PointLight( 0xffffff, .2 );
	light2.shadow.mapSize.set( 2048, 2048 );
	light2.shadow.camera.near = 0.1;
	light2.shadow.camera.far = 20;
	light2.shadow.bias = 0.001;
	light2.shadow.radius = 6;
	light2.castShadow = true;
	camera.add( light2 );
	light2.position.set( -1, .2, 0 );

	const light3 = new SpotLight( 0xffffff, .4 );
	light3.shadow.mapSize.set( 2048, 2048 );
	light3.shadow.camera.near = 0.1;
	light3.shadow.camera.far = 20;
	light3.shadow.bias = 0.001;
	light3.shadow.radius = 12;
	light3.castShadow = true;
	camera.add( light3 );
	light3.position.set( 0, 1, .5 );

	const plane = new Mesh( new PlaneGeometry( 15, 15, 1, 1 ), new MeshStandardMaterial( {
		color: 0xffffff,
		metalness: 0.1,
		roughness: 0.3
	} ) );

	plane.receiveShadow = true;
	camera.add( plane );
	plane.position.set( 0, 0, -7.5 );
	const composer = new EffectComposer( renderer );
	composer.setPixelRatio( renderer.getPixelRatio() );
	const renderPass = new SSAARenderPass( scene, camera );
	renderPass.sampleLevel = 2;
	composer.addPass( renderPass );
	composer.addPass( new SMAAPass( WIDTH * renderer.getPixelRatio(), HEIGHT * renderer.getPixelRatio() ) );
	composer.render();

	// const canvas2d = new OffscreenCanvas( WIDTH, HEIGHT );
	const canvas2d = document.createElement( 'canvas' );
	canvas2d.width = WIDTH;
	canvas2d.height = HEIGHT;
	document.body.appendChild( canvas2d );
	const c2d = canvas2d.getContext( '2d' );
	c2d.drawImage( canvas3d, 0, 0 );
	dispose( mesh, renderer, scene, camera );
	const { width, height, data } = c2d.getImageData( 0, 0, WIDTH, HEIGHT );
	await self.response?.( { width, height, data: Array.from( data ) } );
} )();
