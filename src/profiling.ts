import { tap } from 'rxjs/operators';
import { Map } from 'immutable';

export class ProfileCounter<TKey = string> {
	private counters = Map<TKey, number>();

	public constructor() {}

	public tap( key: TKey ) {
		return tap( () => { this.count( key ); } );
	}

	public count( key: TKey ) {
		let count = this.counters.get( key ) ?? 0;
		count += 1;
		this.counters = this.counters.set( key, count );
	}

	public toString() {
		const entries = Array.from( this.counters.entries() )
			.sort( ( [ , c1 ], [ , c2 ] ) => c1 - c2 );
		const maxLen = Math.max( ...entries.map( ( [ , c ] ) => String( c ).length ) );
		return entries.map( ( [ key, count ] ) =>
			`${String( count ).padStart( maxLen, ' ' )} : ${key}`
		).join( '\n' );
	}
}
