import { bootstrap } from './bootstrap';

import { errorBot } from './error-bot';
import { Subject, merge, timer, firstValueFrom, lastValueFrom } from 'rxjs';
import { take, takeUntil, exhaustMap } from 'rxjs/operators';
import { promisify } from 'util';
import log4js from 'log4js';
import { Lifecycle } from '~lifecycle';
import { handleErrors } from '~rx';
// import os from 'os';

import SingleInstance from 'single-instance';

const hmrReload = new Subject<true>();
const disposed = new Subject<true>();
if( module.hot ) {
	module.hot.addDisposeHandler( () => {
		disposed.next( true );
		disposed.complete();
	} );
	module.hot.accept();

	module.hot.accept( [
		'./bootstrap.ts',
		'./error-bot.ts',
		'./lifecycle.ts'
	],
	() => {
		hmrReload.next( true );
	} );
} else {
	hmrReload.complete();
}

async function exitProcess( exitCode: number, err?: Error ) {
	if( err ) {
		const logger = log4js.getLogger();
		if( logger ) {
			logger.fatal( err );
		} else {
			console.error( err );
		}
	}
	disposed.next( true );
	disposed.complete();
	try {
		await promisify( log4js.shutdown )();
	} catch {}
	process.exit( exitCode );
}

const singleInstance = new SingleInstance( 'error-bot' );
singleInstance.lock().then( async function() {
	// if( os.platform() === 'win32' ) {
	// 	const { Kernel32 } = require( 'win32-api' );
	// 	const kernel32 = Kernel32.load();
	// 	const ES_CONTINUOUS = 0x80000000;
	// 	const ES_AWAYMODE_REQUIRED = 0x00000040;
	// 	const ES_SYSTEM_REQUIRED = 0x00000001;
	// 	// eslint-disable-next-line new-cap
	// 	kernel32.SetThreadExecutionState( ES_CONTINUOUS | ES_SYSTEM_REQUIRED | ES_AWAYMODE_REQUIRED );
	// }
	try {
		await lastValueFrom( timer( 0, 1000 ).pipe(
			exhaustMap( async () => {
				await bootstrap();
				const lifecycle = new Lifecycle( { name: 'error-bot' } );
				try {
					await errorBot( { lifecycle } );

					if( !lifecycle.isShutdown ) {
						merge( hmrReload, disposed )
						.pipe( takeUntil( lifecycle.shutdown$ ), take( 1 ) )
						.subscribe( () => {
							lifecycle.shutdown();
						} );
					}
					if( !lifecycle.isDisposed ) {
						await lastValueFrom( lifecycle.disposed$ );
					}
				} catch( ex ) {
					lifecycle.error( ex );
					await firstValueFrom( timer( 15 * 1000 ) );
				}
			} ),
			handleErrors( {
				get logger() { return log4js.getLogger(); }
			} ),
			takeUntil( disposed )
		) );
	} catch( err ) {
		await exitProcess( 1, err );
	}
	await exitProcess( 0 );
} ).then( null, async ( err: Error ) => {
	await exitProcess( 1, err ?? new Error( 'could not acquire lock' ) );
} );
