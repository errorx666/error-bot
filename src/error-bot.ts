import { NodeBBSession } from './nodebb/session';
import { NodeBBSocket } from './nodebb/socket';
import { auth } from './nodebb/api';
import { hasher as Hasher } from 'node-object-hash';

import { username, password } from '~data/auth.yaml';
import { Subject, ReplaySubject, BehaviorSubject, merge, timer, firstValueFrom } from 'rxjs';
import { takeUntil, take, filter, concatMap, switchMapTo, exhaustMap, shareReplay } from 'rxjs/operators';
import { getLogger } from 'log4js';
import assert from 'assert';

import mongoConfig from '~data/mongodb.yaml';
import { MongoClient } from 'mongodb';

import moduleConfig from '~data/modules.yaml';
import { Lifecycle } from '~lifecycle';
import { handleErrors } from '~rx';

const disposed = new Subject<true>();
if( module.hot ) {
	module.hot.addDisposeHandler( () => {
		disposed.next( true );
		disposed.complete();
	} );
} else {
	disposed.complete();
}

interface ErrorBotParams {
	readonly lifecycle: Lifecycle;
}
export async function errorBot( { lifecycle }: ErrorBotParams ) {
	const logger = getLogger( 'error-bot' );
	const session = new NodeBBSession;
	let socket: NodeBBSocket;
	let client: MongoClient;
	try {
		const modules = new Map<string, Subject<ModuleFactory>>();

		( function loadModules() {
			const requireContext = require.context( './modules', true, /(?<!\.d)\.tsx?$/ );
			const contexts = [
				require.context( './data', true, /\.yaml$/ ).id,
				require.context( './components', true, /(?<!\.d)\.tsx?$/ ).id
			];
			for( const moduleKey of requireContext.keys() ) {
				const moduleName = moduleKey.replace( /^.\/|\.tsx?$/g, '' );
				const s = modules.get( moduleName ) || new ReplaySubject<ModuleFactory>( 1 );
				modules.set( moduleName, s );
				s.next( requireContext( moduleKey ).default as ModuleFactory );
				if( !module.hot ) {
					s.complete();
				}
			}

			if( module.hot ) {
				module.hot.accept( [
					requireContext.id,
					...contexts
				], () => { loadModules(); } );
			}
		}() );
		const hasher = Hasher();
		await auth.logIn( { session, username, password } );

		const bus = new Subject<BusMessage>();
		const done = merge( lifecycle.shutdown$, disposed,
			bus.pipe( filter( ( { type } ) => type === 'reload-all' ) )
		).pipe( take( 1 ), shareReplay( { bufferSize: 1, refCount: true } ) );

		bus
		.pipe( takeUntil( done ) )
		.subscribe( m => {
			const logger = getLogger( 'bus' );
			logger.info( m );
		} );

		socket = await NodeBBSocket.connect( { session } );
		client = await MongoClient.connect( mongoConfig.url, mongoConfig.clientOptions );

		async function startModule<TModuleName extends ModuleName>( moduleName: TModuleName, params = {} as Omit<ModuleParamsMap[TModuleName], keyof ModuleParams> ) {
			const module$ = modules.get( moduleName );
			assert.ok( module$ != null, `Module not found: ${moduleName}` );
			const db = client.db( moduleName );
			const moduleStateDb = db.collection( 'moduleState' );

			const hash = hasher.hash( { moduleName, params } );
			type ModuleState = ModuleParamsMap[TModuleName][ 'state$' ];
			interface SavedState {
				readonly hash: string;
				readonly value: ModuleState;
			}
			const loadedState = await moduleStateDb.findOne<SavedState>( { hash: { $eq: hash } } );

			const innerState$ = new BehaviorSubject<ModuleState>( loadedState?.value );
			innerState$
			.pipe(
				concatMap( async value => {
					await moduleStateDb.findOneAndReplace( { hash: { $eq: hash } }, { hash, value }, { upsert: true } );
				} ),
				takeUntil( done )
			).subscribe();

			const reload$ = new Subject<true>();

			bus.pipe(
				filter( ( { type } ) => ( type === 'reload-modules' ) ),
				filter( ( { moduleNames }: BusMessageReloadModule ) => moduleNames == null || moduleNames.includes( moduleName ) ),
				takeUntil( done )
			).subscribe( () => {
				reload$.next( true );
			} );

			timer( 0, 1000 )
			.pipe(
				switchMapTo( module$ ),
				exhaustMap( async m => {
					const logger = getLogger( `modules/${moduleName}` );
					const lifecycle = new Lifecycle( { name: `modules/${moduleName}` } );

					merge( done, reload$ )
					.pipe( take( 1 ), takeUntil( lifecycle.shutdown$ ) )
					.subscribe( () => {
						lifecycle.shutdown();
					} );

					try {
						await Promise.resolve( m( {
							...( params as any ),
							db,
							moduleName,
							bus,
							session,
							socket,
							lifecycle,
							logger,
							state$: innerState$
						} ) );
						lifecycle.ready();
					} catch( ex ) {
						lifecycle.error( ex );
					}

					await firstValueFrom( lifecycle.disposed$ );
				} ),
				handleErrors( { logger } ),
				takeUntil( done )
			)
			.subscribe( {
				complete() {
					reload$.complete();
					innerState$.complete();
				}
			} );
		}

		for( const moduleDef of moduleConfig ) {
			const entries = Object.entries( moduleDef );
			assert.strictEqual( entries.length, 1 );
			const [ moduleName, moduleParams ] = entries[ 0 ];
			startModule( moduleName as ModuleName, moduleParams );
		}

		lifecycle.ready();
		await firstValueFrom( done );
		lifecycle.shutdown();
	} catch( ex ) {
		logger.fatal( ex );
		lifecycle.error( ex );
	}
	await Promise.all( [
		() => client?.close(),
		() => socket?.close(),
		() => auth.logOut( { session } )
	].map( async fn => {
		try {
			await Promise.resolve( fn() );
		} catch( ex ) {
			lifecycle.error( ex );
		}
	} ) );
	lifecycle.done();
}
