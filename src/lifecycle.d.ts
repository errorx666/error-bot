declare type LifecycleFsmStates = 'starting'|'running'|'shutting-down'|'disposed';
declare type LifecycleFsmTransitions = 'ready'|'error'|'timeout'|'shutdown'|'done';
declare type LifecycleFsm = import( '~fsm' ).Fsm<LifecycleFsmStates, LifecycleFsmTransitions>;
