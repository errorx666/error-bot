require( 'newrelic/lib/config' ).createInstance( require( '~data/newrelic.yaml' ) );
require( 'newrelic' );
import moment from 'moment';
import momentDurationFormatSetup from 'moment-duration-format';
import { install as installXslt } from 'xslt-ts';
import { DOMImplementationImpl, DOMParserImpl, XMLSerializerImpl } from 'xmldom-ts';
import path from 'path';
import log4js from 'log4js';
import { EventEmitter } from 'events';
import { Subject, fromEvent } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { promisify } from 'util';

const disposed = new Subject<true>();
if( module.hot ) {
	module.hot.addDisposeHandler( () => {
		disposed.next( true );
		disposed.complete();
	} );
} else {
	disposed.complete();
}

export async function bootstrap() {
	momentDurationFormatSetup( moment );
	installXslt( new DOMParserImpl, new XMLSerializerImpl, ( new DOMImplementationImpl ) as any );

	EventEmitter.defaultMaxListeners = 50;

	await promisify( log4js.shutdown )();

	const logDir = path.resolve( __dirname, '..', 'log' );
	const layout = {
		type: 'pattern',
		pattern: '%d %p %c %f:%l %m%n'
	};

	function logFile( filename: string ) {
		return {
			type: 'dateFile',
			filename: path.resolve( logDir, filename ),
			keepFileExt: true,
			numBackups: 30,
			pattern: 'yyyy-MM-dd',
			encoding: 'utf-8',
			layout
		};
	}
	const log4jsConfig = {
		appenders: {
			console: {
				type: 'console',
				layout
			},
			stderr: {
				type: 'stderr',
				layout
			},
			stdout: {
				type: 'stdout',
				layout
			},
			master: logFile( 'master.log' ),
			worker: logFile( 'worker.log' ),
			bus: logFile( 'bus.log' ),
			command: logFile( 'command.log' ),
			query: logFile( 'query.log' ),
			process: logFile( 'process.log' ),
			errors: logFile( 'errors.log' ),
			'console-warn': {
				type: 'logLevelFilter',
				appender: 'console',
				level: 'warn'
			},
			'only-errors': {
				type: 'logLevelFilter',
				appender: 'errors',
				level: 'error'
			}
		},
		categories: {
			default: {
				appenders: [ process.env.NODE_ENV === 'production' ? 'console-warn' : 'console', 'master', 'only-errors' ],
				level: 'info',
				enableCallStack: true
			},
			bus: {
				appenders: [ 'bus', 'only-errors' ],
				level: 'info',
				enableCallStack: true
			},
			command: {
				appenders: [ 'command', 'only-errors' ],
				level: 'info',
				enableCallStack: true
			},
			query: {
				appenders: [ 'query', 'only-errors' ],
				level: 'info',
				enableCallStack: true
			},
			process: {
				appenders: [ 'master', 'process', 'only-errors' ],
				level: 'debug',
				enableCallStack: true
			},
			worker: {
				appenders: [ 'worker', 'only-errors' ],
				level: 'info',
				enableCallStack: true
			}
		}
	};
	console.log( 'configuring log4js' );
	log4js.configure( log4jsConfig );
	console.log( 'log4js configured' );

	const consoleLogger = log4js.getLogger( 'console' );
	Object.defineProperties( console, {
		...Object.fromEntries(
			[ 'debug', 'info', 'warn', 'error', 'fatal', 'trace' ]
			.map( name => ( [ name, {
				value: consoleLogger[ name ].bind( consoleLogger ),
				configurable: true
			} ] ) )
		),
		log: {
			value: consoleLogger.info.bind( consoleLogger ),
			configurable: true
		}
	} );

	const processLogger = log4js.getLogger( 'process' );

	const processEvt = {
		beforeExit: 'info',
		exit: 'info',
		disconnect: 'debug',
		message: 'debug',
		multipleResolves: 'debug',
		rejectionHandled: 'debug',
		rejectionUnhandled: 'error',
		uncaughtException: 'fatal',
		warning: 'warn'
	};

	for( const [ evt, type ] of Object.entries( processEvt ) ) {
		fromEvent( process, evt )
		.pipe( takeUntil( disposed ) )
		.subscribe( args => {
			processLogger[ type ]( evt, args );
		} );
	}
}
