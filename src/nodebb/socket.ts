import { fromEvent, throwError, timer, merge, Observable, Subject, firstValueFrom } from 'rxjs';
import { take, switchMap, map, takeUntil } from 'rxjs/operators';

import io from 'socket.io-client';
import type { ConnectOpts, Socket } from 'socket.io-client';


import { userAgent, baseUrl, connectTimeout, emitTimeout } from '~data/config.yaml';
import { NodeBBSession } from './session';
import { getAgent } from '~proxy-agent';
import { getLogger } from 'log4js';

const disposed = new Subject<true>();
if( module.hot ) {
	module.hot.addDisposeHandler( () => {
		disposed.next( true );
		disposed.complete();
	} );
} else {
	disposed.complete();
}

type SessionOpts = { session: NodeBBSession };

const logger = getLogger( 'socket' );

export class NodeBBSocket {
	private constructor( { socket }: { socket: Socket } ) {
		this.socket = socket;

		disposed
		.pipe( take( 1 ) )
		.subscribe( () => {
			this.close();
		} );
	}

	public static async connect( { session }: SessionOpts ) {
		const socket =
			io( baseUrl, {
				forceNew: true,
				timeout: connectTimeout,
				multiplex: false,
				rejectUnauthorized: false,
				reconnectionAttempts: Infinity,
				transports: [ 'websocket' ],
				extraHeaders: {
					'User-Agent': userAgent,
					Cookie: session.jar.getCookieString( baseUrl )
				},
				agent: getAgent( baseUrl )
			} as ConnectOpts );

		await firstValueFrom( merge(
			fromEvent( socket, 'connect' ),
			fromEvent( socket, 'connect_error' )
			.pipe( switchMap( err => {
				logger.error( err );
				return throwError( () => new Error( err as string ) );
			 } ) )
		) );

		return new NodeBBSocket( { socket } );
	}

	public emit<T>( event: string, ...args: any[] ): Promise<T> {
		const { socket } = this;
		return firstValueFrom( merge(
			new Observable<T>( observer => {
				socket.emit( event, ...args, ( err, data ) => {
					if( err ) {
						const message = String( err?.message ?? '' );
						logger.error( new Error( message ) );
						this.error$.next( { message } );
						observer.error( err );
					} else {
						observer.next( data );
						observer.complete();
					}
				} );
				return () => {};
			} ),
			fromEvent( socket, 'error' )
			.pipe( switchMap( err => throwError( err ) ) ),
			merge( timer( emitTimeout ), this.closed$ )
			.pipe( take( 1 ), switchMap( () => throwError( 'emit timeout' ) ) )
		) );
	}

	public getEvent<T extends keyof NodeBB.EventMap>( event: T ): Observable<( NodeBB.EventMap[T] extends object ? NodeBB.EventMap[T] : { value: NodeBB.EventMap[T] } ) & { event: T }> {
		const { socket } = this;
		return fromEvent<NodeBB.EventMap[T]>( socket, event )
		.pipe( map( args => ( {
			event,
			...(
				( typeof args === 'object' && args != null )
				? ( args as object ) :
				{ value: args }
			)
		} ) ), takeUntil( this.closed$ ) ) as any;
	}

	public close() {
		this.socket.close();
		this.error$.complete();
		this.closed$.next( true );
		this.closed$.complete();
	}

	public readonly error$ = new Subject<{ readonly message: string; }>();
	public readonly closed$ = new Subject<true>();

	public socket: Socket;
}
