import assert from 'assert';
import { MessagePort, parentPort, workerData } from 'worker_threads';
import { Lifecycle } from '~lifecycle';
import { fromEvent } from 'rxjs';
import { takeUntil, take, concatMap, filter } from 'rxjs/operators';
import log4js, { getLogger } from 'log4js';
import { promisify } from 'util';
import { handleErrors } from '~rx';

const workerName = workerData.name;
const lifecycle = new Lifecycle( { name: workerName } );
const logger = getLogger( `worker/${workerName}` );

interface VerifyOptions {
	readonly factsDataBitmask: readonly ( readonly [ number, number, number ] )[];
	readonly minesDataBitmask: number;
}
function verify( { factsDataBitmask, minesDataBitmask }: VerifyOptions ) {
	for( const [ pointBitmask, op, n ] of factsDataBitmask ) {
		const totalMines = countBits( pointBitmask & minesDataBitmask );
		switch( op ) {
		case 0:
			if( totalMines !== n ) return false;
			break;
		case 1:
			if( totalMines > n ) return false;
			break;
		case 2:
			if( totalMines < n ) return false;
			break;
		default:
			assert.fail();
		}
	}
	return true;
}

function countBits( n: number ) {
	let result = 0;
	while( n > 0 ) {
		result += ( n & 1 );
		n = n >> 1;
	}
	return result;
}

function getBits( n: number, bits: number ) {
	let result = [] as readonly number[];
	for( let i = 0; i < bits; ++i ) {
		result = [ ...result, ( ( n & 1 ) === 1 ) ? 1 : 0 ];
		n = n >> 1;
	}
	return result;
}

interface Message {
	readonly type: 'request';
	readonly port: MessagePort;
	readonly factsDataBitmask: readonly ( readonly [ number, number, number ] )[];
	readonly knownBitmask: number;
	readonly start: number;
	readonly count: number;
	readonly slots: number;
}

fromEvent<Message>( parentPort, 'message' )
.pipe(
	filter( e => e?.type === 'request' ),
	concatMap( async ( { port, factsDataBitmask, knownBitmask, start, count, slots }: Message ) => {
		try {
			let result = [] as readonly ( readonly number[] )[];
			for( let i = 0; i < count; ++i ) {
				const minesDataBitmask = knownBitmask | ( start + i );
				if( verify( { factsDataBitmask, minesDataBitmask } ) ) {
					result = [ ...result, getBits( minesDataBitmask, slots ) ];
				}
			}
			port.postMessage( { result } );
		} finally {
			port.close();
		}
	} ),
	handleErrors( { logger } ),
	takeUntil( lifecycle.shutdown$ )
)
.subscribe();

lifecycle.shutdown$
.pipe( take( 1 ) )
.subscribe( () => {
	lifecycle.done();
} );

lifecycle.disposed$
.pipe( take( 1 ) )
.subscribe( async () => {
	await promisify( log4js.shutdown )();
	process.exit( 0 );
} );

fromEvent( parentPort, 'close' )
.pipe( take( 1 ), takeUntil( lifecycle.shutdown$ ) )
.subscribe( () => {
	lifecycle.shutdown();
} );

fromEvent<any>( parentPort, 'message' )
.pipe(
	filter( e => e?.type === 'shutdown' ),
	take( 1 ),
	takeUntil( lifecycle.shutdown$ )
)
.subscribe( () => {
	parentPort.close();
} );

if( module.hot ) {
	module.hot.addDisposeHandler( () => {
		lifecycle.shutdown();
	} );
}

lifecycle.ready();

logger.info( 'test' );
