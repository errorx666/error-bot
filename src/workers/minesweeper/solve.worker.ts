import { List, Set, Map } from 'immutable';
import { ThreadPool } from '~threads';
import assert from 'assert';
import { Lifecycle } from '~lifecycle';
import { workerData, parentPort } from 'worker_threads';
import { takeUntil, take, concatMap, filter } from 'rxjs/operators';
import { fromEvent } from 'rxjs';
import log4js, { getLogger } from 'log4js';
import { promisify } from 'util';
import { dump } from '~util';
import { StopWatch } from '~stop-watch';
import { handleErrors } from '~rx';

const workerName = workerData.name;
const lifecycle = new Lifecycle( { name: workerName } );
const logger = getLogger( `worker/${workerName}` );

const formatter = new Intl.NumberFormat( 'en-US', {
	minimumFractionDigits: 0,
	maximumFractionDigits: 2,
	useGrouping: true
} );

type Point = readonly [ number, number ];
type Points = readonly Point[];
const contradiction = Symbol( 'contradiction' );
type Contradiction = readonly [ typeof contradiction, string ];

const enum BruteforceOp {
	eq = 0,
	lte = 1,
	gte = 2
}

interface NumMinesUnwrapped {
	readonly type: BruteforceOp;
	readonly n: number;
}

type NumMines = List<BruteforceOp|number>;
type Fact = List<Set<List<number>>|NumMines>;

interface FactUnwrapped {
	readonly points: Points;
	readonly numMines: NumMinesUnwrapped;
}

interface FactDetails {
	readonly fact: Fact;
	readonly points: Set<List<number>>;
	readonly numMines: NumMinesUnwrapped;
	readonly only: Set<List<number>>;
}

interface FactParams {
	readonly trace: boolean;
	readonly both: Set<List<number>>;
	readonly difference: Set<List<number>>;
	readonly fact1: FactDetails;
	readonly fact2?: FactDetails;
}

interface FactResult {
	readonly newFacts?: Set<Fact>;
	readonly oldFacts?: Set<Fact>;
}

interface BruteForceWorkerRequest {
	readonly knownBitmask: number;
	readonly factsDataBitmask: readonly ( readonly [ number, BruteforceOp, number ] ) [];
	readonly start: number;
	readonly count: number;
	readonly slots: number;
}
type BruteForceWorkerResult = ( 0|1 )[][];

// brute force algorithm is O(n)=2^n ...  This is the maximum n, we're willing to brute force
const BRUTE_FORCE_THRESHOLD = 26;

const bruteForceWorkers = new ThreadPool<BruteForceWorkerRequest, BruteForceWorkerResult>( {
	src: 'minesweeper/brute-force.worker',
	lifecycle,
	logger
} );

function pointToList( point: Point ) {
	return List( point );
}

function listToPoint( list: List<number> ) {
	return list.toArray() as [ number, number ] as Point;
}

function pointsToSet( points: Points ) {
	return Set( points.map( pointToList ) );
}

function setToPoints( set: Set<List<number>> ): Points {
	return set.map( listToPoint ).toArray();
}

function isNumMinesWrapped( numMines: NumMines|NumMinesUnwrapped ): numMines is NumMines {
	return !( 'type' in numMines );
}

function isFactWrapped( fact: Fact|FactUnwrapped ): fact is Fact {
	return !( 'points' in fact );
}

function unwrapNumMines( numMines: NumMines|NumMinesUnwrapped ) {
	if( !isNumMinesWrapped( numMines ) ) return numMines;
	const arr = numMines.toArray();
	assert.equal( arr.length, 2 );
	const [ type, n ] = arr as [ BruteforceOp, number ];
	return { type, n };
}

function wrapNumMines( numMines: NumMines|NumMinesUnwrapped ) {
	if( isNumMinesWrapped( numMines ) ) return numMines;
	return List( [ numMines.type, numMines.n ] ) as NumMines;
}

function unwrapFact( fact: Fact|FactUnwrapped ) {
	if( !isFactWrapped( fact ) ) return fact;
	assert.equal( fact.size, 2 );
	const points = pointsInFact( fact );
	const numMines = fact.get( 1 ) as NumMines;
	return {
		points: setToPoints( points ),
		numMines: unwrapNumMines( numMines )
	};
}

function wrapFact( fact: Fact|FactUnwrapped ) {
	if( isFactWrapped( fact ) ) return fact;
	return List( [ pointsToSet( fact.points ), wrapNumMines( fact.numMines ) ] ) as Fact;
}

function formatPoints( set: Set<List<number>>|Points ) {
	let points: Points;
	if( Array.isArray( set ) ) points = set as Points;
	else points = setToPoints( set as Set<List<number>> );
	return [ '{', sortPoints( points ).map( p => formatPoint( p ) ).join( ', ' ), '}' ].filter( f => !!f ).join( ' ' );
}

function formatNumMines( numMines: NumMines|NumMinesUnwrapped ) {
	let nm: NumMinesUnwrapped;
	if( 'type' in numMines ) nm = numMines as NumMinesUnwrapped;
	else nm = unwrapNumMines( numMines as NumMines );
	let op: string;
	switch( nm.type ) {
	case BruteforceOp.eq: op = '='; break;
	case BruteforceOp.lte: op = '<='; break;
	case BruteforceOp.gte: op = '>='; break;
	default: assert.fail();
	}
	return [ op, nm.n ].join( ' ' );
}

function formatFact( fact: Fact|FactUnwrapped ) {
	fact = unwrapFact( fact );
	return [ formatPoints( fact.points ), formatNumMines( fact.numMines ) ].join( ' ' );
}

function sameFacts( { fact1, fact2, trace }: FactParams ) {
	if( !fact1.points.equals( fact2.points ) ) return {};
	let oldFacts = Set<Fact>();
	let newFacts = Set<Fact>();
	if( fact1.numMines.type === BruteforceOp.eq ) {
		if( fact2.numMines.type === BruteforceOp.eq ) return [ contradiction, `conflicting eq` ] as Contradiction;
		oldFacts = oldFacts.add( fact2.fact );
	}
	if( fact1.numMines.type === BruteforceOp.lte ) {
		if( fact2.numMines.type === BruteforceOp.gte ) {
			if( fact1.numMines.n === fact2.numMines.n ) {
				oldFacts = oldFacts.add( fact1.fact );
				oldFacts = oldFacts.add( fact2.fact );
				newFacts = newFacts.add( List( [ fact1.points, wrapNumMines( { type: BruteforceOp.eq, n: fact1.numMines.n } ) ] ) );
			}
		}
		if( fact2.numMines.type === BruteforceOp.lte ) {
			const largerFact = ( fact1.numMines.n > fact2.numMines.n ) ? fact1 : fact2;
			oldFacts = oldFacts.add( largerFact.fact );
		}
	}
	if( fact1.numMines.type === BruteforceOp.gte ) {
		if( fact2.numMines.type === BruteforceOp.gte ) {
			const smallerFact = ( fact1.numMines.n < fact2.numMines.n ) ? fact1 : fact2;
			oldFacts = oldFacts.add( smallerFact.fact );
		}
		if( fact2.numMines.type === BruteforceOp.lte ) {
			if( fact1.numMines.n === fact2.numMines.n ) {
				oldFacts = oldFacts.add( fact1.fact );
				oldFacts = oldFacts.add( fact2.fact );
				newFacts = newFacts.add( List( [ fact1.points, wrapNumMines( { type: BruteforceOp.eq, n: fact1.numMines.n } ) ] ) );
			}
		}
	}
	oldFacts = oldFacts.subtract( newFacts );
	return { oldFacts, newFacts };
}

function subsetFacts( { fact1, fact2, difference }: FactParams ) {
	const superset = fact1;
	const subset = fact2;
	if( subset.points.size === superset.points.size ) return {};
	if( !subset.points.isSubset( superset.points ) ) return {};
	let oldFacts = Set<Fact>();
	let newFacts = Set<Fact>();
	assert.ok( difference.size > 0 );
	if( superset.numMines.type === BruteforceOp.eq ) {
		if( subset.numMines.type === BruteforceOp.eq ) {
			if( subset.numMines.n > superset.numMines.n ) {
				return [ contradiction, 'subset has more mines than superset' ] as Contradiction;
			} else if( subset.numMines.n === superset.numMines.n ) {
				oldFacts = oldFacts.add( superset.fact );
				newFacts = newFacts.add( List( [ difference, wrapNumMines( { type: BruteforceOp.eq, n: 0 } ) ] ) );
			} else if( subset.numMines.n <= 0 ) {
				oldFacts = oldFacts.add( superset.fact );
				newFacts = newFacts.add( List( [ difference, superset.fact.get( 1 ) ] ) );
			} else if( subset.numMines.n === subset.points.size ) {
				oldFacts = oldFacts.add( superset.fact );
				newFacts = newFacts.add( List( [ difference, wrapNumMines( { type: BruteforceOp.eq, n: superset.numMines.n - subset.numMines.n } ) ] ) );
			} else if( superset.numMines.n > 0 ) {
				const minesInDifference = superset.numMines.n - subset.numMines.n;
				if( minesInDifference <= superset.numMines.n ) {
					newFacts = newFacts.add( List( [ difference, wrapNumMines( { type: BruteforceOp.eq, n: minesInDifference } ) ] ) );
				}
			}
		} else if( subset.numMines.type === BruteforceOp.gte ) {
			const minesInDifferenceAtMost = superset.numMines.n - subset.numMines.n;
			if( minesInDifferenceAtMost < difference.size ) {
				newFacts = newFacts.add( List( [ difference, wrapNumMines( { type: BruteforceOp.lte, n: minesInDifferenceAtMost } ) ] ) );
			}
		} else if( subset.numMines.type === BruteforceOp.lte ) {
			const minesInDifferenceAtLeast = superset.numMines.n - subset.numMines.n;
			if( minesInDifferenceAtLeast > 0 ) {
				newFacts = newFacts.add( List( [ difference, wrapNumMines( { type: BruteforceOp.gte, n: minesInDifferenceAtLeast } ) ] ) );
			}
		}
	}
	if( superset.numMines.type === BruteforceOp.gte ) {
		if( subset.numMines.type === BruteforceOp.eq ) {
			oldFacts = oldFacts.add( superset.fact );
			newFacts = newFacts.add( List( [ difference, wrapNumMines( { type: BruteforceOp.gte, n: superset.numMines.n - subset.numMines.n } ) ] ) );
		} else if( subset.numMines.type === BruteforceOp.gte ) {
			// oldFacts = oldFacts.remove( superset.fact );
			// newFacts = newFacts.add( List( [ difference, wrapNumMines( { type: BruteforceOp.gte, n: superset.numMines.n - subset.numMines.n } ) ] ) );
		} else if( subset.numMines.type === BruteforceOp.lte ) {
			//
		}
	}
	if( superset.numMines.type === BruteforceOp.lte ) {
		if( subset.numMines.type === BruteforceOp.eq ) {
			oldFacts = oldFacts.add( superset.fact );
			newFacts = newFacts.add( List( [ difference, wrapNumMines( { type: BruteforceOp.lte, n: superset.numMines.n - subset.numMines.n } ) ] ) );
		} else if( subset.numMines.type === BruteforceOp.gte ) {
			// oldFacts = oldFacts.remove( superset.fact );
			// newFacts = newFacts.add( List( [ difference, wrapNumMines( { type: BruteforceOp.lte, n: superset.numMines.n - subset.numMines.n } ) ] ) );
		} else if( subset.numMines.type === BruteforceOp.lte ) {
			// newFacts = newFacts.add( List( [ difference, wrapNumMines( { type: BruteforceOp.gte, n: superset.numMines.n - subset.numMines.n } ) ] ) );
		}
	}
	oldFacts = oldFacts.subtract( newFacts );
	return { oldFacts, newFacts };
}

function overlappingFacts( { fact1, fact2, both, difference }: FactParams ) {
	if( fact1.only.size === fact1.points.size || fact1.only.size === 0
	||	fact2.only.size === fact2.points.size || fact2.only.size === 0 ) return {};
	let oldFacts = Set<Fact>();
	let newFacts = Set<Fact>();
	if( fact1.numMines.type === BruteforceOp.eq ) {
		const overflow = fact1.numMines.n - fact1.only.size;
		if( overflow >= both.size ) {
			oldFacts = oldFacts.add( fact1.fact );
			newFacts = newFacts.add( List( [ fact1.only, wrapNumMines( { type: BruteforceOp.eq, n: fact1.only.size } ) ] ) );
			newFacts = newFacts.add( List( [ both, wrapNumMines( { type: BruteforceOp.eq, n: both.size } ) ] ) );
		} else if( overflow > 0 ) {
			// newFacts = newFacts.add( List( [ fact1.only, wrapNumMines( { type: BruteforceOp.lte, n: fact1.only.size } ) ] ) );
			newFacts = newFacts.add( List( [ both, wrapNumMines( { type: BruteforceOp.gte, n: overflow } ) ] ) );
		} else {
			// TODO: more inferences?
		}
		if( fact1.numMines.n === 0 ) {
			oldFacts = oldFacts.add( fact1.fact );
			oldFacts = oldFacts.add( fact2.fact );
			newFacts = newFacts.add( List( [ both, wrapNumMines( { type: BruteforceOp.eq, n: 0 } ) ] ) );
			newFacts = newFacts.add( List( [ fact2.only, wrapNumMines( fact2.numMines ) ] ) );
		}
	}
	oldFacts = oldFacts.subtract( newFacts );
	return { newFacts, oldFacts };
}


function shouldTrace( { fact1, fact2, result }: Omit<FactParams, 'trace'> & { readonly result: FactResult; } ) {
	// if( fact1.points.size > 8 || fact2.points.size > 8 ) return true;
	// const debugPoints = pointsToSet( parsePoints( 'e16 d16-d20 e20 d18' ) );
	// if( fact1.points.equals( debugPoints ) ) return true;
	// if( fact2?.points.equals( debugPoints ) ) return true;
	// if( result?.oldFacts?.some( f => pointsInFact( f ).equals( debugPoints ) ) ) return true;
	// if( result?.newFacts?.some( f => pointsInFact( f ).equals( debugPoints ) ) ) return true;
	// if( result?.oldFacts?.some( f => pointsInFact( f ).intersect( debugPoints ).size > 0 ) ) return true;
	// if( result?.newFacts?.some( f => pointsInFact( f ).intersect( debugPoints ).size > 0 ) ) return true;
	return false;
}

function pointsInFact( fact: Fact ) {
	return fact.get( 0 ) as Set<List<number>>;
}

function splitFacts( facts: Set<Fact> ) {
	let factGroups = Set<Set<Fact>>();
	for( const fact of facts.values() ) {
		let found = false;
		const factPoints = pointsInFact( fact );
		for( const factGroup of factGroups.values() ) {
			const factGroupPoints = factGroup.flatMap( pointsInFact );
			if( factGroupPoints.some( p => factPoints.includes( p ) ) ) {
				found = true;
				factGroups = factGroups.remove( factGroup ).add( factGroup.add( fact ) );
				break;
			}
		}
		if( !found ) factGroups = factGroups.add( Set<Fact>( [ fact ] ) );
	}
	for( ;; ) {
		let oldFactGroups = [] as ReadonlyArray<Set<Fact>>;
		let newFactGroups = [] as ReadonlyArray<Set<Fact>>;
		for( const fg1 of factGroups.values() ) {
			if( oldFactGroups.includes( fg1 ) ) continue;
			for( const fg2 of factGroups.values() ) {
				if( fg1 === fg2 || oldFactGroups.includes( fg2 ) ) continue;
				const fp1 = fg1.flatMap( pointsInFact );
				const fp2 = fg2.flatMap( pointsInFact );
				if( fp1.intersect( fp2 ).size > 0 ) {
					oldFactGroups = [ ...oldFactGroups, fg1, fg2 ];
					newFactGroups = [ ...newFactGroups, fg1.concat( fg2 ) ];
				}
			}
		}
		if( newFactGroups.length === 0 ) {
			assert.strictEqual( oldFactGroups.length, 0 );
			break;
		}
		factGroups = factGroups.subtract( oldFactGroups ).concat( newFactGroups );
	}
	// dump( logger, {
	// 	facts: factGroups.map( fg => fg.map( formatFact ).toArray() ).toArray()
	// } );
	return factGroups;
}

function joinFacts( factGroups: Iterable<Set<Fact>> ) {
	return Set( factGroups ).flatMap( fg => fg );
}

async function bruteForce( facts: Set<Fact>, info: Map<List<number>, 0|1|null> ) {
	let allValues = Map<List<number>, 0|1|null>();
	for( const fact of facts.values() ) {
		const points = pointsInFact( fact );
		for( const point of points.values() ) {
			if( allValues.has( point ) ) continue;
			allValues = allValues.set( point, info.get( point ) );
		}
	}
	const knownValues = allValues.filter( v => v != null );
	const unknownValues = allValues.filter( v => v == null );
	const unknownSet = unknownValues.keySeq().toSet();
	const knownSet = knownValues.keySeq().toSet();
	let factsDataBitmask = [] as readonly [ number, BruteforceOp, number ][];
	const pointMap = knownSet.valueSeq().concat( unknownSet.valueSeq() );
	for( const fact of facts.values() ) {
		const points = pointsInFact( fact );
		if( points.isSubset( knownSet ) ) continue;
		const numMines = fact.get( 1 ) as NumMines;
		factsDataBitmask = [ ...factsDataBitmask, [ points.reduce( ( a, b ) => a | ( 1 << pointMap.indexOf( b ) ), 0 ), numMines.get( 0 ) as BruteforceOp, numMines.get( 1 ) as number ] ];
	}
	const knownBitmask = knownValues.valueSeq().reduce( ( a, b, i ) => a | ( b ? ( 1 << i ) : 0 ), 0 );
	const numPermutations = 2 ** unknownValues.size;
	const chunkSize = Math.min( Math.ceil( numPermutations / bruteForceWorkers.threadCount ), 1024 );
	// const stopWatch = StopWatch.start();
	// stopWatch.mark();
	let results = [] as Promise<BruteForceWorkerResult>[];
	for( let chunk = 0; ; ++chunk ) {
		const start = chunkSize * chunk;
		if( start >= numPermutations ) break;
		results = [ ...results, bruteForceWorkers.request( {
			knownBitmask,
			factsDataBitmask,
			start,
			count: chunkSize,
			slots: unknownValues.size
		} ) ];
	}
	const result = ( await Promise.all( results ) ).flatMap( r => r );
	// stopWatch.mark( 'batch complete' );

	return List(
		result
		.map( answers =>
			Map<List<number>, 0|1>( answers.map( ( v, i ) => [ pointMap.get( i ), v ] as [ List<number>, 0|1 ] ) )
		)
	);
}

function verify( facts: Set<Fact>, values: Map<List<number>, 0|1> ) {
	let result = true;
	for( const fact of facts.values() ) {
		const list = pointsInFact( fact );
		const v = list.toArray().map( p => values.get( p ) );
		if( v.some( v => v == null ) ) {
			result = null;
			continue;
		}
		const totalMines = v.reduce( ( u, v ) => u + v, 0 );
		const numMines = unwrapNumMines( fact.get( 1 ) as NumMines );
		switch( numMines.type ) {
		case BruteforceOp.eq:
			if( totalMines !== numMines.n ) result = false;
			break;
		case BruteforceOp.lte:
			if( totalMines > numMines.n ) result = false;
			break;
		case BruteforceOp.gte:
			if( totalMines < numMines.n ) result = false;
			break;
		default: assert.fail();
		}
		if( result === false ) {
			dump( logger, {
				totalMines,
				numMines: formatNumMines( numMines ),
				points: formatPoints( list ),
				values: v
			} );
		}
		if( result !== true ) break;
	}
	return result;
}

function intersections( x: Points, y: Points ) {
	return x.filter( ( [ xx, xy ] ) =>
		y.some( ( [ yx, yy ] ) => xx === yx && xy === yy )
	);
}

function intersects( x: Points, y: Points ) {
	return intersections( x, y ).length > 0;
}

function exclude( x: Points, y: Points ): Points {
	return x.filter( x => !intersects( [ x ], y ) );
}

interface PointsAroundOptions {
	readonly point: Point;
	readonly width: number;
	readonly height: number;
	readonly size?: number;
	readonly includeSelf?: boolean;
}
function pointsAround( { point, width, height, size = 1, includeSelf = true }: PointsAroundOptions ) {
	return Array.from( function *() {
		for( let x = -size; x <= size; ++x ) {
			const cx = point[ 0 ] + x;
			if( cx < 0 || cx >= width ) continue;
			for( let y = -size; y <= size; ++y ) {
				const cy = point[ 1 ] + y;
				if( cy < 0 || cy >= height ) continue;
				const v = [ cx, cy ] as Point;

				if( includeSelf || !intersects( [ point ], [ v ] ) ) yield v;
			}
		}
	}() );
}

function formatPoint( [ x, y ]: Point ) {
	return 'abcdefghijklmnopqrstuvwxyz'.charAt( y ) + String( x + 1 );
}

function comparePoints( [ x1, y1 ]: Point, [ x2, y2 ]: Point ) {
	if( y1 < y2 ) return -1;
	if( y1 > y2 ) return 1;
	if( x1 < x2 ) return -1;
	if( x1 > x2 ) return 1;
	return 0;
}

function sortPoints( points: Points ): Points {
	return Set( points.map( p => List( p ) ) )
	.map( p => p.toArray() as [ number, number ] as Point )
	.toArray()
	.sort( comparePoints ) as Points;
}

function collectFacts( { revealed, mines, flags, width, height, subset } ) {
	let facts = Set<Fact>();
	let allPoints = Set<List<number>>();
	for( const point of revealed ) {
		const pointsSurrounding = pointsAround( { point, width, height, includeSelf: false } );
		const flagsAround = intersections( flags, pointsSurrounding ); // mines found (not verified!)
		const minesAround = intersections( mines, pointsSurrounding ); // number shown on board
		const unknownAround = exclude( pointsSurrounding, [ ...revealed, ...flags ] ); // squares that are both unrevealed and unflagged
		const mineCount = minesAround.length - flagsAround.length; // mines unaccounted for
		assert.ok( mineCount >= 0 );
		if( unknownAround.length === 0 ) continue;
		if( subset != null && !intersects( unknownAround, subset ) ) continue;
		allPoints = allPoints.union( pointsToSet( unknownAround ) );
		const fact = wrapFact( { points: unknownAround, numMines: { type: BruteforceOp.eq, n: mineCount } } );
		facts = facts.add( fact );
	}

	// const fullRange = getRange( [ 0, 0 ], [ width - 1, height - 1 ] );
	// const allUnrevealed = exclude( fullRange, [ ...wasRevealed, ...wasFlagged ] );
	// if( subset == null ) {
	// 	if( allUnrevealed.length <= 8 ) {
	// 		allPoints = pointsToSet( allUnrevealed );
	// 	}
	// }

	// const unfoundMines = mines.length - flags.length;
	// if( allPoints.size >= allUnrevealed.length ) {
	// 	facts = facts.add( List( [ allPoints, wrapNumMines( { type: BruteforceOp.eq, n: unfoundMines } ) ] ) );
	// }

	// dump( logger, {
	// 	facts: facts.map( f => formatFact( f ) ).toArray()
	// } );

	return facts;
}

function simplifyFact( { fact, trace }: { fact: Fact, trace: boolean } ): FactResult {
	let newFacts = Set<Fact>();
	let oldFacts = Set<Fact>();
	const points = pointsInFact( fact );
	const { type, n } = unwrapNumMines( fact.get( 1 ) as NumMines );
	if( points.size === 0 ) {
		oldFacts = oldFacts.add( fact );
	} else {
		switch( type ) {
		case BruteforceOp.eq:
			if( points.size > 1 ) {
				if( n >= points.size ) {
					for( const point of points.values() ) {
						newFacts = newFacts.add( List( [ Set( [ point ] ), wrapNumMines( { type: BruteforceOp.eq, n: 1 } ) ] ) );
					}
					oldFacts = oldFacts.add( fact );
				} else if( n <= 0 ) {
					for( const point of points.values() ) {
						newFacts = newFacts.add( List( [ Set( [ point ] ), wrapNumMines( { type: BruteforceOp.eq, n: 0 } ) ] ) );
					}
					oldFacts = oldFacts.add( fact );
				}
			}
			break;
		case BruteforceOp.lte:
			if( n >= points.size ) {
				oldFacts = oldFacts.add( fact );
			} else if( n <= 0 ) {
				for( const point of points.values() ) {
					newFacts = newFacts.add( List( [ Set( [ point ] ), wrapNumMines( { type: BruteforceOp.eq, n: 0 } ) ] ) );
				}
				oldFacts = oldFacts.add( fact );
			}
			break;
		case BruteforceOp.gte:
			if( n <= 0 ) {
				oldFacts = oldFacts.add( fact );
			} else if( points.size <= n ) {
				for( const point of points.values() ) {
					newFacts = newFacts.add( List( [ Set( [ point ] ), wrapNumMines( { type: BruteforceOp.eq, n: 1 } ) ] ) );
				}
				oldFacts = oldFacts.add( fact );
			}
			break;
		default: break;
		}
	}
	return { oldFacts, newFacts };
}

interface Message {
	readonly type: 'request';
	readonly width: number;
	readonly height: number;
	readonly mines: Points;
	readonly flags: Points;
	readonly revealed: Points;
	readonly subset?: Points;
	readonly port: MessagePort;
}

fromEvent<Message>( parentPort, 'message' )
.pipe(
	filter( e => e?.type === 'request' ),
	concatMap( async ( { width, height, mines, flags, revealed, subset, port }: Message ) => {
		try {
			let obsoleteFacts = Set<Fact>();
			let mergedFacts = Set<List<Fact>>();
			function mergeFacts( facts: Set<Fact>, ...fns: readonly ( ( params: FactParams ) => FactResult|Contradiction )[] ) {
				let newFacts = Set<Fact>();
				for( const fact1 of facts.values() ) {
					assert.notEqual( fact1, null );
					if( obsoleteFacts.has( fact1 ) ) continue;
					for( const fact2 of facts.values() ) {
						assert.notEqual( fact2, null );
						if( fact1 === fact2 ) continue;
						if( obsoleteFacts.has( fact2 ) ) continue;
						if( mergedFacts.has( List( [ fact1, fact2 ] ) ) ) continue;
						mergedFacts = mergedFacts.add( List( [ fact1, fact2 ] ) );
						const fact1Points = pointsInFact( fact1 );
						assert.ok( Set.isSet( fact1Points ) );
						const fact2Points = pointsInFact( fact2 );
						assert.ok( Set.isSet( fact2Points ) );
						const both = fact1Points.intersect( fact2Points );
						if( both.size === 0 ) continue;
						const fact1NumMines = unwrapNumMines( fact1.get( 1 ) as NumMines );
						const fact2NumMines = unwrapNumMines( fact2.get( 1 ) as NumMines );
						const only1 = fact1Points.subtract( fact2Points );
						const only2 = fact2Points.subtract( fact1Points );
						const difference = only1.concat( only2 );
						const f1 = { fact: fact1, points: fact1Points, numMines: fact1NumMines, only: only1 };
						const f2 = { fact: fact2, points: fact2Points, numMines: fact2NumMines, only: only2 };
						for( const fn of fns ) {
							let result = fn( { fact1: f1, fact2: f2, both, difference, trace: false } );
							if( Array.isArray( result ) && result[ 0 ] === contradiction ) {
								dump( logger, {
									operation: fn.name,
									fact1: formatFact( fact1 ),
									fact2: formatFact( fact2 ),
									contradiction: result[ 1 ]
								} );

								const result2 = fn( { fact1: f1, fact2: f2, both, difference, trace: true } );
								assert.deepEqual( result, result2 );
								return contradiction;
							}
							result = ( result ?? {} ) as FactResult;
							if( !( result.oldFacts?.size > 0 ) && !( result.newFacts?.size > 0 ) ) continue;
							if( shouldTrace( { fact1: f1, fact2: f2, both, difference, result } ) ) {
								dump( logger, {
									operation: fn.name,
									both: formatPoints( both ),
									difference: formatPoints( difference ),
									fact1: formatFact( fact1 ),
									fact2: formatFact( fact2 ),
									oldFacts: result?.oldFacts?.map( f => formatFact( f ) ).toArray(),
									newFacts: result?.newFacts?.map( f => formatFact( f ) ).toArray()
								} );

								const result2 = fn( { fact1: f1, fact2: f2, both, difference, trace: true } );
								assert.deepEqual( result, result2 );
							}
							assert.ok( !result.newFacts?.has( null ) );
							assert.ok( !result.oldFacts?.has( null ) );
							if( result.oldFacts != null ) {
								obsoleteFacts = obsoleteFacts.concat( result.oldFacts );
							}
							if( result.newFacts != null ) {
								newFacts = newFacts.concat( result.newFacts );
							}
							if( obsoleteFacts.has( fact1 ) ) break;
						}
					}
				}
				for( const newFact of newFacts.values() ) {
					const result = simplifyFact( { fact: newFact, trace: false } ) ?? {};
					if( result.oldFacts != null ) {
						obsoleteFacts = obsoleteFacts.concat( result.oldFacts );
					}
					if( result.newFacts != null ) {
						newFacts = newFacts.concat( result.newFacts );
					}
				}
				facts = facts.concat( newFacts ).subtract( obsoleteFacts );
				assert.ok( !facts.has( null ) );
				return facts;
			}

			let allValues = Map<List<number>, 0|1|null>();
			let probabilities = [] as readonly [ Point, number ][];

			function learnValues( points: Set<List<number>>, value: 0|1 ) {
				assert.ok( value != null );
				for( const point of points.values() ) {
					const oldValue = allValues.has( point ) ? allValues.get( point ) : null;
					if( oldValue != null ) assert.equal( oldValue, value );
					allValues = allValues.set( point, value );
				}
			}

			async function doSolve( facts: Set<Set<Fact>> ) {
				const MAX_ITER = 32;
				for( let pass = 0; pass < MAX_ITER; ++pass ) {
					const oldFacts = facts;
					facts = splitFacts( facts.flatMap( f => {
						const result = mergeFacts( f, sameFacts, subsetFacts, overlappingFacts );
						if( result === contradiction ) throw new Error( 'contradiction' );
						return result;
					} ) );
					if( facts === oldFacts ) break;
				}
				return facts;
			}

			const factsSplit = await doSolve( splitFacts( collectFacts( { width, height, mines, revealed, flags, subset } ) ) );
			const factsJoined = joinFacts( factsSplit.values() );

			for( const fact of factsJoined.values() ) {
				const factPoints = pointsInFact( fact );
				for( const factPoint of factPoints.values() ) {
					if( !allValues.has( factPoint ) ) allValues = allValues.set( factPoint, null );
				}
				const { numMines, points } = unwrapFact( fact );
				if( [ BruteforceOp.gte, BruteforceOp.eq ].includes( numMines.type ) ) {
					if( numMines.n >= points.length ) {
						learnValues( factPoints, 1 );
					}
				}
				if( [ BruteforceOp.lte, BruteforceOp.eq ].includes( numMines.type ) ) {
					if( numMines.n <= 0 ) {
						learnValues( factPoints, 0 );
					}
				}
			}

			if( verify( factsJoined, allValues ) === false ) {
				dump( logger, {
					facts: factsJoined.map( formatFact ).toArray(),
					results:
						allValues
						.map( ( value, list ) => ( {
							point: formatPoint( list.toArray() as [ number, number ] as Point ),
							value
						} ) )
						.valueSeq()
						.toArray()
				} );
				assert.fail();
			}

			const stopWatch = StopWatch.start();
			stopWatch.mark();
			await Promise.all( factsSplit.map( async facts => {
				const allPoints = facts.flatMap( pointsInFact );
				const values = allValues.filter( ( _, point ) => allPoints.has( point ) );
				const unknownValues = values.filter( v => v == null );
				if( unknownValues.size <= 0 ) return;
				if( unknownValues.size > BRUTE_FORCE_THRESHOLD ) return;
				const workingSolutions = await bruteForce( facts, values );
				if( lifecycle.isShutdown ) return;
				let p = Map<List<number>, number>();
				for( const workingSolution of workingSolutions.values() ) {
					for( const [ list, value ] of workingSolution.entries() ) {
						if( values.get( list ) != null ) continue;
						let num = p.get( list ) ?? 0;
						if( value === 1 ) ++num;
						p = p.set( list, num );
					}
				}
				for( const [ point, value ] of p.entries() ) {
					if( value === workingSolutions.size ) {
						learnValues( Set( [ point ] ), 1 );
					} else if( value === 0 ) {
						learnValues( Set( [ point ] ), 0 );
					} else {
						const probability = value / workingSolutions.size;
						probabilities = [ ...probabilities, [ point.toArray() as [ number, number ] as Point, probability ] ];
					}
				}
			} ).toArray() );
			stopWatch.mark( 'brute force complete' );

			const values =
				allValues
				.toArray()
				.map( ( [ list, value ] ) => [
					list.toArray() as [ number, number ] as Point,
					value
				] ) as readonly [ Point, 0|1|null ][];

			const wasFlagged = flags;
			const wasRevealed = revealed;
			flags = values.filter( ( [ , value ] ) => value === 1 ).map( ( [ point ] ) => point );
			flags = sortPoints( exclude( flags, [ ...wasFlagged, ...wasRevealed ] ) );
			revealed = values.filter( ( [ , value ] ) => value === 0 ).map( ( [ point ] ) => point );
			revealed = sortPoints( exclude( revealed, [ ...wasFlagged, ...wasRevealed ] ) );

			const falsePositives = flags.filter( f => !intersects( mines, [ f ] ) );
			const falseNegatives = intersections( mines, revealed );

			dump( logger, {
				facts: factsJoined.map( formatFact ).toArray(),
				falsePositives: formatPoints( falsePositives ),
				falseNegatives: formatPoints( falseNegatives ),
				flags: formatPoints( flags ),
				mines: formatPoints( mines ),
				revealed: formatPoints( revealed ),
				probabilities: probabilities.map( ( [ point, probability ] ) => ( [ formatPoint( point ), `${formatter.format( probability * 100 )}%`, intersects( mines, [ point ] ) ] ) ),
				results: Object.fromEntries( values.map( ( [ point, value ] ) => [ formatPoint( point ), value ] ) )
			} );

			port.postMessage( { result: { probabilities, values } } );
		} finally {
			port.close();
		}
	} ),
	handleErrors( { logger } ),
	takeUntil( lifecycle.shutdown$ )
).subscribe();

lifecycle.shutdown$
.pipe( take( 1 ) )
.subscribe( () => {
	bruteForceWorkers.shutdown();
	lifecycle.done();
} );

lifecycle.disposed$
.pipe( take( 1 ) )
.subscribe( async () => {
	await promisify( log4js.shutdown )();
	process.exit( 0 );
} );

fromEvent( parentPort, 'close' )
.pipe( take( 1 ), takeUntil( lifecycle.shutdown$ ) )
.subscribe( () => {
	lifecycle.shutdown();
} );

fromEvent<any>( parentPort, 'message' )
.pipe(
	filter( e => e?.type === 'shutdown' ),
	take( 1 ),
	takeUntil( lifecycle.shutdown$ )
)
.subscribe( () => {
	parentPort.close();
} );

if( module.hot ) {
	module.hot.addDisposeHandler( () => {
		lifecycle.shutdown();
	} );
}

lifecycle.ready();
