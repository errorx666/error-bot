import log4js from 'log4js';
import { EventEmitter } from 'events';
import { fromEvent } from 'rxjs';
import moment from 'moment';
import { install as installXslt } from 'xslt-ts';
import { DOMImplementationImpl, DOMParserImpl, XMLSerializerImpl } from 'xmldom-ts';
import momentDurationFormatSetup from 'moment-duration-format';
import { workerData } from 'worker_threads';

momentDurationFormatSetup( moment );

installXslt( new DOMParserImpl, new XMLSerializerImpl, ( new DOMImplementationImpl ) as any );

EventEmitter.defaultMaxListeners = 50;

const consoleLogger = log4js.getLogger( 'console' );
Object.defineProperties( console, {
	...Object.fromEntries(
		[ 'debug', 'info', 'warn', 'error', 'fatal', 'trace' ]
		.map( name => ( [ name, {
			value: consoleLogger[ name ].bind( consoleLogger ),
			configurable: true
		} ] ) )
	),
	log: {
		value: consoleLogger.info.bind( consoleLogger ),
		configurable: true
	}
} );

const workerName = workerData.name;
const logger = log4js.getLogger( `worker/${workerName}` );

const processEvt = {
	beforeExit: 'info',
	exit: 'info',
	disconnect: 'debug',
	message: 'debug',
	multipleResolves: 'debug',
	rejectionHandled: 'debug',
	rejectionUnhandled: 'error',
	uncaughtException: 'fatal',
	warning: 'warn',
	SIGHUP: 'warn',
	SIGINT: 'warn',
	SIGKILL: 'fatal',
	SIGUSR2: 'warn',
	SIGTERM: 'fatal'
};

for( const [ evt, type ] of Object.entries( processEvt ) ) {
	fromEvent( process, evt )
	.subscribe( args => {
		logger[ type ]( evt, args );
	} );
}

logger.info( `${workerName} starting` );
