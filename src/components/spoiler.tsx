import React, { PureComponent } from 'react';
import type { ReactNode } from 'react';

export interface SpoilerProps {
	readonly summary?: ReactNode;
	readonly open?: boolean;
	readonly children: ReactNode;
}

export class Spoiler extends PureComponent<SpoilerProps> {
	public constructor( props: SpoilerProps ) {
		super( props );
	}

	public render() {
		const { props } = this;
		return <details open={props.open || false}>
			{ props.summary ? <summary>{props.summary}</summary> : <></> }
			{ props.children }
		</details>;
	}
}
