import { PureComponent } from 'react';
import { allowedAttribs, allowedTags } from '~data/html.yaml';
import reactHtmlParser, { convertNodeToElement } from 'react-html-parser';

export interface HtmlPropsBase {
	readonly baseUrl?: URL|string;
}

export interface HtmlPropsRaw extends HtmlPropsBase {
	readonly html: string;
}

export interface HtmlPropsDom extends HtmlPropsBase {
	readonly element: Element;
}

export type HtmlProps = HtmlPropsRaw|HtmlPropsDom;

function getHtml( props: HtmlProps ) {
	if( typeof ( props as HtmlPropsRaw ).html === 'string' ) {
		return ( props as HtmlPropsRaw ).html;
	} else {
		return ( props as HtmlPropsDom ).element?.outerHTML || '';
	}
}

export class Html extends PureComponent<HtmlProps> {
	public constructor( props: HtmlProps ) {
		super( props );
	}

	public render() {
		const { baseUrl } = this.props;
		return reactHtmlParser( getHtml( this.props ), {
			transform( node, index, transform ) {
				if( node.type === 'tag' ) {
					if( !allowedTags.includes( node.name ) ) {
						return null;
					}
					node = { ...node, attribs: Object.fromEntries(
						Object.entries( node.attribs )
						.filter( ( [ key ] ) => !/^(?:on|data-)/i.test( key ) )
						.filter( ( [ key ] ) => allowedAttribs.some( attr => {
							if( Array.isArray( attr ) ) {
								if( node.name !== attr[ 0 ] ) return false;
								return attr.slice( 1 ).includes( key );
							} else {
								return key === attr;
							}
						} ) )
					) };
					if( node.name === 'a' ) {
						if( node.attribs.href ) {
							return convertNodeToElement( { ...node, attribs: {
								...node.attribs,
								href: new URL( node.attribs.href, baseUrl ).href,
								target: '_blank',
								rel: 'noopener noreferrer'
							} }, index, transform );
						} else {
							return convertNodeToElement( { ...node, name: 'span', attribs: {} }, index, transform );
						}
					}
					if( node.name === 'img' ) {
						if( node.attribs.src ) {
							return convertNodeToElement( { ...node, attribs: {
								...node.attribs,
								src: new URL( node.attribs.src, baseUrl ).href
							} }, index, transform );
						}
					}
				}
			}
		} );
	}
}
