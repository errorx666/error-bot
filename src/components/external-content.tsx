import React, { PureComponent } from 'react';
import type { ReactNode } from 'react';
import { Spoiler } from './spoiler';

declare interface ExternalContentProps {
	readonly name: ReactNode;
	readonly url?: string;
	readonly title?: ReactNode;
	readonly children: ReactNode;
	readonly via?: string;
}

export class ExternalContent extends PureComponent<ExternalContentProps> {
	public constructor( props: ExternalContentProps ) {
		super( props );
	}

	public render() {
		const { props } = this;
		const summary = <>{props.name}{' said '}{ props.url ? <>{' in '}<a href={props.url} target="_blank" rel="noopener noreferrer">{props.url}</a> </> : <></> }:</>;
		return <Spoiler summary={summary} open={true}>
			{ props.title ? <h1>{props.title}</h1> : <></> }
			{ props.children }
			{ props.via
			? <p>
				(via{' '}
					<a href={props.via} target="_blank" rel="noopener noreferrer">
						{props.via}
					</a>
				)
			</p> : <></> }
		</Spoiler>;
	}
}
