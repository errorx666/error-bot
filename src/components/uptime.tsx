import React, { PureComponent } from 'react';
import { duration } from 'moment';

export interface UptimeProps {
	ms: number;
}

export class Uptime extends PureComponent<UptimeProps> {
	public constructor( props: UptimeProps ) {
		super( props );
	}

	public render() {
		const { props } = this;
		const uptime = duration( props.ms, 'ms' );
		const iso = uptime.toISOString();
		const formatted = ( uptime as any ).format( 'y [years], d [days], h [hours], m [minutes], s [seconds]', { trim: 'all' } );
		return (
			<p>
				{'I have been alive for '}
				<time dateTime={iso}>
					{formatted}
				</time>
				.
			</p>
		);
	}
}
