import { duration } from 'moment';
import { getLogger } from 'log4js';

function formatMs( ms: number ) {
	return ( duration( ms, 'ms' ) as any ).format();
}

const logger = getLogger( 'stop-watch' );

export class StopWatch {
	public constructor( private startTime: number ) {}
	private lastMark: number;

	public static start() {
		return new StopWatch( this.now );
	}

	public static get now() {
		return +new Date;
	}

	public get sinceMark() {
		const { lastMark } = this;
		if( !lastMark ) return null;
		return StopWatch.now - lastMark;
	}

	public get sinceStart() {
		return StopWatch.now - this.startTime;
	}

	public mark( message?: string ) {
		if( message ) logger.info( message, formatMs( this.sinceMark ) );
		this.lastMark = StopWatch.now;
		return this;
	}

	public toString() {
		return formatMs( this.sinceStart );
	}
}
