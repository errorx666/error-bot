import { Logger } from 'log4js';
import { Lifecycle } from '~lifecycle';
import { timer, Subject, empty, BehaviorSubject, ReplaySubject, merge } from 'rxjs';
import { exhaustMap, takeUntil, switchMap, share, onErrorResumeNext, mapTo, take } from 'rxjs/operators';
import { webSocket, WebSocketSubject } from 'rxjs/webSocket';
import { getAgent } from '~proxy-agent';
import { handleErrors } from '~rx';
import assert from 'assert';

const WebSocket = require( 'ws' );
const WebSocketCtor = function WebSocketCtor( url: string, protocols?: string|string[] ) {
	return new WebSocket( url, protocols, {
		agent: getAgent( url )
	} );
} as any;

export interface ConnectWsOptions {
	readonly url: string;
	readonly logger: Logger;
	readonly lifecycle: Lifecycle;
	readonly retryDelay?: number;
}
export function connectWs<T>( { url, logger, lifecycle, retryDelay } ) {
	if( retryDelay == null ) retryDelay = 1000;
	const ws = new BehaviorSubject<WebSocketSubject<T>>( null );
	const wsIn = ws.pipe(
		switchMap( w => w ?? empty() ),
		takeUntil( lifecycle.shutdown$ ),
		share()
	);

	const wsOut = new Subject<T>();
	const wsConnect = new Subject<boolean>();
	const wsDisconnect = new Subject<boolean>();
	const sub =	timer( 0, retryDelay )
	.pipe(
		exhaustMap( async () => {
			assert( ws.value == null );
			const opener = new ReplaySubject<Event>( 1 );
			const closer = new ReplaySubject<Event|void>( 1 );
			const w = webSocket<T>( {
				openObserver: opener,
				closeObserver: closer,
				closingObserver: closer,
				url,
				WebSocketCtor
			} );
			// logger.info( `connecting to ${url}...` );
			const sub = w.subscribe();
			try {
				const connected =
					await merge(
						opener.pipe( mapTo( true ) ),
						closer.pipe( mapTo( false ) )
					)
					.pipe( take( 1 ) )
					.toPromise();
				if( connected ) {
					// logger.info( `connected to ${url}` );
					wsConnect.next( true );
					ws.next( w );
					await closer.pipe( take( 1 ) ).toPromise();
				}
			} finally {
				sub.unsubscribe();
				closer.complete();
				opener.complete();
				w.complete();
				if( ws.value != null ) {
					ws.next( null );
					wsDisconnect.next( true );
					// logger.info( `disconnected from ${url}` );
				}
			}
 		} ),
		handleErrors( { logger, retryDelay } ),
		takeUntil( lifecycle.shutdown$ )
	)
	.subscribe();

	wsOut
	.pipe(
		onErrorResumeNext( empty() ),
		takeUntil( lifecycle.shutdown$ )
	).subscribe( {
		complete() {
			logger.info( 'ws disconnect' );
			sub.unsubscribe();
			ws.complete();
			wsConnect.complete();
			wsDisconnect.complete();
		}
	} );

	let queue = [] as readonly T[];
	wsOut.pipe(
		handleErrors( { logger, retryDelay } ),
		takeUntil( lifecycle.shutdown$ )
	).subscribe( msg => {
		queue = [ ...queue, msg ];
	} );

	ws.pipe(
		switchMap( w =>
			( w == null )
			? empty()
			: timer( 0, 10 ).pipe( mapTo( w ) )
		),
		takeUntil( lifecycle.shutdown$ )
	).subscribe( ws => {
		assert.notEqual( ws, null );
		if( queue.length === 0 ) return;
		let msg: T;
		( [ msg, ...queue ] = queue );
		// logger.info( `tx> ${JSON.stringify( msg )}` );
		ws.next( msg );
	} );

	return { wsIn, wsOut, wsConnect, wsDisconnect };
}
