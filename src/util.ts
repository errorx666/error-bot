import now from 'performance-now';
import striptags from 'striptags';
import merge from 'lodash/merge';
import { encode } from 'html-entities';
import { Logger } from 'log4js';
import { TimeSpan } from '~time-span';

function tagImpl( fn: Func<string, string> ) {
	return 	( strings: readonly string[], ...keys: readonly any[] ) => {
		const max = Math.max( strings.length, keys.length );
		const out = [] as string[];
		for( let i = 0; i < max; ++i ) {
			const string = String( strings[ i ] ?? '' );
			const key = String( keys[ i ] ?? '' );
			out.push( string, fn( key ) );
		}
		return out.join( '' );
	};
}

export function escapeMarkdown( str: string ) {
	return str.replace( /\[|\]|\(|\)|\*|>|`|_|\\|@/g, s => `\\${s}` );
}

export const tagMarkdown = tagImpl( escapeMarkdown );

export function escapeHtml( str: string ) {
	return encode( str ?? '', { level: 'html5', mode: 'nonAsciiPrintable', numeric: 'hexadecimal' } );
}

export const tagHtml = tagImpl( escapeHtml );

export const tagUrl = tagImpl( encodeURIComponent );

const formatMsTable = [
	[ 1000, 'ms' ],
	[ 60, 's' ],
	[ 60, 'm' ],
	[ Infinity, 'h' ]
] as [ number, string ][];
export function formatMs( ms: number ) {
	let div: number;
	let suffix: string;
	for( [ div, suffix ] of formatMsTable ) {
		if( Math.abs( ms ) < div ) break;
		ms /= div;
	}
	return `${ms.toPrecision( 3 )}${suffix}`;
}

export function perfTest<T extends ( ...args: any ) => any>( name: string, fn: T ): T {
	return function( ...args ) {
		console.log( `${name} started` );
		const before = now();
		const retval = fn.apply( this, args );
		const elapsed = now() - before;
		console.log( `${name} took ${formatMs( elapsed )}` );
		return retval;
	} as T;
}

export function perfTestAsync<T extends ( ...args: any ) => Promise<any>>( name: string, fn: T ): T {
	return async function( ...args ) {
		console.log( `${name} started` );
		const before = now();
		const retval = await fn.apply( this, args );
		const elapsed = now() - before;
		console.log( `${name} took ${formatMs( elapsed )}` );
		return retval;
	} as T;
}

export function normalize( str: string, options?: Partial<NormalizeOptions> ) {
	options = merge( {
		trim: true,
		collapse: true,
		stripTags: true
	} as NormalizeOptions, options );

	if( !str ) str = '';
	if( options.stripTags ) str = striptags( str, [ 'a', 'img' ], ' ' );
	str = str.replace( /\r\n?/g, '\n' );
	if( options.collapse ) str = str.replace( /[^\S\n]+/g, ' ' ).replace( /\n+/g, '\n' );
	if( options.trim ) str = str.replace( /^[^\S\n]+|[^\S\n]+$/gm, '' ).trim();

	return str;
}

export function sleep( ms: number|TimeSpan ) {
	if( typeof ms === 'number' ) {
		return sleep( TimeSpan.fromMilliseconds( ms ) );
	}
	return new Promise<void>( resolve => {
		const m = ms.milliseconds;
		if( isFinite( m ) ) {
			setTimeout( () => { resolve(); }, m );
		}
	} );
}

export async function timeout( ms: number ): Promise<never> {
	await sleep( ms );
	throw new Error( 'Timeout' );
}

export function getTimeout<T>( fn: PromiseLike<T>, ms: number ) {
	return Promise.race<T>( [ Promise.resolve( fn ), timeout( ms ) ] );
}

export function distinct<T>( arr: Enumerable<T> ) {
	return Array.from( new Set( Array.from( arr ) ) );
}

export function allExceptIndex<T>( arr: Enumerable<T>, ...indices: readonly number[] ): T[] {
	return Array.from( arr ).filter( ( _, i ) => !indices.includes( i ) );
}

export function allExceptValue<T>( arr: Enumerable<T>, ...values: readonly T[] ): T[] {
	return Array.from( arr ).filter( ( v ) => !values.includes( v ) );
}

export function dump( logger: Logger, ...vals: readonly any[] ) {
	const { inspect } = require( 'util' );
	logger.info( '%s', ...vals.map( val => inspect( val, {
		depth: 15
	} ) ) );
}

export function getDataUrl( contentType: string, data: string|Buffer ) {
	if( typeof data === 'string' ) data = Buffer.from( data, 'utf8' );
	return `data:${contentType};base64,${data.toString( 'base64' )}`;
}

export function bisect<T>( values: Iterable<T>, index: number ) {
	return [
		[].slice.call( values, 0, index ),
		[].slice.call( values, index )
	] as readonly [ readonly T[], readonly T[] ];
}

export function bisectRight<T>( values: Iterable<T>, index: number ) {
	const arr = Array.from( values );
	return bisect( arr, Math.max( arr.length - index, 0 ) );
}

export function bisectFirst<T>( values: Iterable<T> ) {
	const [ [ first ], next ] = bisect( values, 1 );
	return [ first, next ] as readonly [ T, readonly T[] ];
}

export function bisectLast<T>( values: Iterable<T> ) {
	const [ prev, [ last ] ] = bisectRight( values, 1 );
	return [ prev, last ] as readonly [ readonly T[], T ];
}

export function *permutate<T>( ...values: readonly ( Enumerable<T> )[] ): Iterable<readonly T[]> {
	if( values.length <= 0 ) return;
	if( values.length === 1 ) {
		for( const value of values[ 0 ] ) {
			yield [ value ];
		}
		return;
	}
	const [ allButLast, lastValue ] = bisectLast( values );
	for( const others of permutate( ...allButLast ) ) {
		for( const value of lastValue ) {
			yield [ ...others, value ];
		}
	}
}

export function sum<T = number>( v: Iterable<T>, project: ( value: T ) => number = x => ( x == null ) ? 0 : Number( x ) ) {
	return Array.from( v ).reduce( ( x, y ) => x + project( y ), 0 );
}

export function spacesBetween( v: Iterable<any>|number ) {
	if( v == null ) return 0;
	let len: number;
	if( typeof v === 'number' ) len = Math.floor( v );
	else len = Array.from( v ).length;
	if( len < 2 ) return 0;
	return len - 1;
}

export type Lazy<T> = ( () => T ) & { readonly called: boolean; };

export function lazy<T>( fn: () => T ): Lazy<T> {
	let called = false;
	let value: any;
	return Object.defineProperties( function( ...args: any ) {
		if( !called ) {
			value = fn.apply( this, args );
			called = true;
			fn = null;
		}
		return value;
	} as any, {
		called: { get: () => called }
	} );
}
