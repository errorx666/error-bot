declare const QUERY_IGNORE: unique symbol;

type QueryableValue<T> =
	T
	|typeof QUERY_IGNORE
	|( T extends object
		? { readonly [ TKey in keyof T ]?: T[TKey]|typeof QUERY_IGNORE }
		: never
	);

declare interface QueryLiteral {
	readonly $value: boolean;
}

declare interface QueryEq<T> {
	readonly $eq: T;
}

declare interface QueryGt<T> {
	readonly $gt: T;
}

declare interface QueryGte<T> {
	readonly $gte: T;
}

declare interface QueryIn<T> {
	readonly $in: ( T extends ReadonlyArray<any> ? T : T|readonly T[] );
}

declare interface QueryLt<T> {
	readonly $lt: T;
}

declare interface QueryLte<T> {
	readonly $lte: T;
}

declare interface QueryNe<T> {
	readonly $ne: T;
}

declare interface QueryNin<T> {
	readonly $nin: ( T extends ReadonlyArray<any> ? T : T|readonly T[] );
}

declare type QueryComparison<T> = Partial<QueryEq<T> & QueryGt<T> & QueryGte<T> & QueryIn<T> & QueryLt<T> & QueryLte<T> & QueryNe<T> & QueryNin<T>>;

declare interface QueryIsArray {
	readonly $isArray: boolean;
}

declare interface QueryIsNullish {
	readonly $isNullish: boolean;
}

declare interface QueryIsTruthy {
	readonly $isTruthy: boolean;
}

declare interface QueryIsFalsy {
	readonly $isFalsy: boolean;
}

declare interface QueryType {
	readonly $type: QueryCondition<'bigint'|'boolean'|'function'|'number'|'object'|'string'|'symbol'|'undefined'>;
}

declare interface QueryIsNaN {
	readonly $isNaN: boolean;
}

declare interface QueryIsFinite {
	readonly $isFinite: boolean;
}

declare interface QueryIsInteger {
	readonly $isInteger: boolean;
}

declare interface QueryIsApproximately {
	readonly $isApproximately: number;
}

declare type QueryJs = Partial<QueryIsArray & QueryIsNullish & QueryIsTruthy & QueryIsFalsy & QueryType & QueryIsNaN & QueryIsFinite & QueryIsInteger & QueryIsApproximately>

declare interface QueryRegex {
	readonly $regex: RegExp|string;
}

declare interface QueryWhere<T> {
	readonly $where: ( value: QueryableValue<T> ) => boolean;
}

declare type QueryEvaluation<T> = Partial<QueryRegex & QueryWhere<T>>;

declare interface QueryAll<T> {
	readonly $all: T extends readonly any[] ? QueryValue<ArrayType<T>> : never;
}

declare interface QueryElemMatch<T> {
	readonly $elemMatch: T extends readonly any[] ? QueryValue<ArrayType<T>> : never;
}

declare interface QuerySize {
	readonly $size: QueryCondition<number>;
}

declare type QueryArray<T> = Partial<QueryAll<T> & QueryElemMatch<T> & QuerySize>;

declare type QueryCondition<T> = Partial<QueryComparison<T> & QueryEvaluation<T> & QueryArray<T> & QueryJs>;

declare interface QueryAnd<T> {
	readonly $and: readonly Query<T>[];
}

declare interface QueryNot<T> {
	readonly $not: Query<T>;
}

declare interface QueryNor<T> {
	readonly $nor: readonly Query<T>[];
}

declare interface QueryOr<T> {
	readonly $or: readonly Query<T>[];
}

declare type QueryLogical<T> = Partial<QueryAnd<T> & QueryNot<T> & QueryNor<T> & QueryOr<T>>;

declare type QueryValue<T> = QueryLiteral | QueryCondition<T> | QueryLogical<T>;

declare type QueryObject<T extends object> = {
	readonly [ TKey in keyof T ]?: QueryValue<T[TKey]> | (
		T[TKey] extends object ? QueryObject<T[TKey]> :
		T[TKey] extends string|number|boolean ? T[TKey] :
		never );
}

declare type Query<T> = QueryValue<T> | ( T extends object ? QueryObject<T> : never );
