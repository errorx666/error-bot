/// <reference types="dompurify"/>

declare module '~data/*.yaml';

declare module '~data/auth.yaml' {
	export const username: string;
	export const password: string;
}

declare module '~data/config.yaml' {
	export const userAgent: string;
	export const baseUrl: string;
	export const topicId: number;
	export const connectTimeout: number;
	export const emitTimeout: number;
	export const cycleDelay: number;
	export const actionDelay: number;
	export const retryDelay: number;
	export const proxy: string|undefined;
	export const roles: {
		readonly [ key: string ]: Query<CommandIssuer>;
	};
}

declare module '~data/modules.yaml' {
	const modules: readonly {
		readonly [ TModuleName in ModuleName ]: Omit<ModuleParamsMap[ TModuleName ], keyof ModuleParams<any>>;
	}[];
	export default modules;
}

declare module '~data/mongodb.yaml' {
	import { MongoClientOptions } from 'mongodb';
	export const url: string;
	export const clientOptions: MongoClientOptions;
}

declare module '~data/*.xml' {
	const doc: string;
	export default doc;
}

declare module '~data/covid.yaml' {
	export const apiHost: string;
	export const apiKey: string;
}

declare module '~data/deep-ai.yaml' {
	export const apiKey: string;
}

declare module '~data/giphy.yaml' {
	export const apiKey: string;
}

declare module '~data/*.xslt' {
	const doc: string;
	export default doc;
}

declare module '~data/random.yaml' {
	export const apiKey: string;
}

declare module '~data/wolfram-alpha.yaml' {
	export const appName: string;
	export const appId: string;
}

declare module '~data/m-w.yaml' {
	export const apiKeys: {
		readonly dictionary: string;
		readonly thesaurus: string;
	};
}

declare module '~data/quotes.yaml' {
	const database: { readonly [ name: string ]: {
		readonly aliases: readonly string[]
		readonly avatar: string;
		readonly quotes: readonly string[];
	}; };
	export default database;
}

declare module '~data/minecraft.yaml' {
	export const apiKey: readonly string;
	export const url: readonly string;
}

declare module '~data/webhooks.yaml' {
	export const apiKeys: readonly string[];
}

declare module '~data/html.yaml' {
	export const allowedTags: readonly string[];
	export const allowedAttribs: readonly ( string | readonly string[] )[];
}

declare module '~data/trivia.yaml' {
	export const category: DictionaryOf<number, {
		readonly name: string;
		readonly alias?: readonly string[];
	}>;
	export const difficulty: DictionaryOf<'easy'|'medium'|'hard', {
		readonly name: string;
	}>;
	export const type: DictionaryOf<'multiple'|'boolean', {
		readonly name: string;
	}>;
}
