import assert from 'assert';
import { renderToStaticMarkup } from 'react-dom/server';

import { getLogger } from 'log4js';

// import postcss from 'postcss';
// import cssnano from 'cssnano';
import * as svgo from 'svgo';

import imagemin from 'imagemin';
import imageminPngcrush from 'imagemin-pngcrush';

import postcss from 'postcss';
import cssnano from 'cssnano';


export const ELEMENT_NODE = 1;
/** @deprecated */
export const ATTRIBUTE_NODE = 2;
export const TEXT_NODE = 3;
export const CDATA_SECTION_NODE = 4;
/** @deprecated */
export const ENTITY_REFERENCE_NODE = 5;
/** @deprecated */
export const ENTITY_NODE = 6;
export const PROCESSING_INSTRUCTION_NODE = 7;
export const COMMENT_NODE = 8;
export const DOCUMENT_NODE = 9;
export const DOCUMENT_TYPE_NODE = 10;
export const DOCUMENT_FRAGMENT_NODE = 11;
/** @deprecated */
export const NOTATION_NODE = 12;

const logger = getLogger();

interface CloneXMLDocumentOptions {
	readonly defaultNamespace?: string;
	readonly xmlns?: Readonly<Record<string, string>>;

}
export function cloneDocument( doc: Document, { defaultNamespace = doc.documentElement?.namespaceURI, xmlns = {} } = {} as CloneXMLDocumentOptions ) {
	if( defaultNamespace != null ) {
		xmlns = { ...xmlns, '': defaultNamespace };
	}
	const assignNamespace = ( namespaceURI: string ) => {
		namespaceURI = namespaceURI ?? '';
		if( !namespaceURI && defaultNamespace == null ) {
			defaultNamespace = namespaceURI;
			xmlns = { ...xmlns, '': defaultNamespace };
			return '';
		}
		// 26 prefixes should be enough for anyone
		for( const c of 'abcdefghijklmnopqrstuvwxyz'.split( '' ) ) {
			if( !{}.hasOwnProperty.call( xmlns, c ) ) {
				xmlns = { ...xmlns, [ c ]: namespaceURI };
				return c;
			}
		}
		assert.fail();
	};
	const getNamespacePrefix = ( namespaceURI: string ) => {
		namespaceURI = namespaceURI ?? '';
		for( const [ prefix, namespace ] of Object.entries( xmlns ) ) {
			if( namespace === namespaceURI ) return prefix;
		}
		return assignNamespace( namespaceURI );
	};
	const getQualifiedName = ( namespaceURI: string, localName: string ) => {
		const prefix = getNamespacePrefix( namespaceURI );
		if( prefix ) return `${prefix}:${localName}`;
		else return localName;
	};

	const doc2 = doc.implementation.createDocument(
		doc.documentElement.namespaceURI,
		getQualifiedName( doc.documentElement.namespaceURI, doc.documentElement.localName ),
		doc.doctype
	);

	const cloneElement = ( srcElement: Element, destElement: Element ) => {
		const attrs = Array.from( srcElement.attributes ?? [] );
		for( const attr of attrs ) {
			if( attr.localName === 'xmlns' ) continue;
			if( attr.prefix === 'xmlns' ) continue;
			if( attr.namespaceURI ) {
				destElement.setAttributeNS( attr.namespaceURI, getQualifiedName( attr.namespaceURI, attr.localName ), attr.value );
			} else {
				destElement.setAttribute( attr.localName, attr.value );
			}
		}
		for( const childNode of Array.from( srcElement.childNodes ) ) {
			if( childNode.nodeType === ATTRIBUTE_NODE ) continue;
			let child: Node;
			if( childNode.nodeType === ELEMENT_NODE ) {
				const c = childNode as Element;
				child = cloneElement( c, doc2.createElementNS( c.namespaceURI, getQualifiedName( c.namespaceURI, c.localName ) ) );
			} else {
				child = doc2.importNode( childNode, true );
			}
			destElement.appendChild( child );
		}
		return destElement;
	};

	const documentElement = cloneElement( doc.documentElement, doc2.documentElement );
	for( const [ prefix, namespaceURI ] of Object.entries( xmlns ) ) {
	 	if( prefix ) documentElement.setAttribute( `xmlns:${prefix}`, namespaceURI );
	}
	return doc2;
}

export async function shrinkCss( css: string ) {
	try {
		const result = await postcss( [
			cssnano( {
				preset: [ 'default', {
					discardComments: {
						removeAll: true
					}
				} ]
			} ) as any
		] ).process( css, { from: undefined, to: undefined } );
		return result.css;
	} catch( ex ) {
		logger.fatal( ex );
		return css;
	}
}

export async function shrinkImg( buffer: Buffer, type: string ): Promise<Buffer> {
	try {
		let plugins = [];
		switch( type ) {
		case 'image/png':
			plugins = [ ...plugins, imageminPngcrush( { reduce: true } ) ];
			break;
		}
		return imagemin.buffer( buffer, { plugins } );
	} catch( ex ) {
		logger.fatal( ex );
		return buffer;
	}
}

function maybeCData( el: Element ) {
	if( el.firstChild == null ) return el;
	let text = '';
	for( const node of Array.from( el.childNodes ) ) {
		if( node.nodeType !== Node.TEXT_NODE ) return el;
		text += node.textContent;
	}
	const bloatLen = '<![CDATA[]]>'.length;
	if( text.length < bloatLen ) return el;
	const doc = el.ownerDocument;
	const tn = doc.createTextNode( text );
	const cdata = doc.createCDATASection( text );
	const serializer = new XMLSerializer;
	const cdataLen = serializer.serializeToString( cdata ).length;
	const textNodeLen = serializer.serializeToString( tn ).length;
	if( textNodeLen < cdataLen ) return el;
	while( el.firstChild ) el.removeChild( el.firstChild );
	el.appendChild( cdata );
	return el;
}

export async function shrinkSvg( html: string|JSX.Element ) {
	if( 'props' in ( html as object ) ) html = renderToStaticMarkup( html as JSX.Element );
	try {
		const result = svgo.optimize( String( html ), {
			multipass: true,
			plugins: [
				'cleanupAttrs',
				'convertColors',
				'cleanupNumericValues',
				// 'minifyStyles',
				'convertTransform',
				'removeDesc',
				'removeComments',
				'removeEditorsNSData',
				'removeDoctype',
				'removeMetadata',
				'removeEmptyAttrs',
				'removeTitle',
				'removeScriptElement',
				'removeUnusedNS',
				'removeUnknownsAndDefaults',
				'removeUselessStrokeAndFill',
				'removeUselessDefs'
			]
		} );
		const doc = cloneDocument( ( new DOMParser ).parseFromString( result.data, 'application/xml' ), {
			defaultNamespace: 'http://www.w3.org/2000/svg',
			xmlns: {
				x: 'http://www.w3.org/1999/xlink'
			}
		} );
		for( const e of Array.from( traverse( doc.documentElement ) ) ) {
			if( e.nodeType !== ELEMENT_NODE || ![ 'script', 'style' ].includes( ( e as Element ).localName ) || e.parentNode == null ) continue;
			if( e.textContent?.trim() ) {
				if( ( e as Element ).localName === 'style' ) {
					e.textContent = await shrinkCss( e.textContent );
				}
				maybeCData( e as Element );
			} else {
				e.parentNode.removeChild( e );
			}
		}
		const serializer = new XMLSerializer;
		const xmlStr = serializer.serializeToString( doc.documentElement );
		return xmlStr;
	} catch( ex ) {
		logger.fatal( ex );
		return html as string;
	}
}

export function *traverse( node: Node ): IterableIterator<Node> {
	yield *( function *impl( node: Node, seen: WeakSet<Node> ) {
		if( seen.has( node ) ) return;
		if( node ) {
			seen.add( node );
			yield node;
		}
		if( node.firstChild ) yield *impl( node.firstChild, seen );
		if( node.nextSibling ) yield *impl( node.nextSibling, seen );
	}( node, new WeakSet<Node>() ) );
}
