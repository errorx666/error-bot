import Prando from 'prando';

export function random( min: number, max: number ) {
	return Math.random() * ( max - min ) + min;
}

export function randomInt( min: number, max: number ) {
	return Math.floor( Math.random() * ( max - min + 1 ) + min );
}

declare interface ChooseOptions {
	readonly prng?: Prando;
}
export function choose<T>( values: Iterable<T>|ArrayLike<T>|ReadonlyArray<T>, { prng = new Prando() } = {} as ChooseOptions ) {
	return prng.nextArrayItem( Array.from( values ) );
}

declare interface ShuffleOptions {
	readonly inPlace?: boolean;
	readonly prng?: Prando;
}
export function shuffle<T>( values: Iterable<T>|ArrayLike<T>|ReadonlyArray<T>, options?: ShuffleOptions ): T[];
export function shuffle<T>( values: T[], options: ShuffleOptions & { readonly inPlace: true } ): T[];
export function shuffle<T>( values: T[], { inPlace = false, prng = new Prando() } = {} as ShuffleOptions ) {
	if( !inPlace ) {
		values = Array.from( values );
	}
	for( let i = values.length - 1; i > 0; --i ) {
		const j = prng.nextInt( 0, i );
		[ values[ i ], values[ j ] ] = [ values[ j ], values[ i ] ];
	}
	return values;
}
