import { fromJS, Map } from 'immutable';
import { TimeSpan } from '~time-span';
import { sleep } from '~util';
import assert from 'assert';

export interface AsyncCacheOptions {
	readonly maxAge: TimeSpan;
	readonly retryCount: number;
	readonly retryDelay: TimeSpan;
	readonly timeout: TimeSpan;
}

export class AsyncCache<TKey, TValue> {
	private resolved = Map<TKey, CacheEntry<TValue>>();
	private store = Map<TKey, Promise<TValue>>();
	public readonly maxAge: TimeSpan;
	public readonly retryCount: number;
	public readonly retryDelay: TimeSpan;
	public readonly timeout: TimeSpan;

	public constructor( private readonly fn: ( key: TKey ) => Eventually<TValue>, options = {} as Partial<AsyncCacheOptions> ) {
		this.maxAge = options.maxAge ?? TimeSpan.Infinity;
		this.retryCount = options.retryCount ?? 0;
		this.retryDelay = options.retryDelay ?? TimeSpan.fromSeconds( 1 );
		this.timeout = options.timeout;
	}

	public clear() {
		this.store = this.store.clear();
		this.resolved = this.resolved.clear();
	}

	public delete( key: TKey ) {
		const k = fromJS( key ) as unknown as TKey;
		this.store = this.store.delete( k );
		this.resolved = this.resolved.delete( k );
	}

	public get( key: TKey ): Promise<TValue> {
		const k = fromJS( key ) as unknown as TKey;
		const impl = ( retryCount: number ) => {
			const now = +new Date;
			const { fn } = this;
			if( this.resolved.has( k ) ) {
				const v = this.resolved.get( k );
				if( v.expires <= now ) {
					this.resolved = this.resolved.delete( k );
				} else {
					return v.data;
				}
			}
			const getVal = async () => {
				try {
					let promises = [ Promise.resolve( fn( k ) ) ];
					if( this.timeout ) promises = [ ...promises, sleep( this.timeout ).then( () => { throw new Error( 'timeout' ); } ) ];
					return await Promise.race( promises );
				} catch( ex ) {
					if( retryCount > 0 ) {
						await sleep( this.retryDelay );
						return impl( retryCount - 1 );
					} else {
						throw ex;
					}
				}
			};

			const promise = ( this.store.get( k ) ?? Promise.reject() ).then( null, () => getVal() );
			return this.set( k, promise );
		};
		return impl( this.retryCount );
	}

	public set( key: TKey, value: Eventually<TValue> ) {
		const { maxAge } = this;
		const k = fromJS( key ) as unknown as TKey;
		const promise = Promise.resolve( value );
		this.store = this.store.set( k, promise );
		this.resolved = this.resolved.delete( k );
		return promise.then( data => {
			const storedValue = this.store.get( k );
			if( storedValue !== promise ) return storedValue;
			const expires = +maxAge.fromNow();
			this.resolved = this.resolved.set( k, { data, expires } );
			return data;
		} );
	}

	public save() {
		return Array.from( this.resolved.entries() );
	}

	public load( data: [ TKey, CacheEntry<TValue> ][] ) {
		const now = +new Date;
		this.clear();
		for( const [ key, value ] of data ) {
			if( value.expires > now ) {
				this.resolved = this.resolved.set( fromJS( key ) as unknown as TKey, value );
			}
		}
	}
}

export class AsyncValueCache<TValue> {
	private _cache: AsyncCache<'value', TValue>;
	public constructor( private readonly fn: () => Eventually<TValue>, options = {} as Partial<AsyncCacheOptions> ) {
		this._cache = new AsyncCache<'value', TValue>( key => {
			assert.equal( key, 'value' );
			return fn();
		}, options );
	}

	public clear() {
		return this._cache.clear();
	}

	public get() {
		return this._cache.get( 'value' );
	}

	public set( value: Eventually<TValue> ) {
		this._cache.set( 'value', value );
	}

	public save() {
		return this._cache.save()[ 0 ] ?? [];
	}

	public load( data: CacheEntry<TValue> ) {
		this._cache.load( [ [ 'value', data ] ] );
	}
}
