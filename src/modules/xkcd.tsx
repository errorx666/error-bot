import { concatMap } from 'rxjs/operators';
import { parseCommands } from '~command';

import React, { PureComponent } from 'react';
import { renderToStaticMarkup } from 'react-dom/server';

import { URL } from 'url';
import { ExternalContent } from '~components';

import * as http from '~http';
import { JSDOM } from 'jsdom';
import { handleErrors } from '~rx';

if( module.hot ) {
	module.hot.accept( [ '../components/index.ts', '../http.ts' ] );
}

type ModuleName = 'xkcd';
type Params = ModuleParamsMap[ ModuleName ];

interface XkcdProps {
	name: string;
	title: string;
	url?: string;
	via?: string;
	src: string;
}

class Xkcd extends PureComponent<XkcdProps> {
	public constructor( props: XkcdProps ) {
		super( props );
	}

	public render() {
		const { props } = this;
		return (
			<ExternalContent name='xkcd' title={props.name} url={props.url} via={props.via}>
				<img src={props.src} title={props.title}/>
				<br/>
				<abbr title={props.title}>&shy;</abbr>
			</ExternalContent>
		);
	}
}

export default async function( { moduleName, session, socket, lifecycle, bus, commandFilter, logger }: Params ) {
	if( module.hot ) {
		module.hot.addDisposeHandler( () => {
			lifecycle.shutdown();
		} );
	}

	type XkcdCommandParameters = { random: 'onehot'; latest: 'onehot'; query: 'rest'; };
	parseCommands<XkcdCommandParameters>( {
		command: {
			name: '!xkcd',
			prefix: [ 'x k c d', 'x c k d' ],
			parameters: {
				random: {
					type: 'onehot',
					groupKey: 0,
					allowNone: true
				},
				latest: {
					type: 'onehot',
					groupKey: 0,
					allowNone: true
				},
				query: {
					type: 'rest',
					conflicts: [ 'latest', 'random' ]
				}
			}
		},
		lifecycle,
		filter: commandFilter
	} ).pipe(
		concatMap( async ( {
			responseRoute,
			parameters: { latest, random, query }
		} ) => {
			let url: string;
			let via: string;

			if( latest ) {
				via = url = `https://xkcd.com/`;
			} else if( random || !query ) {
				via = url = 'https://c.xkcd.com/random/comic/';
			} else {
				if( /^\d+$/.test( query ) ) {
					const num = parseInt( query, 10 );
					if( isFinite( num ) ) {
						url = `https://xkcd.com/${num}/`;
					}
				}
				if( !url ) {
					const searchUrl = new URL( 'https://www.explainxkcd.com/wiki/index.php' );
					searchUrl.searchParams.set( 'search', query );
					searchUrl.searchParams.set( 'title', 'Special:Search' );
					searchUrl.searchParams.set( 'fulltext', '1' );
					via = searchUrl.href;
					const { window: { document: searchDoc } } = await http.get( { url: searchUrl.href, html: true } );
					const bestResult =
						Array.from( searchDoc.querySelectorAll( '.mw-search-result-heading a[href]' ) as NodeListOf<HTMLAnchorElement> )
						.map( a => /\/wiki\/index\.php\/(\d+):/.exec( a.href )?.[ 1 ] )
						.filter( r => r != null )[ 0 ];
					if( bestResult ) url = `https://xkcd.com/${bestResult}/`;
					else via = url = 'https://c.xkcd.com/random/comic/';
				}
			}

			let body: JSDOM;
			try {
				for( let i = 0; i < 3; ++i ) {
					const response = await http.get( { url, followRedirect: false, full: true, simple: false, html: true } );
					const location = response.headers?.location;
					if( location && location !== url ) {
						url = location;
						continue;
					}
					if( response.statusCode !== 200 ) throw new Error( `Failed to load ${url}` );
					body = response.body;
					break;
				}
			} catch( ex ) {
				logger.error( ex );
				return;
			}
			const { window: { document } } = body;
			const img = document.querySelector( '#comic img' ) as HTMLImageElement;
			const srcSet = new Map<string, string>();
			srcSet.set( '1x', img.src );
			const srcSets = ( img.srcset ?? '' ).split( ',' ).map( s => s.trim() ).filter( s => !!s );
			for( const ss of srcSets ) {
				if( ss === 'null' ) break;
				const [ src, size = '1x' ] = ss.split( /\s+/, 2 );
				if( src ) srcSet.set( size, src );
			}
			const name = document.querySelector( '#ctitle' ).textContent;
			const imgFullUrl = new URL( srcSet.get( '2x' ) ?? srcSet.get( '1x' ), url ).href;
			const message = renderToStaticMarkup( <Xkcd name={name} title={img.title} url={url} src={imgFullUrl} via={via}/> );
			bus.next( { type: 'response', route: responseRoute, message } );
		} ),
		handleErrors( { logger } )
	).subscribe();

	lifecycle.shutdown$
	.subscribe( () => {
		lifecycle.done();
	} );
}
