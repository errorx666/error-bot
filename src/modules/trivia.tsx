/// <reference path="./trivia.d.ts"/>

import { takeUntil, filter, map, switchMap, delay } from 'rxjs/operators';
import { Subject, merge } from 'rxjs';

import React, { PureComponent, createElement } from 'react';
import type { ReactNode } from 'react';
import { renderToStaticMarkup } from 'react-dom/server';
import { Fsm } from '~fsm';

import { URL } from 'url';

import { shuffle } from '~random';
import { query, parseCommands } from '~command';
import { Spoiler } from '~components';
import { duration } from 'moment';

import * as http from '~http';

if( module.hot ) {
	module.hot.accept( [ '../components/index.ts', '../http.ts' ] );
}

type ModuleName = 'trivia';
type Params = ModuleParamsMap[ ModuleName ];

type GameStates = 'answers'|'results'|'next-question';
type GameTransitions = 'win'|'lose'|'next'|'load'|'ready';

export interface TriviaProps {
	readonly category: string;
	readonly question: string;
	readonly answers: readonly string[];
	readonly correct: number;
	readonly guessed: readonly number[];
	readonly lockouts: readonly TriviaLockout[];
	readonly children?: ReactNode;
}

const letters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

export class Trivia extends PureComponent<TriviaProps> {
	public constructor( props: TriviaProps ) {
		super( props );
	}

	public render() {
		const now = +new Date;
		const { props: { children, category, question, answers, correct, guessed, lockouts } } = this;
		return <>
			<div>
				<p><b>{category}</b></p>
				<p>{question}</p>
				{ answers.map( ( answer, i ) => <div key={`answer-${i}`}>{createElement( guessed.includes( i ) ? ( correct === i ? 'ins' : 'del' ) : 'span', { key: `answer${i}` }, <>
					<b>
						{letters.charAt( i )}
					</b>
					{'. '}
					{answer}
				</> )}</div> ) }
			</div>
			{ children ? <><hr/>{children}</> : <></> }
			{ lockouts.length === 0 ? null : <Spoiler summary="Lockouts">
				{lockouts.map( ( { username, expires, rounds }, i ) => (
					<div key={`lockout-${i}`}>
						<b>{username}</b>
						{ ' is locked out for ' }
						{[
							rounds === 1 ? `${rounds} round or `
						:	rounds > 1 ? `${rounds} rounds or `
						:	'',
							( duration( expires - now, 'ms' ) as any ).format( 'h [hours], m [minutes], s [seconds]', { trim: 'all' } )
						].filter( s => !!s )}.
					</div>
				) )}</Spoiler> }
		</>;
	}
}

const comparer = new Intl.Collator( 'en-US', {
	sensitivity: 'base',
	ignorePunctuation: true,
	usage: 'search'
} );

interface TdbResponse {
	readonly response_code: number;
	readonly results: readonly TriviaQuestion[];
}

const now = () => +( new Date );

export default async function( { moduleName, db, session, socket, lifecycle, bus, tid, commandFilter, state$ }: Params ) {
	if( module.hot ) {
		module.hot.addDisposeHandler( () => {
			lifecycle.shutdown();
		} );
	}

	const isActiveLockout = ( lockout: TriviaLockout ) => ( !!lockout ) && ( lockout.rounds !== 0 ) && ( lockout.expires > now() );

	function getState() {
		let gameState = state$.value;
		if( gameState ) return gameState;
		gameState = {
			gameContext: {
				lockouts: []
			},
			questionPool: [],
			sessionToken: null
		};
		state$.next( gameState );
		return gameState;
	}

	const response$ = new Subject<TriviaResponse>();
	function response( element: JSX.Element, route: ResponseRoute ) {
		response$.next( { element, route } );
	}

	response$.pipe(
		map( ( { element, ...props } ) => ( { ...props, message: renderToStaticMarkup( element ) } ) ),
		takeUntil( lifecycle.shutdown$ )
	)
	.subscribe( ( { message, route } ) => {
		bus.next( { type: 'response', message, route } );
	} );

	async function getSessionToken() {
		const url = new URL( 'https://opentdb.com/api_token.php' );
		url.searchParams.set( 'command', 'request' );
		const { token } = await http.get<{ readonly token: string; }>( {
			url,
			json: true
		} );
		state$.next( {
			...getState(),
			sessionToken: token
		} );
	}

	async function resetSessionToken() {
		const sessionToken = getState().sessionToken;
		if( !sessionToken ) {
			await getSessionToken();
			return;
		}
		const url = new URL( 'https://opentdb.com/api_token.php' );
		url.searchParams.set( 'command', 'reset' );
		url.searchParams.set( 'token', sessionToken );
		await http.get( {
			url,
			json: true
		} );
	}

	async function getQuestion(): Promise<TriviaQuestion> {
		const gameState = getState();
		const { questionPool, sessionToken } = gameState;
		if( questionPool.length > 0 ) {
			const question = questionPool[ 0 ];
			state$.next( { ...gameState, questionPool: questionPool.slice( 1 ) } );
			return question;
		} else if( sessionToken ) {
			const q = questionPool;
			const url = new URL( 'https://opentdb.com/api.php' );
			url.searchParams.set( 'type', 'multiple' );
			url.searchParams.set( 'amount', '50' );
			url.searchParams.set( 'encode', 'url3986' );
			const response = await http.get<TdbResponse>( {
				url,
				json: true
			} );
			if( q !== questionPool ) return await getQuestion();
			if( response.response_code === 0 ) {
				const questionPool = response.results.map( obj => Object.fromEntries(
					Object.entries( obj )
					.map( ( [ key, value ] ) => ( [
						key as keyof TriviaQuestion,
						Array.isArray( value )
						? value.map( v => decodeURIComponent( v ) )
						: decodeURIComponent( value ) as TriviaQuestion[ keyof TriviaQuestion ]
					] ) )
				) as TriviaQuestion );
				state$.next( { ...gameState, questionPool } );
			} else if( response.response_code === 3 ) {
				await getSessionToken();
			} else if( response.response_code === 4 ) {
				await resetSessionToken();
			}
		} else {
			await getSessionToken();
		}
		return await getQuestion();
	}

	const gameUpdate$ = new Subject<TriviaGameUpdate>();
	async function updateGameContext( ctx: Partial<TriviaGameContext>, children = [] as ReactNode ) {
		const state = getState();
		const gameContext = { ...( state.gameContext || {} ), ...ctx } as TriviaGameContext;
		state$.next( { ...state, gameContext } );
		gameUpdate$.next( { gameContext, children } );
	}

	gameUpdate$
	.pipe(
		switchMap( async ( { gameContext, children } ) => {
			const { roundContext: questionState, lockouts } = gameContext;
			return <Trivia {...questionState} lockouts={lockouts}>{children}</Trivia>;
		} ),
		takeUntil( lifecycle.shutdown$ )
	)
	.subscribe( element => {
		response( element, { type: 'thread', tid } );
	} );

	function isLockedOut( issuer: CommandIssuer ) {
		if( !issuer || issuer.type !== 'user' ) return false;
		const { roles, uid } = issuer;
		if( roles.includes( 'owner' ) ) return false;
		const gameContext = getState().gameContext;
		if( !gameContext ) return false;
		return gameContext.lockouts
		.filter( isActiveLockout )
		.some( lockout => lockout.uid === uid );
	}

	function generateLockout( issuer: CommandIssuer, lockout: Omit<TriviaLockout, keyof CommandIssuerUser> ) {
		if( !issuer || issuer.type !== 'user' || issuer.roles.includes( 'owner' ) ) return null;
		const { uid, username, userslug } = issuer;
		return { ...lockout, uid, username, userslug };
	}

	const gameStateFsm = new Fsm<GameStates, GameTransitions>( {
		states: {
			answers: {
				transitions: {
					win: 'results',
					lose: 'results'
				}
			},
			results: {},
			'next-question': {
				transitions: {
					ready: 'answers'
				}
			}
		},
		transitions: {
			next: 'next-question',
			load: 'answers'
		}
	} );

	gameStateFsm.states
	.pipe(
		filter( ( { enter } ) => enter === 'next-question' ),
		delay( 100 ),
		takeUntil( lifecycle.shutdown$ )
	).subscribe( async () => {
		const gameContext = getState().gameContext;
		if( !gameContext ) return false;

		const { question, correct_answer, incorrect_answers, category } = await getQuestion();
		const answersSorted = [ correct_answer, ...incorrect_answers.filter( s => !!s?.trim() ) ];
		const shuffledIndexes = shuffle( answersSorted.map( ( _, i ) => i ) );
		const answers = shuffledIndexes.map( i => answersSorted[ i ] );
		const correct = shuffledIndexes.indexOf( 0 );

		updateGameContext( {
			roundContext: {
				category,
				question,
				correct,
				answers,
				guessed: []
			},
			lockouts:
				( gameContext && gameContext.lockouts || [] )
				.map( ( { rounds, ...l } ) => ( {
					...l,
					...( typeof rounds === 'number' ? { rounds: rounds - 1 } : {} )
				} ) )
				.filter( isActiveLockout )
		} );
		gameStateFsm.transition( 'ready' );
	} );

	gameStateFsm.states
	.pipe(
		filter( ( { enter } ) => enter === 'results' ),
		delay( 6000 ),
		filter( () => gameStateFsm.state === 'results' ),
		takeUntil( lifecycle.shutdown$ )
	).subscribe( () => {
		gameStateFsm.transition( 'next' );
	} );


	gameStateFsm.transitions
	.pipe(
		filter( t => t === 'load' ),
		takeUntil( lifecycle.shutdown$ )
	).subscribe( () => {
		gameStateFsm.transition( 'ready' );
	} );

	const slowPoke = <img src="/assets/uploads/files/1567795880992-7538621b-e3c4-43a0-959f-2860eb885886-image.png"/>;
	const hanzo = <p>:hanzo:</p>;
	function triviaChatCommand
	<TParameterTypeMap extends { [ TParameterName in TParameterNames ]: CommandParameterType; },
	TParameterNames extends keyof TParameterTypeMap & string = keyof TParameterTypeMap & string>
	( cd: CommandDefinition<TParameterTypeMap, TParameterNames> & { readonly prefix: readonly string[]; } ) {
		return merge(
			parseCommands<TParameterTypeMap, TParameterNames>( {
				command: {
					...cd,
					query: {
						$and: [
							query.inThread( tid ),
							cd.query || { $value: true }
						]
					},
					prefix: [ ...cd.prefix, ...cd.prefix.map( p => `trivia ${p}` ) ]
				},
				lifecycle,
				filter: commandFilter
			} ),
			parseCommands<TParameterTypeMap, TParameterNames>( {
				command: {
					...cd,
					query: {
						$and: [
							query.inChat(),
							cd.query || { $value: true }
						]
					},
					prefix: [ ...cd.prefix.map( p => `trivia ${p}` ) ]
				},
				lifecycle,
				filter: commandFilter
			} )
		);
	}

	const cheatPrefixes = [ 'cheat', 'unguess', 'lock out', 'lock', 'un lock', 'next' ];
	parseCommands( {
		command: {
			query: {
				$and: [
					query.fromUser(),
					{ $or: [
						query.inChat(),
						query.inThread( tid )
					] },
					{ $not: query.fromRole( 'admin' ) }
				]
			},
			prefix: [ ...cheatPrefixes, ...cheatPrefixes.map( p => `trivia ${p}` ) ],
			parameters: 'ignore'
		},
		lifecycle,
		filter: commandFilter,
		priority: 10
	} )
	.subscribe( ( { issuer } ) => {
		const { userslug } = issuer as CommandIssuerUser;
		const gameContext = state$.value?.gameContext;
		if( !gameContext ) return;
		const lockout = generateLockout( issuer, { expires: Infinity, rounds: 10 } );
		updateGameContext( {
			lockouts:
				gameContext.lockouts
				.concat( lockout )
				.filter( isActiveLockout )
		}, <p>
			@{userslug} tried to <b>cheat</b>.
		</p> );
	} );


	interface UnlockCommandParameters {
		readonly user: 'user';
	}
	triviaChatCommand<UnlockCommandParameters>( {
		prefix: [ 'un lock' ]
	} )
	.subscribe( ( { parameters: { user: { uid } } } ) => {
		const gameContext = state$.value?.gameContext;
		if( !gameContext ) return;
		if( gameStateFsm.state !== 'answers' ) return;
		if( !gameContext.lockouts.some( l => l.uid === uid ) ) return;
		const lockouts = gameContext.lockouts.filter( l => l.uid !== uid ).filter( isActiveLockout );
		updateGameContext( { lockouts } );
	} );

	interface TriviaCheatCommandParameters {}
	triviaChatCommand<TriviaCheatCommandParameters>( {
		prefix: [ 'cheat' ]
	} )
	.subscribe( ( { responseRoute } ) => {
		const gameContext = state$.value?.gameContext;
		if( !gameContext || !gameContext.roundContext ) return;
		const { answers, question, correct } = gameContext.roundContext;
		const answerLetter = letters.charAt( correct );
		const message = renderToStaticMarkup(
			<>
			<p>{question}</p>
			<p><b>{answerLetter}</b>. {answers[ correct ]}</p>
			</>
		);
		bus.next( { type: 'response', message, route: responseRoute } );
	} );

	interface GuessCommandParameters {
		readonly choice: 'string';
	}
	parseCommands<GuessCommandParameters>( {
		command: {
			query: {
				$and: [
					query.inThread( tid ),
					query.rawMatches( /^[a-z][;.…‽!? ]*$/i )
				]
			},
			parameters: {
				choice: {
					type: 'string',
					position: 0,
					required: true
				}
			}
		},
		lifecycle,
		filter: commandFilter,
		priority: 2
	} ).pipe(
		filter( ( { issuer } ) => !isLockedOut( issuer ) )
	).subscribe( ( { parameters: { choice }, issuer, responseRoute } ) => {
		choice = choice.charAt( 0 );
		const gameContext = getState().gameContext;
		if( !gameContext || !gameContext.roundContext ) return;
		const { roundContext } = gameContext;
		let { guessed, correct, answers } = roundContext;
		const choiceIndex = letters.split( '' ).findIndex( c => comparer.compare( choice, c ) === 0 );
		if( choiceIndex < 0 || choiceIndex >= answers.length ) return;
		const choiceLetter = letters.charAt( choiceIndex );
		if( gameStateFsm.state !== 'answers' ) {
			response( slowPoke, responseRoute );
			return;
		}
		if( gameContext.roundContext.guessed.some( guess => guess === choiceIndex ) ) {
			response( hanzo, responseRoute );
			return;
		}
		guessed = [ ...guessed, choiceIndex ];
		let after = <></> as ReactNode;
		let transition = null as GameTransitions;
		let lockout: TriviaLockout = null;
		if( choiceIndex === correct ) {
			if( issuer.type === 'user' ) {
				after = <>{after}@{issuer.userslug} provided the correct answer: <b>{choiceLetter}</b>!</>;
			} else {
				after = <><b>{choiceLetter}</b> is correct!</>;
			}
			transition = 'win';
		} else {
			after = <><b>{choiceLetter}</b> is incorrect.</>;
			lockout = generateLockout( issuer, { expires: now() + 1000 * 60 * 60, rounds: 2 } );
			const loseAfterNWrong = Math.min( 2, answers.length - 1 );
			if( guessed.length >= loseAfterNWrong ) {
				after = <>{after}{' '}You lose.</>;
				guessed = [ ...guessed, correct ];
				transition = 'lose';
			} else if( lockout ) {
				if( Math.floor( Math.random() * 20 ) === 0 ) {
					lockout = null;
					after = <>{after}{' '}:slot_machine: <b>Lucky!</b> You get an extra guess!</>;
				} else {
					after = <>{after}{' '}Try again later.</>;
				}
			}
		}
		updateGameContext( {
			roundContext: {
				...roundContext,
				guessed
			},
			lockouts: gameContext.lockouts.concat( lockout ).filter( isActiveLockout )
		}, after );
		if( transition ) gameStateFsm.transition( transition );
	} );

	if( state$.value && state$.value.gameContext ) {
		gameStateFsm.transition( 'load' );
		if( !state$.value.gameContext.roundContext ) {
			gameStateFsm.transition( 'next' );
		}
	} else {
		gameStateFsm.transition( 'next' );
	}

	lifecycle.shutdown$
	.subscribe( () => {
		state$.complete();
		gameUpdate$.subscribe();
		response$.complete();
		gameStateFsm.complete();
		lifecycle.done();
	} );
}
