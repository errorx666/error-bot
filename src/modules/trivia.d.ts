declare interface TriviaLockout {
	readonly uid: number;
	readonly username: string;
	readonly userslug: string;
	readonly expires: number;
	readonly rounds?: number;
}

declare interface TriviaRoundContext {
	readonly category: string;
	readonly question: string;
	readonly answers: readonly string[];
	readonly correct: number;
	readonly guessed: readonly number[];
}

declare interface TriviaGameContext {
	readonly roundContext?: TriviaRoundContext;
	readonly lockouts: readonly TriviaLockout[];
}

declare interface TriviaState {
	readonly gameContext: TriviaGameContext;
	readonly sessionToken: string;
	readonly questionPool: readonly TriviaQuestion[];
}

declare interface TriviaQuestion {
	readonly category: string;
	readonly type: 'multiple'|'boolean';
	readonly difficulty: 'easy'|'medium'|'hard';
	readonly question: string;
	readonly correct_answer: string;
	readonly incorrect_answers: readonly string[];
}

declare interface TriviaResponse {
	readonly element: JSX.Element;
	readonly route: ResponseRoute;
}

declare interface TriviaGameUpdate {
	readonly gameContext: TriviaGameContext;
	readonly children: import( 'react' ).ReactNode;
}
