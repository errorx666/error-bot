/* eslint-disable @typescript-eslint/no-unused-vars, prefer-const, no-trailing-spaces, no-multi-spaces */

import React, { ReactChild } from 'react';
import { takeUntil, take, concatMap } from 'rxjs/operators';
import { renderToStaticMarkup } from 'react-dom/server';
import { parseCommands } from '~command';

import { handleErrors } from '~rx';
import { Map } from 'immutable';
import { dump } from '~util';
import _ from 'lodash';

if( module.hot ) {
	module.hot.accept( [ '../components/index.ts' ] );
}

type ModuleName = 'playground';
type Params = ModuleParamsMap[ ModuleName ];

// this module is for experimentation and testing miscellaneous functionality

export default async function( { moduleName, lifecycle, bus, session, socket, tid, logger }: Params ) {
	if( module.hot ) {
		module.hot.addDisposeHandler( () => {
			lifecycle.shutdown();
		} );
	}

	lifecycle.shutdown$
	.subscribe( async () => {
		lifecycle.done();
	} );
}
