/// <reference path="./crossword.d.ts"/>

import * as http from '~http';
import React, { PureComponent, Fragment } from 'react';
import type { ReactNode } from 'react';
import { renderToStaticMarkup } from 'react-dom/server';
import { Spoiler, Html } from '~components';
import { takeUntil, groupBy, mergeMap, map, exhaustMap, concatMap, debounceTime } from 'rxjs/operators';
import { parseCommands, query } from '~command';
import { hasher as Hasher } from 'node-object-hash';
import assert from 'assert';
import { Subject, merge } from 'rxjs';
import { v4 as uuid } from 'uuid';

import { bufferDebounceTime, tapLog } from 'rxjs-util';
import { normalize, dump, getDataUrl, lazy } from '~util';
import { ArrayIndex } from '~array-index';
import { shrinkSvg } from '~dom';
import { handleErrors } from '~rx';

if( module.hot ) {
	module.hot.accept( [ '../components/index.ts', '../dom.ts', '../http.ts' ] );
}

type ModuleName = 'crossword';
type Params = ModuleParamsMap[ ModuleName ];

// type GameStates = 'new-game'|'gameplay'|'game-over';
// type GameTransitions = 'ready'|'load'|'win'|'restart';

interface GameUpdate {
	readonly gameContext: CrosswordGameContext;
	readonly children: ReactNode;
}

interface CrosswordProps {
	readonly title: string;
	readonly boardImg: JSX.Element;
	readonly clues: CrosswordPuzzle['clues'];
	readonly children?: ReactNode;
}

class Crossword extends PureComponent<CrosswordProps> {
	public constructor( props: CrosswordProps ) {
		super( props );
	}

	public render() {
		const { title, clues, boardImg, children } = this.props;

		console.log( { title, clues, boardImg, children } );

		return <>
			<h1>
				<Html html={title ?? ''}/>
			</h1>
			{boardImg}
			<div>
				{children}
			</div>
			<Spoiler summary="Clues">
				<table>
					<tr>
						<th>Across</th>
						<th>Down</th>
					</tr>
					{ range( 0, Math.max( clues.across.length, clues.down.length ) ).map( i => <tr key={`clue-${i}`}>
						<td>
							<Html html={clues.across[ i ]}/>
						</td>
						<td>
							<Html html={clues.down[ i ]}/>
						</td>
					</tr> )}
				</table>
			</Spoiler>
		</>;
	}
}

const hasher = Hasher();

type DirectionLetter = 'a'|'d';

function range( min: number, max: number ) {
	return Array.from( function *() {
		for( let i = min; i < max; ++i ) yield i;
	}() );
}

function isBlank( s: string|void|boolean ) {
	return !s || s === '.' || /^\s*$/.test( String( s ) );
}

const comparer = new Intl.Collator( 'en-US', {
	ignorePunctuation: true,
	sensitivity: 'base',
	usage: 'search'
} );

const formatter = new Intl.NumberFormat( 'en-US', {
	minimumFractionDigits: 0,
	maximumFractionDigits: 2,
	useGrouping: true
} );

const symbols = {
	CHECK: '✓',
	SUN: '☀'
};

function canonicalize( str: string ) {
	str = str && str.toUpperCase().trim();
	if( str == null || str === '.' ) return null;
	for( const [ key, value ] of Object.entries( symbols ) ) {
		if( comparer.compare( str, key ) === 0 ) return value;
	}
	return str;
}

function strEqual( s1: string, s2: string ) {
	if( isBlank( s1 ) ) return isBlank( s2 );
	return comparer.compare( canonicalize( s1 ), canonicalize( s2 ) ) === 0;
}

function toSquareSet( squares: readonly CrosswordSquare[] ): CrosswordSquareSet {
	if( squares == null ) return null;
	squares = Array.from( squares );
	const whiteSquares = squares.filter( s => !s.isBlack );

	const emptyCount = whiteSquares.filter( s => isBlank( s.guessed ) ).length;
	const filledCount = whiteSquares.filter( s => !isBlank( s.guessed ) ).length;
	const correctCount = whiteSquares.filter( s => !isBlank( s.guessed ) && s.isCorrect ).length;
	const incorrectCount = whiteSquares.filter( s => !isBlank( s.guessed ) && !s.isCorrect ).length;

	return Object.assign( squares, {
		byIndex: new ArrayIndex( squares, 'index' ),
		byNumber: new ArrayIndex( squares, 'number' ),
		byXY: new ArrayIndex( squares, 'x', 'y' ),
		emptyCount,
		filledCount,
		correctCount,
		incorrectCount,
		isCorrect: emptyCount === 0 && incorrectCount === 0,
		isEmpty: whiteSquares.every( s => s.isBlank ),
		isFilled: !whiteSquares.some( s => s.isBlank )
	} );
}

function formatCoord( num: number, directionLetter: DirectionLetter ) {
	assert.ok( [ 'a', 'd' ].includes( directionLetter ) );
	const direction = ( directionLetter === 'a' ) ? 'across' : 'down';
	return `${num} ${direction}`;
}

function getClues( gameContext: CrosswordGameContext ) {
	const { puzzle: { clues } } = gameContext;

	function getClue( text: string, num: number, directionLetter: DirectionLetter ) {
		const word = getWordSquares( num, directionLetter, gameContext );
		if( word == null ) return text;
		if( word.isEmpty ) return `${text} (${word.length})`;
		const letters = normalize( word.map( s => s.isBlank ? ' _ ' : s.guessed ).join( '' ) ).replace( /\s+/g, '\u00a0' );
		if( word.isFilled ) return `${text} (${letters})`;
		else return `${text} (${word.length}: ${letters})`;
	}

	const pattern = /([0-9]+)/;

	return {
		across: clues.across.map( clue => {
			const r = pattern.exec( clue );
			if( r == null ) return clue;
			return getClue( clue, parseInt( r[ 1 ], 10 ), 'a' );
		} ),
		down: clues.down.map( clue => {
			const r = pattern.exec( clue );
			if( r == null ) return clue;
			return getClue( clue, parseInt( r[ 1 ], 10 ), 'd' );
		} ),
	};
}

function getWordSquares( num: number, directionLetter: DirectionLetter, gameContext: CrosswordGameContext ) {
	assert.ok( [ 'a', 'd' ].includes( directionLetter ) );
	let squares = [] as readonly CrosswordSquare[];

	const { puzzle: { gridnums, grid, size } } = gameContext;
	const startIndex = gridnums.indexOf( num );
	if( startIndex < 0 ) return null;
	const startY = Math.floor( startIndex / size.cols );
	const startX = startIndex % size.cols;
	let beforeX: number, beforeY: number;
	if( directionLetter === 'a' ) {
		beforeX = startX - 1;
		beforeY = startY;
	} else {
		beforeX = startX;
		beforeY = startY - 1;
	}
	const beforeIndex = beforeY * size.cols + beforeX;
	if( startIndex < 0
	||	!(	( beforeX < 0 || beforeX >= size.cols )
		||	( beforeY < 0 || beforeY >= size.rows )
		||	isBlank( grid[ beforeIndex ] )
	) ) {
		return null;
	}
	const ss = getAllSquares( gameContext );
	for( let i = 0; ; ++i ) {
		let x: number, y: number;
		if( directionLetter === 'a' ) {
			y = startY;
			x = startX + i;
		} else {
			y = startY + i;
			x = startX;
		}
		if( x >= size.cols || y >= size.rows ) break;
		const square = ss.byIndex.get( { index: y * size.cols + x } );
		if( !square || square.isBlack ) break;
		squares = [ ...squares, square ];
	}
	if( squares.length === 0 ) return null;
	return toSquareSet( squares );
}

function getAllSquares( { puzzle: { grid, size, circles, gridnums }, guessed, pencil }: CrosswordGameContext ) {
	const squares = Array.from( { length: grid.length } ).map( ( _, index ) => {
		if( index < 0 || index >= grid.length ) return null;
		const letter = canonicalize( grid[ index ] );
		const isBlack = isBlank( letter );
		const x = Math.floor( index / size.cols );
		const y = index % size.cols;
		const g = isBlack ? null : canonicalize( guessed[ index ] );
		const blank = !isBlack && isBlank( g );
		const gridnum = gridnums[ index ];
		const isCorrect = isBlack || strEqual( letter, g );
		const isPencil = !isBlack && !blank && !!pencil[ index ];
		const isCircled = !isBlack && !blank && ( circles ?? [] )[ index ] === 1;
		return {
			index,
			x,
			y,
			letter,
			...( ( gridnum > 0 ) ? { number: gridnum } : {} ),
			guessed: g,
			isBlack,
			isBlank: blank,
			isCorrect,
			isPencil,
			isCircled
		};
	} );
	return toSquareSet( squares );
}

export default async function( { moduleName, lifecycle, tid, bus, session, state$, commandFilter, logger }: Params ) {
	if( module.hot ) {
		module.hot.addDisposeHandler( () => {
			lifecycle.shutdown();
		} );
	}

	const response$ = new Subject<CrosswordResponse>();
	function response( element: () => JSX.Element|Promise<JSX.Element>, route: ResponseRoute ) {
		response$.next( { element: lazy( () => Promise.resolve( element() ) ), route } );
	}

	response$.pipe(
		groupBy( ( { route } ) => hasher.hash( route ) ),
		mergeMap( response => response.pipe(
			debounceTime( 500 )
		) ),
		concatMap( async ( { element, ...props } ) => ( { ...props, message: renderToStaticMarkup( await element() ) } ) ),
		handleErrors( { logger } ),
		takeUntil( lifecycle.shutdown$ )
	)
	.pipe( tapLog() )
	.subscribe( ( { message, route } ) => {
		bus.next( { type: 'response', message, route } );
	} );

	const gameUpdate$ = new Subject<GameUpdate>();
	function updateGameContext( ctx: Partial<CrosswordGameContext>, children = [] as ReactNode ) {
		const gameState = state$.value || {} as Partial<CrosswordState>;
		let { gameContext: previous, gameContextHistory, gameContextHistoryIndex } = gameState;

		gameContextHistory = ( gameContextHistory ?? [] ).slice( 0, gameContextHistoryIndex ?? Infinity ).slice( -32 ).concat( previous );

		const gameContext = { ...( previous ?? {} ), ...ctx } as CrosswordGameContext;
		state$.next( { ...gameState, gameContext, gameContextHistory, gameContextHistoryIndex: null } );
		gameUpdate$.next( { gameContext, children } );
	}

	function fetchPuzzle( date: string ) {
		return http.get<CrosswordPuzzle>( {
			url: 'https://www.xwordinfo.com/JSON/Data.ashx',
			headers: {
				Referer: 'https://www.xwordinfo.com/Crossword'
			},
			searchParams: {
				format: 'text',
				date
			},
			json: true
		} );
	}

	async function newGame( date = 'random' ) {
		const gameContext = {
			roundId: uuid(),
			puzzle: await fetchPuzzle( date ),
			guessed: []
		};
		updateGameContext( gameContext, <p>New game.</p> );
	}

	if( state$.value == null ) {
		await newGame();
	}

	gameUpdate$
	.pipe(
		groupBy( ( { gameContext: { roundId } } ) => roundId ),
		mergeMap( gameUpdate => gameUpdate.pipe(
			bufferDebounceTime( 300 ),
			map( gameUpdates => {
				const { gameContext } = gameUpdates[ gameUpdates.length - 1 ];
				const { puzzle } = gameContext;
				return async () => {
					const boardImg = await renderGameContext( gameContext );
					const children = gameUpdates.map( ( { children }, i ) => <div key={i}>{children}</div> );
					return <Crossword title={puzzle.title} clues={getClues( gameContext )} boardImg={boardImg}>{children}</Crossword>;
				};
			} )
		) ),
		takeUntil( lifecycle.shutdown$ ),
		tapLog( 'response' )
	)
	.subscribe( element => {
		response( element, { type: 'thread', tid } );
	} );

	const letterFontUrl = getDataUrl( 'font/woff2', require( '!!buffer-loader!~data/shadows-into-light2.woff2' ) );
	async function renderBoard( { size: { cols, rows }, cells, youWin }: CrosswordRenderParams ) {
		const squareSize = 32;
		const padding = 16;
		const innerWidth = Math.ceil( rows * ( squareSize - .5 ) );
		const innerHeight = Math.ceil( cols * ( squareSize - .5 ) );
		const outerWidth = innerWidth + padding * 2;
		const outerHeight = innerHeight + padding * 2;

		interface Props {
			readonly x?: number;
			readonly y?: number;
			readonly classNames?: readonly string[];
		}
		function getProps( { x, y, classNames }: Props ) {
			let retval = {};
			if( x ) retval = { ...retval, x };
			if( y ) retval = { ...retval, y };
			if( classNames && classNames.length > 0 ) retval = { ...retval, className: classNames.join( ' ' ) };
			return retval;
		}

		let highlights = [] as readonly string[];
		for( const cell of cells ) {
			if( cell.black !== true && cell.highlight ) {
				highlights = [ ...highlights, cell.highlight ];
			}
		}
		highlights = Array.from( new Set( highlights ) );

		const svg = <svg xmlns="http://www.w3.org/2000/svg" viewBox={`0 0 ${outerWidth} ${outerHeight}`}>
			{( cells.some( c => c.black !== true && c.pencil ) || youWin ) ? <filter id="pencil" x="0" y="0" width="100%" height="100%">
				<feTurbulence type="fractalNoise" baseFrequency="0.4 1.2" result="noise" numOctaves="2"/>
				<feDisplacementMap in="SourceGraphic" in2="noise" scale="3" xChannelSelector="G" yChannelSelector="B"/>
			</filter> : <></>}
			<filter id="paper">
				<feTurbulence type="fractalNoise" baseFrequency=".04" result="noise" numOctaves="4"/>
				<feDiffuseLighting surfaceScale="4" in="noise">
					<fePointLight x={outerWidth * .5} y={outerHeight * .25} z="140"/>
				</feDiffuseLighting>
				<feComposite operator="arithmetic" k1=".25"/>
			</filter>
			<style>
{`@font-face{font-family:'Shadows Into Light';font-style:normal;font-weight:400;src:url(${letterFontUrl}) format('woff2')}`}
{range( 1, rows ).map( y => `svg>g:nth-of-type(${y + 1}){transform:translateY(${y * ( squareSize - .5 )}px)}` )}
{range( 1, cols ).map( x => `svg>g>g:nth-of-type(${x + 1}){transform:translateX(${x * ( squareSize - .5 )}px)}` )}
{`rect{width:100%;height:100%;pointer-events:none}`}
{`g rect{fill:#fff;stroke:#000;width:${squareSize}px;height:${squareSize}px}`}
{cells.some( c => c.black !== true && c.pencil ) ? `.p{fill:#777;filter:url(#pencil)}` : ''}
{cells.some( c => c.black !== true && ( c.letter != null || c.number != null ) ) ? `text{fill:#000;dominant-baseline:hanging}` : ''}
{cells.some( c => c.black !== true && c.circle ) ? `circle{cx:50%;cy:50%;r:45%;stroke:#000;fill:none}` : ''}
{highlights.map( ( h, i ) => `.h${i}{fill:${h}}` )}
{cells.some( c => c.black !== true && c.number != null ) ? `.n{font:bold .7rem 'Courier New','Courier',monospace;transform:translate(.1rem,.1rem)}` : ''}
{cells.some( c => c.black !== true && c.letter != null ) ? `.l{font:1.6rem 'Shadows Into Light',sans-serif;text-anchor:middle;transform:translate(16px,4px)}` : ''}
{`svg + rect{filter:url(#paper)}`}
{youWin ? `.win{transform:translateX(70%) translateY(25%) rotate(32deg);filter:url(#pencil)}` : ''}
{youWin ? `.win rect{stroke:#f00;fill:none;width:30%;height:64px;rx:32px;ry:16px;transform:translate(-15%,-32px);stroke-width:8px}` : ''}
{youWin ? `.win text{fill:#f00;font:32px Impact,sans-serif;text-anchor:middle;transform:translateY(-12px)}` : ''}
			</style>
			<svg x={padding} y={padding} width={innerWidth} height={innerHeight}>
				{ cells.some( c => c.black ) ? <rect fill="#000"></rect> : <></> }
				{range( 0, rows ).map( y =>
					<g key={`row-${y}`}>{
						range( 0, cols ).map( x => {
							const key = `box-${x}-${y}`;
							const index = y * cols + x;
							const cell = cells[ index ];
							if( cell.black === true ) return <g key={key}/>;
							const { circle, number, highlight, pencil } = cell;
							const letter = canonicalize( cell.letter );
							let classNames = [] as string[];
							if( highlight ) {
								classNames = [ ...classNames, `h${highlights.indexOf( highlight )}` ];
							}
							let children: ReactNode;
							children = <rect {...getProps( { classNames } )}/>;
							if( circle ) {
								children = <>{children}<circle/></>;
							}
							if( letter != null ) {
								let classNames = [ 'l' ];
								if( pencil ) classNames = [ ...classNames, 'p' ];
								children = <>{children}<text {...getProps( { classNames } )}>{letter}</text></>;
							}
							if( number != null ) {
								children = <>{children}<text className="n">{number || ''}</text></>;
							}
							return <g key={key}>{children}</g>;
						} )}
					</g>
				)}
			</svg>
			<rect/>
			{ youWin ?
				<g className="win">
					<rect/>
					<text>WINNER</text>
				</g>
			: <></> }
		</svg>;
		const dataUrl = getDataUrl( 'image/svg+xml', await shrinkSvg( svg ) );
		return <img src={dataUrl} width="100%"/>;
	}

	async function renderGameContext( gameContext: CrosswordGameContext ) {
		const { puzzle } = gameContext;

		const squares = getAllSquares( gameContext );

		return await renderBoard( {
			size: puzzle.size,
			cells: squares.map( ( { isBlack, isCircled: circle, isPencil: pencil, number, guessed } ) => (
				isBlack
				? { black: true }
				: {
					black: false,
					circle,
					number,
					letter: guessed,
					pencil
				}
			) ),
			youWin: squares.isCorrect && squares.isFilled
		} );
	}

	function crosswordChatCommand
	<TParameterTypeMap extends { [ TParameterName in TParameterNames ]: CommandParameterType; },
	TParameterNames extends keyof TParameterTypeMap & string = keyof TParameterTypeMap & string>
	( cd: CommandDefinition<TParameterTypeMap, TParameterNames> & { readonly prefix: readonly string[]; } ) {
		return merge(
			parseCommands<TParameterTypeMap, TParameterNames>( {
				command: {
					...cd,
					query: {
						$and: [
							query.inThread( tid ),
							cd.query || { $value: true }
						]
					},
					prefix: [ ...cd.prefix, ...cd.prefix.map( p => `crossword ${p}` ) ]
				},
				priority: 1,
				lifecycle,
				filter: commandFilter
			} ),
			parseCommands<TParameterTypeMap, TParameterNames>( {
				command: {
					...cd,
					query: {
						$and: [
							query.inChat(),
							cd.query || { $value: true }
						]
					},
					prefix: [ ...cd.prefix.map( p => `crossword ${p}` ) ]
				},
				priority: 1,
				lifecycle,
				filter: commandFilter
			} )
		);
	}

	crosswordChatCommand( {
		prefix: [ 'print' ],
		parameters: 'ignore'
	} )
	.subscribe( () => {
		updateGameContext( {} );
	} );

	crosswordChatCommand( {
		query: {
			$and: [
				query.fromUser(),
				{ $not: query.fromRole( 'admin' ) }
			]
		},
		prefix: [ 'cheat', 'new game', 'wrong count', 'wrong', 'clean', 'fix', 'check', 'test' ],
		parameters: 'ignore'
	} )
	.subscribe( ( { issuer } ) => {
		if( issuer.type !== 'user' ) return;
		const message = renderToStaticMarkup( <p>@{issuer.userslug} tried to <b>cheat</b>.</p> );
		bus.next( { type: 'response', message, route: { type: 'thread', tid } } );
	} );

	const coordPattern = /^([0-9]+)[:. ]*(a(?:cross)?|d(?:own)?)$/i;
	interface CrosswordCheatCommandParams {
		coord: 'rest'
	}
	crosswordChatCommand<CrosswordCheatCommandParams>( {
		query: query.fromRole( 'admin' ),
		prefix: [ 'cheat' ],
		parameters: {
			coord: {
				type: 'rest'
			}
		}
	} )
	.pipe( concatMap( async ( { parameters: { coord }, responseRoute } ) => {
		const { gameContext } = state$.value;
		const { puzzle } = gameContext;

		if( coord ) {
			const matches = coordPattern.exec( coord );
			if( !matches ) return;
			const num = parseInt( matches[ 1 ], 10 );
			const directionLetter = matches[ 2 ].charAt( 0 ).toLowerCase() as DirectionLetter;

			const word = getWordSquares( num, directionLetter, gameContext );
			if( word == null ) {
				const message = renderToStaticMarkup( <><b>{formatCoord( num, directionLetter )}</b>: `404 Not Found`</> );
				bus.next( { type: 'response', message, route: responseRoute } );
				return;
			}
			const g = word.map( ( { letter, isBlank, isCorrect, index } ) => {
				const key = index;
				if( isBlank ) return <Fragment key={key}>{letter}</Fragment>;
				else if( isCorrect ) return <ins key={key}>{letter}</ins>;
				else return <del key={key}>{letter}</del>;
			} );
			const message = renderToStaticMarkup( <p><b>{g}</b>: {word.isCorrect ? 'correct' : 'incorrect'}</p> );
			bus.next( { type: 'response', message, route: responseRoute } );
			return;
		}

		const squares = getAllSquares( gameContext );

		const img = await renderBoard( {
			size: puzzle.size,
			cells: squares.map( ( { isBlack, isBlank, isCircled: circle, number, isCorrect, letter } ) => (
				isBlack
				? { black: true }
				: {
					black: false,
					circle,
					number,
					letter,
					highlight: isCorrect ? '#cfc' : isBlank ? false : '#fcc'
				}
			) ),
			youWin: false
		} );
		const message = renderToStaticMarkup( img );
		bus.next( { type: 'response', message, route: responseRoute } );
	} ) )
	.subscribe();

	crosswordChatCommand( {
		query: query.fromRole( 'admin' ),
		prefix: [ 'render' ]
	} )
	.pipe( concatMap( async ( { responseRoute } ) => {
		const { gameContext } = state$.value;
		const img = await renderGameContext( gameContext );
		const message = renderToStaticMarkup( img );
		bus.next( { type: 'response', message, route: responseRoute } );
	} ) )
	.subscribe();

	interface CrosswordUndoCommandParams {
		n: 'number';
	}
	crosswordChatCommand<CrosswordUndoCommandParams>( {
		prefix: [ 'un do' ],
		parameters: {
			n: {
				alias: [ 'steps' ],
				type: 'number',
				query: {
					value: {
						$gte: 1,
						$lt: Number.MAX_SAFE_INTEGER,
						$isInteger: true,
						$isFinite: true
					}
				},
				default: 1
			}
		}
	} )
	.subscribe( ( { parameters: { n } } ) => {
		const gameState = state$.value;
		let { gameContextHistory, gameContextHistoryIndex } = gameState;
		if( gameContextHistory == null ) return;
		if( gameContextHistoryIndex == null ) {
			gameContextHistoryIndex = gameContextHistory.length;
			gameContextHistory = [ ...gameContextHistory, gameState.gameContext ];
		}
		gameContextHistoryIndex = gameContextHistoryIndex - n;
		if( gameContextHistoryIndex < 0 || gameContextHistoryIndex >= gameContextHistory.length ) return;
		const gameContext = gameContextHistory[ gameContextHistoryIndex ];
		if( gameContext == null ) return;
		state$.next( { ...gameState, gameContext, gameContextHistory, gameContextHistoryIndex } );
		gameUpdate$.next( { gameContext, children: null } );
	} );

	interface CrosswordRedoCommandParams {
		n: 'number';
	}
	crosswordChatCommand<CrosswordRedoCommandParams>( {
		prefix: [ 're do' ],
		parameters: {
			n: {
				alias: [ 'steps' ],
				type: 'number',
				query: {
					value: {
						$gte: 1,
						$lt: Number.MAX_SAFE_INTEGER,
						$isInteger: true,
						$isFinite: true
					}
				},
				default: 1
			}
		}
	} )
	.subscribe( ( { parameters: { n } } ) => {
		const gameState = state$.value;
		let { gameContextHistory, gameContextHistoryIndex } = gameState;
		if( gameContextHistory == null || gameContextHistoryIndex == null ) return;
		gameContextHistoryIndex = gameContextHistoryIndex + n;
		if( gameContextHistoryIndex < 0 || gameContextHistoryIndex >= gameContextHistory.length ) return;
		const gameContext = gameContextHistory[ gameContextHistoryIndex ];
		if( gameContext == null ) return;
		state$.next( { ...gameState, gameContext, gameContextHistory, gameContextHistoryIndex } );
		gameUpdate$.next( { gameContext, children: null } );
	} );

	interface CrosswordNewGameCommandParams {
		date: 'rest';
	}
	crosswordChatCommand<CrosswordNewGameCommandParams>( {
		query: query.fromRole( 'admin' ),
		prefix: [ 'new game' ],
		parameters: {
			date: {
				type: 'rest',
				default: 'random'
			}
		}
	} ).pipe(
		exhaustMap( async ( { parameters: { date } } ) => {
			await newGame( date );
		} )
	).subscribe();

	interface CrosswordCleanCommandParams {
		whole: 'boolean';
		coord: 'rest';
	}
	crosswordChatCommand<CrosswordCleanCommandParams>( {
		query: query.fromRole( 'admin' ),
		prefix: [ 'clean', 'fix' ],
		parameters: {
			whole: {
				type: 'boolean',
				default: true
			},
			coord: {
				type: 'rest'
			}
		}
	} )
	.subscribe( ( { parameters: { coord, whole }, responseRoute } ) => {
		const { gameContext } = state$.value;

		let squares: CrosswordSquareSet;
		if( coord ) {
			const matches = coordPattern.exec( coord );
			if( !matches ) return;
			const num = parseInt( matches[ 1 ], 10 );
			const directionLetter = matches[ 2 ].charAt( 0 ).toLowerCase() as DirectionLetter;

			squares = getWordSquares( num, directionLetter, gameContext );
			if( squares == null ) {
				const message = renderToStaticMarkup( <><b>{formatCoord( num, directionLetter )}</b>: `404 Not Found`</> );
				bus.next( { type: 'response', message, route: responseRoute } );
				return;
			}
		} else {
			squares = getAllSquares( gameContext );
		}

		const gridnums = Array.from( new Set( gameContext.puzzle.gridnums ) ).filter( n => n != null && n > 0 );
		const allWords = [ 'a', 'd' ].flatMap( ( directionLetter: DirectionLetter ) =>
			gridnums.map( num => getWordSquares( num, directionLetter, gameContext ) )
		).filter( w => w != null );

		updateGameContext( {
			...gameContext,
			guessed: gameContext.guessed.map( ( g, index ) => {
				const square = squares.byIndex.get( { index } );
				if( square == null ) return g;
				const { isCorrect, letter } = square;
				if( !isCorrect ) return null;
				if( !whole ) return letter;
				const intersectingWords = allWords.filter( word => word.byIndex.has( { index } ) );
				if(
					intersectingWords.some( word => word.isCorrect )
				||	intersectingWords.every( word => word.every( l => l.isBlank || l.isCorrect ) )
				) return letter;
				else return null;
			} ),
			pencil: gameContext.pencil.map( ( p, index ) => squares.byIndex.has( { index } ) ? false : p )
		} );
	} );

	interface CrosswordWrongCommandParams {
		highlight: 'boolean';
		coord: 'rest';
	}
	crosswordChatCommand<CrosswordWrongCommandParams>( {
		query: query.fromRole( 'admin' ),
		prefix: [ 'wrong count', 'wrong', 'doing it wrong', 'check' ],
		parameters: {
			highlight: {
				alias: [ 'show', 'peek', 'reveal' ],
				type: 'boolean',
				default: false
			},
			coord: {
				type: 'rest'
			}
		}
	} )
	.pipe( concatMap( async ( { parameters: { coord, highlight }, responseRoute } ) => {
		const { gameContext } = state$.value;
		const { puzzle } = gameContext;

		if( coord ) {
			const matches = coordPattern.exec( coord );
			if( !matches ) return;
			const num = parseInt( matches[ 1 ], 10 );
			const directionLetter = matches[ 2 ].charAt( 0 ).toLowerCase() as DirectionLetter;

			const word = getWordSquares( num, directionLetter, gameContext );
			if( word == null ) {
				const message = renderToStaticMarkup( <><b>{formatCoord( num, directionLetter )}</b>: `404 Not Found`</> );
				bus.next( { type: 'response', message, route: responseRoute } );
				return;
			}

			const g = word.map( ( { guessed, isBlank, isCorrect, index } ) => {
				const key = index;
				if( isBlank ) return <Fragment key={key}>_</Fragment>;
				if( !highlight ) return <Fragment key={key}>{guessed}</Fragment>;
				if( isCorrect ) return <ins key={key}>{guessed}</ins>;
				else return <del key={key}>{guessed}</del>;
			} );
			const message = renderToStaticMarkup( <p><b>{g}</b>: {word.isCorrect ? 'correct' : 'incorrect'}</p> );
			bus.next( { type: 'response', message, route: responseRoute } );
			return;
		}

		const squares = getAllSquares( gameContext );

		let img: JSX.Element;

		if( highlight ) {
			img = await renderBoard( {
				size: puzzle.size,
				cells: squares.map( ( { isBlack, isBlank, guessed, isCircled: circle, number, isCorrect, isPencil } ) => (
					isBlack
					? { black: true }
					: {
						black: false,
						circle,
						number,
						letter: guessed,
						pencil: isPencil,
						highlight: isCorrect ? '#cfc' : isBlank ? false : '#fcc'
					}
				) ),
				youWin: squares.isFilled && squares.isCorrect
			} );
		} else {
			img = await renderGameContext( gameContext );
		}

		const { correctCount, incorrectCount, emptyCount } = getAllSquares( gameContext );
		const totalCount = correctCount + incorrectCount;

		const pctRight = formatter.format( ( totalCount === 0 ) ? 0 : ( correctCount / totalCount * 100 ) ) + '%';
		const pctWrong = formatter.format( ( totalCount === 0 ) ? 0 : ( incorrectCount / totalCount * 100 ) ) + '%';

		const message = renderToStaticMarkup( <Crossword title={puzzle.title} boardImg={img} clues={getClues( gameContext )}>
			<hr/>
			<div><b>{emptyCount}</b> empty</div>
			<div><b>{correctCount}</b> (<b>{pctRight}</b>) correct</div>
			<div><b>{incorrectCount}</b> (<b>{pctWrong}</b>) incorrect</div>
			<hr/>
		</Crossword> );

		bus.next( { type: 'response', message, route: responseRoute } );
	} ) )
	.subscribe();

	interface CrosswordEraseCommandParams {
		coord: 'rest';
	}
	parseCommands<CrosswordEraseCommandParams>( {
		command: {
			query: query.inThread( tid ),
			prefix: [ 'erase', 'delete', 'clear' ],
			parameters: {
				coord: {
					type: 'rest',
					required: true,
					query: {
						value: {
							$or: [
								{ $regex: coordPattern },
								{ $in: [ 'all' ] }
							]
						}
					}
				}
			}
		},
		lifecycle,
		filter: commandFilter
	} )
	.subscribe( ( { parameters: { coord }, issuer, responseRoute } ) => {
		const gameContext = state$.value.gameContext;
		let squares: CrosswordSquareSet;
		if( comparer.compare( coord, 'all' ) === 0 ) {
			if( !issuer.roles.includes( 'admin' ) ) return;
			squares = getAllSquares( gameContext );
		} else {
			const matches = coordPattern.exec( coord );
			if( !matches ) return;
			const num = parseInt( matches[ 1 ], 10 );
			const directionLetter = matches[ 2 ].charAt( 0 ).toLowerCase() as DirectionLetter;
			const word = getWordSquares( num, directionLetter, gameContext );
			if( word == null ) {
				const message = renderToStaticMarkup( <><b>{formatCoord( num, directionLetter )}</b>: `404 Not Found`</> );
				bus.next( { type: 'response', message, route: responseRoute } );
				return;
			}

			squares = getWordSquares( num, directionLetter, gameContext );
		}
		updateGameContext( {
			...gameContext,
			guessed: gameContext.guessed.map( ( g, index ) =>
				squares.byIndex.has( { index } ) ? null : g
			),
			pencil: gameContext.pencil.map( ( p, index ) =>
				squares.byIndex.has( { index } ) ? false : p
			)
		} );
	} );

	interface CrosswordPencilCommandParams {
		coord: 'rest'
	}
	parseCommands<CrosswordPencilCommandParams>( {
		command: {
			query: query.inThread( tid ),
			prefix: [ 'pencil' ],
			parameters: {
				coord: {
					type: 'rest',
					required: true,
					query: {
						value: {
							$regex: coordPattern
						}
					}
				}
			}
		},
		lifecycle,
		filter: commandFilter
	} )
	.subscribe( ( { parameters: { coord }, responseRoute } ) => {
		const matches = coordPattern.exec( coord );
		if( !matches ) return;
		const num = parseInt( matches[ 1 ], 10 );
		const directionLetter = matches[ 2 ].charAt( 0 ).toLowerCase() as DirectionLetter;
		const gameContext = state$.value.gameContext;
		const squares = getWordSquares( num, directionLetter, gameContext );
		if( squares == null ) {
			const message = renderToStaticMarkup( <><b>{formatCoord( num, directionLetter )}</b>: `404 Not Found`</> );
			bus.next( { type: 'response', message, route: responseRoute } );
			return;
		}

		updateGameContext( {
			...gameContext,
			pencil: gameContext.pencil.map( ( p, index ) =>
				squares.byIndex.has( { index } ) ? !squares.byIndex.get( { index } ).isBlank : p
			)
		} );
	} );

	interface CrosswordPencilCommandParams {
		coord: 'rest'
	}
	parseCommands<CrosswordPencilCommandParams>( {
		command: {
			query: query.inThread( tid ),
			prefix: [ 'pen', 'ink' ],
			parameters: {
				coord: {
					type: 'rest',
					required: true,
					query: {
						value: {
							$regex: coordPattern
						}
					}
				}
			}
		},
		lifecycle,
		filter: commandFilter
	} )
	.subscribe( ( { parameters: { coord }, responseRoute } ) => {
		const matches = coordPattern.exec( coord );
		if( !matches ) return;
		const num = parseInt( matches[ 1 ], 10 );
		const directionLetter = matches[ 2 ].charAt( 0 ).toLowerCase() as DirectionLetter;
		const gameContext = state$.value.gameContext;
		const squares = getWordSquares( num, directionLetter, gameContext );
		if( squares == null ) {
			const message = renderToStaticMarkup( <><b>{formatCoord( num, directionLetter )}</b>: `404 Not Found`</> );
			bus.next( { type: 'response', message, route: responseRoute } );
			return;
		}
		updateGameContext( {
			...gameContext,
			pencil: gameContext.pencil.map( ( p, index ) =>
				squares.byIndex.has( { index } ) ? false : p
			)
		} );
	} );

	const guessPattern = /^([0-9]+)[:. ]*(a(?:cross)?|d(?:own)?)[:. ]+(.+?)(\$?)$/i;
	interface CrosswordGuessCommandParams {
		pen: 'onehot',
		pencil: 'onehot',
		guess: 'rest'
	}
	parseCommands<CrosswordGuessCommandParams>( {
		command: {
			query: query.inThread( tid ),
			parameters: {
				pen: {
					alias: [ 'ink' ],
					type: 'onehot',
					groupKey: 'pencil'
				},
				pencil: {
					type: 'onehot',
					groupKey: 'pencil',
					default: true
				},
				guess: {
					type: 'rest',
					required: true,
					query: {
						value: {
							$regex: guessPattern
						}
					}
				}
			}
		},
		lifecycle,
		filter: commandFilter,
		priority: -1
	} )
	.subscribe( ( { raw, parameters, parameterInfo, responseRoute } ) => {
		const { guess, pencil } = parameters;
		const pencilExplicit = parameterInfo.pencil.supplied || parameterInfo.pen.supplied;
		logger.info( { pencil, pencilExplicit, guess } );

		let gameContext = state$.value.gameContext;

		const matches = guessPattern.exec( guess );
		const num = parseInt( matches[ 1 ], 10 );
		const directionLetter = matches[ 2 ].charAt( 0 ).toLowerCase() as DirectionLetter;
		const letters = matches[ 3 ].toUpperCase();

		const validChars = Array.from( new Set( [ ...'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789?_'.split( '' ), ...gameContext.puzzle.grid ].filter( c => !isBlank( c ) ) ) );
		if( !letters.split( '' ).every( c => validChars.some( cc => strEqual( c, cc ) ) ) ) {
			return;
		}

		const anchorEnd = matches[ 4 ] === '$';

		const { puzzle, guessed, pencil: pencilled = [] } = gameContext;

		const word = getWordSquares( num, directionLetter, gameContext );
		if( word == null ) {
			const message = renderToStaticMarkup( <><b>{formatCoord( num, directionLetter )}</b>: `404 Not Found`</> );
			bus.next( { type: 'response', message, route: responseRoute } );
			return;
		}
		if( letters.length > word.length ) {
			const message = renderToStaticMarkup( <><b>{formatCoord( num, directionLetter )}</b>: `413 Payload Too Large`</> );
			bus.next( { type: 'response', message, route: responseRoute } );
			return;
		}
		let offset = 0;
		if( anchorEnd ) {
			offset = word.length - letters.length;
		}

		let guesses = Array.from( { length: puzzle.grid.length } ) as readonly string[];
		let pencils = Array.from( { length: puzzle.grid.length } ) as readonly boolean[];
		for( let i = 0; i < letters.length; ++i ) {
			let letter = letters.charAt( i );
			if( letter === '?' ) continue;
			if( letter === '_' ) letter = null;
			const { index } = word[ offset + i ];
			guesses = guesses.map( ( g, i ) => ( index === i ) ? letter : g );
			pencils = pencils.map( ( g, i ) => // eslint-disable-next-line no-negated-condition
				( index !== i ) ? g
				: ( (
					( pencilExplicit || !strEqual( letter, guessed[ i ] ) )
					? pencil
					: g
				) && !isBlank( letter ) )
			);
		}

		gameContext = {
			...gameContext,
			guessed: guesses.map( ( g, i ) =>
				( g === undefined ) ? guessed[ i ] : g
			).map( g => isBlank( g ) ? null : g ),
			pencil: pencils.map( ( p, i ) =>
				( p === undefined ) ? ( pencilled[ i ] || false ) : p
			)
		};

		const { isCorrect, isFilled, emptyCount, incorrectCount } = getAllSquares( gameContext );

		const youWin = isCorrect && isFilled;

		let children: ReactNode;

		if( emptyCount === 0 ) {
			if( incorrectCount > 0 ) {
				assert.equal( youWin, false );
				children = <p>
					<hr/>
					<b>{incorrectCount}</b> incorrect
					<hr/>
				</p>;
			} else {
				assert.equal( youWin, true );
				children = <p>You win!</p>;
			}
		}

		updateGameContext( gameContext, children );

		if( youWin ) newGame();
	} );

	interface CrosswordTestCommandParams {
		highlight: 'boolean';
		guess: 'rest';
	}
	crosswordChatCommand<CrosswordTestCommandParams>( {
		query: query.fromRole( 'admin' ),
		prefix: [ 'test' ],
		parameters: {
			highlight: {
				alias: [ 'show', 'peek', 'reveal' ],
				type: 'boolean',
				default: false
			},
			guess: {
				type: 'rest'
			}
		}
	} )
	.subscribe( ( { parameters: { guess, highlight }, responseRoute } ) => {
		logger.debug( guess );

		const gameContext = state$.value.gameContext;

		const matches = guessPattern.exec( guess );
		const num = parseInt( matches[ 1 ], 10 );
		const directionLetter = matches[ 2 ].charAt( 0 ).toLowerCase() as DirectionLetter;
		const letters = matches[ 3 ].toUpperCase();

		const validChars = Array.from( new Set( [ ...'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789?_'.split( '' ), ...gameContext.puzzle.grid ].filter( c => !isBlank( c ) ) ) );
		if( !letters.split( '' ).every( c => validChars.some( cc => strEqual( c, cc ) ) ) ) {
			return;
		}

		const anchorEnd = matches[ 4 ] === '$';

		const word = getWordSquares( num, directionLetter, gameContext );
		if( word == null ) {
			const message = renderToStaticMarkup( <><b>{formatCoord( num, directionLetter )}</b>: `404 Not Found`</> );
			bus.next( { type: 'response', message, route: responseRoute } );
			return;
		}
		if( letters.length > word.length ) {
			const message = renderToStaticMarkup( <><b>{formatCoord( num, directionLetter )}</b>: `413 Payload Too Large`</> );
			bus.next( { type: 'response', message, route: responseRoute } );
			return;
		}
		let offset = 0;
		if( anchorEnd ) {
			offset = word.length - letters.length;
		}

		let isCorrect = word.length === letters.length;
		const g = word.map( ( { letter }, i ) => {
			const key = i;
			const g = canonicalize( letters.charAt( i - offset ) );
			if( strEqual( letter, g ) ) {
				if( highlight ) return <ins key={key}>{g}</ins>;
			} else {
				isCorrect = false;
			}
			if( isBlank( g ) ) return <Fragment key={key}>_</Fragment>;
			else if( highlight ) return <del key={key}>{g}</del>;
			else return <Fragment key={key}>{g}</Fragment>;
		} );
		const message = renderToStaticMarkup( <p><b>{g}</b>: {isCorrect ? 'correct' : 'incorrect'}</p> );
		bus.next( { type: 'response', message, route: responseRoute } );
	} );

	dump( logger, state$.value?.gameContext?.puzzle );

	lifecycle.shutdown$
	.subscribe( () => {
		lifecycle.done();
	} );
}
