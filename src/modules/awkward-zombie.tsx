import { concatMap } from 'rxjs/operators';
import { parseCommands } from '~command';
import * as http from '~http';

import React, { PureComponent } from 'react';
import { renderToStaticMarkup } from 'react-dom/server';

import { get as levenshtein } from 'fast-levenshtein';
import { normalize } from '~util';
import { shuffle } from '~random';
import { ExternalContent } from '~components';

import { URL } from 'url';
import { handleErrors } from '~rx';

if( module.hot ) {
	module.hot.accept( [ '../components/index.ts', '../http.ts' ] );
}

type ModuleName = 'awkward-zombie';
type Params = ModuleParamsMap[ ModuleName ];

interface AwkwardZombieProps {
	id: string;
	date: string;
	name: string;
	game: string;
	url?: string;
	src: string;
}

class AwkwardZombie extends PureComponent<AwkwardZombieProps> {
	public constructor( props: AwkwardZombieProps ) {
		super( props );
	}

	public render() {
		const { props } = this;
		return (
			<ExternalContent name="Awkward Zombie" url={props.url} title={`${props.id}: ${props.name}`}>
				<img src={props.src} title={`${props.name} - ${props.game}`}/>
			</ExternalContent>
		);
	}
}

export default async function( { moduleName, session, socket, lifecycle, bus, commandFilter, logger }: Params ) {
	if( module.hot ) {
		module.hot.addDisposeHandler( () => {
			lifecycle.shutdown();
		} );
	}

	type AwkwardZombieCommandParameters = { query: 'rest'; };
	parseCommands<AwkwardZombieCommandParameters>( {
		command: {
			name: '!awkward-zombie',
			prefix: [ 'awkward zombie', 'a z' ],
			parameters: {
				query: {
					type: 'rest',
					required: true
				}
			}
		},
		lifecycle,
		filter: commandFilter
	} ).pipe(
		concatMap( async ( {
			responseRoute,
			parameters: { query }
		} ) => {
			const archiveUrl = new URL( 'http://awkwardzombie.com/comic/archive' );
			const { window: { document: directoryDocument } } = await http.get( {
				url: archiveUrl,
				html: true
			} );
			const rows = Array.from( directoryDocument.querySelectorAll( '.archive-line' ) );
			const comics = rows.map( row => {
				const [ , id, date ] = /(#\d+), ([0-9]{2}-[0-9]{2}-[0-9]{2})/i.exec( row.querySelector( '.archive-date' ).textContent );
				const url = new URL( row.querySelector( 'a' ).href, archiveUrl ).href;
				const name = row.querySelector( '.archive-title' ).textContent;
				const searchName = normalize( name.toLowerCase() );
				const game = row.querySelector( '.archive-game' ).textContent;
				const searchGame = normalize( game.toLowerCase() );
				const nameDistance = searchName.indexOf( query ) < 0 ? levenshtein( searchName, query ) : 0;
				const gameDistance = searchGame.indexOf( query ) < 0 ? levenshtein( searchGame, query ) : 0;
				return { id, date, url, name, game, distance: Math.min( nameDistance, gameDistance ) };
			} );
			const bestDistance = comics.map( c => c.distance ).sort()[ 0 ];
			const bestMatches = comics.filter( c => c.distance <= bestDistance );
			const { id, url, date, name, game } = shuffle( bestMatches )[ 0 ];
			const { window: { document } } = await http.get( { url, html: true } );
			const img = document.querySelector( '#cc-comicbody img' ) as HTMLImageElement;
			if( !img ) return;
			const message = renderToStaticMarkup( <AwkwardZombie name={name} game={game} date={date} id={id} url={url} src={img.src}/> );
			bus.next( { type: 'response', message, route: responseRoute } );
		} ),
		handleErrors( { logger } )
	).subscribe();

	lifecycle.shutdown$
	.subscribe( () => {
		lifecycle.done();
	} );
}
