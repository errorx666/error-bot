import { concatMap } from 'rxjs/operators';
import { parseCommands } from '~command';

import * as http from '~http';

import React, { PureComponent } from 'react';
import { renderToStaticMarkup } from 'react-dom/server';

import { URL } from 'url';
import { ExternalContent } from '~components';
import { handleErrors } from '~rx';

if( module.hot ) {
	module.hot.accept( [ '../components/index.ts', '../http.ts' ] );
}

type ModuleName = 'explosm';
type Params = ModuleParamsMap[ ModuleName ];

interface ExplosmProps {
	name: string;
	url: string;
	via?: string;
	src: string;
}

class Explosm extends PureComponent<ExplosmProps> {
	public constructor( props: ExplosmProps ) {
		super( props );
	}

	public render() {
		const { props } = this;
		return (
			<ExternalContent name="Cyanide &amp; Happiness" url={props.url} title={props.name} via={props.via}>
				<img src={props.src}/>
			</ExternalContent>
		);
	}
}

export default async function( { moduleName, lifecycle, bus, commandFilter, logger }: Params ) {
	if( module.hot ) {
		module.hot.addDisposeHandler( () => {
			lifecycle.shutdown();
		} );
	}

	type ExplosmCommandParameters = { query: 'rest'; };
	parseCommands<ExplosmCommandParameters>( {
		command: {
			name: '!explosm',
			prefix: [ 'explosm', 'c h', 'cyanide', 'cyanide and happiness' ],
			parameters: {
				query: {
					type: 'rest',
					required: true
				}
			}
		},
		lifecycle,
		filter: commandFilter
	} ).pipe(
		concatMap( async ( {
			responseRoute,
			parameters: { query }
		} ) => {
			const searchUrl = new URL( 'http://www.explosmsearch.net/search' );
			searchUrl.searchParams.set( 'q', query );
			const via = searchUrl.href;
			const { window: { document } } = await http.get( { url: searchUrl, html: true } );
			const img = document.querySelector( 'a[itemprop="url"] img' ) as HTMLImageElement;
			const name = img.alt || '';
			const link = img.closest( 'a' );
			const url = new URL( link.href, searchUrl ).href;
			const src = new URL( img.src, url ).href;
			const message = renderToStaticMarkup( <Explosm name={name} url={url} src={src} via={via}/> );
			bus.next( { type: 'response', message, route: responseRoute } );
		} ),
		handleErrors( { logger } )
	).subscribe();

	lifecycle.shutdown$
	.subscribe( () => {
		lifecycle.done();
	} );
}
