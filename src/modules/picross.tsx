/// <reference path="./picross.d.ts"/>

import React, { PureComponent } from 'react';
import type { ReactNode } from 'react';
import { merge, partition, Subject } from 'rxjs';
import assert from 'assert';
import { Fsm } from '~fsm';
import { takeUntil, groupBy, debounceTime, mergeMap, map, concatMap, filter, delay } from 'rxjs/operators';
import { renderToStaticMarkup } from 'react-dom/server';
import { parseCommands, query } from '~command';
import { dump, getDataUrl, lazy, spacesBetween, sum } from '~util';
import { hasher as Hasher } from 'node-object-hash';
import _ from 'lodash';
import { v4 as uuid } from 'uuid';
import { createCanvas, Image, loadImage } from 'canvas';
import glob from 'fast-glob';
import { Map } from 'immutable';
import path from 'path';
import { ProfileCounter } from '~profiling';
// import { gzip } from 'zlib';
// import fs from 'fs-extra';

import parseDataUrl from 'data-urls';

import { transform } from '~css';
const { translate } = transform;

import { shrinkImg, shrinkSvg } from '~dom';
import { rateLimit, tapLog } from 'rxjs-util';
import { handleErrors } from '~rx';
import { get } from '~http';
// import { promisify } from 'util';

if( module.hot ) {
	module.hot.accept( [ '../components/index.ts', '../profiling.ts' ] );
}

type Highlight = boolean|null;
type Clue = readonly [ number, Highlight ];
type Clues = readonly [ Highlight, ...Clue[] ];

type Point = readonly [ number, number ];
type Points = readonly Point[];

type ModuleName = 'picross';
type Params = ModuleParamsMap[ ModuleName ];

type GameStates = 'new-game'|'gameplay'|'game-over';
type GameTransitions = 'ready'|'load'|'win'|'lose'|'restart';

interface GameUpdate {
	readonly gameContext: PicrossGameContext;
	readonly children: ReactNode;
}

interface PicrossProps {
	readonly gridImg: JSX.Element;
	readonly hints: number;
	readonly children?: ReactNode;
}

declare interface PicrossResponse {
	readonly force?: boolean;
	readonly element: () => Promise<JSX.Element>;
	readonly route: ResponseRoute;
	readonly time: number;
}

class Picross extends PureComponent<PicrossProps> {
	public constructor( props: PicrossProps ) {
		super( props );
	}

	public render() {
		const { gridImg, children } = this.props;
		return <>
			{gridImg}
			<div>
				{children}
			</div>
		</>;
	}
}

const noRepeatLimit = 150;
const imageDir = path.resolve( __dirname, '..', '..', 'data', 'picross' );

const pointCharset = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

function formatPoint( [ x, y ]: Point ) {
	return pointCharset.charAt( y ) + String( x + 1 );
}

function samePoint( point1: Point, point2: Point ) {
	return point1[ 0 ] === point2[ 0 ] && point1[ 1 ] === point2[ 1 ];
}

type Grid<T = number> = readonly( readonly T[] )[];
type CellValue = boolean|'x';

interface PicrossRenderParams {
	readonly size: {
		readonly cols: number;
		readonly rows: number;
	};
	readonly showCoordinates: boolean;
	readonly showHighlights: boolean;
	readonly showMarks: boolean;
	readonly grid: Grid<CellValue>;
	readonly clues: GetCluesResult;
}

const hasher = Hasher();

function hasRedHighlight( clues: GetCluesResult ) {
	return [ ...clues.rows, ...clues.cols ].some( c => c[ 0 ] === false || ( c.slice( 1 ) as Clue[] ).some( ( [ , h ] ) => h === false ) );
}

async function renderBoard( { grid, clues, size, showCoordinates, showHighlights, showMarks }: PicrossRenderParams ) {
	const squareSize = 32;
	const padding = 4;
	const widestClue = Math.max( ...clues?.rows.map( ( [ , ...c ] ) => c.length ) ) ?? 0;
	const tallestClue = Math.max( ...clues?.cols.map( ( [ , ...c ] ) => c.length ) ) ?? 0;
	const gridWidthInner = size.cols * squareSize;
	const gridHeightInner = size.rows * squareSize;
	const gridWidthOuter = gridWidthInner + padding * 2;
	const gridHeightOuter = gridHeightInner + padding * 2;
	const textOffsetX = 14.5;
	const textOffsetY = 20;
	const cluesYSizeInner = tallestClue * squareSize;
	const cluesYSizeOuter = ( cluesYSizeInner > 0 ) ? cluesYSizeInner + padding * 2 : padding;
	const cluesXSizeInner = widestClue * squareSize;
	const cluesXSizeOuter = ( cluesXSizeInner > 0 ) ? cluesXSizeInner + padding * 2 : padding;
	const widthOuter = gridWidthOuter + cluesXSizeOuter;
	const heightOuter = gridHeightOuter + cluesYSizeOuter;

	if( !grid.flat().includes( 'x' ) ) {
		showMarks = false;
	}

	if( showHighlights && clues.cols.every( ( [ , h ] ) => h == null ) && clues.rows.every( ( [ , h ] ) => h == null ) ) {
		showHighlights = false;
	}

	const cellMap = Map<CellValue, string>( [ [ true, 'a' ], [ false, 'b' ], [ 'x', showMarks ? 'x' : 'b' ] ] );

	const thumbnailUrl = getDataUrl( 'image/png', await gridToImageBuffer( { width: size.cols, height: size.rows, grid } ) );

	function highlightClass( value: Highlight ) {
		if( !showHighlights ) return '';
		switch( value ) {
		case null: return '';
		case true: return 'y';
		case false: return 'n';
		default: throw new Error( `Unexpected highlight: ${value}` );
		}
	}

	function coord( x: number, y: number ) {
		if( !showCoordinates ) return <></>;
		return <text>{formatPoint( [ x, y ] )}</text>;
		// let s = '';
		// if( ( x % 5 ) === 0
		// || ( ( ( x + 1 ) % 5 ) === 0 && ( y + 1 ) % 5 ) === 0
		// || ( ( ( x + 3 ) % 5 ) === 0 && ( y + 3 ) % 5 ) === 0
		//  ) s += pointCharset.charAt( y );
		// if( ( y % 5 ) === 0
		// || ( ( ( x + 1 ) % 5 ) === 0 && ( y + 1 ) % 5 ) === 0
		// || ( ( ( x + 3 ) % 5 ) === 0 && ( y + 3 ) % 5 ) === 0
		// ) s += String( x + 1 );
		// if( !s ) return <></>;
		// return <text>{s}</text>;
	}

	const svg = <svg xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" viewBox={`0 0 ${widthOuter} ${heightOuter}`}>
		<symbol id={cellMap.get( true )}>
			<g className="s">
				<svg x="0" y="0" width="32" height="32"><rect fill="#888"/></svg>
			</g>
		</symbol>
		<symbol id={cellMap.get( false )}>
			<g className="s">
				<svg x="0" y="0" width="32" height="32"><rect fill="#888"/></svg>
				<svg x=".5" y=".5" width="29.5" height="29.5"><rect fill="#fff"/></svg>
			</g>
		</symbol>
		{showMarks ?
		<symbol id={cellMap.get( 'x' )}>
			<g className="s">
				<svg width="32" height="32">
					<rect fill="#888" />
				</svg>
				<svg x=".5" y=".5" width="29.5" height="29.5"><rect fill="#eee"/></svg>
				<g transform="translate(13 4) rotate(45 1.5 12)">
					<svg width="3" height="24">
						<rect fill="#888" />
					</svg>
				</g>
				<g transform="translate(13 4) rotate(315 1.5 12)">
					<svg width="3" height="24">
						<rect fill="#888" />
					</svg>
				</g>
			</g>
		</symbol> : <></>}
		<style>
			{showCoordinates ? `@namespace x url(http://www.w3.org/1999/xlink);` : ''}
			{`#g{${translate( cluesXSizeOuter, cluesYSizeOuter )}}`}
			{Array.from( { length: size.rows }, ( _, i ) => `#g > g:nth-child(${i + 1}){${translate( 0, i * squareSize )}}` )}
			{Array.from( { length: size.cols }, ( _, i ) => `#g > g > g:nth-child(${i + 1}){${translate( i * squareSize, 0 )}}` )}
			{`rect{pointer-events:none;width:${squareSize}px;height:${squareSize}px}`}
			{`text{dominant-baseline:middle;fill:#000;font:32px Helvetica,Arial,sans-serif;text-anchor:middle}`}
			{showHighlights ? `text.y{fill:#090}text.n{fill:#900}` : ''}
			{showCoordinates ? `#g text{dominant-baseline:hanging;fill:#666;font-size:14px;text-anchor:start;${translate( 3, 3 )}}` : ''}
			{showCoordinates ? `#g use[x|href="#${cellMap.get( true )}"] + text{fill: #ccc}` : ''}
			{`#hx{${translate( 0, cluesYSizeOuter )}}`}
			{clues.rows.map( ( _, i ) => `#h .r:nth-child(${i + 2}){transform:translateY(${i * squareSize}px)}` )}
			{Array.from( { length: widestClue }, ( _, i ) => `#h .r text:nth-last-child(${widestClue - i}){${translate( i * squareSize + textOffsetX, textOffsetY )}}` )}
			{`#hy{${translate( cluesXSizeOuter, 0 )}}`}
			{clues.cols.map( ( _, i ) => `#h .c:nth-child(${i + 2}){${translate( i * squareSize, 0 )}}` )}
			{Array.from( { length: tallestClue }, ( _, i ) => `#h .c text:nth-last-child(${tallestClue - i}){${translate( textOffsetX, i * squareSize + textOffsetY )}}` )}
			{`#hx > rect{fill:#fff;width:${cluesXSizeOuter}px;height:${gridHeightInner}px}`}
			{`#hy > rect{fill:#fff;width:${gridWidthInner}px;height:${cluesYSizeOuter}px}`}
			{`#h .r rect{width:${cluesXSizeOuter}px;height:${squareSize}px}`}
			{`#h .c rect{width:${squareSize}px;height:${cluesYSizeOuter}px}`}
			{`#h .h:nth-of-type(odd) rect{fill:#fff}`}
			{`#h .h:nth-of-type(even) rect{fill:#ccc}`}
			{showHighlights ? `#h .h.y:nth-of-type(odd) rect{fill:#aca}` : ''}
			{showHighlights ? `#h .h.y:nth-of-type(even) rect{fill:#9a9}` : ''}
			{showHighlights ? `#h .h.n:nth-of-type(odd) rect{fill:#caa}` : ''}
			{showHighlights ? `#h .h.n:nth-of-type(even) rect{fill:#a99}` : ''}
			{`#r rect{fill:#933}`}
			{`#r rect:first-child,#r rect:nth-child(2),#r rect:last-child{fill:#000}`}
			{`#rx rect{width:${widthOuter - padding * 2}px;height:1px}`}
			{`#ry rect{width:1px;height:${heightOuter - padding * 2}px}`}
			{`image{image-rendering:optimizeSpeed;image-rendering:pixelated}`}
		</style>
		<g id="h">
			<g id="hy">
				<rect/>
				{clues.cols.map( ( [ h, ...col ], i ) =>
					<g key={`hint-y-${i}`} className={[ 'h', 'c', highlightClass( h ) ].filter( c => c ).join( ' ' )}>
						{( h == null && ( i % 2 ) === 0 ) ? <></> : <rect/>}
						{col.map( ( [ num, b ], j ) => <text className={highlightClass( b )} key={`hint-y-${i}-${j}`}>{num}</text> )}
					</g>
				)}
			</g>
			<g id="hx">
				<rect/>
				{clues.rows.map( ( [ h, ...row ], i ) =>
					<g key={`hint-x-${i}`} className={[ 'h', 'r', highlightClass( h ) ].filter( c => c ).join( ' ' )}>
						{( h == null && ( i % 2 ) === 0 ) ? <></> : <rect/>}
						{row.map( ( [ num, b ], j ) => <text className={highlightClass( b )} key={`hint-x-${i}-${j}`}>{num}</text> )}
					</g>
				)}
			</g>
		</g>
		<g id="g">
			{grid.map( ( row, i ) =>
				<g key={`grid-${i}`}>
					{row.map( ( cell, j ) =>
						<g key={`grid-${i}-${j}`}>
							<use xlinkHref={`#${cellMap.get( cell )}`}/>
							{coord( j, i )}
						</g>
					)}
				</g>
			)}
		</g>
		<g id="r">
			<g id="rx">
				<rect/>
				{Array.from( { length: Math.floor( size.rows / 5 ) + 1 } ).map( ( _, i ) =>
					<rect key={`rx-${i}`} y={cluesYSizeOuter + squareSize * i * 5 - .5}/>
				)}
			</g>
			<g id="ry">
				<rect/>
				{Array.from( { length: Math.floor( size.cols / 5 ) + 1 } ).map( ( _, i ) =>
					<rect key={`ry-${i}`} x={cluesXSizeOuter + squareSize * i * 5 - .5}/>
				)}
			</g>
		</g>
		<image x={2} y={2} width={cluesXSizeOuter - 4} height={cluesYSizeOuter - 4} preserveAspectRatio="xMidYMid meet" href={thumbnailUrl}/>
	</svg>;

	const svgBuffer = Buffer.from( await shrinkSvg( svg ), 'utf8' );
	return <img src={getDataUrl( 'image/svg+xml', svgBuffer )} width="100%"/>;

	// const svgzBuffer = await promisify( gzip )( svgBuffer, { level: 9 } );
	// const wwwRoot = path.resolve( process.cwd(), 'www' );
	// const hash = hasher.hash( svgzBuffer );
	// const filename = `${hash}.svgz`;
	// const relativePath = path.join( 'picross', ( new Date ).toISOString().slice( 0, 10 ), filename );
	// const absolutePath = path.resolve( wwwRoot, relativePath );
	// await fs.ensureFile( absolutePath );
	// await fs.writeFile( absolutePath, svgzBuffer );
	// const baseUrl = `https://bot.error-un.limited/`;
	// return <img src={new URL( relativePath, baseUrl ).href} width="100%"/>;
}

function getRow<T>( grid: Grid<T>, row: number ) {
	return [ ...grid[ row ] ];
}

function getCol<T>( grid: Grid<T>, col: number ) {
	return [ ...grid ].map( g => g[ col ] );
}

function getNum( src: Iterable<CellValue> ) {
	let result = [] as number[];
	let c = 0;
	for( const cell of src ?? [] ) {
		if( cell === true ) ++c;
		else if( c > 0 ) {
			result = [ ...result, c ];
			c = 0;
		}
	}
	if( c > 0 ) result = [ ...result, c ];
	if( result.length === 0 ) return [ 0 ];
	return result;
}

interface BoardNums {
	readonly isEmpty: boolean;
	readonly isFull: boolean;
	readonly values: readonly CellValue[];
	readonly size: number;
	readonly full: readonly number[];
	readonly left: readonly number[];
	readonly right: readonly number[];
}
function getBoardNums( it: Iterable<CellValue> ) {
	const values = Array.from( it );
	const size = values.length;
	const full = getNum( values );
	const l = _.takeWhile( values, x => x !== false );
	let left: readonly number[];
	let right: readonly number[];
	let isFull: boolean;
	let isEmpty: boolean;
	if( l.length < values.length ) {
		isEmpty = values.every( v => v === false );
		isFull = false;
		left = getNum( l );
		right = getNum( _.takeRightWhile( values, x => x !== false ) );
	} else { // arr is completely filled; side-to-side algorithm will just show false positives because left and right overlap
		isEmpty = false;
		isFull = true;
		left = [];
		right = [];
	}
	return { isEmpty, isFull, values, size, full, left, right };
}

function *permute( clues: readonly number[], size: number ): IterableIterator<boolean[]> {
	if( clues.length < 1 ) return;
	const allClues = sum( clues );
	const whitespace = spacesBetween( clues );
	assert.ok( size >= allClues + whitespace );
	let firstClue: number;
	( [ firstClue, ...clues ] = clues );
	const isLastClue = clues.length === 0;
	const reservedSpace = isLastClue ? 0 : ( whitespace + sum( clues ) );
	const spaceForFirstClue = size - reservedSpace;
	assert.ok( spaceForFirstClue >= firstClue );
	const maxOffset = spaceForFirstClue - firstClue;
	assert.ok( maxOffset >= 0 );
	for( let offset = 0; offset <= maxOffset; ++offset ) {
		const after =
			isLastClue
			? [ Array.from( { length: size - offset - firstClue }, () => false ) ]
			: Array.from( permute( clues, size - offset - firstClue - 1 ), x => [ false, ...x ] );
		for( const postfix of after ) {
			const val = [
				...Array.from( { length: offset }, () => false ),
				...Array.from( { length: firstClue }, () => true ),
				...postfix
			];
			assert.ok( val.length === size );
			yield val;
		}
	}
}

interface GetCluesOptions {
	readonly board?: Grid<CellValue>;
	readonly solution: Grid<CellValue>;
	readonly size: { readonly rows: number; readonly cols: number };
	readonly showHighlights?: boolean;
}
interface GetCluesResult {
	readonly cols: readonly Clues[];
	readonly rows: readonly Clues[];
}
function getClues( { board, solution, size, showHighlights = true }: GetCluesOptions ) {
	if( board == null ) showHighlights = false;

	interface GetCluesOptions {
		readonly solutionNum: readonly number[];
		readonly boardNums?: BoardNums;
	}
	function getClues( { solutionNum, boardNums }: GetCluesOptions ): Clues {
		if( boardNums == null || !showHighlights ) return [ null, ...solutionNum.map( v => [ v, null ] as Clue ) ];

		let { isEmpty, isFull, size, full, left, right, values } = boardNums;
		if( _.isEqual( full, solutionNum ) ) {
			return [ true, ...solutionNum.map( v => [ v, true ] as Clue ) ];
		}
		let clueHighlight = null as Highlight;
		if( !isEmpty ) {
			let possibleConfigurations = Array.from( permute( solutionNum, size ) );
			for( let i = 0; i < size; ++i ) {
				const value = values[ i ];
				if( value === false ) continue;
				possibleConfigurations = possibleConfigurations.filter( v => v[ i ] === ( value === true ) );
				if( possibleConfigurations.length === 0 ) {
					clueHighlight = false;
					break;
				}
			}
		}
		left = left.filter( l => l > 0 );
		right = right.filter( r => r > 0 );
		let cluesLeft = [] as readonly Clue[];
		let cluesRight = [] as readonly Clue[];
		while( left.length > 0 ) {
			if( solutionNum[ 0 ] == null ) break;
			if( !isFull && left[ 0 ] < solutionNum[ 0 ] ) break;
			if( left[ 0 ] !== solutionNum[ 0 ] ) assert.ok( clueHighlight === false );
			cluesLeft = [ ...cluesLeft, [ solutionNum[ 0 ], left[ 0 ] === solutionNum[ 0 ] ] ];
			left = left.slice( 1 );
			solutionNum = solutionNum.slice( 1 );
		}
		while( right.length > 0 ) {
			if( solutionNum[ solutionNum.length - 1 ] == null ) break;
			if( !isFull && right[ right.length - 1 ] < solutionNum[ solutionNum.length - 1 ] ) break;
			if( right[ right.length - 1 ] !== solutionNum[ solutionNum.length - 1 ] ) assert.ok( clueHighlight === false );
			cluesRight = [ [ solutionNum[ solutionNum.length - 1 ], right[ right.length - 1 ] === solutionNum[ solutionNum.length - 1 ] ], ...cluesRight ];
			right = right.slice( 0, right.length - 1 );
			solutionNum = solutionNum.slice( 0, solutionNum.length - 1 );
		}
		const cluesMid = solutionNum.map( n => ( [ n, isFull ? false : null ] as Clue ) );
		return [ clueHighlight, ...cluesLeft, ...cluesMid, ...cluesRight ];
	}
	return {
		rows: Array.from( { length: size.rows } )
			.map( ( _, y ) => {
				const solutionNum = getNum( [ ...solution[ y ] ] );
				const boardNums = showHighlights ? getBoardNums( getRow( board, y ) ) : null;
				return getClues( { solutionNum, boardNums } );
			} ),
		cols: Array.from( { length: size.cols } )
			.map( ( _, x ) => {
				const solutionNum = getNum( solution.map( r => r[ x ] ) );
				const boardNums = showHighlights ? getBoardNums( getCol( board, x ) ) : null;
				return getClues( { solutionNum, boardNums } );
			} )
	} as GetCluesResult;
}

function getGameStats( { board, solution, width, height }: Pick<PicrossGameContext, 'board'|'solution'|'width'|'height'> ) {
	const size = { cols: width, rows: height };
	const clues = getClues( { board, solution, size } );

	const solutionClues = getClues( { solution, size, showHighlights: false } );
	const filledClues = getClues( { solution: board, size, showHighlights: false } );
	const potentialFilledClues = getClues( { solution: board.map( bb => bb.map( b => ( b === false ) ? true : b ) ), size, showHighlights: false } );

	const isInvertedWin = _.isEqual( solutionClues, potentialFilledClues );
	const isWin = isInvertedWin || _.isEqual( solutionClues, filledClues );
	const isGameOver = isWin;
	return { clues, size, isWin, isInvertedWin, isGameOver };
}

async function renderGameContext( { board, solution, width, height }: Pick<PicrossGameContext, 'board'|'solution'|'width'|'height'> ) {
	const { clues, size, isGameOver } = getGameStats( { board, solution, width, height } );
	return await renderBoard( { grid: board, clues, size, showCoordinates: !isGameOver, showHighlights: !isGameOver, showMarks: !isGameOver } );
}

const pointPattern = /^([a-z])[- ]?([0-9]{1,2})(?:\s*[-:]\s*(?:([a-z])[- ]?)?([0-9]{1,2}))?$/i;
function parsePoints( pointStr: string, size?: { readonly width: number; readonly height: number; } ) {
	function parseRange( pointStr: string ) {
		const squareParts = pointPattern.exec( pointStr );
		if( squareParts == null ) return [];
		function parsePoint( x: string, y: string ) {
			if( x == null || y == null ) return null;
			const xCoord = parseInt( x, 10 ) - 1;
			const yCoord = pointCharset.indexOf( y );
			if( isNaN( xCoord ) || isNaN( yCoord ) || xCoord < 0 || yCoord < 0 || xCoord >= size?.width || yCoord >= size?.height ) return null;
			return [ xCoord, yCoord ] as Point;
		}
		const [ , y1, x1, y2, x2 ] = squareParts;
		const point1 = parsePoint( x1, y1 );
		if( point1 == null ) return [];
		const point2 = parsePoint( x2, y2 ?? y1 ) ?? point1;
		if( point2 == null ) return [];
		return getRange( point1, point2 );
	}
	const p = /([a-z])[- ]?([0-9]{1,2})(?:\s*[-:]\s*(?:[a-z][- ]?)?[0-9]{1,2})?/gi;
	let points = [] as Points;
	let m: RegExpMatchArray;
	while( ( m = p.exec( pointStr ) ) != null ) {
		points = [ ...points, ...parseRange( m[ 0 ].trim() ) ];
	}
	return points;
}

function getRange( [ startX, startY ]: Point, [ endX, endY ]: Point ) {
	( [ startX, endX ] = [ Math.min( startX, endX ), Math.max( startX, endX ) ] );
	( [ startY, endY ] = [ Math.min( startY, endY ), Math.max( startY, endY ) ] );
	let points = [] as Points;
	for( let y = startY; y <= endY; ++y ) {
	for( let x = startX; x <= endX; ++x ) {
			points = [ ...points, [ x, y ] ];
		}
	}
	return points;
}

function imageToGrid( img: Image ) {
	const width = img.naturalWidth;
	const height = img.naturalHeight;
	const canvas = createCanvas( width, height );
	const c2d = canvas.getContext( '2d' );
	c2d.drawImage( img, 0, 0, width, height );
	const imgData = c2d.getImageData( 0, 0, width, height );
	let grid = [] as Grid<boolean>;
	for( let y = 0; y < height; ++y ) {
		let row = [] as readonly boolean[];
		for( let x = 0; x < width; ++x ) {
			const offset = ( y * width + x ) * 4;
			const [ r, g, b ] = imgData.data.slice( offset, offset + 3 );
			const set = Math.round( ( r + g + b / 3 ) / 255 ) === 0;
			row = [ ...row, set ];
		}
		grid = [ ...grid, row ];
	}
	return { width, height, grid, size: { cols: width, rows: height } };
}

function gridId( grid: Grid<boolean>, width: number, height: number ) {
	const { rows, cols } = getClues( { solution: grid, size: { cols: width, rows: height }, showHighlights: false } );
	return hasher.hash( [ height, ...rows.flatMap( ( [ , ...r ] ) => [ ...r.map( ( [ n ] ) => n ), -1 ] ), width, ...cols.flatMap( ( [ , ...c ] ) => [ ...c.map( ( [ n ] ) => n ), -1 ] ) ] );
}

async function gridToImageBuffer( { width, height, grid }: { readonly width: number; readonly height: number; readonly grid: Grid<CellValue>; } ) {
	const canvas = createCanvas( width, height );
	const c2d = canvas.getContext( '2d' );
	const imgData = c2d.createImageData( width, height );
	for( let y = 0; y < height; ++y ) {
		for( let x = 0; x < width; ++x ) {
			const offset = ( y * width + x ) * 4;
			imgData.data[ offset + 0 ] = imgData.data[ offset + 1 ] = imgData.data[ offset + 2 ] = ( grid[ y ][ x ] === true ) ? 0 : 255;
			imgData.data[ offset + 3 ] = 255;
		}
	}
	c2d.putImageData( imgData, 0, 0 );
	let buffer = canvas.toBuffer( 'image/png', {} );
	buffer = await shrinkImg( buffer, 'image/png' );
	return buffer;
}

export default async function( { moduleName, lifecycle, bus, session, socket, tid, commandFilter, state$, logger }: Params ) {
	if( module.hot ) {
		module.hot.addDisposeHandler( () => {
			lifecycle.shutdown();
		} );
	}

	const counter = new ProfileCounter;

	const response$ = new Subject<PicrossResponse>();
	function response( element: () => JSX.Element|Promise<JSX.Element>, route: ResponseRoute, force?: boolean ) {
		response$.next( { element: lazy( () => Promise.resolve( element() ) ), route, force, time: ( new Date ).getTime() } );
	}

	const gameUpdate$ = new Subject<GameUpdate>();
	async function updateGameContext( ctx: Partial<PicrossGameContext>, children = [] as ReactNode ) {
		const gameState = state$?.value ?? {} as PicrossState;
		const gameContext = { ...( gameState.gameContext ), ...ctx } as PicrossGameContext;
		state$.next( { ...gameState, gameContext } );
		gameUpdate$.next( { gameContext, children } );
		return gameContext;
	}

	let lastRoundId: string = null;
	gameUpdate$
	.pipe(
		map( gameUpdate => {
			const { children, gameContext } = gameUpdate;
			const { hints } = gameContext;
			try {
				const { isGameOver, isInvertedWin } = getGameStats( { ...gameContext } );
				return {
					isGameOver,
					roundId: gameContext.roundId,
					element: async () => {
						const boardImg = await renderGameContext( { ...gameContext, board: isInvertedWin ? gameContext.board.map( bb => bb.map( b => b === false ? true : b ) ) : gameContext.board } );
						return <Picross gridImg={boardImg} hints={hints}>{children}</Picross>;
					}
				};
			} catch( ex ) {
				logger.fatal( ex );
				throw ex;
			}
		} ),
		handleErrors( { logger } ),
		takeUntil( lifecycle.shutdown$ )
	)
	.subscribe( ( { isGameOver, roundId, element } ) => {
		const force = isGameOver || lastRoundId !== roundId;
		lastRoundId = roundId;
		response( element, { type: 'thread', tid }, force );
	} );

	const gameStateFsm = new Fsm<GameStates, GameTransitions>( {
		states: {
			'new-game': {
				transitions: {
					ready: 'gameplay'
				}
			},
			gameplay: {
				transitions: {
					lose: 'game-over',
					win: 'game-over'
				}
			},
			'game-over': {
				transitions: {}
			}
		},
		transitions: {
			restart: 'new-game',
			load: 'gameplay'
		}
	} );

	gameStateFsm.states.pipe( takeUntil( lifecycle.shutdown$ ), tapLog() ).subscribe();
	gameStateFsm.transitions.pipe( takeUntil( lifecycle.shutdown$ ), tapLog() ).subscribe();

	const [ forcedResponse$, unforcedResponse$ ] = partition(
		response$.pipe( map( r => ( { ...r, routeHash: hasher.hash( r.route ) } ) ) ),
		r => !!r.force
	);

	const lastResponse = {} as Record<string, number>;
	merge(
		forcedResponse$,
		unforcedResponse$.pipe(
			groupBy( ( { routeHash } ) => routeHash ),
			mergeMap( response => response.pipe(
				debounceTime( 500 )
			) )
		)
	).pipe(
		concatMap( async ( { element, ...props } ) => ( { ...props, message: renderToStaticMarkup( await element() ) } ) ),
		handleErrors( { logger } ),
		takeUntil( lifecycle.shutdown$ )
	)
	.subscribe( ( { message, routeHash, force, time, route } ) => {
		if( lastResponse[ routeHash ] > time ) {
			if( !force ) return;
		} else {
			lastResponse[ routeHash ] = time;
		}
		bus.next( { type: 'response', message, route } );
	} );

	interface NewGameOptions {
		readonly issuer: CommandIssuer;
		readonly file?: string;
		readonly usedGrids?: readonly string[];
	}
	async function newGame( { issuer, file, usedGrids }: NewGameOptions ) {
		const roundId = uuid();

		let images = _.shuffle( await glob( [ file ?? '**/*.png' ], {
			cwd: imageDir,
			caseSensitiveMatch: false,
			onlyFiles: true,
			absolute: true
		} ) );

		let width: number;
		let height: number;
		let grid: Grid<boolean>;
		let id: string;

		while( images.length > 0 ) {
			file = images[ 0 ];
			if( !file ) throw new Error( 'No suitable file to load' );
			images = images.slice( 1 );
			try {
				const img = await loadImage( file );
				( { width, height, grid } = imageToGrid( img ) );
				id = gridId( grid, width, height );
				if( images.length === 0 || usedGrids?.some( rid => _.isEqual( id, rid ) ) ) continue;
				break;
			} catch( ex ) {
				logger.fatal( `Failed to load ${file}`, ex );
			}
		}

		const gameContext = await updateGameContext( {
			roundId,
			width,
			height,
			solution: grid,
			board: grid.map( row => row.map( () => false ) ),
			hints: 0,
			history: [ { command: 'new-game', issuer }
		] }, <p>New game.</p> );
		if( gameStateFsm.state === 'new-game' ) gameStateFsm.transition( 'ready' );
		else gameStateFsm.transition( 'load' );
		return { id, grid, width, height, gameContext };
	}

	gameStateFsm.states
	.pipe(
		filter( ( { enter } ) => enter === 'new-game' ),
		takeUntil( lifecycle.shutdown$ ),
		concatMap( async () => {
			let state = state$.value ?? {} as Partial<PicrossState>;
			const file = state.nextFile;
			let usedGrids = state.usedGrids ?? [];
			const savedGames = state.savedGames ?? {};
			if( file != null ) {
				state = _.omit( state, 'nextFile' );
			}
			const { id, gameContext } = await newGame( { issuer: null, file, usedGrids } );
			usedGrids = [ id, ...usedGrids ].slice( 0, noRepeatLimit );
			const nextState: PicrossState = { ...state, gameContext, savedGames, usedGrids };
			dump( logger, nextState );
			state$.next( nextState );
			gameStateFsm.transition( 'ready' );
		} )
	).subscribe();

	gameStateFsm.transitions
	.pipe(
		filter( t => t === 'load' ),
		takeUntil( lifecycle.shutdown$ )
	).subscribe( () => {
		const gameContext = state$.value?.gameContext;
		const { isGameOver } = getGameStats( { ...gameContext } );
		if( isGameOver ) {
			gameStateFsm.transition( 'restart' );
		}
	} );

	gameStateFsm.states
	.pipe(
		filter( ( { enter } ) => enter === 'game-over' ),
		delay( 6000 ),
		filter( () => gameStateFsm.state === 'game-over' ),
		takeUntil( lifecycle.shutdown$ )
	).subscribe( () => {
		gameStateFsm.transition( 'restart' );
	} );

	const slowPoke = <img src="/assets/uploads/files/1567795880992-7538621b-e3c4-43a0-959f-2860eb885886-image.png"/>;
	const hanzo = <p>:hanzo:</p>;
	const spoopy = <p>:face_screaming_in_fear:</p>;
	const wtf = <p>:wtf:</p>;
	const warthog = <p>:kneeling_warthog:</p>;

	function picrossChatCommand
	<TParameterTypeMap extends { [ TParameterName in TParameterNames ]: CommandParameterType; },
	TParameterNames extends keyof TParameterTypeMap & string = keyof TParameterTypeMap & string>
	( cd: CommandDefinition<TParameterTypeMap, TParameterNames> & { readonly prefix: readonly string[]; } ) {
		return merge(
			parseCommands<TParameterTypeMap, TParameterNames>( {
				command: {
					...cd,
					query: {
						$and: [
							query.inThread( tid ),
							cd.query || { $value: true }
						]
					},
					prefix: [ ...cd.prefix, ...cd.prefix.map( p => `picross ${p}` ) ]
				},
				lifecycle,
				filter: commandFilter
			} ),
			parseCommands<TParameterTypeMap, TParameterNames>( {
				command: {
					...cd,
					query: {
						$and: [
							query.inChat(),
							cd.query || { $value: true }
						]
					},
					prefix: [ ...cd.prefix.map( p => `picross ${p}` ) ]
				},
				lifecycle,
				filter: commandFilter
			} )
		);
	}

	picrossChatCommand( {
		query: {
			$and: [
				query.fromUser(),
				{ $not: query.fromRole( 'admin' ) }
			]
		},
		prefix: [ 'cheat', 'next file', 'next' ],
		parameters: 'ignore'
	} )
	.subscribe( ( { issuer } ) => {
		if( issuer.type !== 'user' ) return;
		const gameContext = state$.value?.gameContext;
		if( !gameContext ) return;
		const { userslug } = issuer;
		updateGameContext( {}, <p>
			@{userslug} tried to <b>cheat</b>.
		</p> );
	} );

	interface SquareCommandParameters {
		readonly force: 'boolean';
		readonly tokens: 'rest';
	}
	const commandTokenPatterns = {
		auto: /^(?:auto(?:matic)?)\s*([1-9])?$/i,
		fill: /^(?:f(?:ill)?|show)$/i,
		hint: /^(?:h(?:int)?)$/i,
		mark: /^(?:m(?:ark)?)$/i,
		clear: /^(?:c(?:lear)?)$/i
	} as Readonly<Record<PicrossCommand, RegExp>>;

	type VirtualSquareCommandCalc = {
		readonly snapshot: PicrossGameContext;
		readonly count: number;
	};
	type VirtualSquareCommand = Command<{}> & {
		readonly parameters: {
			readonly command: PicrossCommand;
			readonly square: Point;
			readonly allSquares: readonly Point[];
			readonly force: boolean;
		};
		readonly tokenIndex: number;
	} & VirtualSquareCommandCalc;

	parseCommands<SquareCommandParameters>( {
		command: {
			query: query.inThread( tid ),
			parameters: {
				force: {
					type: 'boolean',
					default: false
				},
				tokens: {
					type: 'rest',
					required: true
				}
			}
		},
		lifecycle,
		filter: commandFilter,
		priority: -1
	} ).pipe(
		map( c => ( { ...c, roundId: state$.value?.gameContext.roundId } ) ),
		rateLimit( 5 ),
		concatMap( ( { parameters, ...c } ) => {
			counter.count( 'command phase 1' );
			const gameContext = state$.value?.gameContext;
			if( !gameContext ) return [];
			const { width, height } = gameContext;
			let commands = [] as readonly VirtualSquareCommand[];
			let command: PicrossCommand;
			const tokens = parameters.tokens.split( /\s+/g );
			const tokenMap = Map( Object.entries( commandTokenPatterns ) as [ PicrossCommand, RegExp ][] );
			const placeholders = { count: null } as VirtualSquareCommandCalc;
			for( let tokenIndex = 0; tokenIndex < tokens.length; ++tokenIndex ) {
				const token = tokens[ tokenIndex ];
				let isCommand = false;
				for( const [ cmd, re ] of tokenMap.entries() ) {
					if( re.test( token ) ) {
						switch( cmd ) {
						case 'auto': {
							let n = 1;
							const m = re.exec( token )?.[ 1 ];
							if( /^\d+$/.test( m ) ) {
								n = Math.max( 1, Number( m ) );
							}
							for( let i = 0; i < n; ++i ) {
								const virtualCommand = { ...c, ...placeholders, parameters: { command: cmd, square: null, force: parameters.force, allSquares: [] }, tokenIndex, snapshot: gameContext };
								commands = [ ...commands, virtualCommand ];
							}
							break;
						}
						case 'hint': {
							const virtualCommand = { ...c, ...placeholders, parameters: { command: cmd, square: null, force: parameters.force, allSquares: [] }, tokenIndex, snapshot: gameContext };
							commands = [ ...commands, virtualCommand ];
							break;
						}
						default: {
							command = cmd;
							break;
						}
						}
						isCommand = true;
						break;
					}
				}
				if( command == null || isCommand ) continue;
				const allSquares = parsePoints( token, { width, height } );
				for( const square of allSquares ) {
					const virtualCommand = { ...c, ...placeholders, parameters: { command, square, force: parameters.force, allSquares }, tokenIndex, snapshot: gameContext };
					commands = [ ...commands, virtualCommand ];
				}
			}
			const count = commands.length;
			commands = commands.map( c => ( { ...c, count } ) );
			return commands;
		} ),
		rateLimit( 5 ),
		concatMap( async ( { parameters: { command, square, allSquares, force }, timestamp, issuer, responseRoute, snapshot } ) => {
			counter.count( 'command phase 2' );
			const gameContext = state$.value?.gameContext;
			if( !gameContext ) return;
			const { width, height } = gameContext;
			let { board, solution, history, hints } = gameContext;

			const beforeStats = getGameStats( { ...gameContext } );

			if( gameStateFsm.state !== 'gameplay' || beforeStats.isGameOver || gameContext.roundId !== snapshot.roundId ) {
				// logger.info( {
				// 	msg: ':slowpoke:',
				// 	state: gameStateFsm.state,
				// 	isGameOver: beforeStats.isGameOver,
				// 	roundId: gameContext.roundId,
				// 	roundId2: roundId
				// } );
				if( timestamp > gameContext.lastGuess ) {
					response( () => slowPoke, responseRoute );
				}
				return;
			}

			const currentValue = ( square == null ) ? null : board[ square[ 1 ] ][ square[ 0 ] ];
			const allOldValues = allSquares.map( ( [ x, y ] ) => snapshot.board[ y ][ x ] );
			switch( command ) {
			case 'fill':
				assert.ok( square != null && currentValue != null && allSquares.length >= 1 && allOldValues.length >= 1 );
				if( currentValue === true ) {
					if( allOldValues.every( v => v === true ) ) {
						response( () => hanzo, responseRoute );
					}
					return;
				}
				if( !force && currentValue === 'x' ) {
					if( allOldValues.every( v => v === 'x' ) ) {
						response( () => spoopy, responseRoute );
					}
					return;
				}
				break;
			case 'clear':
				assert.ok( square != null && currentValue != null && allSquares.length >= 1 && allOldValues.length >= 1 );
				if( currentValue === false ) {
					if( allOldValues.every( v => v === false ) ) {
						response( () => hanzo, responseRoute );
					}
					return;
				}
				break;
			case 'mark':
				assert.ok( square != null && currentValue != null && allSquares.length >= 1 && allOldValues.length >= 1 );
				if( currentValue === 'x' ) {
					if( allOldValues.every( v => v === 'x' ) ) {
						response( () => hanzo, responseRoute );
					}
					return;
				}
				if( !force && currentValue === true ) {
					if( allOldValues.every( v => v === true ) ) {
						response( () => spoopy, responseRoute );
					}
					return;
				}
				break;
			case 'auto': {
				assert.ok( square == null && currentValue == null && allSquares.length === 0 && allOldValues.length === 0 );
				const cluesBefore = getClues( { solution, board, size: { cols: width, rows: height }, showHighlights: true } );
				if( !force ) {
					if( hasRedHighlight( cluesBefore ) ) {
						response( () => spoopy, responseRoute );
						return;
					}
				}

				const boardBefore = board;
				let boardAfter = board.map( bb => bb.map( () => null ) ) as Grid<CellValue|null>;

				function putBoard( y: number, x: number, value: CellValue ) {
					// logger.info( 'putBoard', { x, y, value, before: boardBefore[ y ][ x ], after: boardAfter[ y ][ x ] } );
					if( boardBefore[ y ][ x ] === value ) {
						return false; // no change needed, original board already in correct state
					}
					if( boardBefore[ y ][ x ] !== false ) {
						// logger.info( 'putBoard', 1 );
						return true; // change not allowed, can only change empty squares
					}
					if( boardAfter[ y ][ x ] != null && boardAfter[ y ][ x ] !== value ) {
						// logger.info( 'putBoard', 2 );
						return true; // change not allowed, conflicting change
					}
					boardAfter = boardAfter.map( ( bb, yy ) => bb.map( ( b, xx ) => ( yy === y && xx === x ) ? value : b ) );
					return false;
				}

				function setRow( value: readonly CellValue[], y: number ) {
					for( let x = 0; x < width; ++x ) {
						if( putBoard( y, x, value[ x ] ) ) return true;
					}
					return false;
				}

				function setCol( value: readonly CellValue[], x: number ) {
					for( let y = 0; y < height; ++y ) {
						if( putBoard( y, x, value[ y ] ) ) return true;
					}
					return false;
				}

				function fill( size: number, getter: ( grid: Grid<CellValue>, i: number ) => readonly CellValue[], setter: ( values: readonly CellValue[], i: number ) => boolean ) {
					for( let i = 0; i < size; ++i ) {
						let values = getter( boardBefore, i );
						const boardNums = getBoardNums( values );
						if( boardNums.isFull ) continue;
						const solutionNum = getNum( getter( solution, i ) );
						let clueIndex = 0;
						let fillSize = 0;
						for( let j = 0; j < values.length; ++j ) {
							const clue = solutionNum[ clueIndex ];
							if( clue == null ) break;
							const value = values[ j ];
							assert.ok( value != null );
							if( value === 'x' ) {
								if( fillSize === clue ) {
									++clueIndex;
									fillSize = 0;
								} else if( fillSize > 0 ) {
									break;
								}
							} else if( value === true ) {
								++fillSize;
							} else {
								// if( value !== false ) logger.info( value );
								assert.ok( value === false );
								if( fillSize === 0 ) {
									break;
								} else if( fillSize < clue ) {
									values = values.map( ( v, i ) => i === j ? true : v );
									++fillSize;
								} else if( fillSize === clue ) {
									values = values.map( ( v, i ) => i === j ? 'x' : v );
									break;
								}
							}
						}
						clueIndex = Math.max( 0, solutionNum.length - 1 );
						fillSize = 0;
						for( let j = values.length - 1; j >= 0; --j ) {
							const clue = solutionNum[ clueIndex ];
							if( clue == null ) break;
							const value = values[ j ];
							assert.ok( value != null );
							if( value === 'x' ) {
								if( fillSize === clue ) {
									--clueIndex;
									fillSize = 0;
								} else if( fillSize > 0 ) {
									break;
								}
							} else if( value === true ) {
								++fillSize;
							} else {
								// if( value !== false ) logger.info( value );
								assert.ok( value === false );
								if( fillSize === 0 ) {
									break;
								} else if( fillSize < clue ) {
									values = values.map( ( v, i ) => i === j ? true : v );
									++fillSize;
								} else if( fillSize === clue ) {
									values = values.map( ( v, i ) => i === j ? 'x' : v );
									break;
								}
							}
						}
						const markedValues = values.map( v => v === false ? 'x' : v );
						const filledValues = values.map( v => v === false ? true : v );
						const proposals = [ markedValues, filledValues ];
						for( const proposal of proposals ) {
							if( _.isEqual( solutionNum, getNum( proposal ) ) ) {
								if( setter( proposal, i ) ) {
									return true;
								}
								break;
							}
						}
						if( setter( values, i ) ) {
							return true;
						}
					}
					return false;
				}

				if( fill( height, getRow, setRow ) ) {
					// logger.info( 'auto', 1 );
					response( () => wtf, responseRoute );
					return;
				}

				if( fill( width, getCol, setCol ) ) {
					// logger.info( 'auto', 2 );
					response( () => wtf, responseRoute );
					return;
				}

				// make sure nothing in our overlay contradicts the original board state
				if( boardBefore.some( ( bb, y ) => bb.some( ( b, x ) => boardAfter[ y ][ x ] != null && b !== false && b !== boardAfter[ y ][ x ] ) ) ) {
					// logger.info( 'auto', 3 );
					response( () => wtf, responseRoute );
					return;
				}

				if( boardAfter.every( bb => bb.every( b => b == null ) ) ) {
					response( () => warthog, responseRoute );
					return;
				}

				// merge overlay to board state
				board = boardBefore.map( ( bb, y ) => bb.map( ( b, x ) => boardAfter[ y ][ x ] ?? boardBefore[ y ][ x ] ) );

				const cluesAfter = getClues( { solution, board, size: { cols: width, rows: height }, showHighlights: true } );
				if( !force && hasRedHighlight( cluesAfter ) ) {
					response( () => wtf, responseRoute );
					return;
				}
				break;
			}
			case 'hint':
				assert.ok( square == null && currentValue == null && allSquares.length === 0 && allOldValues.length === 0 );
				break;
			default: assert.fail( `Unexpected command: ${command}` );
			}

			let transition = null as GameTransitions;
			let message = <></> as ReactNode;

			switch( command ) {
			case 'fill':
				board = board.map( ( row, y ) =>
					row.map( ( cell, x ) =>
						samePoint( [ x, y ], square ) ? true : cell
				) );
				history = [ ...history, { command, square, force, issuer } ];
				break;
			case 'clear':
				board = board.map( ( row, y ) =>
					row.map( ( cell, x ) =>
						samePoint( [ x, y ], square ) ? false : cell
				) );
				history = [ ...history, { command, square, force, issuer } ];
				break;
			case 'mark':
				board = board.map( ( row, y ) =>
					row.map( ( cell, x ) =>
						samePoint( [ x, y ], square ) ? 'x' : cell
				) );
				history = [ ...history, { command, square, force, issuer } ];
				break;
			case 'auto':
				history = [ ...history, { command, force, issuer } ];
				break;
			case 'hint':
				++hints;
				history = [ ...history, { command, force, issuer } ];
				break;
			default: assert.fail( `Unexpected command: ${command}` );
			}
			const { isWin } = getGameStats( { board, solution, width, height } );

			if( isWin ) {
				if( [ 'hint', 'auto' ].includes( command ) ) {
					message = <>{message}<p>:fireworks: I won!</p></>;
				} else {
					message = <>{message}<p>:fireworks: You win!</p></>;
				}
				transition = 'win';
			}

			await updateGameContext( {
				board,
				solution,
				lastGuess: timestamp,
				history,
				hints
			},
				<div>
					{ message }
				</div>
			);
			if( transition ) gameStateFsm.transition( transition );
			logger.info( counter.toString() );
		} ),
		handleErrors( { logger } )
	).subscribe();

	interface PicrossNewGameCommandParameters {
		readonly file: 'string';
	}
	picrossChatCommand<PicrossNewGameCommandParameters>( {
		prefix: [ 'new game' ],
		parameters: {
			file: {
				type: 'string',
				required: false,
				position: 0
			}
		}
	} )
	.pipe( concatMap( async ( { issuer, parameters: { file } } ) => {
		let state = state$.value ?? {} as Partial<PicrossState>;
		let usedGrids = state.usedGrids ?? [];
		const savedGames = state.savedGames ?? {};
		if( file == null ) {
			file = state.nextFile;
			if( file != null ) {
				state = _.omit( state$.value, 'nextfile' );
			}
		}
		const { id, gameContext } = await newGame( { issuer, file, usedGrids } );
		usedGrids = [ id, ...usedGrids ].slice( 0, noRepeatLimit );
		const nextState: PicrossState = { ...state, gameContext, savedGames, usedGrids };
		dump( logger, nextState );
		state$.next( nextState );
	} )	)
	.subscribe();

	picrossChatCommand( {
		prefix: [ 'cheat' ]
	} )
	.pipe( concatMap( async ( { responseRoute } ) => {
		const gameContext = state$.value?.gameContext;
		if( !gameContext ) return;

		const { size, clues } = getGameStats( { ...gameContext } );

		const img = await renderBoard( {
			size,
			clues,
			grid: gameContext.solution,
			showCoordinates: false,
			showHighlights: true,
			showMarks: false
		} );
		const message = renderToStaticMarkup( img );
		bus.next( { type: 'response', message, route: responseRoute } );
	} ) )
	.subscribe();

	picrossChatCommand( {
		prefix: [ 'print' ],
		parameters: 'ignore'
	} )
	.subscribe( () => {
		updateGameContext( {} );
	} );

	interface NextFileCommandParameters {
		readonly clear: 'boolean';
		readonly file: 'string';
	}
	picrossChatCommand<NextFileCommandParameters>( {
		query: query.fromRole( 'admin' ),
		prefix: [ 'next file', 'next' ],
		parameters: {
			clear: {
				alias: [ 'unset' ],
				type: 'boolean',
				default: false,
				conflicts: [ 'file' ]
			},
			file: {
				type: 'string',
				position: 0,
				conflicts: [ 'clear' ]
			}
		}
	} )
	.subscribe( ( { responseRoute, parameters: { clear, file } } ) => {
		const gameState = state$.value;
		if( !gameState ) return;
		if( clear ) {
			state$.next( { ...gameState, nextFile: null } );
			bus.next( { type: 'response', message: `Next file cleared.`, route: responseRoute } );
		} else {
			if( !file ) return;
			state$.next( { ...gameState, nextFile: file } );
			bus.next( { type: 'response', message: `Next file: **${file}**`, route: responseRoute } );
		}
	} );

	picrossChatCommand( {
		query: query.fromRole( 'admin' ),
		prefix: [ 'render' ]
	} )
	.pipe( concatMap( async ( { responseRoute } ) => {
		const { gameContext } = state$.value;
		const img = await renderGameContext( { ...gameContext } );
		const message = renderToStaticMarkup( img );
		bus.next( { type: 'response', message, route: responseRoute } );
	} ) )
	.subscribe();

	interface PreviewCommandParams {
		readonly pathOrUrl: 'string';
		readonly showCoordinates: 'boolean';
		readonly showHighlights: 'boolean';
	}
	picrossChatCommand<PreviewCommandParams>( {
		query: query.fromRole( 'admin' ),
		prefix: [ 'preview' ],
		parameters: {
			pathOrUrl: {
				type: 'string',
				required: true,
				position: 0
			},
			showCoordinates: {
				type: 'boolean',
				required: false,
				default: true
			},
			showHighlights: {
				type: 'boolean',
				required: false,
				default: false
			}
		}
	} )
	.pipe( concatMap( async ( { responseRoute, parameters: { pathOrUrl, showCoordinates, showHighlights } } ) => {
		let img: Image;
		if( /^data:/.test( pathOrUrl ) ) {
			const buffer = parseDataUrl( pathOrUrl ).body;
			img = await loadImage( buffer );
		} else if( /\/\//.test( pathOrUrl ) ) {
			const buffer = await get( {
				url: pathOrUrl,
				encoding: null
			} );
			img = await loadImage( buffer );
		} else {
			pathOrUrl = path.resolve( imageDir, pathOrUrl );
			img = await loadImage( pathOrUrl );
		}
		const { grid, size } = imageToGrid( img );
		const clues = getClues( { solution: grid, size, showHighlights } );
		const message = renderToStaticMarkup( await renderBoard( { grid, size, clues, showCoordinates, showHighlights, showMarks: false } ) );
		bus.next( { type: 'response', message, route: responseRoute } );
	} ) )
	.subscribe();


	if( state$.value ) {
		const gameContext = state$.value.gameContext;
		if( gameContext ) {
			gameStateFsm.transition( 'load' );
		}
	}

	if( gameStateFsm.state === null ) {
		gameStateFsm.transition( 'restart' );
	}

	lifecycle.shutdown$
	.subscribe( async () => {
		gameUpdate$.subscribe();
		response$.complete();
		gameStateFsm.complete();
		lifecycle.done();
	} );
}
