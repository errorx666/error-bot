import { concatMap } from 'rxjs/operators';
import React from 'react';
import { URL } from 'url';

import * as http from '~http';

import { apiKeys } from '~data/m-w.yaml';
import { parseCommands } from '~command';

import xsltDictionaryText from '~data/dictionary.xslt';
import { xsltProcess, getParser } from 'xslt-ts';

import { tagUrl } from '~util';
import { renderToStaticMarkup } from 'react-dom/server';
import { ExternalContent } from '~components';

if( module.hot ) {
	module.hot.accept( [ '../components/index.ts', '../http.ts' ] );
}

type ModuleName = 'm-w';
type Params = ModuleParamsMap[ ModuleName ];

export default async function( { moduleName, lifecycle, bus, commandFilter }: Params ) {
	if( module.hot ) {
		module.hot.addDisposeHandler( () => {
			lifecycle.shutdown();
		} );
	}

	const parser = getParser();
	const xsltDictionaryDoc = parser.parseFromString( xsltDictionaryText, 'application/xml' );

	type DefineCommandParameters = { word: 'rest'; };
	parseCommands<DefineCommandParameters>( {
		command: {
			name: '!define',
			prefix: [ 'define' ],
			parameters: {
				word: {
					type: 'rest',
					required: true
				}
			}
		},
		lifecycle,
		filter: commandFilter
	} ).pipe(
		concatMap( async ( { parameters: { word }, responseRoute } ) => {
			const url = new URL( tagUrl`https://www.dictionaryapi.com/api/v1/references/collegiate/xml/${word}` );
			url.searchParams.set( 'key', apiKeys.dictionary );

			const response = await http.get( { url, full: true } );

			const isXml = /xml/.test( response.headers[ 'content-type' ] );
			let message: string;
			if( isXml ) {
				const xmlDoc = parser.parseFromString( response.body, 'application/xml' );
				message = xsltProcess( xmlDoc, xsltDictionaryDoc );
			} else {
				message = renderToStaticMarkup( <ExternalContent name="Merriam-Webster">
					<pre>{response.body}</pre>
				</ExternalContent> );
			}
			bus.next( { type: 'response', message, route: responseRoute } );
		} )
	).subscribe();

	lifecycle.shutdown$
	.subscribe( () => {
		lifecycle.done();
	} );
}
