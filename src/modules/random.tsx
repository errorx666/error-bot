import { take, filter } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { takeUntil, concatMap } from 'rxjs/operators';
import { parseCommands } from '~command';
import { rateLimit } from 'rxjs-util';

import React, { PureComponent } from 'react';
import type { ReactNode } from 'react';
import { renderToStaticMarkup } from 'react-dom/server';

import { sleep } from '~util';

import { apiKey } from '~data/random.yaml';

import { v4 as uuid } from 'uuid';
import { ExternalContent } from '~components';

import * as http from '~http';
import { handleErrors } from '~rx';

if( module.hot ) {
	module.hot.accept( [ '../components/index.ts', '../http.ts' ] );
}

type ModuleName = 'random';
type Params = ModuleParamsMap[ ModuleName ];

interface RandomProps {
	children: ReactNode;
}

class Random extends PureComponent<RandomProps> {
	public constructor( props: RandomProps ) {
		super( props );
	}

	public render() {
		const { props } = this;
		return <ExternalContent name="random.org">
			{props.children}
		</ExternalContent>;
	}
}

interface JsonRpcRequest {
	readonly jsonrpc: '2.0';
	readonly id: string;
	readonly method: string;
	readonly params: object;
}

interface JsonRpcResponse {
	readonly jsonrpc: '2.0';
	readonly id: string;
	readonly result: object;
}

export default async function( { moduleName, lifecycle, bus, commandFilter, logger }: Params ) {
	if( module.hot ) {
		module.hot.addDisposeHandler( () => {
			lifecycle.shutdown();
		} );
	}

	const requestMessages = new Subject<JsonRpcRequest>();
	const responseMessages = new Subject<JsonRpcResponse>();

	requestMessages.pipe(
		rateLimit( 100 ),
		concatMap( async request => {
			const url = 'https://api.random.org/json-rpc/2/invoke';

			const response = await http.post( {
				url,
				json: true,
				body: request
			} );

			responseMessages.next( response );
		} ),
		handleErrors( { logger } ),
		takeUntil( lifecycle.shutdown$ )
	).subscribe();

	async function request( method: string, params: any ) {
		const id = uuid();
		const p = responseMessages.pipe(
			filter( rm => rm.id === id ),
			take( 1 ),
			takeUntil( lifecycle.shutdown$ )
		).toPromise();

		requestMessages.next( {
			jsonrpc: '2.0',
			id,
			method,
			params: {
				apiKey,
				...params
			}
		} );

		return ( await p ).result;
	}

	type RandomIntegerCommandParameters = { n: 'number'; min: 'number'; max: 'number'; replacement: 'boolean'; base: 'number'; };
	parseCommands<RandomIntegerCommandParameters>( {
		command: {
			name: '!random-integer',
			prefix: [ 'random integer' ],
			parameters: {
				n: {
					alias: [ 'count', 'num' ],
					type: 'number',
					default: 1
				},
				min: {
					type: 'number',
					position: 0,
					default: 0
				},
				max: {
					type: 'number',
					position: 1,
					default: 100
				},
				replacement: {
					alias: [ 'replace' ],
					type: 'boolean',
					default: true
				},
				base: {
					type: 'number',
					default: 10,
					query: {
						value: { $in: [ 8, 10, 16 ] }
					}
				}
			}
		},
		lifecycle,
		filter: commandFilter
	} )
	.pipe(
		concatMap( async ( {
			responseRoute,
			parameters: {
				n,
				min,
				max,
				replacement,
				base
			}
		} ) => {
			const result = await request( 'generateIntegers', {
				n,
				min,
				max,
				replacement,
				base
			} ) as any;
			const data = result.random.data as readonly number[];
			const message = renderToStaticMarkup(
				<Random>
					<ul>{data.map( ( n, i ) =>
						<li key={i}>{n}</li>
					)}</ul>
				</Random>
			);
			bus.next( { type: 'response', message, route: responseRoute } );
			await sleep( result.advisoryDelay );
		} ),
		handleErrors( { logger } )
	).subscribe();

	type RandomStringCommandParameters = { n: 'number'; length: 'number'; characters: 'string'; replacement: 'boolean'; };
	parseCommands<RandomStringCommandParameters>( {
		command: {
			name: '!random-string',
			prefix: [ 'random string' ],
			parameters: {
				n: {
					alias: [ 'count', 'num' ],
					type: 'number',
					default: 1,
					query: {
						value: {
							$gte: 1,
							$lte: 100,
							$isFinite: true,
							$isInteger: true
						}
					}
				},
				length: {
					alias: [ 'len' ],
					type: 'number',
					default: 10,
					query: {
						value: {
							$gte: 1,
							$lte: 256,
							$isFinite: true,
							$isInteger: true
						}
					}
				},
				characters: {
					alias: [ 'chars', 'charset' ],
					type: 'string',
					default: 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!#$%&*_-'
				},
				replacement: {
					alias: [ 'replace' ],
					type: 'boolean',
					default: true
				}
			}
		},
		lifecycle,
		filter: commandFilter
	} )
	.pipe(
		concatMap( async ( {
			responseRoute,
			parameters: {
				n,
				length,
				characters,
				replacement
			}
		} ) => {
			const result = await request( 'generateStrings', {
				n,
				length,
				characters,
				replacement
			} ) as any;
			const data = result.random.data as readonly string[];
			const message = renderToStaticMarkup(
				<Random>
					<ul>{data.map( ( n, i ) =>
						<li key={i}>{n}</li>
					)}</ul>
				</Random>
			);
			bus.next( { type: 'response', message, route: responseRoute } );
			await sleep( result.advisoryDelay );
		} ),
		handleErrors( { logger } )
	).subscribe();

	type RandomUuidCommandParameters = { n: 'number'; };
	parseCommands<RandomUuidCommandParameters>( {
		command: {
			name: '!random-uuid',
			prefix: [
				'random uuid',
				'random id'
			],
			parameters: {
				n: {
					alias: [ 'count', 'num' ],
					type: 'number',
					default: 1,
					query: {
						value: {
							$gte: 1,
							$lte: 100,
							$isFinite: true,
							$isInteger: true
						}
					}
				}
			}
		},
		lifecycle,
		filter: commandFilter
	} )
	.pipe(
		concatMap( async ( {
			responseRoute,
			parameters: {
				n
			}
		} ) => {
			const result = await request( 'generateUUIDs', {
				n,
			} ) as any;
			const data = result.random.data as readonly string[];
			const message = renderToStaticMarkup(
				<Random>
					<ul>{data.map( ( n, i ) =>
						<li key={i}>{n}</li>
					)}</ul>
				</Random>
			);
			bus.next( { type: 'response', message, route: responseRoute } );
			await sleep( result.advisoryDelay );
		} ),
		handleErrors( { logger } )
	).subscribe();

	lifecycle.shutdown$
	.subscribe( () => {
		lifecycle.done();
	} );
}
