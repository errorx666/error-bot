declare interface MinecraftAuthMessage {
	readonly type: 'auth';
	readonly key: string;
}

declare interface MinecraftAuthRequestMessage {
	readonly type: 'authRequest';
}

declare interface MinecraftResponseMessage {
	readonly type: 'response';
	readonly message: string;
}

declare interface MinecraftServerChatMessage {
	readonly type: 'serverChat';
	readonly username: string;
	readonly message: string;
}

declare interface MinecraftLivingDeathMessage {
	readonly type: 'livingDeath';
	readonly name: string;
	readonly damageType: string;
	readonly deathMessage: string;
	readonly trueSource: string;
	readonly immediateSource: string;
}

declare interface MinecraftAdvancementMessage {
	readonly type: 'advancement';
	readonly name: string;
	readonly advancement: string;
}

declare interface MinecraftPlayerLoggedInMessage {
	readonly type: 'playerLoggedIn';
	readonly name: string;
}

declare interface MinecraftPlayerLoggedOutMessage {
	readonly type: 'playerLoggedOut';
	readonly name: string;
}

declare interface MinecraftPingMessage {
	readonly type: 'ping';
}

declare interface MinecraftPongMessage {
	readonly type: 'pong';
}

declare interface MinecraftMessageMap {
	advancement: MinecraftAdvancementMessage;
	auth: MinecraftAuthMessage;
	authRequest: MinecraftAuthRequestMessage;
	response: MinecraftResponseMessage;
	serverChat: MinecraftServerChatMessage;
	livingDeath: MinecraftLivingDeathMessage;
	playerLoggedIn: MinecraftPlayerLoggedInMessage;
	playerLoggedOut: MinecraftPlayerLoggedOutMessage;
	ping: MinecraftPingMessage;
	pong: MinecraftPongMessage;
}

declare type MinecraftMessage = MinecraftMessageMap[ keyof MinecraftMessageMap ];
