import * as api from '~nodebb/api';
import { concatMap } from 'rxjs/operators';
import { parseCommands } from '~command';
import { apiKey } from '~data/deep-ai.yaml';
import * as http from '~http';

import React, { PureComponent } from 'react';
import { renderToStaticMarkup } from 'react-dom/server';

import { URL } from 'url';
import { ExternalContent } from '~components';
import { handleErrors } from '~rx';

if( module.hot ) {
	module.hot.accept( [ '../components/index.ts', '../http.ts' ] );
}

type ModuleName = 'deep-ai';
type Params = ModuleParamsMap[ ModuleName ];

interface DeepAiImageProps {
	title: string;
	src: string;
}

class DeepAiImage extends PureComponent<DeepAiImageProps> {
	public constructor( props: DeepAiImageProps ) {
		super( props );
	}

	public render() {
		const { props } = this;
		return (
			<ExternalContent name={props.title}>
				<img src={props.src}/>
			</ExternalContent>
		);
	}
}

export default async function( { moduleName, session, lifecycle, bus, commandFilter, logger }: Params ) {
	if( module.hot ) {
		module.hot.addDisposeHandler( () => {
			lifecycle.shutdown();
		} );
	}

	const headers = {
		'Api-Key': apiKey
	};

	async function downloadAndUploadImage( { src, cid }: { src: string; cid?: number; } ) {
		const response = await http.get( {
			url: src,
			encoding: null,
			headers,
			full: true
		} );
		const contentType = response.headers[ 'content-type' ];
		const filename = new URL( src ).pathname.split( /\//g ).pop();
		const url = await api.posts.upload( { session, filename, buffer: response.body, contentType, cid } );
		return { url, filename, contentType };
	}

	type DeepDreamCommandParameters = { src: 'url'; };
	parseCommands<DeepDreamCommandParameters>( {
		command: {
			name: '!deep-dream',
			prefix: [ 'deep dream', 'd d', 'dream' ],
			parameters: {
				src: {
					type: 'url',
					position: 0,
					required: true
				}
			}
		},
		lifecycle,
		filter: commandFilter
	} ).pipe(
		concatMap( async ( {
			responseRoute,
			parameters: { src }
		} ) => {
			const url = 'https://api.deepai.org/api/deepdream';
			const { output_url } = await http.post<{ readonly output_url: string; }>( {
				url,
				headers,
				formData: {
					image: src.href
				},
				json: true
			} );
			const { url: imageUrl } = await downloadAndUploadImage( { src: output_url } );
			const message = renderToStaticMarkup( <DeepAiImage title="Deep Dream" src={imageUrl}/> );
			bus.next( { type: 'response', message, route: responseRoute } );
		} ),
		handleErrors( { logger } )
	).subscribe();

	type Waifu2xCommandParameters = { src: 'url'; };
	parseCommands<Waifu2xCommandParameters>( {
		command: {
			name: '!waifu-2x',
			prefix: [ 'waifu 2x', 'waifu' ],
			parameters: {
				src: {
					type: 'url',
					position: 0,
					required: true
				}
			}
		},
		lifecycle,
		filter: commandFilter
	} ).pipe(
		concatMap( async ( {
			responseRoute,
			parameters: { src }
		} ) => {
			const url = 'https://api.deepai.org/api/waifu2x';
			const { output_url } = await http.post<{ readonly output_url: string; }>( {
				url,
				headers,
				formData: {
					image: src.href
				},
				json: true
			} );
			const { url: imageUrl } = await downloadAndUploadImage( { src: output_url } );
			const message = renderToStaticMarkup( <DeepAiImage title="Waifu2x" src={imageUrl}/> );
			bus.next( { type: 'response', message, route: responseRoute } );
		} ),
		handleErrors( { logger } )
	).subscribe();

	type DeepMaskCommandParameters = { src: 'url'; };
	parseCommands<DeepMaskCommandParameters>( {
		command: {
			name: '!deep-mask',
			prefix: [ 'deep mask', 'd m' ],
			parameters: {
				src: {
					type: 'url',
					position: 0,
					required: true
				}
			}
		},
		lifecycle,
		filter: commandFilter
	} ).pipe(
		concatMap( async ( {
			responseRoute,
			parameters: { src }
		} ) => {
			const url = 'https://api.deepai.org/api/deepmask';
			const { output_url } = await http.post<{ readonly output_url: string; }>( {
				url,
				headers,
				formData: {
					image: src.href
				},
				json: true
			} );
			const { url: imageUrl } = await downloadAndUploadImage( { src: output_url } );
			const message = renderToStaticMarkup( <DeepAiImage title="Deep Mask" src={imageUrl}/> );
			bus.next( { type: 'response', message, route: responseRoute } );
		} ),
		handleErrors( { logger } )
	).subscribe();

	type SuperResolutionCommandParameters = { src: 'url'; };
	parseCommands<SuperResolutionCommandParameters>( {
		command: {
			name: '!super-resolution',
			prefix: [ 'super resolution', 'zoom and enhance', 's r', 'zoom', 'enhance' ],
			parameters: {
				src: {
					type: 'url',
					position: 0,
					required: true
				}
			}
		},
		lifecycle,
		filter: commandFilter
	} ).pipe(
		concatMap( async ( {
			responseRoute,
			parameters: { src }
		} ) => {
			const url = 'https://api.deepai.org/api/torch-srgan';
			const { output_url } = await http.post<{ readonly output_url: string; }>( {
				url,
				headers,
				formData: {
					image: src.href
				},
				json: true
			} );
			const { url: imageUrl } = await downloadAndUploadImage( { src: output_url } );
			const message = renderToStaticMarkup( <DeepAiImage title="Super Resolution" src={imageUrl}/> );
			bus.next( { type: 'response', message, route: responseRoute } );
		} ),
		handleErrors( { logger } )
	).subscribe();

	type NeuralStyleCommandParameters = { content: 'url'; style: 'url'; };
	parseCommands<NeuralStyleCommandParameters>( {
		command: {
			name: '!neural-style',
			prefix: [ 'neural style', 'n s', 'style' ],
			parameters: {
				content: {
					type: 'url',
					alias: [ 'src' ],
					position: 0,
					required: true
				},
				style: {
					type: 'url',
					position: 1,
					required: true
				}
			}
		},
		lifecycle,
		filter: commandFilter
	} ).pipe(
		concatMap( async ( {
			responseRoute,
			parameters: { content, style }
		} ) => {
			const url = 'https://api.deepai.org/api/neural-style';
			const { output_url } = await http.post<{ readonly output_url: string; }>( {
				url,
				headers,
				formData: {
					content: content.href,
					style: style.href
				},
				json: true
			} );
			const { url: imageUrl } = await downloadAndUploadImage( { src: output_url } );
			const message = renderToStaticMarkup( <DeepAiImage title="Neural Style" src={imageUrl}/> );
			bus.next( { type: 'response', message, route: responseRoute } );
		} ),
		handleErrors( { logger } )
	).subscribe();

	type ColorizerCommandParameters = { src: 'url'; };
	parseCommands<ColorizerCommandParameters>( {
		command: {
			name: '!colorizer',
			prefix: [ 'colori[sz]er?' ],
			parameters: {
				src: {
					type: 'url',
					position: 0,
					required: true
				}
			}
		},
		lifecycle,
		filter: commandFilter
	} ).pipe(
		concatMap( async ( {
			responseRoute,
			parameters: { src }
		} ) => {
			const url = 'https://api.deepai.org/api/colorizer';
			const { output_url } = await http.post<{ readonly output_url: string; }>( {
				url,
				headers,
				formData: {
					image: src.href
				},
				json: true
			} );
			const { url: imageUrl } = await downloadAndUploadImage( { src: output_url } );
			const message = renderToStaticMarkup( <DeepAiImage title="Colorizer" src={imageUrl}/> );
			bus.next( { type: 'response', message, route: responseRoute } );
		} ),
		handleErrors( { logger } )
	).subscribe();

	type InPaintingCommandParameters = { src: 'url'; };
	parseCommands<InPaintingCommandParameters>( {
		command: {
			name: '!in-painting',
			prefix: [ 'in painting', 'in paint', 'paint', 'fill in' ],
			parameters: {
				src: {
					type: 'url',
					position: 0,
					required: true
				}
			}
		},
		lifecycle,
		filter: commandFilter
	} ).pipe(
		concatMap( async ( {
			responseRoute,
			parameters: { src }
		} ) => {
			const url = 'https://api.deepai.org/api/inpainting';
			const { output_url } = await http.post<{ readonly output_url: string; }>( {
				url,
				headers,
				formData: {
					image: src.href
				},
				json: true
			} );
			const { url: imageUrl } = await downloadAndUploadImage( { src: output_url } );
			const message = renderToStaticMarkup( <DeepAiImage title="Image Inpainting" src={imageUrl}/> );
			bus.next( { type: 'response', message, route: responseRoute } );
		} ),
		handleErrors( { logger } )
	).subscribe();

	type Pix2PixCommandParameters = { src: 'url'; };
	parseCommands<Pix2PixCommandParameters>( {
		command: {
			name: '!pix-to-pix',
			prefix: [ 'pix to pix', 'pix', 'p t p', 'p 2 p' ],
			parameters: {
				src: {
					type: 'url',
					position: 0,
					required: true
				}
			}
		},
		lifecycle,
		filter: commandFilter
	} ).pipe(
		concatMap( async ( {
			responseRoute,
			parameters: { src }
		} ) => {
			const url = 'https://api.deepai.org/api/pix2pix';
			const { output_url } = await http.post<{ readonly output_url: string; }>( {
				url,
				headers,
				formData: {
					image: src.href
				},
				json: true
			} );
			const { url: imageUrl } = await downloadAndUploadImage( { src: output_url } );
			const message = renderToStaticMarkup( <DeepAiImage title="Pix 2 Pix" src={imageUrl}/> );
			bus.next( { type: 'response', message, route: responseRoute } );
		} ),
		handleErrors( { logger } )
	).subscribe();

	type PlacesCommandParameters = { src: 'url'; };
	parseCommands<PlacesCommandParameters>( {
		command: {
			name: '!places',
			prefix: [ 'places?', 'scene', 'where is', "where's", 'where', 'place' ],
			parameters: {
				src: {
					type: 'url',
					position: 0,
					required: true
				}
			}
		},
		lifecycle,
		filter: commandFilter
	} ).pipe(
		concatMap( async ( {
			responseRoute,
			parameters: { src }
		} ) => {
			const url = 'https://api.deepai.org/api/places';
			const { output } = await http.post<{ readonly output: {
				readonly places: {
					readonly confidence: number;
					readonly name: string;
				}[];
			}; }>( {
				url,
				headers,
				formData: {
					image: src.href
				},
				json: true
			} );
			const message = renderToStaticMarkup( <details>
				<summary>Places said:</summary>
				<ul>
					{ output.places.map( ( { name, confidence }, i ) =>
						<li key={i}>
							<b>{name}</b> ({confidence})
						</li>
					) }
				</ul>
			</details> );
			bus.next( { type: 'response', message, route: responseRoute } );
		} ),
		handleErrors( { logger } )
	).subscribe();

	type CelebrityRecognitionCommandParameters = { src: 'url'; };
	parseCommands<CelebrityRecognitionCommandParameters>( {
		command: {
			name: '!celebrity-recognition',
			prefix: [ 'celebrity recognition', 'c r', 'who is', "who's", 'who is', 'celebrity', 'celeb', 'recog' ],
			parameters: {
				src: {
					type: 'url',
					position: 0,
					required: true
				}
			}
		},
		lifecycle,
		filter: commandFilter
	} ).pipe(
		concatMap( async ( {
			responseRoute,
			parameters: { src }
		} ) => {
			const url = 'https://api.deepai.org/api/celebrity-recognition';
			const { output } = await http.post<{
				output: {
					celebrities: {
						confidence: number;
						name: string;
						bounding_box: [ number, number, number, number ];
					}[];
				}
			}>( {
				url,
				headers,
				formData: {
					image: src.href
				},
				json: true
			} );
			const message = renderToStaticMarkup( <details>
				<summary>Celebrity Recognition said:</summary>
				<ul>
					{ output.celebrities.map( ( { name, confidence }, i ) =>
						<li key={i}>
							<b>{name}</b> ({confidence})
						</li>
					) }
				</ul>
			</details> );
			bus.next( { type: 'response', message, route: responseRoute } );
		} ),
		handleErrors( { logger } )
	).subscribe();

	type NeuralTalkCommandParameters = { src: 'url'; };
	parseCommands<NeuralTalkCommandParameters>( {
		command: {
			name: '!neural-talk',
			prefix: [ 'neural talk', 'n t' ],
			parameters: {
				src: {
					type: 'url',
					position: 0,
					required: true
				}
			}
		},
		lifecycle,
		filter: commandFilter
	} ).pipe(
		concatMap( async ( {
			responseRoute,
			parameters: { src }
		} ) => {
			const url = 'https://api.deepai.org/api/neuraltalk';
			const { output } = await http.post( {
				url,
				headers,
				formData: {
					image: src.href
				},
				json: true
			} );
			const message = renderToStaticMarkup( <p>{output}</p> );
			bus.next( { type: 'response', message, route: responseRoute } );
		} ),
		handleErrors( { logger } )
	).subscribe();

	lifecycle.shutdown$
	.subscribe( () => {
		lifecycle.done();
	} );
}
