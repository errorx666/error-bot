import * as api from '~nodebb/api';

import { parseCommands } from '~command';

import { createCanvas } from 'canvas';

import { concatMap } from 'rxjs/operators';
import React from 'react';
import { renderToStaticMarkup } from 'react-dom/server';
import { handleErrors } from '~rx';

if( module.hot ) {
	module.hot.accept( [ '../components/index.ts' ] );
}

type ModuleName = 'fractal-gen';
type Params = ModuleParamsMap[ ModuleName ];

const domain = {
	min: { x: -2, y: -1.25 },
	max: { x: .5, y: 1.25 }
};

const MAX_ITER = 255;

function round( num: number, amt: number = 100 ) {
	return Math.round( num * amt ) / amt;
}


export default async function( { moduleName, session, lifecycle, bus, commandFilter, logger }: Params ) {
	if( module.hot ) {
		module.hot.addDisposeHandler( () => {
			lifecycle.shutdown();
		} );
	}

	function getPalette() {
		const canvas = createCanvas( MAX_ITER + 1, 1 );
		const { width, height } = canvas;
		const c2d = canvas.getContext( '2d' );
		const gradient = c2d.createLinearGradient( .5, .5, width - .5, height - .5 );
		const colorStops = MAX_ITER;
		const pctStep = 1 / ( colorStops - 1 );
		const hueShift = Math.round( Math.random() * 360 );
		const hueScale = Math.random() * 1.9 + .1;
		for( let i = 0; i < colorStops; ++i ) {
			const pct = i * pctStep;
			const color = `hsl(${round( ( ( pct * 360 ) + hueShift ) * hueScale % 360 )},80%,50%)`;
			try {
				gradient.addColorStop( pct, color );
			} catch( ex ) {
				logger.error( ex, color );
			}
		}
		c2d.fillStyle = gradient;
		c2d.fillRect( 0, 0, width, height );
		const imageData = c2d.getImageData( 0, 0, width, height );
		return new Uint8ClampedArray( [ ...imageData.data ] );
	}

	function mandelbrot( ca: number, cb: number ) {
		let za = 0, zb = 0;
		let n: number;
		for( n = 0; n < MAX_ITER; ++n ) {
			const za2 = za * za - zb * zb;
			const zb2 = 2 * za * zb;
			za = za2 + ca;
			zb = zb2 + cb;
			if( ( za ** 2 + zb ** 2 ) > 4 ) break;
		}
		return n;
	}

	type MandelbrotCommandParameters = { zoom: 'number'; };
	parseCommands<MandelbrotCommandParameters>( {
		command: {
			name: '!mandelbrot',
			prefix: [ 'mandelbrot' ],
			parameters: {
				zoom: {
					type: 'number',
					default: () => 1 + Math.random() * 1,
					query: {
						value: {
							$gt: 0,
							$lte: 10,
							$isFinite: true
						}
					}
				}
			}
		},
		lifecycle,
		filter: commandFilter
	} ).pipe(
		concatMap( async ( {
			responseRoute,
			parameters: { zoom }
		} ) => {
			const width = 600, height = 600;
			const canvas = createCanvas( width, height );
			const c2d = canvas.getContext( '2d', { pixelFormat: 'A8' } );
			const imageData = c2d.createImageData( width, height );

			function putPixel( x: number, y: number, paletteIndex: number ) {
				const offset = ( y * width + x );
				imageData.data[ offset ] = paletteIndex;
			}
			const zoomFactor = 1 / zoom;

			const offsetMax = 0.75;
			const windowOffset = { x: ( Math.random() - .5 ) * offsetMax * zoom, y: ( Math.random() - .5 ) * offsetMax * zoom };

			const window = {
				min: { x: domain.min.x * zoomFactor + windowOffset.x, y: domain.min.y * zoomFactor + windowOffset.y },
				max: { x: domain.max.x * zoomFactor + windowOffset.x, y: domain.max.y * zoomFactor + windowOffset.y }
			};
			const windowSize = {
				x: window.max.x - window.min.x,
				y: window.max.y - window.min.y
			};

			for( let x = 0; x < width; ++x ) {
				for( let y = 0; y < height; ++y ) {
					const cartX = x / width;
					const cartY = 1 - ( y / height );

					const realPart = window.min.x + ( cartX * windowSize.x );
					const imaginaryPart = window.min.y + ( cartY * windowSize.y );

					const colorIndex = mandelbrot( realPart, imaginaryPart );
					putPixel( x, y, colorIndex );
				}
			}

			c2d.putImageData( imageData, 0, 0 );

			const contentType = 'image/png';
			const buffer = canvas.toBuffer( contentType, {
				compressionLevel: 9,
				palette: getPalette()
			} );

			const filename = 'mandelbrot.png';
			const url = await api.posts.upload( { session, filename, buffer, contentType } );
			const message = renderToStaticMarkup( <img src={url} alt="Mandelbrot"/> );
			bus.next( { type: 'response', message, route: responseRoute } );
		} ),
		handleErrors( { logger } )
	).subscribe();

	lifecycle.shutdown$
	.subscribe( () => {
		lifecycle.done();
	} );
}
