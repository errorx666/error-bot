import { concatMap } from 'rxjs/operators';
import { parseCommands } from '~command';

import React, { PureComponent } from 'react';
import { renderToStaticMarkup } from 'react-dom/server';

import * as http from '~http';

import { URL } from 'url';
import { ExternalContent } from '~components';
import { Spoiler } from '~components';
import { JSDOM } from 'jsdom';
import { handleErrors } from '~rx';

if( module.hot ) {
	module.hot.accept( [ '../components/index.ts', '../http.ts' ] );
}

type ModuleName = 'ohnorobot';
type Params = ModuleParamsMap[ ModuleName ];

interface QwantzProps {
	readonly url: string;
	readonly via?: string;
	readonly src: string;
	readonly hiddenMessages: readonly string[];
}

class Qwantz extends PureComponent<QwantzProps> {
	public constructor( props: QwantzProps ) {
		super( props );
	}

	public render() {
		const { props } = this;
		return (
			<ExternalContent name="Dinosaur Comics" url={props.url} via={props.via}>
				<p>
					<img src={props.src}/>
				</p>
				{ this.props.hiddenMessages.map( ( message, i ) => (
					<p key={i}>
						<small>{message}</small>
					</p>
				) ) }
			</ExternalContent>
		);
	}
}

interface SmbcProps {
	readonly url: string;
	readonly via?: string;
	readonly src: string;
	readonly title: string;
	readonly name: string;
	readonly hiddenSrc: string;
}

class Smbc extends PureComponent<SmbcProps> {
	public constructor( props: SmbcProps ) {
		super( props );
	}

	public render() {
		const { props } = this;
		return (
			<ExternalContent name={<><abbr title="Saturday Morning Breakfast Cereal">SMBC</abbr>{' '}Comics</>} url={props.url} via={props.via}>
				<h1>{props.name}</h1>
				<p>
					<img src={props.src} title={props.title}/>
					<br/>
					<abbr title={props.title}>&shy;</abbr>
				</p>
				{ props.hiddenSrc
				? <Spoiler summary="Hidden Comic">
					<img src={props.hiddenSrc}/>
				</Spoiler> : <></> }
			</ExternalContent>
		);
	}
}

async function search( comicId: number, query: string ) {
	const searchUrl = new URL( 'http://www.ohnorobot.com/index.php' );
	searchUrl.searchParams.set( 's', query );
	searchUrl.searchParams.set( 'Search', 'Search' );
	searchUrl.searchParams.set( 'comic', String( comicId ) );
	const via = searchUrl.href;
	const { window: { document: searchDocument } } = await http.get( { url: via, html: true } );
	const url = new URL( ( searchDocument.querySelector( '.searchlink' ) as HTMLAnchorElement ).href, via ).href;
	const { window: { document } } = await http.get( { url, html: true } );
	return { via, url, document };
}

export default async function( { moduleName, lifecycle, bus, commandFilter, logger }: Params ) {
	if( module.hot ) {
		module.hot.addDisposeHandler( () => {
			lifecycle.shutdown();
		} );
	}

	type OhNoRobotCommandParameters = { query: 'rest'; };

	parseCommands<OhNoRobotCommandParameters>( {
		command: {
			name: '!dinosaur-comics',
			prefix: [ 'dinosaur-comics', 'qwantz' ],
			parameters: {
				query: {
					type: 'rest',
					required: true
				}
			}
		},
		lifecycle,
		filter: commandFilter
	} ).pipe(
		concatMap( async ( {
			responseRoute,
			parameters: { query }
		} ) => {
			const comicId = 23;
			const { via, url, document } = await search( comicId, query );
			const img = document.querySelector( '.comic' ) as HTMLImageElement;
			const src = new URL( img.src, url ).href;

			const hiddenMessages = [
				img.title,
				new URL( ( document.querySelector( '.topnav a[href^="mailto:"]' ) as HTMLAnchorElement ).href ).searchParams.get( 'subject' )
			];
			const commentSpanHtml = /<!--\s*(<span class="rss-title">\s*(.*?)\s*<\/span>)\s*-->/is.exec( document.documentElement.innerHTML )?.[ 1 ];
			if( commentSpanHtml ) {
				hiddenMessages.push( new JSDOM( commentSpanHtml ).window.document.querySelector( 'span' ).textContent );
			}
			const message = renderToStaticMarkup( <Qwantz url={url} src={src} hiddenMessages={hiddenMessages} via={via}/> );
			bus.next( { type: 'response', message, route: responseRoute } );
		} ),
		handleErrors( { logger } )
	).subscribe();

	parseCommands<OhNoRobotCommandParameters>( {
		command: {
			name: '!smbc-comics',
			prefix: [ 's m b c comics', 's m b c' ],
			parameters: {
				query: {
					type: 'rest',
					required: true
				}
			}
		},
		lifecycle,
		filter: commandFilter
	} ).pipe(
		concatMap( async ( {
			responseRoute,
			parameters: { query }
		} ) => {
			const comicId = 137;
			const { via, url, document } = await search( comicId, query );
			const img = document.querySelector( '#cc-comic' ) as HTMLImageElement;
			const src = new URL( img.src, url ).href;
			let name = document.querySelector( 'title' ).textContent;
			const hyphenIndex = name.indexOf( '-' );
			if( hyphenIndex >= 0 ) name = name.slice( hyphenIndex + 1 ).trim();
			const hiddenImg = document.querySelector( '#aftercomic img' ) as HTMLImageElement;
			let hiddenSrc = '';
			if( hiddenImg ) hiddenSrc = new URL( hiddenImg.src, url ).href;
			const message = renderToStaticMarkup( <Smbc name={name} url={url} src={src} hiddenSrc={hiddenSrc} title={img.title} via={via}/> );
			bus.next( { type: 'response', message, route: responseRoute } );
		} ),
		handleErrors( { logger } )
	).subscribe();


	lifecycle.shutdown$
	.subscribe( () => {
		lifecycle.done();
	} );
}
