import React, { PureComponent } from 'react';
import { renderToStaticMarkup } from 'react-dom/server';

import { parseCommands, registeredCommands } from '~command';

if( module.hot ) {
	module.hot.accept( [ '../components/index.ts' ] );
}

type ModuleName = 'help';
type Params = ModuleParamsMap[ ModuleName ];

interface HelpProps {
	commands: readonly CommandDefinition<any, any>[];
}

class Help extends PureComponent<HelpProps> {
	public constructor( props: HelpProps ) {
		super( props );
	}

	public render() {
		const { props } = this;
		return ( <div>
			<h1>Commands:</h1>
			{props.commands
			.map( ( command, i ) =>
				<div key={i}>
					<h2>{ command.name }</h2>
				</div>
			)}
		</div> );
	}
}

const comparer = new Intl.Collator( 'en-US' );

export default async function( { moduleName, session, socket, lifecycle, bus, commandFilter }: Params ) {
	if( module.hot ) {
		module.hot.addDisposeHandler( () => {
			lifecycle.shutdown();
		} );
	}

	interface HelpCommandParameters {
		query: 'rest';
	}
	parseCommands<HelpCommandParameters>( {
		command: {
			name: '!help',
			prefix: [ 'help', 'man' ],
			parameters: {
				query: {
					type: 'rest',
					default: ''
				}
			}
		},
		lifecycle,
		filter: commandFilter
	} )
	.subscribe( ( { parameters: { query }, responseRoute } ) => {
		const commands =
			registeredCommands
			.map( c => c.options.command )
			.filter( ( { name } ) => !!name )
			.filter( c => c.name?.indexOf( query ) >= 0 )
			.sort( ( c1, c2 ) => comparer.compare( c1.name, c2.name ) );
		const message = renderToStaticMarkup( <Help commands={commands} /> );
		bus.next( { type: 'response', route: responseRoute, message } );
	} );

	lifecycle.shutdown$
	.subscribe( () => {
		lifecycle.done();
	} );
}
