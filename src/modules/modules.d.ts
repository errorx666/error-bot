/// <reference path="./crossword.d.ts"/>
/// <reference path="./hangman.d.ts"/>
/// <reference path="./magic-8-ball.d.ts"/>
/// <reference path="./minesweeper.d.ts"/>
/// <reference path="./picross.d.ts"/>
/// <reference path="./trivia.d.ts"/>
/// <reference path="./webhooks.d.ts"/>
/// <reference path="./zemu.d.ts"/>

declare interface NormalizeOptions {
	trim: boolean;
	collapse: boolean;
	stripTags: boolean;
}

declare interface BusMessageBase<TTypeName extends string> {
	readonly type: TTypeName;
}

declare interface BusMessageResponse extends BusMessageBase<'response'> {
	readonly route: ResponseRoute;
	readonly message: string;
}

declare interface BusMessageReloadModule extends BusMessageBase<'reload-modules'> {
	readonly moduleNames?: readonly ModuleName[];
}

declare interface BusMessageReloadAll extends BusMessageBase<'reload-all'> {}

declare interface BusMessageMinecraftChatReceive extends BusMessageBase<'minecraft-chat-receive'> {
	readonly timestamp: number;
	readonly username: string;
	readonly message: string;
}

declare interface BusMessageMinecraftChatSend extends BusMessageBase<'minecraft-chat-send'> {
	readonly message: string;
}

declare type BusMessage = BusMessageResponse|BusMessageReloadModule|BusMessageReloadAll|BusMessageMinecraftChatReceive|BusMessageMinecraftChatSend;
declare type BusMessageType = BusMessage['type'];

declare interface ModuleParams<TModuleName extends ModuleName = ModuleName, TModuleState extends object = any> {
	readonly moduleName: TModuleName;
	readonly db: import( 'mongodb' ).Db;
	readonly state$: BehaviorSubject<TModuleState|null>;
	readonly session: NodeBBSession;
	readonly socket: NodeBBSocket;
	readonly bus: Observer<BusMessage> & Observable<BusMessage>;
	readonly lifecycle: import( '~lifecycle' ).Lifecycle;
	readonly logger: import( 'log4js' ).Logger;
	readonly commandFilter?: Query<RawCommand>; // deprecated?
}

declare interface ModuleParamsMap {
	'async-queue': ModuleParams<'async-queue'> & {
		readonly delay: number;
		readonly retries: number;
		readonly queue: [ () => Eventually, number ][];
	};
	'amazingsuperpowers': ModuleParams<'amazingsuperpowers'>;
	'awkward-zombie': ModuleParams<'awkward-zombie'>;
	'bash': ModuleParams<'bash'>;
	'casino': ModuleParams<'casino'>;
	'cli-proxy': ModuleParams<'cli-proxy'> & {
		readonly url: string;
		readonly tid: number;
	};
	'covid': ModuleParams<'covid'>;
	'crossword': ModuleParams<'crossword', CrosswordState> & {
		readonly tid: number;
	};
	'command-parser': ModuleParams<'command-parser'> & {};
	'deep-ai': ModuleParams<'deep-ai'>;
	'dilbert': ModuleParams<'dilbert'>;
	'explosm': ModuleParams<'explosm'>;
	'fractal-gen': ModuleParams<'fractal-gen'>;
	'giphy': ModuleParams<'giphy'>;
	'hangman': ModuleParams<'hangman', HangmanState> & {
		readonly tid: number;
	};
	'health': ModuleParams<'health'>;
	'help': ModuleParams<'help'>;
	'httpcat': ModuleParams<'httpcat'>;
	'jargon': ModuleParams<'jargon'>;
	'query': ModuleParams<'query'>;
	'quotes': ModuleParams<'quotes'>;
	'magic-8-ball': ModuleParams<'magic-8-ball'>;
	'minecraft': ModuleParams<'minecraft'> & {
		readonly tid: number;
	};
	'minesweeper': ModuleParams<'minesweeper', MinesweeperState> & {
		readonly tid: number;
	};
	'm-w': ModuleParams<'m-w'>;
	'ohnorobot': ModuleParams<'ohnorobot'>;
	'pbf': ModuleParams<'pbf'>;
	'penny-arcade': ModuleParams<'penny-arcade'>;
	'picross': ModuleParams<'picross', PicrossState> & {
		readonly tid: number;
	};
	'playground': ModuleParams<'playground'> & {
		readonly tid: number;
	};
	'random': ModuleParams<'random'>;
	'response-queue': ModuleParams<'response-queue'>;
	'scryfall': ModuleParams<'scryfall'>;
	'secret': ModuleParams<'secret'> & {
		readonly tid: number;
	};
	'sudoku': ModuleParams<'sudoku'> & {
		readonly tid: number;
	};
	'trivia': ModuleParams<'trivia', TriviaState> & {
		readonly tid: number;
	};
	'tv-tropes': ModuleParams<'tv-tropes'>;
	'uptime': ModuleParams<'uptime'>;
	'wolfram-alpha': ModuleParams<'wolfram-alpha'>;
	'wtfery': ModuleParams<'wtfery'> & {
		readonly tid: number;
	};
	'xkcd': ModuleParams<'xkcd'>;
	'webhooks': ModuleParams<'webhooks', WebhooksState> & {
		readonly port: number;
	};
	'www': ModuleParams<'www'> & {
		readonly port: number;
	};
	'zemu': ModuleParams<'zemu', ZemuState> & {
		readonly tid: number;
		readonly zfile: string;
	};
}

declare type ModuleName = keyof ModuleParamsMap;

declare type ModuleFactory<T extends ModuleParams = ModuleParams> = Func<T, Eventually<void>>;
