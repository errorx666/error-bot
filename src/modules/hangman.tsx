/// <reference path="./hangman.d.ts"/>

import { takeUntil, filter, delay, groupBy, concatMap, mergeMap, map, debounceTime } from 'rxjs/operators';
import { Subject, merge } from 'rxjs';

import sowpods from 'sowpods';
import { Fsm } from '~fsm';
import React from 'react';
import type { ReactNode } from 'react';
import { renderToStaticMarkup } from 'react-dom/server';
import { bufferDebounceTime, rateLimit } from 'rxjs-util';
import { parseCommands, query } from '~command';
import { Hangman } from '~components';
import { hasher as Hasher } from 'node-object-hash';

import { v4 as uuid } from 'uuid';

if( module.hot ) {
	module.hot.accept( [ '../components/index.ts' ] );
}

type ModuleName = 'hangman';
type Params = ModuleParamsMap[ ModuleName ];

type GameStates = 'new-game'|'gameplay'|'game-over';
type GameTransitions = 'ready'|'load'|'win'|'lose'|'restart';

const minWordLength = 8;
const maxWordLength = 32;

const words = [ ...sowpods ].filter( w => w.length >= minWordLength && w.length <= maxWordLength );

interface GameUpdate {
	readonly gameContext: HangmanGameContext;
	readonly children: ReactNode;
}

const comparer = new Intl.Collator( 'en-US', {
	ignorePunctuation: true,
	sensitivity: 'base',
	usage: 'search'
} );

const validChar = /^[a-z]$/i;
const invalidChars = /[^a-z]+/gi;
function strEqual( str1: string, str2: string ) {
	return comparer.compare( str1.replace( invalidChars, '' ), str2.replace( invalidChars, '' ) ) === 0;
}

const now = () => +( new Date );
const hasher = Hasher();

export default async function( { moduleName, session, lifecycle, bus, tid, commandFilter, state$ }: Params ) {
	if( module.hot ) {
		module.hot.addDisposeHandler( () => {
			lifecycle.shutdown();
		} );
	}

	const isActiveLockout = ( lockout: HangmanLockout ) => ( !!lockout ) && ( lockout.turns !== 0 ) && ( lockout.expires > now() );

	const response$ = new Subject<HangmanResponse>();
	function response( element: JSX.Element, route: ResponseRoute ) {
		response$.next( { element, route } );
	}

	response$.pipe(
		groupBy( ( { route } ) => hasher.hash( route ) ),
		mergeMap( response => response.pipe(
			debounceTime( 1000 )
		) ),
		map( ( { element, ...props } ) => ( { ...props, message: renderToStaticMarkup( element ) } ) ),
		takeUntil( lifecycle.shutdown$ )
	)
	.subscribe( ( { message, route } ) => {
		bus.next( { type: 'response', message, route } );
	} );

	const gameUpdate$ = new Subject<GameUpdate>();
	async function updateGameContext( ctx: Partial<HangmanGameContext>, children = [] as ReactNode ) {
		const gameState = state$.value && state$.value || {} as Partial<HangmanState>;
		const gameContext = { ...( gameState.gameContext || {} ), ...ctx } as HangmanGameContext;
		state$.next( { ...gameState, gameContext } );
		gameUpdate$.next( { gameContext, children } );
	}

	gameUpdate$
	.pipe(
		groupBy( ( { gameContext: { roundId } } ) => roundId ),
		mergeMap( gameUpdate => gameUpdate.pipe(
			bufferDebounceTime( 300 ),
			concatMap( async gameUpdates => {
				const { gameContext } = gameUpdates[ gameUpdates.length - 1 ];
				const { word, guessed, lockouts } = gameContext;
				const children = gameUpdates.map( ( { children }, i ) => <div key={i}>{children}</div> );
				return <Hangman word={word} guessed={guessed} lockouts={lockouts.filter( isActiveLockout )}>{children}</Hangman>;
			} )
		) ),
		takeUntil( lifecycle.shutdown$ )
	)
	.subscribe( element => {
		response( element, { type: 'thread', tid } );
	} );

	function isLockedOut( issuer: CommandIssuer ) {
		if( !issuer ) return false;
		if( issuer.roles.includes( 'admin' ) ) return false;
		if( issuer.type !== 'user' ) return false;
		const { uid } = issuer;
		const gameContext = state$.value.gameContext;
		if( !gameContext ) return false;
		return gameContext.lockouts
		.filter( isActiveLockout )
		.some( lockout => lockout.uid === uid );
	}

	function generateLockout( issuer: CommandIssuer, lockout: Omit<HangmanLockout, keyof CommandIssuerUser> ) {
		if( issuer?.type !== 'user' ) return null;
		if( issuer.roles.includes( 'admin' ) ) return null;
		const { uid, username, userslug } = issuer;
		return { ...lockout, uid, username, userslug };
	}

	const gameStateFsm = new Fsm<GameStates, GameTransitions>( {
		states: {
			'new-game': {
				transitions: {
					ready: 'gameplay'
				}
			},
			gameplay: {
				transitions: {
					lose: 'game-over',
					win: 'game-over'
				}
			},
			'game-over': {
				transitions: {}
			}
		},
		transitions: {
			restart: 'new-game',
			load: 'gameplay'
		}
	} );

	gameStateFsm.states
	.pipe(
		filter( ( { enter } ) => enter === 'new-game' ),
		takeUntil( lifecycle.shutdown$ )
	).subscribe( () => {
		let word: string;
		if( state$.value && state$.value.nextWord ) {
			word = state$.value.nextWord;
			state$.next( { ...state$.value, nextWord: null } );
		} else {
			word = words[ Math.floor( Math.random() * words.length ) ];
		}
		word = word.toUpperCase();
		const roundId = uuid();
		updateGameContext( { roundId, word, guessed: [], lockouts: [] }, <p>New game.</p> );
		gameStateFsm.transition( 'ready' );
	} );

	gameStateFsm.transitions
	.pipe(
		filter( t => t === 'load' ),
		takeUntil( lifecycle.shutdown$ )
	).subscribe( () => {
		const gameContext = state$.value?.gameContext;
		if( !gameContext ) return;
		const { word, guessed } = gameContext;
		if( word.split( '' ).every( c => !validChar.test( c ) || guessed.includes( c ) ) ) {
			gameStateFsm.transition( 'restart' );
		}
	} );

	gameStateFsm.states
	.pipe(
		filter( ( { enter } ) => enter === 'game-over' ),
		delay( 6000 ),
		filter( () => gameStateFsm.state === 'game-over' ),
		takeUntil( lifecycle.shutdown$ )
	).subscribe( () => {
		gameStateFsm.transition( 'restart' );
	} );

	const letterGuessTimeout = 1000 * 60 * 60;
	const wordGuessTimeout = 1000 * 60 * 60 * 12;
	const slowPoke = <img src="/assets/uploads/files/1567795880992-7538621b-e3c4-43a0-959f-2860eb885886-image.png"/>;
	const hanzo = <p>:hanzo:</p>;

	function hangmanChatCommand
	<TParameterTypeMap extends { [ TParameterName in TParameterNames ]: CommandParameterType; },
	TParameterNames extends keyof TParameterTypeMap & string = keyof TParameterTypeMap & string>
	( cd: CommandDefinition<TParameterTypeMap, TParameterNames> & { readonly prefix: readonly string[]; } ) {
		return merge(
			parseCommands<TParameterTypeMap, TParameterNames>( {
				command: {
					...cd,
					query: {
						$and: [
							query.inThread( tid ),
							cd.query || { $value: true }
						]
					},
					prefix: [ ...cd.prefix, ...cd.prefix.map( p => `hangman ${p}` ) ]
				},
				lifecycle,
				filter: commandFilter
			} ),
			parseCommands<TParameterTypeMap, TParameterNames>( {
				command: {
					...cd,
					query: {
						$and: [
							query.inChat(),
							cd.query || { $value: true }
						]
					},
					prefix: [ ...cd.prefix.map( p => `hangman ${p}` ) ]
				},
				lifecycle,
				filter: commandFilter
			} )
		);
	}

	hangmanChatCommand( {
		query: {
			$and: [
				query.fromUser(),
				{ $not: query.fromRole( 'admin' ) }
			]
		},
		prefix: [ 'cheat', 'unguess', 'next word', 'lock out', 'lock', 'un lock', 'next word', 'next' ],
		parameters: 'ignore'
	} )
	.subscribe( ( { issuer } ) => {
		const gameContext = state$.value?.gameContext;
		if( !gameContext ) return;
		const lockout = generateLockout( issuer, { expires: Infinity } );
		if( !lockout ) return;
		const { uid, userslug } = lockout;
		updateGameContext( {
			lockouts:
				gameContext.lockouts
				.filter( l => l.uid !== uid )
				.concat( lockout )
				.filter( isActiveLockout )
		}, <p>
			@{userslug} tried to <b>cheat</b>.
		</p> );
	} );

	interface UnguessCommandParameters {
		readonly guess: 'rest';
	}
	hangmanChatCommand<UnguessCommandParameters>( {
		prefix: [ 'unguess' ],
		parameters: {
			guess: {
				type: 'rest',
				required: true
			}
		}
	} )
	.subscribe( ( { parameters: { guess } } ) => {
		const gameContext = state$.value?.gameContext;
		if( !gameContext ) return;
		if( gameStateFsm.state !== 'gameplay' ) return;
		const index = gameContext.guessed.findIndex( g => strEqual( g, guess ) );
		if( index < 0 ) return;
		const guessed = [ ...gameContext.guessed ];
		guessed.splice( index, 1 );
		updateGameContext( { guessed } );
	} );

	interface LockOutCommandParameters {
		readonly user: 'user';
	}
	hangmanChatCommand<LockOutCommandParameters>( {
		prefix: [ 'lock out', 'lock' ],
		parameters: {
			user: {
				type: 'user',
				position: 0,
				required: true
			}
		}
	} )
	.subscribe( async ( { parameters: { user } } ) => {
		const gameContext = state$.value?.gameContext;
		if( !gameContext ) return;
		if( gameStateFsm.state !== 'gameplay' ) return;

		const lockouts =
			gameContext.lockouts
			.filter( l => l.uid !== user.uid )
			.concat( generateLockout( { type: 'user', ...user } as CommandIssuerUser, { expires: Infinity } ) )
			.filter( isActiveLockout );
		updateGameContext( { lockouts } );
	} );

	hangmanChatCommand( {
		prefix: [ 'un lock all', 'un lock everyone' ],
		parameters: 'ignore'
	} )
	.subscribe( () => {
		const gameContext = state$.value?.gameContext;
		if( !gameContext ) return;
		if( gameStateFsm.state !== 'gameplay' ) return;
		if( gameContext.lockouts.length === 0 ) return;
		updateGameContext( { lockouts: [] } );
	} );

	interface UnlockCommandParameters {
		readonly user: 'user';
	}
	hangmanChatCommand<UnlockCommandParameters>( {
		prefix: [ 'un lock' ],
		parameters: {
			user: {
				type: 'user',
				position: 0,
				required: true
			}
		}
	} )
	.subscribe( ( { parameters: { user: { uid } } } ) => {
		const gameContext = state$.value?.gameContext;
		if( !gameContext ) return;
		if( gameStateFsm.state !== 'gameplay' ) return;
		if( !gameContext.lockouts.some( l => l.uid === uid ) ) return;
		const lockouts = gameContext.lockouts.filter( l => l.uid !== uid ).filter( isActiveLockout );
		updateGameContext( { lockouts } );
	} );

	interface GuessLetterCommandParameters {
		readonly letter: 'rest';
	}
	parseCommands<GuessLetterCommandParameters>( {
		command: {
			query: {
				$and: [
					query.inThread( tid ),
					query.rawMatches( /^[a-z](?:[- ]+[a-z])*$/i )
				]
			},
			parameters: {
				letter: {
					type: 'rest',
					required: true
				}
			}
		},
		lifecycle,
		filter: commandFilter,
		priority: 2
	} ).pipe(
		mergeMap( ( { parameters: { letter, ...p }, ...c } ) =>
			Array.from( new Set(
				letter.toUpperCase()
				.split( /[ -]+/g )
				.filter( l => l.length === 1 && validChar.test( l ) )
			) )
			.map( l => ( { ...c, parameters: { ...p, letter: l } } ) )
		),
		rateLimit( 10 ),
		filter( ( { issuer } ) => !isLockedOut( issuer ) ),
		filter( ( { parameters: { letter } } ) => ( letter.length === 1 ) && validChar.test( letter ) )
	).subscribe( ( { parameters: { letter }, timestamp, issuer, responseRoute } ) => {
		const gameContext = state$.value?.gameContext;
		if( !gameContext ) return;
		if( gameStateFsm.state !== 'gameplay' ) {
			if( timestamp > gameContext.lastGuess ) {
				response( slowPoke, responseRoute );
			}
			return;
		}
		if( gameContext.guessed.some( guess => strEqual( guess, letter ) ) ) {
			if( timestamp > gameContext.lastGuess ) {
				response( hanzo, responseRoute );
			}
			return;
		}
		const isWrong = gameContext.word.indexOf( letter ) < 0;
		const { word } = gameContext;
		const guessed = [ ...gameContext.guessed, letter ];
		const wrongLetters = guessed.filter( c => !validChar.test( c ) || word.indexOf( c ) < 0 );
		let after = <></> as ReactNode;
		let transition = null as GameTransitions;

		let lockout = isWrong ? generateLockout( issuer, { expires: now() + letterGuessTimeout, turns: 2 } ) : null;
		let lockouts = gameContext.lockouts;
		if( word.split( '' ).every( c => !validChar.test( c ) || guessed.includes( c ) ) ) {
			after = <>You win!</>;
			transition = 'win';
			lockout = null;
			lockouts = [];
		} else if( wrongLetters.length >= 6 ) {
			after = <>You lose!</>;
			transition = 'lose';
			lockout = null;
			lockouts = [];
		} else if( isActiveLockout( lockout ) ) {
			if( Math.floor( Math.random() * 20 ) === 0 ) {
				lockout = null;
				after = <>:slot_machine: <b>Lucky!</b> You get an extra guess!</>;
			} else {
				after = <>Try again later.</>;
			}
		}
		updateGameContext( {
			guessed,
			lastGuess: timestamp,
			lockouts: lockouts.map( lockout => ( {
				...lockout,
				...( lockout.turns > 0 ? { turns: lockout.turns - 1 } : {} )
			} ) )
			.concat( lockout )
			.filter( isActiveLockout )
		},
			<div>
				{ isWrong ? <>There is no <b>{letter}</b>{ ( letter === 'I' ) ? ', dear' : <></> }.{' '}</> : <></> }
				{ after }
			</div>
		);
		if( transition ) gameStateFsm.transition( transition );
	} );

	interface HangmanNewGameCommandParameters {}
	hangmanChatCommand<HangmanNewGameCommandParameters>( {
		prefix: [ 'new game', 'restart' ],
	} )
	.subscribe( () => {
		gameStateFsm.transition( 'restart' );
	} );

	hangmanChatCommand( {
		prefix: [ 'cheat' ]
	} )
	.subscribe( ( { responseRoute } ) => {
		const gameContext = state$.value?.gameContext;
		if( !gameContext ) return;

		bus.next( { type: 'response', message: `Word: **${gameContext.word}**`, route: responseRoute } );
	} );

	hangmanChatCommand( {
		prefix: [ 'print' ],
		parameters: 'ignore'
	} )
	.subscribe( () => {
		updateGameContext( {} );
	} );

	interface HangmanSetNextWordCommandParameters {
		readonly clear: 'boolean';
		readonly word: 'rest';
	}
	hangmanChatCommand<HangmanSetNextWordCommandParameters>( {
		prefix: [ 'next word', 'next' ],
		parameters: {
			clear: {
				alias: [ 'unset' ],
				type: 'boolean',
				default: false,
				conflicts: [ 'word' ]
			},
			word: {
				type: 'rest',
				conflicts: [ 'clear' ]
			}
		}
	} )
	.subscribe( ( { parameters: { clear, word }, responseRoute } ) => {
		const gameState = state$.value;
		if( !gameState ) return;

		if( clear ) {
			const nextWord = null;
			state$.next( { ...gameState, nextWord } );
			bus.next( { type: 'response', message: `Next word cleared.`, route: responseRoute } );
		} else {
			if( !word ) return;
			const nextWord = word.toUpperCase();
			state$.next( { ...gameState, nextWord } );
			bus.next( { type: 'response', message: `Next word: **${nextWord}**`, route: responseRoute } );
		}
	} );

	interface GuessWordCommandParameters {
		readonly word: 'rest';
	}
	parseCommands<GuessWordCommandParameters>( {
		command: {
			query: {
				$and: [
					query.inThread( tid ),
					query.rawMatches( /^(["']).{6,30}\1$/i )
				]
			},
			parameters: {
				word: {
					type: 'rest'
				}
			}
		},
		lifecycle,
		processParameters: s =>
			s
			.replace( /[^-_a-z ]/gi, '' )
			.toUpperCase(),
		priority: 3
	} ).pipe(
		filter( ( { issuer } ) => !isLockedOut( issuer ) )
	).subscribe( ( { parameters: { word }, timestamp, issuer, responseRoute } ) => {
		const gameContext = state$.value?.gameContext;
		if( !gameContext ) return;
		if( gameStateFsm.state !== 'gameplay' ) {
			if( timestamp > gameContext.lastGuess ) {
				response( slowPoke, responseRoute );
			}
			return;
		}
		if( gameContext.guessed.some( guess => strEqual( guess, word ) ) ) {
			if( timestamp > gameContext.lastGuess ) {
				response( hanzo, responseRoute );
			}
			return;
		}
		if( strEqual( word, gameContext.word ) ) {
			let guessed = [ ...gameContext.guessed ];
			for( const c of word.split( '' ) ) {
				if( validChar.test( c ) && !guessed.includes( c ) ) {
					guessed = [ ...guessed, c ];
				}
			}
			updateGameContext( { guessed, lastGuess: timestamp, lockouts: [] }, <p>You win!</p> );
			gameStateFsm.transition( 'win' );
		} else {
			const guessed = [ ...gameContext.guessed, word ];
			const wrongCount = guessed.filter( c => word.indexOf( c ) < 0 ).length;
			const youLose = wrongCount >= 6;
			const lockout = generateLockout( issuer, { expires: now() + wordGuessTimeout } );
			let lockouts = gameContext.lockouts.map( lockout => ( {
				...lockout,
				...( lockout.turns > 0 ? { turns: lockout.turns - 1 } : {} )
			} ) )
			.concat( lockout )
			.filter( isActiveLockout );

			if( youLose ) lockouts = [];

			updateGameContext( {
				guessed,
				lastGuess: timestamp,
				lockouts
			}, <p>
				The word is not <b>{word}</b>.{ ' ' }
				{ youLose ? <>You lose.</> : isActiveLockout( lockout ) ? <>Better luck next time.</> : <></> }
			</p> );

			if( youLose ) {
				gameStateFsm.transition( 'lose' );
			}
		}
	} );

	if( state$.value ) {
		const gameContext = state$.value.gameContext;
		if( gameContext ) {
			gameStateFsm.transition( 'load' );
		}
	}

	if( gameStateFsm.state === null ) {
		gameStateFsm.transition( 'restart' );
	}

	lifecycle.shutdown$
	.subscribe( () => {
		gameUpdate$.subscribe();
		response$.complete();
		gameStateFsm.complete();
		lifecycle.done();
	} );
}
