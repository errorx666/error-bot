import { concatMap } from 'rxjs/operators';
import React from 'react';
import { renderToStaticMarkup } from 'react-dom/server';
import { parseCommands } from '~command';
import { tagUrl, dump } from '~util';
import { handleErrors } from '~rx';
import { get } from '~http';
import { apiHost, apiKey } from '~data/covid.yaml';
import { AsyncCache, AsyncValueCache } from '~async-cache';
import { TimeSpan } from '~time-span';

if( module.hot ) {
	module.hot.accept( [ '../components/index.ts', '../http.ts' ] );
}

type ModuleName = 'covid';
type Params = ModuleParamsMap[ ModuleName ];

export default async function( { moduleName, lifecycle, bus, logger }: Params ) {
	if( module.hot ) {
		module.hot.addDisposeHandler( () => {
			lifecycle.shutdown();
		} );
	}

	const formatter = new Intl.NumberFormat( 'en-US', { useGrouping: true } );

	function compare( str1: string, str2: string ) {
		const spacePattern = /(?:[-_ ]|of)/gi;
		const ignorePattern = /(?:['"`]|the)/gi;

		function normalize( str: string ) {
			return String( str ?? '' ).trim().toLowerCase().replace( spacePattern, ' ' ).replace( ignorePattern, '' ).trim();
		}

		return normalize( str1 ) === normalize( str2 );
	}

	const countryCache = new AsyncValueCache<readonly CountryInfo[]>( () =>
		get( {
			url: tagUrl`https://${apiHost}/help/countries`,
			headers: {
				'x-rapidapi-host': apiHost,
				'x-rapidapi-key': apiKey
			},
			json: true
		} ), {
			maxAge: TimeSpan.fromDays( 1 )
		}
	);
	interface CountryInfo {
		readonly name: string;
		readonly alpha2code: string;
		readonly alpha3code: string;
		readonly latitude: number;
		readonly longitude: number;
	}
	function getListOfCountries() {
		return countryCache.get();
	}

	const latestInfoCache = new AsyncValueCache<[ LatestInfo ]>( () =>
		get( {
			url: tagUrl`https://${apiHost}/totals`,
			headers: {
				'x-rapidapi-host': apiHost,
				'x-rapidapi-key': apiKey
			},
			json: true
		} ), {
			maxAge: TimeSpan.fromHours( 1 )
		}
	);
	interface LatestInfo {
		readonly confirmed: string;
		readonly recovered: string;
		readonly critical: string;
		readonly deaths: string;
	}
	function getLatestTotals() { // eslint-disable-line @typescript-eslint/no-unused-vars
		return latestInfoCache.get();
	}

	const countryCovidCache = new AsyncCache<string, [CountryCovidInfo]>( code =>
		get( {
			url: tagUrl`https://${apiHost}/country/code`,
			headers: {
				'x-rapidapi-host': apiHost,
				'x-rapidapi-key': apiKey
			},
			searchParams: {
				code
			},
			json: true
		} ), {
			maxAge: TimeSpan.fromHours( 1 )
		} );
	interface CountryCovidInfo {
		readonly country: string;
		readonly confirmed: number;
		readonly recovered: number;
		readonly critical: number;
		readonly deaths: number;
		readonly latitude: number;
		readonly longitude: number;
	}
	function getCountryCovidInfo( code: string ) {
		return countryCovidCache.get( code );
	}

	interface CovidCommandParameters {
		countries: 'rest';
	}
	parseCommands<CovidCommandParameters>( {
		command: {
			name: '!covid-19',
			prefix: [ 'covid 19', 'covid', 'coronavirus', 'corona' ],
			parameters: {
				countries: {
					type: 'rest',
					default: 'us es it fr de gb cn'
				}
			}
		},
		lifecycle
	} )
	.pipe(
		concatMap( async ( { parameters: { countries }, responseRoute } ) => {
			const allCountries = await getListOfCountries();
			let unknownCountries = [] as readonly string[];
			const countryCodes =
				new Set( countries.split( /\s+/g )
				.map( name => {
					name = name.trim();
					const country = allCountries.find( country =>
						[
							country.alpha3code,
							country.alpha2code,
							country.name
						].some( c => compare( name, c ) )
					);
					if( country == null ) {
						unknownCountries = [ ...unknownCountries, name ];
						return null;
					}
					return country.alpha2code;
				} )
				.filter( s => !!s ) );
			if( unknownCountries.length > 0 ) {
				const message = renderToStaticMarkup( <p>Unknown countries:
					<ul>
						{unknownCountries.map( ( name, i ) => <li key={i}>{name}</li> )}
					</ul>
				</p> );
				bus.next( {	type: 'response', message, route: responseRoute } );
				return;
			}
			const info =
			 	( await Promise.all(
					[ ...countryCodes.values() ]
					.map( countryCode => getCountryCovidInfo( countryCode ) )
				) ).flat();

			dump( logger, info );

			type Field = 'confirmed'|'recovered'|'critical'|'deaths';
			const subtotals = {
				confirmed: info.reduce( ( prev, curr ) => prev + curr.confirmed, 0 ),
				critical: info.reduce( ( prev, curr ) => prev + curr.critical, 0 ),
				recovered: info.reduce( ( prev, curr ) => prev + curr.recovered, 0 ),
				deaths: info.reduce( ( prev, curr ) => prev + curr.deaths, 0 )
			} as Pick<CountryCovidInfo, Field>;

			function getDecoration( country: string, field: Field, value: number ) {
				const formattedValue = formatter.format( value );
				const spaces = ' '.repeat( 11 - formattedValue.length );
				let node = <>{formattedValue}</>;

				if( compare( field, 'recovered' ) ) node = <ins>{node}</ins>;
				else if( compare( field, 'critical' ) ) node = <ins><del>{node}</del></ins>;
				else if( compare( field, 'deaths' ) ) node = <del>{node}</del>;
				node = <pre>{spaces}{node}</pre>;
				if( compare( country, 'china' ) ) {
					if( compare( field, 'confirmed' ) ) node = <>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:airquotes:{node}</>;
					else node = <><br/>{node}</>;
				} else if( compare( country, 'north korea' ) ) {
					if( compare( field, 'confirmed' ) ) node = <>:collision:</>;
					else if( compare( field, 'deaths' ) ) node = <>:gun:</>;
					else node = <>:skull_and_crossbones:</>;
				}
				return node;
			}

			const message = renderToStaticMarkup( <table>
				<caption>COVID-19 Statistics</caption>
				<thead>
					<th scope="col">
						Country
					</th>
					<th scope="col">
						Confirmed
					</th>
					<th scope="col">
						Recovered
					</th>
					<th scope="col">
						Critical
					</th>
					<th scope="col">
						Deaths
					</th>
				</thead>
				<tbody>
					{info.map( ( { country, confirmed, recovered, critical, deaths } ) =>
						<tr key={country}>
							<td>{country}</td>
							<td>{getDecoration( country, 'confirmed', confirmed )}</td>
							<td>{getDecoration( country, 'recovered', recovered )}</td>
							<td>{getDecoration( country, 'critical', critical )}</td>
							<td>{getDecoration( country, 'deaths', deaths )}</td>
						</tr>
					)}
					{ info.length > 1 ?
						<tr>
							<th scope="row">Subtotal</th>
							<td>{getDecoration( 'subtotal', 'confirmed', subtotals.confirmed )}</td>
							<td>{getDecoration( 'subtotal', 'recovered', subtotals.recovered )}</td>
							<td>{getDecoration( 'subtotal', 'critical', subtotals.critical )}</td>
							<td>{getDecoration( 'subtotal', 'deaths', subtotals.deaths )}</td>
						</tr>
					: <></> }
				</tbody>
			</table> );
			bus.next( {	type: 'response', message, route: responseRoute } );
		} ),
		handleErrors( { logger } )
	)
	.subscribe();

	if( module.hot ) {
		module.hot.addDisposeHandler( data => {
			data.countryCache = countryCache.save();
			countryCache.clear();
			data.latestInfoCache = latestInfoCache.save();
			latestInfoCache.clear();
			data.countryCovidCache = countryCovidCache.save();
			countryCovidCache.clear();
		} );
		if( module.hot.data?.countryCache ) {
			countryCache.load( module.hot.data.countryCache );
		}
		if( module.hot.data?.latestInfoCache ) {
			latestInfoCache.load( module.hot.data.latestInfoCache );
		}
		if( module.hot.data?.countryCovidCache ) {
			countryCovidCache.load( module.hot.data.countryCovidCache );
		}
	}

	lifecycle.shutdown$
	.subscribe( () => {
		lifecycle.done();
	} );
}
