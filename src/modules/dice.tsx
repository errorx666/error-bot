import React, { ReactChild } from 'react';
import { parseCommands } from '~command';
import { handleErrors } from '~rx';
import { firstValueFrom, from, Subject } from 'rxjs';
import { concatMap, groupBy, mergeMap, takeUntil, toArray } from 'rxjs/operators';
import { renderToStaticMarkup } from 'react-dom/server';
import { Spoiler } from '~components';
import { hasher as Hasher } from 'node-object-hash';

import { bufferDebounceTime } from 'rxjs-util';
import assert from 'assert';
import { Map, Set } from 'immutable';
import { dump, Lazy, lazy } from '~util';
import _ from 'lodash';
import * as math from 'mathjs';

type Params = ModuleParams;

if( module.hot ) {
	module.hot.accept( [ '../components/index.ts' ] );
}

type Direction = 'ltr'|'rtl';

interface Program {
	readonly type: 'program';
	readonly statements: readonly Statement[];
}

interface FunctionDeclarationStatement {
	readonly type: 'functionDeclarationStatement';
	readonly name: string;
	readonly arguments: readonly string[];
	readonly expression: Expression;
}

interface VariableDeclarationStatement {
	readonly type: 'variableDeclarationStatement';
	readonly kind: 'let';
	readonly name: string;
	readonly expression: Expression;
}

interface ExpressionStatement {
	readonly type: 'expressionStatement';
	readonly expression: Expression;
	readonly label: string;
}

type Statement=FunctionDeclarationStatement|VariableDeclarationStatement|ExpressionStatement;

interface BooleanLiteral {
	readonly type: 'booleanLiteral';
	readonly value: boolean;
}

interface NumericLiteral {
	readonly type: 'numericLiteral';
	readonly value: number;
	readonly implicit: boolean;
}

type Literal = BooleanLiteral|NumericLiteral;

interface EqualityOperator {
	readonly type: 'equalityOperator';
	readonly symbol: '=='|'eq';
}

interface InequalityOperator {
	readonly type: 'inequalityOperator';
	readonly symbol: '!='|'<>'|'≠'|'ne';
}

interface GreaterThanOperator {
	readonly type: 'greaterThanOperator';
	readonly symbol: '>'|'gt';
}

interface GreaterThanEqualOperator {
	readonly type: 'greaterThanEqualOperator';
	readonly symbol: '>='|'≥'|'ge';
}

interface LessThanOperator {
	readonly type: 'lessThanOperator';
	readonly symbol: '<'|'lt';
}

interface LessThanEqualOperator {
	readonly type: 'lessThanEqualOperator';
	readonly symbol: '<='|'≤'|'le';
}

type ComparisonOperator=EqualityOperator|InequalityOperator|GreaterThanOperator|GreaterThanEqualOperator|LessThanOperator|LessThanEqualOperator;

interface AdditionOperator {
	readonly type: 'additionOperator';
	readonly symbol: '+';
}

interface SubtractionOperator {
	readonly type: 'subtractionOperator';
	readonly symbol: '-';
}

interface MultiplicationOperator {
	readonly type: 'multiplicationOperator';
	readonly symbol: '*'|'×'|'';
}

interface ExponentiationOperator {
	readonly type: 'exponentiationOperator';
	readonly symbol: '^'|'**';
}

interface DivisionOperator {
	readonly type: 'divisionOperator';
	readonly symbol: '/'|'÷';
}

interface AndOperator {
	readonly type: 'andOperator';
	readonly symbol: '&&'|'and';
}

interface XorOperator {
	readonly type: 'xorOperator';
	readonly symbol: 'xor';
}

interface OrOperator {
	readonly type: 'orOperator';
	readonly symbol: '||'|'or';
}

interface ModOperator {
	readonly type: 'modOperator';
	readonly symbol: '%'|'mod';
}

interface NotOperator {
	readonly type: 'notOperator';
	readonly symbol: '!';
}

interface MinusOperator {
	readonly type: 'minusOperator';
	readonly symbol: '-';
}

interface InOperator {
	readonly type: 'inOperator';
	readonly symbol: 'in';
}

interface NinOperator {
	readonly type: 'ninOperator';
	readonly symbol: 'nin';
}

type UnaryOperator=MinusOperator|NotOperator;
type BinaryOperator=ComparisonOperator|AdditionOperator|SubtractionOperator|MultiplicationOperator|DivisionOperator|ExponentiationOperator|ModOperator|AndOperator|OrOperator|XorOperator;
type OneToManyOperator=InOperator|NinOperator;
type Operator=UnaryOperator|BinaryOperator|OneToManyOperator;

interface DiscardDiceModifier {
	readonly type: 'discardDiceModifier';
	readonly count: number;
	readonly criteria: 'highest'|'lowest';
}

interface KeepDiceModifier {
	readonly type: 'keepDiceModifier';
	readonly count: number;
	readonly criteria: 'highest'|'lowest';
}

interface RerollDiceModifier {
	readonly type: 'rerollDiceModifier';
	readonly condition: Expression;
	readonly times: number;
	readonly negate: boolean;
}

type DiceModifier=DiscardDiceModifier|KeepDiceModifier|RerollDiceModifier;
interface DiceExpression {
	readonly type: 'diceExpression';
	readonly count: Expression;
	readonly sides: number;
	readonly modifiers: readonly DiceModifier[];
}

interface UnaryExpression {
	readonly type: 'unaryExpression';
	readonly operator: UnaryOperator;
	readonly argument: Expression;
}

interface BinaryExpression {
	readonly type: 'binaryExpression';
	readonly left: Expression;
	readonly operator: BinaryOperator;
	readonly right: Expression;
}

interface OneToManyExpression {
	readonly type: 'oneToManyExpression';
	readonly left: Expression;
	readonly operator: OneToManyOperator;
	readonly right: readonly Expression[];
}

interface TernaryExpression {
	readonly type: 'ternaryExpression';
	readonly condition: Expression;
	readonly left: Expression
	readonly right: Expression;
}

interface IdentifierExpression {
	readonly type: 'identifierExpression';
	readonly name: string;
}

interface CallExpression {
	readonly type: 'callExpression';
	readonly functionName: string;
	readonly arguments: readonly Expression[];
}

interface BracketExpression {
	readonly type: 'bracketExpression';
	readonly expression: Expression;
	readonly open: BracketOpenToken['symbol'];
	readonly close: BracketCloseToken['symbol'];
}

type Expression=DiceExpression|UnaryExpression|BinaryExpression|BracketExpression|Literal|CallExpression|IdentifierExpression|TernaryExpression|OneToManyExpression;

const allExpressionTypes: readonly Expression['type'][] = [
	'binaryExpression',
	'booleanLiteral',
	'bracketExpression',
	'callExpression',
	'diceExpression',
	'identifierExpression',
	'numericLiteral',
	'unaryExpression',
	'ternaryExpression',
	'oneToManyExpression'
];

function isExpression<T extends Expression['type'] = Expression['type']>( node: any, ...types: readonly T[] ): node is Expression & { readonly type: T; } {
	if( types.length === 0 ) types = allExpressionTypes as T[];
	return ( typeof node === 'object' ) && types.includes( node?.type );
}

function join( children: readonly ReactChild[], separator: ReactChild ) {
	return children.reduce( ( r, child ) => ( r == null ) ? child : <>{r}{separator}{child}</>, null );
}

type AstNode=Expression|Operator|Statement;

interface LabelToken {
	readonly type: 'label';
	readonly label: string;
}

interface KeywordToken {
	readonly type: 'keyword';
	readonly keyword: 'def'|'let';
}

interface LambdaToken {
	readonly type: 'lambda';
}

interface IdentifierToken {
	readonly type: 'identifier';
	readonly name: string;
}

interface DiceToken {
	readonly type: 'dice';
	readonly sides: number;
}

interface DiceModifierToken {
	readonly type: 'diceModifier';
	readonly text: string;
}

interface NumericLiteralToken {
	readonly type: 'numericLiteral';
	readonly value: number;
}

interface BooleanLiteralToken {
	readonly type: 'booleanLiteral';
	readonly value: boolean;
}

type LiteralToken=NumericLiteralToken|BooleanLiteralToken;

interface OperatorToken {
	readonly type: 'operator';
	readonly symbol: Operator['symbol'];
}

interface BracketOpenToken {
	readonly type: 'bracketOpen';
	readonly symbol: '('|'[';
	readonly close: BracketCloseToken['symbol'];
}

interface BracketCloseToken {
	readonly type: 'bracketClose';
	readonly symbol: ')'|']';
	readonly open: BracketOpenToken['symbol'];
}

type BracketToken = BracketOpenToken|BracketCloseToken;

interface SeparatorToken {
	readonly type: 'separator';
	readonly symbol: ','|';';
}

interface WhitespaceToken {
	readonly type: 'whitespace';
	readonly ws: string;
}

interface TernaryToken {
	readonly type: 'ternary';
	readonly symbol: '?'|':';
}

type Token=LabelToken|KeywordToken|LambdaToken|SeparatorToken|BracketToken|IdentifierToken|DiceToken|LiteralToken|OperatorToken|WhitespaceToken|DiceModifierToken|TernaryToken;

interface TreeNode<T> {
	readonly value: T;
	readonly children: readonly TreeNode<T>[];
}

const brackets = Map<BracketOpenToken['symbol'], BracketCloseToken['symbol']>( [
	[ '(', ')' ],
	[ '[', ']' ]
] );

const bracketsReverse = Map<BracketCloseToken['symbol'], BracketOpenToken['symbol']>(
	Array.from( brackets.entries(), ( [ key, value ] ) => [ value, key ] )
);

function isToken<T extends Token['type']>( token: any, ...type: readonly T[] ): token is Token & { readonly type: T; } {
	return ( typeof token === 'object' ) && type.includes( token?.type );
}

export default async function( { bus, commandFilter, lifecycle, logger }: Params ) {
	if( module.hot ) {
		module.hot.addDisposeHandler( () => {
			lifecycle.shutdown();
		} );
	}

	const hasher = Hasher();
	const numformat = new Intl.NumberFormat( 'en-US', { useGrouping: true } );

	interface Response {
		route: ResponseRoute;
		element: ReactChild;
	}
	const response$ = new Subject<Response>();

	response$.pipe(
		bufferDebounceTime( 100 ),
		mergeMap( r =>
			from( r )
			.pipe( groupBy( e => hasher.hash( e.route ) ) )
		),
		mergeMap( async r => {
			const elements = await firstValueFrom( r.pipe( toArray() ) );
			assert.ok( elements.length > 0 );
			const [ firstElement ] = elements;
			const route = firstElement.route;
			const element = elements.reduce( ( x, y ) => <>{x}<div>{y.element}</div></>, <></> );
			const message = renderToStaticMarkup( element );
			bus.next( { type: 'response', message, route } );
		} ),
		handleErrors( { logger } ),
		takeUntil( lifecycle.shutdown$ )
	).subscribe();

	function lex( str: string ) {
		let ret = [] as readonly Token[];
		const label = /^\s*([^:?,;]{1,128})\s*:\s*/i;
		const ws = /^\s+/i;
		const ternary = /^[?:]/;
		const separator = /^[,;]/;
		const bracketOpen = /^[([]/;
		const bracketClose = /^[)\]]/;
		const operator = /^(?:\*{1,2}|&{2}|\|{2}|>=?|<=?|!=?|[-+/÷^×≠≥≤%]|={2}|<>|(?:and|x?or|eq|ne|gt|lt|ge|le|mod)(?![_a-z0-9]))/i;
		const dice = /^d([0-9]+)(?![_])/;
		const diceModifier = /^(?:[dk][0-9]+[lh]?|r(?:\s*!)?(?=\s*[(a-z0-9]))(?![_])/i;
		const keyword = /^(?:def|let)(?![_a-z0-9])/i;
		const lambda = /^=>?/;
		const identifier = /^(?:[_a-z][_a-z0-9]{0,32}|[αβγδεζηθικλμνξοπρστυφχψω])/i;
		const numericLiteral = /^(?:[0-9]+(?:\.[0-9]*)?|\.[0-9]+)/;
		const booleanLiteral = /^(?:true|false)(?![_a-z0-9])/i;
		while( str.length > 0 ) {
			const prevToken = _.findLast( ret, e => !isToken( e, 'whitespace' ) );
			const allowLabel = ( prevToken == null ) || ( isToken( prevToken, 'separator' ) && prevToken.symbol === ';' );
			const allowModifier = isToken( prevToken, 'dice', 'diceModifier' );
			let tokens = [] as readonly Token[];
			let match: RegExpExecArray;
			if( allowLabel && ( match = label.exec( str ) ) != null ) {
				tokens = [ { type: 'label', label: match[ 1 ] } ];
			} else if( ( match = ws.exec( str ) ) != null ) {
				tokens = [ { type: 'whitespace', ws: match[ 0 ] } ];
			} else if( ( match = ternary.exec( str ) ) != null ) {
				tokens = [ { type: 'ternary', symbol: match[ 0 ] as TernaryToken['symbol'] } ];
			} else if( ( match = separator.exec( str ) ) != null ) {
				tokens = [ { type: 'separator', symbol: match[ 0 ] as SeparatorToken['symbol'] } ];
			} else if( ( match = bracketOpen.exec( str ) ) != null ) {
				const symbol = match[ 0 ] as BracketOpenToken['symbol'];
				const close = brackets.get( symbol );
				tokens = [ { type: 'bracketOpen', symbol, close } ];
			} else if( ( match = bracketClose.exec( str ) ) != null ) {
				const symbol = match[ 0 ] as BracketCloseToken['symbol'];
				const open = bracketsReverse.get( symbol );
				tokens = [ { type: 'bracketClose', symbol, open } ];
			} else if( ( match = keyword.exec( str ) ) != null ) {
				tokens = [ { type: 'keyword', keyword: match[ 0 ] as KeywordToken['keyword'] } ];
			} else if( ( match = operator.exec( str ) ) != null ) {
				tokens = [ { type: 'operator', symbol: match[ 0 ] as OperatorToken['symbol'] } ];
			} else if( ( match = lambda.exec( str ) ) != null ) {
				tokens = [ { type: 'lambda' } as LambdaToken ];
			} else if( allowModifier && ( match = diceModifier.exec( str ) ) != null ) {
				tokens = [ { type: 'diceModifier', text: match[ 0 ] } as DiceModifierToken ];
			} else if( ( match = dice.exec( str ) ) != null ) {
				const [ , msides ] = match;
				const sides = parseInt( msides, 10 );
				if( !Number.isSafeInteger( sides ) ) throw new Error( `invalid sides: ${msides}` );
				tokens = [ { type: 'dice', sides } ];
			} else if( ( match = booleanLiteral.exec( str ) ) != null ) {
				tokens = [ { type: 'booleanLiteral', value: match[ 0 ].toLowerCase() === 'true' } ];
			} else if( ( match = identifier.exec( str ) ) != null ) {
				tokens = [ { type: 'identifier', name: match[ 0 ] } ];
			} else if( ( match = numericLiteral.exec( str ) ) != null ) {
				const value = parseFloat( match[ 0 ] );
				if( !Number.isFinite( value ) ) throw new Error( 'invalid number' );
				tokens = [ { type: 'numericLiteral', value } ];
			}
			if( tokens.length === 0 || match == null || match?.[ 0 ]?.length === 0 ) {
				throw new Error( `lex failed; remaining: ${str}; match: ${match?.[ 0 ] ?? 'null'}` );
			}
			str = str.slice( match[ 0 ].length );
			ret = [ ...ret, ...tokens ];
		}
		return ret;
	}

	function parse( tokens: readonly Token[] ): Program {
		type Node = TreeNode<AstNode|Token>;

		if( tokens.length > 1024 ) throw new Error( 'too many tokens' );

		function buildTree() {
			const tree = { value: null, children: Array.from( function *() {
				while( tokens.length > 0 ) {
					const node = ( function impl( parent: Node ): Node {
						let children = [] as readonly Node[];
						const opening = isToken( parent?.value, 'bracketOpen' ) ? parent.value : null;
						let closing = null as BracketCloseToken;
						let token: Token;
						while( tokens.length > 0 ) {
							( [ token, ...tokens ] = tokens );
							let child = { value: token, children: [] } as Node;
							if( isToken( child.value, 'keyword' ) ) {
								if( [ 'def', 'let' ].includes( child.value.keyword ) ) {
									child = impl( child );
								}
							} else if( isToken( child.value, 'bracketClose' ) ) {
								closing = child.value;
								break;
							} else if( isToken( child.value, 'bracketOpen' ) ) {
								child = impl( child );
							} else if( ( parent.value == null || (
								isToken( parent.value, 'keyword' )
								&& [ 'def', 'let' ].includes( parent.value.keyword )
							) ) && (
								isToken( child.value, 'separator' )
								&& ( child.value.symbol === ';' )
							) ) {
								break;
							}
							children = [ ...children, child ];
						}
						if( opening?.close != closing?.symbol ) {
							throw new Error( `mismatched bracket: expected: ${opening?.close ?? 'none'}, saw: ${closing?.symbol ?? 'none'}` );
						}
						return { ...parent, children };
					}( { value: null, children: [] } ) );
					yield node;
				}
			}() ) } as Node;
			// dump( logger, { tree } );
			return tree;
		}

		function removeWhitespace( node: Node ) {
			let children = [] as readonly Node[];
			for( const child of node.children ) {
				if( isToken( child.value, 'whitespace' ) ) {
					continue;
				}
				children = [ ...children, child ];
			}
			node = { ...node, children: children.map( c => removeWhitespace( c ) ) };
			return node;
		}

		function replaceSimpleExpressions( node: Node ): Node {
			if( isToken( node.value, 'numericLiteral' ) ) {
				node = { ...node, value: { type: 'numericLiteral', value: node.value.value } as NumericLiteral };
			} else if( isToken( node.value, 'booleanLiteral' ) ) {
				node = { ...node, value: { type: 'booleanLiteral', value: node.value.value } as BooleanLiteral };
			}
			const children = node.children.map( c => replaceSimpleExpressions( c ) );
			return { ...node, children };
		}

		const oneToManyOperators = Map<OneToManyOperator['symbol'], OneToManyOperator['type']>( [
			[ 'in', 'inOperator' ],
			[ 'nin', 'ninOperator' ]
		] );
		function replaceOneToManyExpressions( node: Node ): Node {
			let children = [] as readonly Node[];
			for( let i = 0; i < node.children.length; ++i ) {
				let [ child, next, next2 ] = node.children.slice( i, i + 3 );
				if( isToken( next?.value, 'identifier' ) && isToken( next2?.value, 'bracketOpen' ) ) {
					let value: Expression;
					if( oneToManyOperators.has( next.value.name as OneToManyOperator['symbol'] ) ) {
						value = { type: 'oneToManyExpression', operator: { type: oneToManyOperators.get( next.value.name as OneToManyOperator['symbol'] ), symbol: next.value.name }, left: null, right: null } as OneToManyExpression;
						child = { ...child, children: [ child, ...next2.children ], value };
						i += 2;
					}
				}
				children = [ ...children, child ];
			}
			children = children.map( c => replaceOneToManyExpressions( c ) );
			return { ...node, children };
		}

		function replaceCallExpressions( node: Node ): Node {
			let children = [] as readonly Node[];
			for( let i = 0; i < node.children.length; ++i ) {
				let [ child, next ] = node.children.slice( i, i + 2 );
				if( isToken( child.value, 'identifier' ) && isToken( next?.value, 'bracketOpen' ) ) {
					child = { ...child, children: next.children, value: { type: 'callExpression', functionName: child.value.name, arguments: [] } as CallExpression };
					++i;
				}
				children = [ ...children, child ];
			}
			children = children.map( c => replaceCallExpressions( c ) );
			return { ...node, children };
		}

		function replaceBracketExpressions( node: Node ): Node {
			let children = [] as readonly Node[];
			for( let child of node.children ) {
				if( isToken( child.value, 'bracketOpen' ) ) {
					child = { ...child, value: { type: 'bracketExpression', expression: null, open: child.value.symbol, close: child.value.close } as BracketExpression };
				}
				children = [ ...children, child ];
			}
			children = children.map( c => replaceBracketExpressions( c ) );
			return { ...node, children };
		}

		function replaceIdentifierExpressions( node: Node ): Node {
			let children = [] as readonly Node[];
			for( let child of node.children ) {
				if( isToken( child.value, 'identifier' ) ) {
					child = { ...child, value: { type: 'identifierExpression', name: child.value.name } as IdentifierExpression };
				}
				children = [ ...children, child ];
			}
			children = children.map( c => replaceIdentifierExpressions( c ) );
			return { ...node, children };
		}

		function replaceUnaryExpressions( node: Node ): Node {
			let children = [] as readonly Node[];
			const c = [ null, ...node.children ];
			for( let i = 0; i < c.length - 1; ++i ) {
				let [ prev, child, next ] = c.slice( i, i + 3 );
				if( !isExpression( prev?.value ) && isToken( child.value, 'operator' ) && isExpression( next?.value ) ) {
					if( child.value.symbol === '-' ) {
						child = { ...child, children: [ next ], value: { type: 'unaryExpression', operator: { type: 'minusOperator', symbol: child.value.symbol } } as UnaryExpression };
						++i;
					} else if( child.value.symbol === '!' ) {
						child = { ...child, children: [ next ], value: { type: 'unaryExpression', operator: { type: 'notOperator', symbol: child.value.symbol } } as UnaryExpression };
						++i;
					}
				}
				children = [ ...children, child ];
			}
			children = children.map( c => replaceUnaryExpressions( c ) );
			return { ...node, children };
		}

		function replaceDiceExpressions( node: Node ): Node {
			let children = [] as readonly Node[];
			for( let i = 0; i < node.children.length; ++i ) {
				let [ child, next, ...after ] = node.children.slice( i );
				if( isToken( child.value, 'dice' ) ) {
					const diceExpression = { type: 'diceExpression', sides: child.value.sides, count: null, modifiers: [] } as DiceExpression;
					after = [ next, ...after ];
					child = { value: diceExpression, children: [] };
				} else if( isToken( next?.value, 'dice' ) ) {
					let c = [] as readonly Node[];
					if( isToken( child.value, 'separator', 'label', 'lambda' ) ) {
						children = [ ...children, child ];
					} else {
						if( !isExpression( child.value ) ) throw new Error( `Dice sides must be an expression` );
						c = [ child ];
					}
					const diceExpression = { type: 'diceExpression', sides: next.value.sides, count: null, modifiers: [] } as DiceExpression;
					child = { value: diceExpression, children: c };
					++i;
				}
				if( isExpression( child.value, 'diceExpression' ) ) {
					let modifier: Node;
					while( isToken( after[ 0 ]?.value, 'diceModifier' ) ) {
						++i;
						const isReroll = after[ 0 ].value.text.charAt( 0 ).toLowerCase() === 'r';
						( [ modifier, ...after ] = after );
						if( isReroll ) {
							++i;
							modifier = { ...modifier, children: [ after[ 0 ] ] };
						}
						child = { ...child, children: [ ...child.children, modifier ] };
						if( isReroll ) break;
					}
					// dump( logger, { child, after } );
				}
				children = [ ...children, child ];
			}
			children = children.map( c => replaceDiceExpressions( c ) );
			return { ...node, children };
		}

		function replaceBinaryExpressions( node: Node ): Node {
			const opss = [
				[ 'rtl', [ [ '^', '**' ], 'exponentiationOperator' ] ],
				[ 'ltr',
				  [ [ '*', '×', '' ], 'multiplicationOperator' ],
				  [ [ '/', '÷' ], 'divisionOperator' ]
				], [ 'ltr',
					[ [ '+' ], 'additionOperator' ],
					[ [ '-' ], 'subtractionOperator' ]
				], [ 'ltr',
					[ [ '%', 'mod' ], 'modOperator' ],
				], [ 'ltr',
					[ [ '==', 'eq' ], 'equalityOperator' ],
					[ [ '!=', '<>', '≠', 'ne' ], 'inequalityOperator' ],
					[ [ '<', 'lt' ], 'lessThanOperator' ],
					[ [ '<=', '≤', 'le' ], 'lessThanEqualOperator' ],
					[ [ '>', 'gt' ], 'greaterThanOperator' ],
					[ [ '>=', '≥', 'ge' ], 'greaterThanEqualOperator' ]
				], [ 'ltr',
					[ [ 'xor' ], 'xorOperator' ],
					[ [ '||', 'or' ], 'orOperator' ],
					[ [ '&&', 'and' ], 'andOperator' ]
				]
			  ] as readonly ( readonly [ Direction, ...( readonly [ readonly Operator['symbol'][], Operator['type'] ] )[] ] )[];
			let change: boolean;
			do {
				change = false;
				for( const [ direction, ...ops ] of opss ) {
					const isRtl = direction === 'rtl';
					const opMap = Map<Operator[ 'symbol' ], Operator[ 'type' ]>(
						ops.flatMap( ( [ symbols, op ] ) =>
							symbols.map( symbol =>
								[ symbol, op ] as [ Operator[ 'symbol' ], Operator[ 'type' ] ]
							)
						)
					);
					const implicitMultiply = opMap.has( '' ) && !isExpression( node.value, 'binaryExpression' );
					let children = [] as readonly Node[];
					const c = isRtl ? [ ...node.children ].reverse() : node.children;
					for( let i = 0; i < c.length; ++i ) {
						let [ child, next, next2 ] = c.slice( i, i + 3 );
						if( child.value != null ) {
							if( implicitMultiply && isExpression( child.value, 'identifierExpression', 'numericLiteral', 'bracketExpression' ) && isExpression( next?.value, 'bracketExpression', 'identifierExpression', 'callExpression' ) ) {
								child = { ...child, children: isRtl ? [ next, child ] : [ child, next ], value: { type: 'binaryExpression', operator: { type: opMap.get( '' ), symbol: '' }, left: null, right: null } as BinaryExpression };
								++i;
								change = true;
							} else if( isExpression( child.value ) && isToken( next?.value, 'operator' ) && isExpression( next2?.value ) ) {
								if( opMap.has( next.value.symbol ) ) {
									child = { ...child, children: isRtl ? [ next2, child ] : [ child, next2 ], value: { type: 'binaryExpression', operator: { type: opMap.get( next.value.symbol ), symbol: next.value.symbol }, left: null, right: null } as BinaryExpression };
									i += 2;
									change = true;
								}
							}
						}
						children =
							isRtl
							? [ child, ...children ]
							: [ ...children, child ];
						if( change ) {
							children =
								isRtl
								? [ ...c.slice( i + 1 ).reverse(), ...children ]
								: [ ...children, ...c.slice( i + 1 ) ];
							break;
						}
					}
					node = { ...node, children };
					if( change ) break;
				}
			} while( change );
			node = { ...node, children: node.children.map( c => replaceBinaryExpressions( c ) ) };
			return node;
		}

		function replaceTernaryExpressions( node: Node ): Node {
			let children = node.children;
			let change: boolean;
			do {
				change = false;
				const c = [ ...children ].reverse();
				children = [];
				for( let i = 0; i < c.length; ++i ) {
					let [ child, next, next2, next3, next4 ] = c.slice( i, i + 6 );
					// dump( logger, { child: child.value.type, next: next?.value.type, next2: next2?.value.type, next3: next3?.value.type, next4: next4?.value.type } );
					if( isToken( next?.value, 'ternary' ) && ( next.value.symbol === ':' ) && isToken( next3?.value, 'ternary' ) && ( next3.value.symbol === '?' ) ) {
						child = { children: [ next4, next2, child ], value: { type: 'ternaryExpression', condition: null, left: null, right: null } };
						i += 4;
						change = true;
					}
					children = [ child, ...children ];
				}
			} while( change );
			children = children.map( c => replaceTernaryExpressions( c ) );
			return { ...node, children };
		}

		function finalizeProgram( root: Node ): Program {
			if( root.value != null ) throw new Error( 'root should not have value' );
			let children = [] as readonly Node[];
			for( const child of root.children ) {
				if( isToken( child.value, 'separator' ) ) continue;
				children = [ ...children, child ];
			}
			if( children.length === 0 ) throw new Error( 'should have at least 1 statement' );

			function handleExpressionList( n: readonly TreeNode<Expression>[] ) {
				let prev: Node;
				let argnodes = [] as readonly TreeNode<Expression>[];
				for( const argnode of n ) {
					if( isToken( argnode.value, 'separator' ) ) {
						if( prev == null ) {
							// dump( logger, child );
							throw new Error( 'expression list cannot begin with separator' );
						}
						if( isToken( prev.value, 'separator' ) ) {
							// dump( logger, child );
							throw new Error( 'must not have 2 consecutive separators' );
						}
						prev = argnode;
						continue;
					}
					argnodes = [ ...argnodes, argnode ];
					prev = argnode;
				}
				return argnodes;
			}
			function handleExpression( child: TreeNode<Expression> ) {
				if( isExpression( child.value, 'binaryExpression' ) ) {
					if( child.children.length !== 2 ) {
						// dump( logger, child );
						throw new Error( 'binaryExpression should have 2 children' );
					}
					const [ left, right ] = child.children;
					if( !isExpression( left.value ) || !isExpression( right.value ) ) {
						// dump( logger, child );
						throw new Error( 'children of binaryExpression should be expressions' );
					}
					return { ...child.value, left: handleExpression( left ), right: handleExpression( right ) };
				}
				if( isExpression( child.value, 'unaryExpression' ) ) {
					if( child.children.length !== 1 ) {
						// dump( logger, child );
						throw new Error( 'unaryExpression should have 1 child' );
					}
					const [ argument ] = child.children;
					if( !isExpression( argument.value ) ) {
						// dump( logger, child );
						throw new Error( 'child of unaryExpression should be an expression' );
					}
					return { ...child.value, argument: handleExpression( argument ) };
				}
				if( isExpression( child.value, 'oneToManyExpression' ) ) {
					const expr = handleExpression( child.children[ 0 ] );
					if( !isExpression( expr ) ) {
						// dump( logger, child );
						throw new Error( 'first child of oneToManyExpression should be an expression' );
					}
					children = handleExpressionList( child.children.slice( 1 ) ).map( n => handleExpression( n ) );
					return { ...child.value, left: expr, right: children };
				}
				if( isExpression( child.value, 'callExpression' ) ) {
					return { ...child.value, arguments: handleExpressionList( child.children ).map( n => handleExpression( n ) ) };
				}
				if( isExpression( child.value, 'bracketExpression' ) ) {
					if( child.children.length !== 1 ) {
						// dump( logger, child );
						throw new Error( 'bracketExpression should have 1 child' );
					}
					const expr = handleExpression( child.children[ 0 ] );
					if( !isExpression( expr ) ) {
						// dump( logger, child );
						throw new Error( 'child of bracketExpression should be an expression' );
					}
					return { ...child.value, expression: expr };
				}
				if( isExpression( child.value, 'diceExpression' ) ) {
					// dump( logger, { child } );
					let [ count, ...mods ] = child.children as Node[];
					let countExpr: Expression;
					if( count == null || isToken( count.value, 'diceModifier' ) ) {
						if( count != null ) mods = [ count, ...mods ];
						countExpr = { type: 'numericLiteral', value: 1, implicit: true } as NumericLiteral;
					} else {
						countExpr = handleExpression( count as TreeNode<Expression> );
					}
					let modifiers = [] as readonly DiceModifier[];
					for( const mod of mods ) {
						if( !isToken( mod.value, 'diceModifier' ) ) throw new Error( 'expected modifier' );
						let [ , l1, neg, l2, n, lh ] = /^(?:(r)\s*(!)?|([dk])([0-9]+)([lh])?)$/i.exec( mod.value.text );
						const l = l1 ?? l2;
						lh = lh?.toLowerCase();
						let criteria = null as 'lowest'|'highest';
						let modifier: DiceModifier;
						const num = parseInt( n, 10 );
						switch( l ) {
							case 'D':
								criteria = 'highest';
								// fall through
							case 'd':
								criteria = lh === 'l' ? 'lowest' : lh === 'h' ? 'highest' : ( criteria ?? 'lowest' );
								modifier = { type: 'discardDiceModifier', count: num, criteria };
								break;
							case 'K':
								criteria = 'lowest';
								// fall through
							case 'k':
								criteria = lh === 'l' ? 'lowest' : lh === 'h' ? 'highest' : ( criteria ?? 'highest' );
								modifier = { type: 'keepDiceModifier', count: num, criteria };
								break;
							case 'R':
							case 'r': {
								if( mod.children.length !== 1 ) {
									dump( logger, mod );
									throw new Error( 'rerollDiceModifier should have 1 child' );
								}
								let expr = handleExpression( mod.children[ 0 ] as TreeNode<Expression> );
								if( !isExpression( expr ) ) {
									// dump( logger, mod );
									throw new Error( 'child of rerollDiceModifier should be an expression' );
								}
								if( isExpression( expr, 'numericLiteral' ) ) {
									expr = {
										type: 'binaryExpression',
										operator: { type: 'equalityOperator', symbol: 'eq' },
										left: {
											type: 'identifierExpression',
											name: 'n'
										} as IdentifierExpression,
										right: expr
									} as BinaryExpression;
								} else if( isExpression( expr, 'identifierExpression' ) ) {
									expr = {
										type: 'callExpression',
										functionName: expr.name,
										arguments: [ { type: 'identifierExpression', name: 'n' } ]
									} as CallExpression;
								}
								modifier = { type: 'rerollDiceModifier', condition: expr, times: ( l === 'R' ) ? Infinity : 1, negate: ( neg === '!' ) };
							}
						}
						if( modifier == null ) throw new Error( `Unexpected modifier: ${mod.value.text}` );
						modifiers = [ ...modifiers, modifier ];
					}
					return { ...child.value, count: countExpr, modifiers } as DiceExpression;
				}
				if( isExpression( child.value, 'ternaryExpression' ) ) {
					if( child.children.length !== 3 ) {
						// dump( logger, child );
						throw new Error( 'ternaryExpression should have 3 children' );
					}
					const [ condition, left, right ] = child.children.map( x => handleExpression( x ) );
					return { ...child.value, condition, left, right };
				}
				if( !isExpression( child.value ) ) {
					dump( logger, child );
					throw new Error( 'child should be an expression' );
				}
				if( child.children.length > 0 ) {
					// dump( logger, child );
					throw new Error( 'expression should not have children' );
				}
				return child.value;
			}

			// dump( logger, { root } );
			let statements = [] as readonly Statement[];
			for( const stmt of children ) {
				if( stmt.value != null ) throw new Error( 'statement should not have value' );
				let label = null as string;
				let statement: Statement;
				for( const child of stmt.children ) {
					if( isToken( child.value, 'label' ) ) {
						if( label != null ) throw new Error( 'Only one label allowed per statement' );
						label = child.value.label;
						continue;
					}
					if( isExpression( child.value ) ) {
						statement = { type: 'expressionStatement', expression: handleExpression( child as TreeNode<Expression> ), label };
						label = null;
					} else if( isToken( child.value, 'keyword' ) ) {
						if( child.value.keyword === 'def' ) {
							if( child.children.length !== 3 ) {
								// dump( logger, child );
								throw new Error( 'def statement should have 3 children' );
							}
							const [ callExpr, lambda, expr ] = child.children;
							if( !isExpression( callExpr.value, 'callExpression' ) ) {
								// dump( logger, child );
								throw new Error( 'def statement expected: identifier' );
							}
							if( !isToken( lambda.value, 'lambda' ) ) {
								// dump( logger, child );
								throw new Error( 'def statement expected: lambda' );
							}
							if( !isExpression( expr.value ) ) {
								// dump( logger, child );
								throw new Error( 'def statement expected: expression' );
							}
							const arglist = handleExpression( callExpr as TreeNode<Expression> ) as CallExpression;
							if( !arglist.arguments.every( a => isExpression( a, 'identifierExpression' ) ) ) {
								throw new Error( 'argument list may only contain identifiers' );
							}
							statement = { type: 'functionDeclarationStatement', name: arglist.functionName, arguments: arglist.arguments.map( ( a: IdentifierExpression ) => a.name ), expression: handleExpression( expr as TreeNode<Expression> ) };
						} else if( child.value.keyword === 'let' ) {
							if( child.children.length !== 3 ) {
								// dump( logger, child );
								throw new Error( 'let statement should have 3 children' );
							}
							const [ name, lambda, expr ] = child.children;
							if( !isExpression( name.value, 'identifierExpression' ) ) {
								// dump( logger, child );
								throw new Error( 'let statement expected: identifier' );
							}
							if( !isToken( lambda.value, 'lambda' ) ) {
								// dump( logger, child );
								throw new Error( 'let statement expected: lambda' );
							}
							if( !isExpression( expr.value ) ) {
								// dump( logger, child );
								throw new Error( 'let statement expected: expression' );
							}
							statement = { type: 'variableDeclarationStatement', kind: child.value.keyword, name: name.value.name, expression: handleExpression( expr as TreeNode<Expression> ) };
						}
					}
					if( statement == null ) {
						dump( logger, child );
						throw new Error( 'Unknown statement' );
					}
					statements = [ ...statements, statement ];
					if( label != null ) throw new Error( `Orphan label: ${label}` );
				}
			}

			return { type: 'program', statements };
		}

		let root = buildTree();
		// dump( logger, { root } );
		root = removeWhitespace( root );
		root = replaceSimpleExpressions( root );
		root = replaceOneToManyExpressions( root );
		root = replaceCallExpressions( root );
		root = replaceBracketExpressions( root );
		root = replaceIdentifierExpressions( root );
		root = replaceDiceExpressions( root );
		root = replaceUnaryExpressions( root );
		root = replaceBinaryExpressions( root );
		root = replaceTernaryExpressions( root );
		// dump( logger, { root } );
		const program = finalizeProgram( root );
		dump( logger, { program } );
		return program;
	}


	interface ExpressionResultBase<T> {
		readonly value: T;
		readonly display: ReactChild;
	}
	interface ResultDie {
		readonly value: number;
		readonly sides: number;
		readonly discarded: boolean;
		readonly reroll: boolean;
	}
	interface ExpressionResultDice extends ExpressionResultBase<number> {
		readonly type: 'resultDice';
		readonly dice: readonly ResultDie[];
	}
	interface ExpressionResultNumber extends ExpressionResultBase<number> {
		readonly type: 'resultNumber';
	}
	interface ExpressionResultBoolean extends ExpressionResultBase<boolean> {
		readonly type: 'resultBoolean';
	}

	function format( result: ExpressionResultValue ) {
		if( typeof result === 'number' ) {
			if( Object.is( result, NaN ) ) return 'NaN';
			if( result === Infinity ) return '∞';
			if( result === -Infinity ) return '-∞';
			return numformat.format( result );
		}
		if( typeof result === 'boolean' ) return String( result );
		throw new Error( `Unexpected expression result: ${typeof result}` );
	}

	function formatDie( result: ResultDie ) {
		let ret = <i>{format( result.value )}</i>;
		if( result.discarded ) ret = <del>{ret}</del>;
		if( result.reroll ) ret = <ins>{ret}</ins>;
		return ret;
	}

	function expressionResult( value: ExpressionResultValue, display = null as ReactChild ) {
		const type = ( typeof value === 'boolean' ? 'resultBoolean' : 'resultNumber' ) as ExpressionResult['type'];
		return { type, value, display } as ExpressionResult;
	}

	type ExpressionResult = ExpressionResultNumber|ExpressionResultBoolean|ExpressionResultDice;
	type ExpressionResultValue = ExpressionResult['value'];

	const fnMapInitial = Map<string, ( ...args: readonly ( () => ExpressionResultValue )[] ) => ExpressionResultValue>( [
		[ 'abs', ( x: () => number ) => Math.abs( x() ) ],
		[ 'acos', ( x: () => number ) => Math.acos( x() ) ],
		[ 'avg', ( ...args: readonly ( () => number )[] ) => args.reduce( ( x, y ) => x + y(), 0 ) / args.length ],
		[ 'atan', ( x: () => number ) => Math.atan( x() ) ],
		[ 'atan2', ( y: () => number, x: () => number ) => Math.atan2( y(), x() ) ],
		[ 'asin', ( x: () => number ) => Math.asin( x() ) ],
		[ 'ceil', ( x: () => number ) => Math.ceil( x() ) ],
		[ 'cos', ( x: () => number ) => Math.cos( x() ) ],
		[ 'deg2rad', ( x: () => number ) => x() * Math.PI / 180 ],
		[ 'even', ( x: () => number ) => ( x() % 2 ) === 0 ],
		[ 'exp', ( x: () => number ) => Math.exp( x() ) ],
		[ 'floor', ( x: () => number ) => Math.floor( x() ) ],
		[ 'gcd', ( ...args: readonly ( () => number )[] ) => math.gcd( ...args.map( a => a() ) ) ],
		[ 'hypot', ( ...args: readonly ( () => number )[] ) => math.hypot( ...args.map( a => a() ) ) ],
		[ 'iif', ( x: () => boolean, y: () => ExpressionResultValue, z: () => ExpressionResultValue ) => x() ? y() : z() ],
		[ 'lcm', ( x: () => number, y: () => number ) => math.lcm( x(), y() ) ],
		[ 'ln', ( x: () => number ) => Math.log( x() ) ],
		[ 'log', ( x: () => number ) => Math.log10( x() ) ],
		[ 'log2', ( x: () => number ) => Math.log2( x() ) ],
		[ 'median', ( ...args: readonly ( () => number )[] ) => math.median( ...args.map( a => a() ) ) ],
		[ 'negative', ( x: () => number ) => x() < 0 ],
		[ 'odd', ( x: () => number ) => ( x() % 2 ) === 1 ],
		[ 'positive', ( x: () => number ) => x() > 0 ],
		[ 'pow', ( x: () => number, y: () => number ) => Math.pow( x(), y() ) ],
		[ 'prime', ( x: () => number ) => math.isPrime( x() ) ],
		[ 'max', ( ...args: readonly ( () => number )[] ) => Math.max( ...args.map( a => a() ) ) ],
		[ 'min', ( ...args: readonly ( () => number )[] ) => Math.min( ...args.map( a => a() ) ) ],
		[ 'rad2deg', ( x: () => number ) => x() * 180 / Math.PI ],
		[ 'random', () => Math.random() ],
		[ 'randomInt', ( x: () => number, y: () => number ) => math.randomInt( x(), y() ) ],
		[ 'root', ( x: () => number, y: () => number ) => math.nthRoot( x(), y() ) ],
		[ 'round', ( x: () => number ) => Math.round( x() ) ],
		[ 'sin', ( x: () => number ) => Math.sin( x() ) ],
		[ 'signum', ( x: () => number ) => Math.sign( x() ) ],
		[ 'sqrt', ( x: () => number ) => Math.sqrt( x() ) ],
		[ 'stddev', ( ...args: readonly ( () => number )[] ) => math.std( args.map( a => a() ) ) ],
		[ 'sum', ( ...args: readonly ( () => number )[] ) => args.reduce( ( x, y ) => x + y(), 0 ) ],
		[ 'tan', ( x: () => number ) => Math.tan( x() ) ],
		[ 'variance', ( ...args: readonly ( () => number )[] ) => math.variance( args.map( a => a() ) ) ]
	] );

	const varMapInitial = Map<string, () => ExpressionResultValue>( [
		[ 'e', () => Math.E ],
		[ 'pi', () => Math.PI ],
		[ 'tau', () => Math.PI * 2 ]
	] );

	interface EvaluationContext {
		readonly fnMap: Map<string, ( ...args: readonly ( () => ExpressionResultValue )[] ) => ExpressionResultValue>;
		readonly varMap: Map<string, () => ExpressionResultValue>;
	}
	async function evaluate( prg: Program ): Promise<ReactChild> {
		let output = null as ReactChild;
		let ttl = 16384;

		function tick( c = 1 ) {
			ttl = ttl - c;
			if( ttl < 0 ) throw new Error( 'ttl expired' );
		}

		const varAliases = Map<string, string>( [
			[ 'α', 'alpha' ],
			[ 'β', 'beta' ],
			[ 'γ', 'gamma' ],
			[ 'δ', 'delta' ],
			[ 'ε', 'epsilon' ],
			[ 'ζ', 'zeta' ],
			[ 'η', 'eta' ],
			[ 'θ', 'theta' ],
			[ 'ι', 'iota' ],
			[ 'κ', 'kappa' ],
			[ 'λ', 'lambda' ],
			[ 'μ', 'mu' ],
			[ 'ν', 'nu' ],
			[ 'ξ', 'xi' ],
			[ 'ο', 'omicron' ],
			[ 'π', 'pi' ],
			[ 'ρ', 'rho' ],
			[ 'σ', 'sigma' ],
			[ 'τ', 'tau' ],
			[ 'υ', 'upsilon' ],
			[ 'φ', 'phi' ],
			[ 'χ', 'chi' ],
			[ 'ψ', 'psi' ],
			[ 'ω', 'omega' ]
		] );

		const binaryOperatorMap = Map<BinaryOperator['type'], ( x: () => ExpressionResultValue, y: () => ExpressionResultValue ) => ExpressionResultValue>( [
			[ 'equalityOperator', ( x: () => ExpressionResultValue, y: () => ExpressionResultValue ) => x() === y() ],
			[ 'inequalityOperator', ( x: () => ExpressionResultValue, y: () => ExpressionResultValue ) => x() !== y() ],
			[ 'lessThanOperator', ( x: () => number, y: () => number ) => x() < y() ],
			[ 'lessThanEqualOperator', ( x: () => number, y: () => number ) => x() <= y() ],
			[ 'greaterThanOperator', ( x: () => number, y: () => number ) => x() > y() ],
			[ 'greaterThanEqualOperator', ( x: () => number, y: () => number ) => x() >= y() ],
			[ 'andOperator', ( x: () => boolean, y: () => boolean ) => x() && y() ],
			[ 'modOperator', ( x: () => number, y: () => number ) => x() % y() ],
			[ 'orOperator', ( x: () => boolean, y: () => boolean ) => x() || y() ],
			[ 'xorOperator', ( x: () => boolean, y: () => boolean ) => x() != y() ],
			[ 'additionOperator', ( x: () => number, y: () => number ) => x() + y() ],
			[ 'divisionOperator', ( x: () => number, y: () => number ) => x() / y() ],
			[ 'exponentiationOperator', ( x: () => number, y: () => number ) => x() ** y() ],
			[ 'multiplicationOperator', ( x: () => number, y: () => number ) => x() * y() ],
			[ 'subtractionOperator', ( x: () => number, y: () => number ) => x() - y() ]
		] );

		const unaryOperatorMap = Map<UnaryOperator['type'], ( x: () => ExpressionResultValue ) => ExpressionResultValue>( [
			[ 'minusOperator', x => -x() ],
			[ 'notOperator', x => !x() ]
		] );

		const oneToManyOperatorMap = Map<OneToManyOperator['type'], ( x: () => ExpressionResultValue, y: readonly ( () => ExpressionResultValue )[] ) => ExpressionResultValue>( [
			[ 'inOperator', ( x, y ) => y.some( y => x() == y() ) ],
			[ 'ninOperator', ( x, y ) => !y.some( y => x() == y() ) ]
		] );

		const displayMap = Map<Expression['type'], ( x: Expression ) => ReactChild>( [
			[ 'binaryExpression', ( expr: BinaryExpression ) =>
				<>{getDisplay( expr.left )}{' '}{expr.operator.symbol}{' '}{getDisplay( expr.right )}</> ],
			[ 'booleanLiteral', ( expr: BooleanLiteral ) =>
				format( expr.value ) ],
			[ 'bracketExpression', ( expr: BracketExpression ) =>
				<>{expr.open}{getDisplay( expr.expression )}{expr.close}</> ],
			[ 'callExpression', ( expr: CallExpression ) =>
			 	<>{expr.functionName}({join( expr.arguments.map( a => getDisplay( a ) ), ', ' )})</> ],
			[ 'diceExpression', ( expr: DiceExpression ) => {
				const mods = join( expr.modifiers.map( formatModifier ), '' );
				return <>{getDisplay( expr.count )}d{expr.sides}{mods}</>;
			} ],
			[ 'identifierExpression', ( expr: IdentifierExpression ) => expr.name ],
			[ 'numericLiteral', ( expr: NumericLiteral ) =>
				expr.implicit ? <></> : format( expr.value ) ],
			[ 'unaryExpression', ( expr: UnaryExpression ) =>
				<>{expr.operator.symbol}{getDisplay( expr.argument )}</> ],
			[ 'ternaryExpression', ( expr: TernaryExpression ) => <>{getDisplay( expr.condition )}{' '}?{' '}{getDisplay( expr.left )}{' '}:{' '}{getDisplay( expr.right )}</> ],
			[ 'oneToManyExpression', ( expr: OneToManyExpression ) =>
			 	<>{getDisplay( expr.left )} {expr.operator.symbol}({join( expr.right.map( a => getDisplay( a ) ), ', ' )})</> ],
		] );

		function getDisplay( expr: Expression, lazy?: Lazy<ExpressionResult> ): ReactChild {
			if( lazy?.called ) return lazy().display;
			const fn = displayMap.get( expr.type );
			if( fn == null ) throw new Error( `Unknown expression type: ${expr.type}` );
			return fn( expr );
		}

		function formatModifier( d: DiceModifier ) {
			switch( d.type ) {
			case 'discardDiceModifier': return <>d{d.count}{d.criteria.charAt( 0 )}</>;
			case 'keepDiceModifier': return <>k{d.count}{d.criteria.charAt( 0 )}</>;
			case 'rerollDiceModifier':
				return <>
					{isFinite( d.times ) ? 'r' : 'R'}
					{d.negate ? '!' : <></>}
					{( isExpression( d.condition, 'binaryExpression' ) && ( d.condition.operator.type === 'equalityOperator' ) )
					? getDisplay( d.condition.right )
					: ( isExpression( d.condition, 'callExpression' ) && ( d.condition.arguments.length === 1 ) && isExpression( d.condition.arguments[ 0 ], 'identifierExpression' ) && d.condition.arguments[ 0 ].name === 'n' )
					? ( ' ' + d.condition.functionName )
					: getDisplay( d.condition )}
				</>;
			default: throw new Error( `Not implemented: ${( d as DiceModifier ).type}` );
			}
		}

		const exprMap = Map<Expression['type'], ( x: Expression, ctx: EvaluationContext ) => ExpressionResult>( [
			[ 'binaryExpression', ( expr: BinaryExpression, ctx ) => {
				tick( 1 );
				const fn = binaryOperatorMap.get( expr.operator.type );
				if( fn == null ) throw new Error( `Unknown binary operator: ${expr.operator.type}` );
				const left = lazy( () => evaluateExpression( expr.left, ctx ) );
				const right = lazy( () => evaluateExpression( expr.right, ctx ) );
				const value = fn( () => left().value, () => right().value );
				return expressionResult( value, <>{getDisplay( expr.left, left )}{` ${expr.operator.symbol} `.replace( /^\s+$/, '' )}{getDisplay( expr.right, right )}{' '}<sup>({format( value )})</sup></> );
			} ],
			[ 'booleanLiteral', ( expr: BooleanLiteral ) => expressionResult( expr.value, getDisplay( expr ) ) ],
			[ 'bracketExpression', ( expr: BracketExpression, ctx ) => {
				const result = evaluateExpression( expr.expression, ctx );
				return expressionResult( result.value, <>{expr.open}{result.display}{expr.close}</> );
			} ],
			[ 'oneToManyExpression', ( expr: OneToManyExpression, ctx ) => {
				tick( 1 );
				const fn = oneToManyOperatorMap.get( expr.operator.type );
				if( fn == null ) throw new Error( `Unknown one to many operator: ${expr.operator.type}` );
				const left = lazy( () => evaluateExpression( expr.left, ctx ) );
				const right = expr.right.map( e => lazy( () => evaluateExpression( e, ctx ) ) );
				const result = fn( () => left().value, right.map( r => () => r().value ) );
				return expressionResult( result, <>{getDisplay( expr.left, left )}{' '}{expr.operator.symbol}{' '}({join( expr.right.map( ( r, i ) => getDisplay( r, right[ i ] ) ), ', ' )}){' '}<sup>({format( result )})</sup></> );
			} ],
			[ 'callExpression', ( expr: CallExpression, ctx ) => {
				tick( 1 );
				const fn = ctx.fnMap.get( expr.functionName );
				if( fn == null ) throw new Error( `Unknown function: ${expr.functionName}` );
				const args = expr.arguments.map( e => lazy( () => evaluateExpression( e, ctx ) ) );
				const result = fn( ...( args.map( e => () => e().value ) as readonly ( () => ExpressionResultValue )[] ) );
				const arglist = join( args.map( ( e, i ) => getDisplay( expr.arguments[ i ], e ) ), ', ' );
				return expressionResult( result, <>{expr.functionName}({arglist}){' '}<sup>({format( result )})</sup></> );
			} ],
			[ 'diceExpression', ( expr: DiceExpression, ctx ) => {
				const countR = evaluateExpression( expr.count, ctx );
				const count = countR.value;
				if( !Number.isSafeInteger( count ) || count < 1 ) {
					throw new Error( 'Expected a positive integer number of dice' );
				}
				function rollDice( sides: number, count: number ) {
					tick( count );
					return Array.from( { length: count }, () => {
						const value = Math.floor( Math.random() * sides ) + 1;
						return { value, sides, discarded: false, reroll: false } as ResultDie;
					} );
				}

				let fn = () => rollDice( expr.sides, Number( count ) );
				for( const modifier of expr.modifiers ) {
					const oldFn = fn;
					switch( modifier.type ) {
					case 'keepDiceModifier':
						fn = () => {
							const before = oldFn();
							let sorted = before.filter( d => !d.discarded ).map( ( d, i ) => [ d.value, i ] );
							sorted = _.orderBy( sorted, ( [ value ] ) => value, modifier.criteria === 'highest' ? 'desc' : 'asc' );
							sorted = sorted.slice( 0, modifier.count );
							const keepIndices = Set( sorted.map( ( [ , index ] ) => index ) );
							const after = before.map( ( d, i ) => ( { ...d, discarded: !keepIndices.has( i ) } ) );
							// dump( logger, { modifier, before, after } );
							return after;
						};
						break;
					case 'discardDiceModifier':
						fn = () => {
							const before = oldFn();
							let sorted = before.filter( d => !d.discarded ).map( ( d, i ) => [ d.value, i ] );
							sorted = _.orderBy( sorted, ( [ value ] ) => value, modifier.criteria === 'highest' ? 'desc' : 'asc' );
							sorted = sorted.slice( modifier.count );
							const keepIndices = Set( sorted.map( ( [ , index ] ) => index ) );
							const after = before.map( ( d, i ) => ( { ...d, discarded: !keepIndices.has( i ) } ) );
							// dump( logger, { modifier, before, after } );
							return after;
						};
						break;
					case 'rerollDiceModifier':
						fn = () => {
							const before = oldFn();
							let after = before;
							for( let pass = 0; pass < modifier.times; ++pass ) {
								if( !after.some( d => !d.discarded ) ) break;
								if( pass > 100 ) {
									throw new Error( 'Too many rerolls' );
								}
								const discardedIndices = after.map( ( d, i ) => {
									if( d.discarded ) return null;
									const varMap = ctx.varMap;
									if( modifier.negate !== evaluateExpression( modifier.condition, { ...ctx, varMap: varMap.set( 'n', () => d.value ) } ).value ) {
										return i;
									}
								} ).filter( i => i != null );
								if( discardedIndices.length === 0 ) break;
								after = [ ...after.map( ( a, i ) => discardedIndices.includes( i ) ? { ...a, discarded: true } : a ), ...rollDice( expr.sides, discardedIndices.length ).map( d => ( { ...d, reroll: true } ) ) ];
							}
							// dump( logger, { modifier, before, after } );
							return after;
						};
						break;
					default: throw new Error( `Unexpected dice modifier: ${modifier}` );
					}
				}

				const dice = fn();
				const keptDice = dice.filter( d => !d.discarded );
				const value = keptDice.reduce( ( x, y ) => x + y.value, 0 );
				const mods = join( expr.modifiers.map( formatModifier ), '' );
				// dump( logger, { dice } );
				return expressionResult( value, <>{countR.display}d{expr.sides}{mods}{' '}<sup>({dice.length === 1 ? <b>{formatDie( dice[ 0 ] )}</b> : join( dice.map( d => formatDie( d ) ), ', ' )}{ dice.length > 1 ? <>:{' '}<b>{format( value )}</b></> : <></> })</sup></> );
			} ],
			[ 'identifierExpression', ( expr: IdentifierExpression, ctx ) => {
				const name = varAliases.get( expr.name ) ?? expr.name.toLowerCase();
				const value = ctx.varMap.get( name );
				if( value == null ) {
					// dump( logger, { name, varMap: Object.fromEntries( ctx.varMap.entries() ) } );
					throw new Error( `Unknown identifier: ${expr.name}` );
				}
				const result = value();
				return expressionResult( result, <>{expr.name}{' '}<sup>(<i>{format( result )}</i>)</sup></> );
			} ],
			[ 'numericLiteral', ( expr: NumericLiteral ) => expressionResult( expr.value, getDisplay( expr ) ) ],
			[ 'unaryExpression', ( expr: UnaryExpression, ctx ) => {
				tick( 1 );
				const fn = unaryOperatorMap.get( expr.operator.type );
				if( fn == null ) {
					throw new Error( `Unknown unary operator: ${expr.operator.type}` );
				}
				const result = evaluateExpression( expr.argument, ctx );
				return expressionResult( fn( () => result.value ), <>{expr.operator.symbol}{result.display}</> );
			} ],
			[ 'ternaryExpression', ( expr: TernaryExpression, ctx ) => {
				tick( 1 );
				const cond = evaluateExpression( expr.condition, ctx );
				const left = lazy( () => evaluateExpression( expr.left, ctx ) );
				const right = lazy( () => evaluateExpression( expr.right, ctx ) );
				const result = cond.value ? left() : right();
				return expressionResult( result.value, <>{cond.display}{' '}?{' '}{getDisplay( expr.left, left )}{' '}:{' '}{getDisplay( expr.right, right )}{' '}<sup>({format( result.value )})</sup></> );
			} ]
		] );

		function evaluateExpression( expr: Expression, ctx: EvaluationContext ): ExpressionResult {
			// dump( logger, { expr } );
			const e = exprMap.get( expr.type );
			if( e == null ) throw new Error( `Unknown expression type ${expr.type}` );
			return e( expr, ctx );
		}

		const statementMap = ( () => {
			let ctx = { fnMap: fnMapInitial, varMap: varMapInitial } as EvaluationContext;
			return Map<Statement['type'], ( stmt: Statement ) => ExpressionResult>( [
				[ 'functionDeclarationStatement', ( stmt: FunctionDeclarationStatement ) => {
					tick( 1 );
					ctx = { ...ctx, fnMap: ctx.fnMap.set( stmt.name, ( ...args ) => {
						if( args.length !== stmt.arguments.length ) {
							throw new Error( `Function ${stmt.name} expected ${stmt.arguments.length} arguments` );
						}
						const oldVars = ctx.varMap;
						let vars = oldVars;
						for( let i = 0; i < args.length; ++i ) {
							const argName = stmt.arguments[ i ];
							vars = vars.set( argName, args[ i ] );
						}
						const result = evaluateExpression( stmt.expression, { ...ctx, varMap: vars } ).value;
						return result;
					} ) };
					return null;
				} ],
				[ 'expressionStatement', ( stmt: ExpressionStatement ) => {
					tick( 1 );
					const result = evaluateExpression( stmt.expression, ctx );
					return result;
				} ],
				[ 'variableDeclarationStatement', ( stmt: VariableDeclarationStatement ) => {
					tick( 1 );
					const name = varAliases.get( stmt.name ) ?? stmt.name.toLowerCase();
					const c = ctx;
					const value = lazy( () => {
						const result = evaluateExpression( stmt.expression, c ).value;
						if( !Number.isFinite( result ) && typeof result !== 'boolean' ) throw new Error( 'result out of range' );
						return result;
					} );
					ctx = { ...ctx, varMap: ctx.varMap.set( name, value ) };
					// dump( logger, { varMap: Object.fromEntries( ctx.varMap.entries() ) } );
					return null;
				} ]
			] );
		} )();

		for( const stmt of prg.statements ) {
			const fn = statementMap.get( stmt.type );
			if( fn == null ) throw new Error( `Unknown statement: ${stmt.type}` );
			const result = fn( stmt );
			if( result != null ) {
				const label = ( stmt.type === 'expressionStatement' ) ? stmt.label : null;
				const f = <div>
					<div>
						{ label ? <><b>{label}</b>:{' '}</> : null }
						{format( result.value )}
					</div>
					<Spoiler>
						{result.display} = {format( result.value )}
					</Spoiler>
				</div>;
				output = join( [ output, f ], <hr/> );
			}
		}

		if( output == null ) throw new Error( 'no output' );

		return output;
	}

	interface EvalCommandParameters {
		readonly query: 'rest';
	}
	parseCommands<EvalCommandParameters>( {
		command: {
			name: '!roll',
			prefix: [ 'roll', 'dice', 'die', 'calc', 'eval', 'calculate', 'evaluate' ],
			parameters: {
				query: {
					type: 'rest'
				}
			}
		},
		normalize: {
			collapse: false,
			stripTags: false,
			trim: false
		},
		filter: commandFilter,
		lifecycle
	} ).pipe(
		concatMap( async ( { parameters: { query }, responseRoute } ) => {
			try {
				const lexed = lex( query );
				// dump( logger, { query, lexed } );
				const parsed = parse( lexed );
				dump( logger, { query, parsed } );
				const result = await evaluate( parsed );
				response$.next( { route: responseRoute, element: <>{result}</> } );
			} catch( ex ) {
				logger.error( ex );
				response$.next( { route: responseRoute, element: <pre><b>Error</b>: {ex.message}</pre> } );
			}
		} ),
		handleErrors( { logger } )
	).subscribe();

	lifecycle.shutdown$
	.subscribe( () => {
		response$.complete();
		lifecycle.done();
	} );
}
