/// <reference path="./minesweeper.d.ts"/>

import { takeUntil, filter, delay, groupBy, concatMap, mergeMap, map, debounceTime, exhaustMap } from 'rxjs/operators';
import { Subject, merge } from 'rxjs';

import assert from 'assert';
import { Fsm } from '~fsm';
import { ThreadPool } from '~threads';
import React, { PureComponent, Fragment } from 'react';
import type { ReactNode } from 'react';
import { renderToStaticMarkup } from 'react-dom/server';
import { rateLimit, tapLog } from 'rxjs-util';
import { parseCommands, query } from '~command';
import { hasher as Hasher } from 'node-object-hash';

import { v4 as uuid } from 'uuid';
import { shuffle } from '~random';
import { Spoiler } from '~components';
import { duration } from 'moment';
import { handleErrors } from '~rx';
import { fromJS, Set, List, Map, Collection } from 'immutable';
import { getDataUrl, lazy } from '~util';
import { shrinkSvg } from '~dom';
import Prando from 'prando';

if( module.hot ) {
	module.hot.accept( [ '../components/index.ts', '../dom.ts' ] );
}

type PointKey = Collection<any, any>;
type Point = readonly [ number, number ];
type Points = readonly Point[];

type ModuleName = 'minesweeper';
type Params = ModuleParamsMap[ ModuleName ];

type GameStates = 'new-game'|'gameplay'|'game-over';
type GameTransitions = 'ready'|'load'|'win'|'lose'|'restart';

interface GameUpdate {
	readonly gameContext: MinesweeperGameContext;
	readonly children: ReactNode;
}

interface MinesweeperProps {
	readonly boardImg: JSX.Element;
	readonly flags: number;
	readonly mines: number;
	readonly hints: number;
	readonly seed: string;
	readonly lockouts: readonly MinesweeperLockout[];
	readonly children?: ReactNode;
}

class Minesweeper extends PureComponent<MinesweeperProps> {
	public constructor( props: MinesweeperProps ) {
		super( props );
	}

	public render() {
		const now = +new Date;
		const { boardImg, children, lockouts, mines, hints, flags, seed } = this.props;
		const formatter = new Intl.NumberFormat( 'en-US', {
			useGrouping: true
		} );

		return <>
			{boardImg}
			<div>
				{children}
			</div>
			<Spoiler summary="Stats">
				{ ( mines > 0 )
				? <><span><b>{formatter.format( mines )}</b> {mines === 1 ? 'mine' : 'mines'}</span>{ ' ' }</>
				: <></> }
				{ ( flags > 0 )
				? <><span><b>{formatter.format( flags )}</b> {flags === 1 ? 'flag' : 'flags'}</span>{ ' ' }</>
				: <></> }
				{ ( hints > 0 )
				? <><span><b>{formatter.format( hints )}</b> {hints === 1 ? 'hint' : 'hints'}</span>{ ' ' }</>
				: <></> }
				<span><b>Seed</b> {seed}</span>
			</Spoiler>
			{ lockouts.length === 0 ? <></> : <Spoiler summary="Lockouts">
				{lockouts.map( ( { username, expires, turns }, i ) => (
					<div key={`d${i}`}>
						<b>{username}</b>
						{ ' is locked out for ' }
						{[
							turns === 1 ? `${turns} turn or `
						:	turns > 1 ? `${turns} turns or `
						:	'',
							( duration( expires - now, 'ms' ) as any ).format( 'h [hours], m [minutes], s [seconds]', { trim: 'all' } )
						].filter( s => !!s )}.
					</div>
				) )}
			</Spoiler> }
		</>;
	}
}

const now = () => +( new Date );
const hasher = Hasher();
const formatter = new Intl.NumberFormat( 'en-US', {
	minimumFractionDigits: 0,
	maximumFractionDigits: 2,
	useGrouping: true
} );

function range( min: number, max: number ) {
	return Array.from( function *() {
		for( let i = min; i < max; ++i ) yield i;
	}() );
}

// function getSubsetPoints( points: Points ): Points[] {
// 	return Array.from(
// 		function *impl( points ): IterableIterator<Points> {
// 			if( points.length <= 1 ) return;
// 			for( let i = 0; i < points.length; ++i ) {
// 				const sub = points.filter( ( _, j ) => i !== j );
// 				yield sub;
// 				yield *impl( sub );
// 			}
// 		}( points )
// 	);
// }

const WIDTH = 20;
const HEIGHT = 20;
const MINE_DENSITY = .24;

interface PointsAroundOptions {
	readonly point: Point;
	readonly width: number;
	readonly height: number;
	readonly size?: number;
	readonly includeSelf?: boolean;
}
function pointsAround( { point, width, height, size = 1, includeSelf = true }: PointsAroundOptions ) {
	return Array.from( function *() {
		for( let x = -size; x <= size; ++x ) {
			const cx = point[ 0 ] + x;
			if( cx < 0 || cx >= width ) continue;
			for( let y = -size; y <= size; ++y ) {
				const cy = point[ 1 ] + y;
				if( cy < 0 || cy >= height ) continue;
				const v = [ cx, cy ] as Point;

				if( includeSelf || !intersects( [ point ], [ v ] ) ) yield v;
			}
		}
	}() );
}

function formatPoint( [ x, y ]: Point ) {
	return 'abcdefghijklmnopqrstuvwxyz'.charAt( y ) + String( x + 1 );
}

function comparePoints( [ x1, y1 ]: Point, [ x2, y2 ]: Point ) {
	if( y1 < y2 ) return -1;
	if( y1 > y2 ) return 1;
	if( x1 < x2 ) return -1;
	if( x1 > x2 ) return 1;
	return 0;
}

function sortPoints( points: Points ): Points {
	return Set( points.map( p => List( p ) ) )
	.map( p => p.toArray() as [ number, number ] as Point )
	.toArray()
	.sort( comparePoints ) as Points;
}

async function renderBoard( { size: { cols, rows }, cells, showCoordinates }: MinesweeperRenderParams ) {
	const radius = 16;
	const squareSize = radius * 2;
	const padding = 4;
	const innerWidth = rows * squareSize;
	const innerHeight = cols * squareSize;
	const outerWidth = innerWidth + padding * 2;
	const outerHeight = innerHeight + padding * 2;

	const svg = <svg xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" viewBox={`0 0 ${outerWidth} ${outerHeight}`}>
	<symbol id="r">
		<svg x="0" y="0" width="32" height="32"><rect fill="#7b7b7b"/></svg>
		<svg x="2" y="2" width="30" height="30"><rect fill="#bdbdbd"/></svg>
	</symbol>
{ ( cells.includes( false ) || cells.includes( 'mine' ) || cells.includes( 'mistake' ) || cells.includes( 'flag' ) ) ?
	<symbol id="h">
		<polygon points="0 0 32 0 0 32" fill="#fff" />
		<polygon points="32 32 32 0 0 32" fill="#7b7b7b" />
		<g transform="scale(2) rotate(45 8 8)">
			<svg x="7.5" y="-4" width="1" height="32">
				<rect fill="#bdbdbd"/>
			</svg>
		</g>
		<svg x="4" y="4" width="24" height="24"><rect fill="#bdbdbd"/></svg>
	</symbol>
: <></> }
{ cells.includes( 'mistake' ) ?
	<symbol id="w">
		<use xlinkHref="#h"/>
		<use xlinkHref="#l"/>
		<circle cx="17" cy="17" r="12" fill="none" stroke="#f00" strokeWidth="2.25"/>
		<g transform="rotate(45 17 17)">
			<svg x="16" y="5" width="3" height="24" fill="#f00">
				<rect />
			</svg>
		</g>
	</symbol>
: <></> }
{ cells.includes( 'flag' ) ?
	<symbol id="f">
		 <use xlinkHref="#h"/>
		<svg x="16" y="14" width="2" height="6"><rect/></svg>
		<svg x="12" y="20" width="8" height="2"><rect/></svg>
		<svg x="8" y="22" width="16" height="4"><rect/></svg>
		<polygon points="8 11 18 6 18 16" fill="#f00" />
	</symbol>
: <></> }
{ ( cells.includes( 'mine' ) || cells.includes( 'explosion' ) || cells.includes( 'mistake' ) ) ?
	<symbol id="l">
		<g transform="rotate(38 17 17)">
			<svg x="16" y="5" width="2" height="24">
				<rect/>
			</svg>
		</g>
		<g transform="rotate(322 17 17)">
			<svg x="16" y="5" width="2" height="24">
				<rect/>
			</svg>
		</g>
		<circle fill="#000" cx="17" cy="17" r="9"/>
		<circle fill="#fff" cx="14" cy="14" r="2"/>
	</symbol>
: <></> }
{ cells.includes( 'mine' ) ?
	<symbol id="m">
		 <use xlinkHref="#h"/>
		 <use xlinkHref="#l"/>
	</symbol>
: <></> }
{ cells.includes( 'explosion' ) ? <>
	<radialGradient id="ge">
		<stop offset="0%" stopColor="#ff00"/>
		<stop offset="30%" stopColor="#ff0f"/>
		<stop offset="70%" stopColor="#ff00"/>
		<stop offset="100%" stopColor="#ff0f"/>
	</radialGradient>
	<symbol id="e">
		<svg x="0" y="0" width="32" height="32">
			<rect fill="#7b7b7b" />
		</svg>
		<svg x="2" y="2" width="30" height="30">
			<rect fill="#f00" />
		</svg>
		<svg x="2" y="2" width="30" height="30">
			<circle fill="url(#ge)" cx="15" cy="15" r="24" className="e" />
		</svg>
		<use xlinkHref="#l" />
	</symbol>
</> : <></> }
		<style>
{range( 1, rows ).map( y => `svg>g:nth-of-type(${y + 1}){transform:translateY(${y * squareSize}px)}` )}
{range( 1, cols ).map( x => `svg>g>g:nth-of-type(${x + 1}){transform:translateX(${x * squareSize}px)}` )}
{`rect{pointer-events:none;width:${squareSize}px;height:${squareSize}px}`}
{`text{dominant-baseline:hanging;color:#000;font:'Courier New','Courier',monospace;text-anchor:middle}`}
{cells.includes( 'explosion' ) ? `@keyframes e{0%{transform:scale(0);opacity:0}25%{opacity:1}75%{opacity:1}100%{transform:scale(6);opacity:0}}` : ''}
{cells.includes( 'explosion' ) ? `.e{transform-origin: 50%;animation: e 6s ease-in infinite}` : ''}
{`.c{font-size:14px;transform:translate(16px, 10px)}`}
{`.n{font-weight:bold;font-size:32px;transform:translate(${radius + 1}px, 4px)}`}
{cells.includes( 1 ) ? `.n1{fill:#00f}` : ''}
{cells.includes( 2 ) ? `.n2{fill:#008000}` : ''}
{cells.includes( 3 ) ? `.n3{fill:#f00}` : ''}
{cells.includes( 4 ) ? `.n4{fill:#000080}` : ''}
{cells.includes( 5 ) ? `.n5{fill:#800000}` : ''}
{cells.includes( 6 ) ? `.n6{fill:#008080}` : ''}
{cells.includes( 7 ) ? `.n7{fill:#000}` : ''}
{cells.includes( 8 ) ? `.n8{fill:#808080}` : ''}
		</style>
		<svg x={padding} y={padding} width={innerWidth} height={innerHeight}>
			{range( 0, rows ).map( y =>
				<g key={`row-${y}`}>{
					range( 0, cols ).map( x => {
						const key = `box-${x}-${y}`;
						const index = y * cols + x;
						const cell = cells[ index ];
						let children: ReactNode;
						if( cell === false ) {
							children = <use xlinkHref="#h"/>;
						} else if( typeof cell === 'number' ) {
							children = <use xlinkHref="#r"/>;
						}
						if( cell !== false ) {
							if( typeof cell === 'number' ) {
								if( cell > 0 ) {
									children = <>{children}<text className={`n n${cell}`}>{cell}</text></>;
								}
							} else if( cell === 'explosion' ) {
								children = <>{children}<use xlinkHref="#e"/></>;
							} else if( cell === 'flag' ) {
								children = <>{children}<use xlinkHref="#f"/></>;
							} else if( cell === 'mistake' ) {
								children = <>{children}<use xlinkHref="#w"/></>;
							} else if( cell === 'mine' ) {
								children = <>{children}<use xlinkHref="#m"/></>;
							}
						}
						if( showCoordinates && ( cell === false ) ) {
							const name = formatPoint( [ x, y ] );
							children = <>{children}<text className={`c`}>{name}</text></>;
						}
						return <g key={key}>{children}</g>;
					} )}
				</g>
			)}
		</svg>
	</svg>;
	const dataUrl = getDataUrl( 'image/svg+xml', await shrinkSvg( svg ) );
	return <img src={dataUrl} width="100%"/>;
}

// eslint-disable-next-line @typescript-eslint/no-unused-vars
function formatPoints( points: Points ) {
	assert.ok( points != null );
	return [ '{', sortPoints( points ).map( p => formatPoint( p ) ).join( ', ' ), '}' ].filter( f => !!f ).join( ' ' );
}

function intersections( x: Points, y: Points ) {
	assert.ok( x != null );
	assert.ok( y != null );
	return x.filter( ( [ xx, xy ] ) =>
		y.some( ( [ yx, yy ] ) => xx === yx && xy === yy )
	);
}

function intersects( x: Points, y: Points ) {
	return intersections( x, y ).length > 0;
}

function exclude( x: Points, y: Points ): Points {
	assert.ok( x, null );
	assert.ok( y, null );
	return x.filter( x => !intersects( [ x ], y ) );
}

function isAdjacent( [ xx, xy ]: Point, [ yx, yy ]: Point ) {
	if( ( xx === yx ) && ( xy === yy ) ) return false;
	if( Math.abs( xx - yx ) > 1 ) return false;
	if( Math.abs( xy - yy ) > 1 ) return false;
	return true;
}

function numAdjacent( pt: Point, mines: Points ) {
	return mines.filter( mine => isAdjacent( pt, mine ) ).length;
}

function getGameStats( gameContext: Pick<MinesweeperGameContext, 'mines'|'revealed'|'flags'|'width'|'height'> ) {
	const hasMines = gameContext.mines.length > 0;
	const isLose = hasMines && intersects( gameContext.revealed, gameContext.mines );
	const ri = intersections( gameContext.mines, gameContext.flags );
	const allPoints = getRange( [ 0, 0 ], [ gameContext.width - 1, gameContext.height - 1 ] );
	const falseFlags = exclude( gameContext.flags, gameContext.mines );
	const isWin = hasMines && !isLose && falseFlags.length === 0 && (
		( ri.length === gameContext.mines.length && ri.length === gameContext.flags.length )
	||	exclude( allPoints, [ ...gameContext.revealed, ...gameContext.mines, ...gameContext.flags ] ).length === 0
	);
	const isGameOver = isLose || isWin;
	return { hasMines, isLose, isWin, isGameOver };
}

async function renderGameContext( { mines, flags, revealed, width, height }: MinesweeperGameContext ) {
	const { isGameOver } = getGameStats( { mines, flags, revealed, width, height } );
	return await renderBoard( {
		showCoordinates: !isGameOver,
		size: { rows: height, cols: width },
		cells: range( 0, height ).flatMap( y =>
			range( 0, width ).map( x => {
				const point = [ x, y ] as Point;
				const isRevealed = intersects( revealed, [ point ] );
				const isMine = intersects( mines, [ point ] );
				const isFlag = intersects( flags, [ point ] );
				if( isRevealed && isMine ) return 'explosion';
				if( isGameOver ) {
					if( isMine ) {
						if( isFlag ) return 'flag';
						else return 'mine';
					}
					if( isFlag ) return 'mistake';
				} else {
					if( isFlag ) return 'flag';
				}
				if( !isRevealed ) return false;
				return numAdjacent( point, mines );
			} )
		)
	} );
}

type GenerateMinesOptions = {
	readonly width: number;
	readonly height: number;
	readonly seed?: string;
	readonly exclude?: Points;
} & ( { readonly count: number; readonly density?: never; } | { readonly density: number; readonly count?: never; } );
function generateMines( options: GenerateMinesOptions ) {
	const { width, height, exclude, seed = uuid() } = options;
	const prng = new Prando( seed );
	let availablePoints = [] as Points;
	for( let x = 0; x < width; ++x ) {
		for( let y = 0; y < height; ++y ) {
			if( exclude != null && intersects( [ [ x, y ] ], exclude ) ) continue;
			availablePoints = [ ...availablePoints, [ x, y ] ];
		}
	}
	let count: number;
	if( options.count == null ) count = width * height * options.density;
	else count = options.count;
	return shuffle( availablePoints, { prng } ).slice( 0, count );
}

function autoReveal( { mines, flags, revealed, width, height }: Pick<MinesweeperGameContext, 'mines'|'flags'|'revealed'|'width'|'height'> ) {
	for( ;; ) {
		const { isGameOver } = getGameStats( { mines, flags, revealed, width, height } );
		if( isGameOver ) break;
		const wasRevealed = revealed;
		for( let x = 0; x < width; ++x ) {
			for( let y = 0; y < width; ++y ) {
				const square = [ x, y ] as Point;
				if( !intersects( wasRevealed, [ square ] ) ) continue;
				if( numAdjacent( square, mines ) > numAdjacent( square, flags ) ) continue;
				revealed = reveal( { width, height, revealed, flags, mines }, exclude( pointsAround( { point: square, width, height, includeSelf: true } ), [ ...wasRevealed, ...flags ] ) );
			}
		}
		if( wasRevealed.length === revealed.length ) break;
	}
	return revealed;
}

function reveal( { mines, flags, revealed, width, height }: Pick<MinesweeperGameContext, 'mines'|'flags'|'revealed'|'width'|'height'>, points: Points ) {
	revealed = [ ...revealed, ...points ];
	let seen = Set<PointKey>();
	( function impl( points: Points ) {
		for( const point of points ) {
			const k = fromJS( point );
			if( seen.has( k ) ) continue;
			seen = seen.add( k );
			( function ( point: Point ) {
				const [ x, y ] = point;
				if( x < 0 || y < 0 || x >= width || y >= height ) return;
				if( intersects( flags, [ point ] ) ) return;
				if( !intersects( revealed, [ point ] ) ) {
					revealed = [ ...revealed, point ];
				}
				if( intersects( mines, [ point ] ) ) return;
				if( numAdjacent( point, mines ) === 0 ) {
					impl( [
						[ x - 1, y - 1 ],
						[ x, y - 1 ],
						[ x + 1, y - 1 ],
						[ x - 1, y ],
						[ x + 1, y ],
						[ x - 1, y + 1 ],
						[ x, y + 1 ],
						[ x + 1, y + 1 ]
					] );
				}
			}( point ) );
		}
	}( points ) );
	return revealed;
}

function getRange( [ startX, startY ]: Point, [ endX, endY ]: Point ) {
	( [ startX, endX ] = [ Math.min( startX, endX ), Math.max( startX, endX ) ] );
	( [ startY, endY ] = [ Math.min( startY, endY ), Math.max( startY, endY ) ] );
	let points = [] as Points;
	for( let y = startY; y <= endY; ++y ) {
	for( let x = startX; x <= endX; ++x ) {
			points = [ ...points, [ x, y ] ];
		}
	}
	return points;
}

export default async function( { moduleName, session, lifecycle, bus, tid, commandFilter, state$, logger }: Params ) {
	if( module.hot ) {
		module.hot.addDisposeHandler( () => {
			lifecycle.shutdown();
		} );
	}

	const isActiveLockout = ( lockout: MinesweeperLockout ) => ( !!lockout ) && ( lockout.turns !== 0 ) && ( lockout.expires > now() );

	interface GetHintOptions {
		readonly width: number;
		readonly height: number;
		readonly mines: Points;
		readonly flags: Points;
		readonly revealed: Points;
		readonly seed: string;
	}
	function getHint( { width, height, mines, flags, revealed, seed }: GetHintOptions ) {
		if( mines.length === 0 ) return;
		if( intersects( mines, revealed ) ) return;
		const allSquares = getRange( [ 0, 0 ], [ width - 1, height - 1 ] );
		const nonAdjacentMines = mines.filter( m => numAdjacent( m, revealed ) === 0 );
		const prng = new Prando( seed );
		const possibleMines = [
			...shuffle( exclude( mines, [ ...flags, ...revealed, ...nonAdjacentMines ] ), { prng } ),
			...shuffle( exclude( nonAdjacentMines, [ ...flags, ...revealed ] ), { prng } )
		];
		const possibleSafe = shuffle(
			allSquares
			.filter( r => !intersects( [ ...revealed, ...flags, ...mines ], [ r ] ) )
			.filter( r => [ 0, numAdjacent( r, allSquares ) ].includes( numAdjacent( r, mines ) ) ),
			{ prng }
		);
		let message = <><h2>Hint:</h2></>;
		const fn = [
				possibleSafe.length > 0 ? () => {
					const safe = possibleSafe[ 0 ];
					assert.ok( safe != null );
					revealed = reveal( { mines, flags, revealed, width, height }, [ safe ] );
					message = <>{message}<p>Revealed: <b>{formatPoint( safe )}</b>.</p></>;
				} : null,
				possibleMines.length > 0 ? () => {
					const mine = possibleMines[ 0 ];
					assert.ok( mine != null );
					flags = [ ...flags, mine ];
					message = <>{message}<p>Flagged: <b>{formatPoint( mine )}</b>.</p></>;
				} : null
			]
			.filter( f => f != null )[ 0 ];
		assert.ok( fn != null );
		fn();
		return { revealed, flags, message };
	}

	const lastActivity$ = new Subject<string>();
	// const inactiveRevealTime = 30 * 60 * 1000;
	// lastActivity$.pipe(
	// 	debounceTime( inactiveRevealTime ),
	// 	filter( roundId => roundId != null ),
	// 	takeUntil( lifecycle.shutdown$ )
	// ).subscribe( roundId => {
	// 	const now = +new Date;
	// 	const gameContext = state$.value?.gameContext;
	// 	if( gameContext?.roundId !== roundId || gameContext?.lastGuess == null ) return;
	// 	if( ( now - inactiveRevealTime ) <= gameContext.lastGuess ) return;
	// 	if( gameStateFsm.state !== 'gameplay' ) return;

	// 	let { history } = gameContext;

	// 	history = [ ...history, { command: 'hint', point: null, force: true, issuer: null } ];

	// 	getHint( gameContext );
	// } );

	const response$ = new Subject<MinesweeperResponse>();
	function response( element: () => JSX.Element|Promise<JSX.Element>, route: ResponseRoute ) {
		response$.next( { element: lazy( () => Promise.resolve( element() ) ), route } );
	}

	response$.pipe(
		groupBy( ( { route } ) => hasher.hash( route ) ),
		mergeMap( response => response.pipe(
			debounceTime( 500 )
		) ),
		concatMap( async ( { element, ...props } ) => ( { ...props, message: renderToStaticMarkup( await element() ) } ) ),
		handleErrors( { logger } ),
		takeUntil( lifecycle.shutdown$ )
	)
	.subscribe( ( { message, route } ) => {
		bus.next( { type: 'response', message, route } );
	} );

	const gameUpdate$ = new Subject<GameUpdate>();
	async function updateGameContext( ctx: Partial<MinesweeperGameContext>, children = [] as ReactNode ) {
		const gameState = state$?.value ?? {} as MinesweeperState;
		let gameContext = { ...( gameState.gameContext ), ...ctx } as MinesweeperGameContext;
		if( gameContext.lockouts == null ) gameContext = { ...gameContext, lockouts: [] };
		lastActivity$.next( gameContext.roundId );
		state$.next( { ...gameState, gameContext } );
		gameUpdate$.next( { gameContext, children } );
	}

	gameUpdate$
	.pipe(
		map( gameUpdate => {
			const { children, gameContext } = gameUpdate;
			const { lockouts = [], seed, hints } = gameContext;
			return async () => {
				const boardImg = await renderGameContext( gameContext );
				return <Minesweeper boardImg={boardImg} mines={gameContext.mines.length} flags={gameContext.flags.length} lockouts={lockouts.filter( isActiveLockout )} hints={hints} seed={seed}>{children}</Minesweeper>;
			};
		} ),
		// groupBy( ( { gameContext: { roundId } } ) => roundId ),
		// mergeMap( gameUpdate => gameUpdate.pipe(
		// 	bufferDebounceTime( 100 ),
		// 	map( gameUpdates => {
		// 		try {
		// 			const { gameContext } = gameUpdates[ gameUpdates.length - 1 ];
		// 			const { lockouts, seed, hints } = gameContext;
		// 			const boardImg = renderGameContext( gameContext );
		// 			const children = gameUpdates.map( ( { children }, i ) => <div key={i}>{children}</div> );
		// 			return <Minesweeper boardImg={boardImg} mines={gameContext.mines.length} flags={gameContext.flags.length} lockouts={lockouts.filter( isActiveLockout )} hints={hints} seed={seed}>{children}</Minesweeper>;
		// 		} catch( ex ) {
		// 			logger.fatal( ex );
		// 			throw ex;
		// 		}
		// 	} )
		// ) ),
		handleErrors( { logger } ),
		takeUntil( lifecycle.shutdown$ )
	)
	.subscribe( element => {
		response( element, { type: 'thread', tid } );
	} );

	function isLockedOut( issuer: CommandIssuer ) {
		if( !issuer ) return false;
		if( issuer.roles.includes( 'admin' ) ) return false;
		if( issuer.type !== 'user' ) return false;
		const { uid } = issuer;
		const gameContext = state$.value.gameContext;
		if( !gameContext ) return false;
		return gameContext.lockouts
		.filter( isActiveLockout )
		.some( lockout => lockout.uid === uid );
	}

	function generateLockout( issuer: CommandIssuer, lockout: Omit<MinesweeperLockout, keyof CommandIssuerUser> ) {
		if( issuer?.type !== 'user' ) return null;
		if( issuer.roles.includes( 'admin' ) ) return null;
		const { uid, username, userslug } = issuer;
		return { ...lockout, uid, username, userslug };
	}

	const gameStateFsm = new Fsm<GameStates, GameTransitions>( {
		states: {
			'new-game': {
				transitions: {
					ready: 'gameplay'
				}
			},
			gameplay: {
				transitions: {
					lose: 'game-over',
					win: 'game-over'
				}
			},
			'game-over': {
				transitions: {}
			}
		},
		transitions: {
			restart: 'new-game',
			load: 'gameplay'
		}
	} );

	gameStateFsm.states.pipe( takeUntil( lifecycle.shutdown$ ), tapLog() ).subscribe();
	gameStateFsm.transitions.pipe( takeUntil( lifecycle.shutdown$ ), tapLog() ).subscribe();

	interface NewGameOptions {
		readonly seed: string;
		readonly issuer: CommandIssuer;
	}
	function newGame( { seed, issuer }: NewGameOptions ) {
		const roundId = uuid();
		seed = seed ?? uuid();
		updateGameContext( { roundId, seed, width: WIDTH, height: HEIGHT, mines: [], flags: [], revealed: [], hints: 0, history: [ { command: 'new-game', seed, issuer } ] }, <p>New game.</p> );
		if( gameStateFsm.state === 'new-game' ) gameStateFsm.transition( 'ready' );
		else gameStateFsm.transition( 'load' );
	}

	gameStateFsm.states
	.pipe(
		filter( ( { enter } ) => enter === 'new-game' ),
		takeUntil( lifecycle.shutdown$ )
	).subscribe( () => {
		newGame( { seed: null, issuer: null } );
		gameStateFsm.transition( 'ready' );
	} );

	gameStateFsm.transitions
	.pipe(
		filter( t => t === 'load' ),
		takeUntil( lifecycle.shutdown$ )
	).subscribe( () => {
		const gameContext = state$.value?.gameContext;
		if( !gameContext || gameContext.mines.length === 0 ) return;
		const { isGameOver } = getGameStats( gameContext );
		if( isGameOver ) {
			gameStateFsm.transition( 'restart' );
		}
	} );

	gameStateFsm.states
	.pipe(
		filter( ( { enter } ) => enter === 'game-over' ),
		delay( 6000 ),
		filter( () => gameStateFsm.state === 'game-over' ),
		takeUntil( lifecycle.shutdown$ )
	).subscribe( () => {
		lastActivity$.next( null );
		gameStateFsm.transition( 'restart' );
	} );

	const gameOverTimeout = 1000 * 60 * 60;
	const slowPoke = <img src="/assets/uploads/files/1567795880992-7538621b-e3c4-43a0-959f-2860eb885886-image.png"/>;
	const hanzo = <p>:hanzo:</p>;
	const spoopy = <p>:face_screaming_in_fear:</p>;

	function minesweeperChatCommand
	<TParameterTypeMap extends { [ TParameterName in TParameterNames ]: CommandParameterType; },
	TParameterNames extends keyof TParameterTypeMap & string = keyof TParameterTypeMap & string>
	( cd: CommandDefinition<TParameterTypeMap, TParameterNames> & { readonly prefix: readonly string[]; } ) {
		return merge(
			parseCommands<TParameterTypeMap, TParameterNames>( {
				command: {
					...cd,
					query: {
						$and: [
							query.inThread( tid ),
							cd.query || { $value: true }
						]
					},
					prefix: [ ...cd.prefix, ...cd.prefix.map( p => `mine sweeper ${p}` ) ]
				},
				lifecycle,
				filter: commandFilter
			} ),
			parseCommands<TParameterTypeMap, TParameterNames>( {
				command: {
					...cd,
					query: {
						$and: [
							query.inChat(),
							cd.query || { $value: true }
						]
					},
					prefix: [ ...cd.prefix.map( p => `mine sweeper ${p}` ) ]
				},
				lifecycle,
				filter: commandFilter
			} )
		);
	}

	minesweeperChatCommand( {
		query: {
			$and: [
				query.fromUser(),
				{ $not: query.fromRole( 'admin' ) }
			]
		},
		prefix: [ 'cheat', 'lock out', 'lock', 'un lock' ],
		parameters: 'ignore'
	} )
	.subscribe( ( { issuer } ) => {
		const gameContext = state$.value?.gameContext;
		if( !gameContext ) return;
		const lockout = generateLockout( issuer, { expires: Infinity } );
		if( !lockout ) return;
		const { uid, userslug } = lockout;
		updateGameContext( {
			lockouts:
				gameContext.lockouts
				.filter( l => l.uid !== uid )
				.concat( lockout )
				.filter( isActiveLockout )
		}, <p>
			@{userslug} tried to <b>cheat</b>.
		</p> );
	} );

	interface LockOutCommandParameters {
		readonly user: 'user';
	}
	minesweeperChatCommand<LockOutCommandParameters>( {
		prefix: [ 'lock out', 'lock' ],
		parameters: {
			user: {
				type: 'user',
				position: 0,
				required: true
			}
		}
	} )
	.subscribe( async ( { parameters: { user } } ) => {
		const gameContext = state$.value?.gameContext;
		if( !gameContext ) return;
		if( gameStateFsm.state !== 'gameplay' ) return;

		const lockouts =
			gameContext.lockouts
			.filter( l => l.uid !== user.uid )
			.concat( generateLockout( { type: 'user', ...user } as CommandIssuerUser, { expires: Infinity } ) )
			.filter( isActiveLockout );
		updateGameContext( { lockouts } );
	} );

	minesweeperChatCommand( {
		prefix: [ 'un lock all', 'un lock everyone' ],
		parameters: 'ignore'
	} )
	.subscribe( () => {
		const gameContext = state$.value?.gameContext;
		if( !gameContext ) return;
		if( gameStateFsm.state !== 'gameplay' ) return;
		if( gameContext.lockouts.length === 0 ) return;
		updateGameContext( { lockouts: [] } );
	} );

	interface UnlockCommandParameters {
		readonly user: 'user';
	}
	minesweeperChatCommand<UnlockCommandParameters>( {
		prefix: [ 'un lock' ],
		parameters: {
			user: {
				type: 'user',
				position: 0,
				required: true
			}
		}
	} )
	.subscribe( ( { parameters: { user: { uid } } } ) => {
		const gameContext = state$.value?.gameContext;
		if( !gameContext ) return;
		if( gameStateFsm.state !== 'gameplay' ) return;
		if( !gameContext.lockouts.some( l => l.uid === uid ) ) return;
		const lockouts = gameContext.lockouts.filter( l => l.uid !== uid ).filter( isActiveLockout );
		updateGameContext( { lockouts } );
	} );

	const pointPattern = /^([a-z])[- ]?([0-9]{1,2})(?:\s*[-:]\s*(?:([a-z])[- ]?)?([0-9]{1,2}))?$/i;
	function parsePoints( pointStr: string, size?: { readonly width: number; readonly height: number; } ) {
		function parseRange( pointStr: string ) {
			const squareParts = pointPattern.exec( pointStr );
			if( squareParts == null ) return [];
			function parsePoint( x: string, y: string ) {
				if( x == null || y == null ) return null;
				const xCoord = parseInt( x, 10 ) - 1;
				const yCoord = 'abcdefghijklmnopqrstuvwxyz'.indexOf( y.toLowerCase() );
				if( isNaN( xCoord ) || isNaN( yCoord ) || xCoord < 0 || yCoord < 0 || xCoord >= size?.width || yCoord >= size?.height ) return null;
				return [ xCoord, yCoord ] as Point;
			}

			const [ , y1, x1, y2, x2 ] = squareParts;
			const point1 = parsePoint( x1, y1 );
			const point2 = parsePoint( x2, y2 ?? y1 ) ?? point1;
			assert.notEqual( point1, null );
			assert.notEqual( point2, null );
			return getRange( point1, point2 );
		}
		const p = /([a-z])[- ]?([0-9]{1,2})(?:\s*[-:]\s*(?:[a-z][- ]?)?[0-9]{1,2})?/gi;
		let points = [] as Points;
		let m: RegExpMatchArray;
		while( ( m = p.exec( pointStr ) ) != null ) {
			points = [ ...points, ...parseRange( m[ 0 ].trim() ) ];
		}
		return points;
	}


	interface SolveWorkerRequest {
		readonly width: number;
		readonly height: number;
		readonly mines: Points;
		readonly flags: Points;
		readonly revealed: Points;
		readonly subset?: Points;
	}
	type SolveWorkerResult = {
		readonly values: readonly ( readonly [ Point, ( 0|1|null ) ] )[];
		readonly probabilities: readonly ( readonly [ Point, number ] )[];
	}
	const solveWorkers = new ThreadPool<SolveWorkerRequest, SolveWorkerResult>( {
		src: 'minesweeper/solve.worker',
		threadCount: 1,
		logger,
		lifecycle
	} );

	interface SolveOptions {
		readonly width: number;
		readonly height: number;
		readonly mines: Points;
		readonly flags: Points;
		readonly revealed: Points;
		readonly subset?: Points;
	}
	async function solve( { width, height, mines, flags, revealed, subset }: SolveOptions ) {
		return { flags: [], revealed: [], probabilities: [] };
		// const wasFlagged = flags;
		// const wasRevealed = revealed;
		// const { values, probabilities } = await solveWorkers.request( { width, height, mines, flags, revealed, subset } );

		// flags = values.filter( ( [ , value ] ) => value === 1 ).map( ( [ point ] ) => point );
		// flags = sortPoints( exclude( flags, [ ...wasFlagged, ...wasRevealed ] ) );
		// revealed = values.filter( ( [ , value ] ) => value === 0 ).map( ( [ point ] ) => point );
		// revealed = sortPoints( exclude( revealed, [ ...wasFlagged, ...wasRevealed ] ) );

		// return { flags, revealed, probabilities };
	}



	interface SolveCommandParameters {
		subset: 'string';
	}
	minesweeperChatCommand<SolveCommandParameters>( {
		prefix: [ 'solve' ],
		parameters: {
			subset: {
				alias: [ 'range' ],
				type: 'string',
				required: false,
				position: 0
			}
		}
	} )
	.pipe(
		rateLimit( 10 ),
		filter( ( { issuer } ) => !isLockedOut( issuer ) ),
		exhaustMap( async ( { parameters, responseRoute } ) => {
			let subsetRange: Points;
			if( parameters.subset != null ) subsetRange = parsePoints( parameters.subset );
			const gameContext = state$.value?.gameContext;
			if( gameContext == null ) return;
			if( gameStateFsm.state !== 'gameplay' ) {
				response( () => slowPoke, responseRoute );
				return;
			}

			let { flags, revealed, probabilities } = await solve( { ...gameContext, subset: subsetRange } );

			let messages = [] as readonly ReactNode[];
			if( revealed.length > 0 ) {
				messages = [ ...messages, `reveal ${revealed.map( formatPoint ).join( ' ' )}` ];
			}
			if( flags.length > 0 ) {
				messages = [ ...messages, `flag ${flags.map( formatPoint ).join( ' ' )}` ];
			}

			if( probabilities.length > 0 ) {
				probabilities = [ ...probabilities ].sort( ( [ pt1, p1 ], [ pt2, p2 ] ) => ( p1 === p2 ) ? comparePoints( pt1, pt2 ) : p1 - p2 );
				messages = [ ...messages,
					<Spoiler key="Probabilities" summary="Probabilities">
						{probabilities.map( ( [ point, probability ], index ) => <div key={index}>
							{formatPoint( point )}
							{' mine probability: '}
							<b>
								{formatter.format( probability * 100 )}{'%'}
							</b>
						</div> )}
					</Spoiler>
				];
			}

			response( () => messages.length > 0 ? <>{messages.map( ( m, i ) => <Fragment key={i}>{m}{'\n'}</Fragment> )}</> : <>:person_shrugging:</>, responseRoute );
		} ),
		handleErrors( {
			logger
		} )
	)
	.subscribe();

	const hintTimeout = 60 * 60 * 1000;

	interface SquareCommandParameters {
		readonly force: 'boolean';
		readonly tokens: 'rest';
	}

	const commandTokenPatterns = {
		flag: /^(?:f(?:lag)?|mark)$/i,
		hint: /^(?:h(?:int)?)$/i,
		reveal: /^(?:r(?:eveal)?|show|investigate|open|poke|stab)$/i,
		unflag: /^(?:u(?:n[- ]?(?:flag|mark))?|clear)$/i
	} as Readonly<Record<MinesweeperCommand, RegExp>>;

	parseCommands<SquareCommandParameters>( {
		command: {
			query: query.inThread( tid ),
			parameters: {
				force: {
					type: 'boolean',
					default: false
				},
				tokens: {
					type: 'rest',
					required: true
				}
			}
		},
		lifecycle,
		filter: commandFilter,
		priority: -1
	} ).pipe(
		map( c => ( { ...c, roundId: state$.value?.gameContext.roundId } ) ),
		rateLimit( 10 ),
		concatMap( ( { parameters, ...c } ) => {
			const gameContext = state$.value?.gameContext;
			if( !gameContext ) return [];
			const { width, height } = gameContext;
			return Array.from( ( function *() {
				let command: MinesweeperCommand;
				const tokens = parameters.tokens.split( /\s+/g );
				const tokenMap = Map( Object.entries( commandTokenPatterns ) as [ MinesweeperCommand, RegExp ][] );
				for( const token of tokens ) {
					let isCommand = false;
					for( const [ cmd, re ] of tokenMap.entries() ) {
						if( re.test( token ) ) {
							if( cmd === 'hint' ) {
								yield { ...c, parameters: { command: cmd, square: null, force: parameters.force } };
							} else {
								command = cmd;
							}
							isCommand = true;
							break;
						}
					}
					if( command == null || isCommand ) continue;
					for( const square of parsePoints( token, { width, height } ) ) {
						yield {
							...c,
							parameters: { command, square, force: parameters.force }
						};
					}
				}
			}() ) );
		} ),
		filter( ( { issuer } ) => !isLockedOut( issuer ) ),
		concatMap( async ( { parameters: { command, square, force }, timestamp, issuer, responseRoute, roundId } ) => {
			const gameContext = state$.value?.gameContext;
			if( !gameContext ) return;
			const { width, height } = gameContext;
			const seed = gameContext.seed ?? gameContext.roundId;
			let { mines, flags, revealed, lockouts, history, hints } = gameContext;

			const beforeStats = getGameStats( gameContext );

			if( gameStateFsm.state !== 'gameplay' || beforeStats.isGameOver || gameContext.roundId !== roundId ) {
				if( timestamp > gameContext.lastGuess ) {
					response( () => slowPoke, responseRoute );
				}
				return;
			}

			switch( command ) {
			case 'reveal':
				if( intersects( revealed, [ square ] ) ) {
					if( timestamp > gameContext.lastGuess ) {
						response( () => hanzo, responseRoute );
					}
					return;
				}

				if( !force && intersects( flags, [ square ] ) ) {
					if( timestamp > gameContext.lastGuess ) {
						response( () => spoopy, responseRoute );
					}
					return;
				}
				break;
			case 'flag':
				if( intersects( [ ...revealed, ...flags ], [ square ] ) ) {
					if( timestamp > gameContext.lastGuess ) {
						response( () => hanzo, responseRoute );
					}
					return;
				}
				break;
			case 'unflag':
				if( !intersects( flags, [ square ] ) ) {
					if( timestamp > gameContext.lastGuess ) {
						response( () => hanzo, responseRoute );
					}
					return;
				}
				break;
			case 'hint': break;
			default: assert.fail( `Unexpected command: ${command}` );
			}

			if( [ 'reveal', 'hint' ].includes( command ) ) {
				if( !beforeStats.hasMines ) {
					let exclude: Points = [];
					if( square != null ) exclude = pointsAround( { point: square, width, height, includeSelf: true } );
					mines = generateMines( { width, height, exclude, density: MINE_DENSITY, seed } );
				}
			}

			let transition = null as GameTransitions;
			let message = <></> as ReactNode;
			let lockout = null as MinesweeperLockout;
			let lastGuess = timestamp;

			switch( command ) {
			case 'reveal':
				revealed = reveal( { width, height, mines, flags, revealed }, [ square ] );
				history = [ ...history, { command, square, force, issuer } ];
				break;
			case 'flag':
				flags = [ ...flags, square ];
				history = [ ...history, { command, square, force, issuer } ];
				break;
			case 'unflag':
				flags = flags.filter( f => !intersects( [ f ], [ square ] ) );
				history = [ ...history, { command, square, issuer } ];
				break;
			case 'hint': {
				const hint = getHint( { width, height, mines, flags, revealed, seed } );
				if( beforeStats.hasMines ) {
					const solveResult = await solve( { width, height, mines, flags, revealed } );
					if( solveResult.flags.length === 0 && solveResult.revealed.length === 0 ) {
						message = <>{message}<p>This one&apos;s on me.</p></>;
					} else {
						if( !force ) {
							response( () => <>Are you sure you want a hint?  (Use `--force`)</>, responseRoute );
							return;
						}
						lockout = generateLockout( issuer, { expires: now() + hintTimeout } );
					}
				}
				( { revealed, flags } = hint );
				message = <>{message}{hint.message}</>;
				history = [ ...history, { command, force, issuer } ];
				lastGuess = null;
				++hints;
				break;
			}
			default: assert.fail( `Unexpected command: ${command}` );
			}
			flags = exclude( flags, revealed );

			revealed = autoReveal( { mines, flags, revealed, width, height } );

			flags = exclude( flags, revealed );

			const { isLose, isWin } = getGameStats( { mines, flags, revealed, width, height } );

			if( isLose ) {
				assert.notStrictEqual( command, 'hint' );
				assert.strictEqual( isWin, false );

				message = <>:collision: You lose!</>;
				if( issuer.type === 'user' ) {
					const shotInTheDark =
						intersections( mines, revealed )
						.some( m => numAdjacent( m, [ ...flags, ...revealed ] ) === 0 );

					if( shotInTheDark ) {
						message = <>{message}<p>:giggity: I sunk @{issuer.userslug}&apos;s :ship:!</p></>;
					}
				}
				transition = 'lose';
				lockout = generateLockout( issuer, { turns: 5, expires: now() + gameOverTimeout } );
			} else if( isWin ) {
				if( command === 'hint' ) {
					message = <>{message}<p>:fireworks: I won!</p></>;
				} else {
					message = <>{message}<p>:fireworks: You win!</p></>;
					lockouts = [];
				}
				transition = 'win';
			}

			updateGameContext( {
				flags,
				revealed,
				mines,
				lastGuess,
				history,
				hints,
				lockouts: lockouts.map( lockout => ( {
					...lockout,
					...( lockout?.turns > 0 ? { turns: lockout.turns - 1 } : {} )
				} ) )
				.concat( lockout )
				.filter( isActiveLockout )
			},
				<div>
					{ message }
				</div>
			);
			if( transition ) gameStateFsm.transition( transition );
		} ),
		handleErrors( { logger } )
	).subscribe();

	interface MinesweeperNewGameCommandParameters {
		readonly seed: 'string';
	}
	minesweeperChatCommand<MinesweeperNewGameCommandParameters>( {
		prefix: [ 'new game', 'restart' ],
		parameters: {
			seed: {
				type: 'string',
				position: 0,
				required: false
			}
		}
	} )
	.subscribe( ( { parameters: { seed = null }, issuer } ) => {
		newGame( { seed, issuer } );
	} );

	interface MinesweeperLoadStateCommandParameters {
		readonly name: 'string';
	}
	minesweeperChatCommand<MinesweeperLoadStateCommandParameters>( {
		prefix: [ 'load state', 'load', 'restore state', 'restore' ],
		parameters: {
			name: {
				type: 'string',
				position: 0,
				required: false,
				default: 'default'
			}
		}
	} )
	.subscribe( ( { parameters: { name }, responseRoute } ) => {
		const state = state$.value;
		if( state == null ) return;

		const states = state.savedGames ?? {};
		if( !{}.hasOwnProperty.call( states, name ) ) {
			response( () => <>No game saved in slot `{name}`.</>, responseRoute );
			return;
		}
		updateGameContext( state.savedGames[ name ], <>Game loaded from slot `{name}`.</> );
	} );

	interface MinesweeperSaveStateCommandParameters {
		readonly name: 'string';
	}
	minesweeperChatCommand<MinesweeperSaveStateCommandParameters>( {
		prefix: [ 'save state', 'save' ],
		parameters: {
			name: {
				type: 'string',
				position: 0,
				required: false,
				default: 'default'
			}
		}
	} )
	.subscribe( ( { parameters: { name }, responseRoute } ) => {
		const state = state$.value;
		if( state == null ) return;
		const { lockouts, ...gameContext } = state.gameContext;

		let savedGames = state.savedGames ?? {};
		savedGames = { ...savedGames, [ name ]: gameContext };
		state$.next( { ...state, savedGames } );
		response( () => <>Game saved in slot `{name}`.</>, responseRoute );
	} );

	interface MinesweeperDeleteStateCommandParameters {
		readonly name: 'string';
	}
	minesweeperChatCommand<MinesweeperDeleteStateCommandParameters>( {
		prefix: [ 'delete state', 'delete' ],
		parameters: {
			name: {
				type: 'string',
				position: 0,
				required: false,
				default: 'default'
			}
		}
	} )
	.subscribe( ( { parameters: { name }, responseRoute } ) => {
		const state = state$.value;
		if( state == null ) return;

		const states = state.savedGames ?? {};
		if( !{}.hasOwnProperty.call( states, name ) ) {
			response( () => <>No game saved in slot `{name}`.</>, responseRoute );
			return;
		}
		const { [ name ]: savedGame, ...savedGames } = state.savedGames ?? {};
		state$.next( { ...state, savedGames } );
		response( () => <>Game deleted from slot `{name}`.</>, responseRoute );
	} );

	interface MinesweeperHistoryGameCommandParameters {}
	minesweeperChatCommand<MinesweeperHistoryGameCommandParameters>( {
		prefix: [ 'history' ],
		parameters: {}
	} )
	.subscribe( ( { responseRoute } ) => {
		const gameContext = state$.value?.gameContext;
		if( gameContext == null ) return;
		const { history } = gameContext;

		let historyLines = [] as readonly string[];
		let prevCommand: MinesweeperHistoryEntry['command'];

		function appendLine( line: string ) {
			historyLines = [ ...historyLines, line ];
		}
		let lineBuffer = '';
		function flushBuffer() {
			if( lineBuffer ) {
				appendLine( lineBuffer );
				lineBuffer = '';
			}
	}
		for( const record of history ) {
			switch( record.command ) {
			case 'new-game':
				flushBuffer();
				appendLine( `new-game --seed="${record.seed}"` );
				break;
			case 'flag':
			case 'unflag':
			case 'reveal':
				if( record.command === prevCommand ) {
					lineBuffer = lineBuffer + ' ' + formatPoint( record.square );
				} else {
					flushBuffer();
					lineBuffer = record.command + ' ' + formatPoint( record.square );
				}
				break;
			case 'hint':
				flushBuffer();
				appendLine( 'hint' );
				break;
			default: assert.fail();
			}
			prevCommand = record.command;
		}
		flushBuffer();

		response( () => <pre>
			{historyLines.map( ( h, i ) => <Fragment key={i}>{h}{'\n'}</Fragment> )}
		</pre>, responseRoute );
	} );

	minesweeperChatCommand( {
		prefix: [ 'cheat' ]
	} )
	.pipe( concatMap( async ( { responseRoute } ) => {
		const gameContext = state$.value?.gameContext;
		if( !gameContext ) return;

		const img = await renderBoard( {
			size: { rows: gameContext.height, cols: gameContext.width },
			cells: range( 0, gameContext.height ).flatMap( y =>
				range( 0, gameContext.width ).map( x => {
					const point = [ x, y ] as Point;
					if( intersects( gameContext.mines, [ point ] ) ) {
						if( intersects( gameContext.revealed, [ point ] ) ) return 'explosion';
						if( intersects( gameContext.flags, [ point ] ) ) return 'flag';
						else return 'mine';
					}
					if( intersects( gameContext.flags, [ point ] ) ) return 'mistake';
					if( intersects( gameContext.revealed, [ point ] ) ) return numAdjacent( point, gameContext.mines );
					else return false;
				} )
			),
			showCoordinates: false
		} );
		const message = renderToStaticMarkup( img );
		bus.next( { type: 'response', message, route: responseRoute } );
	} ) )
	.subscribe();

	minesweeperChatCommand( {
		prefix: [ 'print' ],
		parameters: 'ignore'
	} )
	.subscribe( () => {
		updateGameContext( {} );
	} );

	minesweeperChatCommand( {
		query: query.fromRole( 'admin' ),
		prefix: [ 'render' ]
	} )
	.pipe( concatMap( async ( { responseRoute } ) => {
		const { gameContext } = state$.value;
		const img = await renderGameContext( gameContext );
		const message = renderToStaticMarkup( img );
		bus.next( { type: 'response', message, route: responseRoute } );
	} ) )
	.subscribe();

	if( state$.value ) {
		const gameContext = state$.value.gameContext;
		if( gameContext ) {
			gameStateFsm.transition( 'load' );
			lastActivity$.next( gameContext.roundId );
		}
	}

	if( gameStateFsm.state === null ) {
		gameStateFsm.transition( 'restart' );
	}

	lifecycle.shutdown$
	.subscribe( async () => {
		gameUpdate$.subscribe();
		response$.complete();
		gameStateFsm.complete();
		lastActivity$.complete();
		lifecycle.done();
	} );
}
