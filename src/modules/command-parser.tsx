import * as api from '~nodebb/api';

import React from 'react';

import { filter, map, mergeMap, toArray } from 'rxjs/operators';
import { Observable, merge, never, from } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { roles as roleMap, baseUrl } from '~data/config.yaml';
import { registeredCommands } from '~command';
import { JSDOM } from 'jsdom';
import { normalize, allExceptIndex, allExceptValue, dump } from '~util';
import { AsyncCache } from '~async-cache';

import assert from 'assert';
import { evalQuery } from '~query';

import { hasher as Hasher } from 'node-object-hash';
import { TimeSpan } from '~time-span';
import { handleErrors } from '~rx';
import { FriendlyError } from '~friendly-error';
import { renderToStaticMarkup } from 'react-dom/server';

const hasher = Hasher();

if( module.hot ) {
	module.hot.accept( [ '../components/index.ts', '../query.ts' ] );
}

type ModuleName = 'command-parser';
type Params = ModuleParamsMap[ ModuleName ];

function getLines( raw: string ) {
	return ( raw ?? '' )
		.split( /\n/g )
		.filter( s => !/^@[-_\w\d]+\s+said/i.test( s ) )
		.filter( s => !/^[->*]/i.test( s ) )
		.map( s => s.replace( /[\u0000-\u0008\u000b-\u001f\u007f\u0080-\u009f\u00ad\u180e\u200b-\u200d\u2028\u2029\u2060\ufeff]/gi, '' ) ) // eslint-disable-line no-control-regex
		.map( s => s.replace( /^\s*(@[-_\w\d]+\s*)+/gi, '' ).trim() )
		.filter( s => !!s );
}

function addHash<T extends object>( obj: T ): T & { readonly hash: string; } {
	let hash = null as string;
	return { ...obj, get hash() {
		return hash = hash ?? hasher.hash( obj );
	} };
}

function getUserRoles( userInfo: Omit<UserInfo, 'roles'> ) {
	return getRoles( addHash( { type: 'user', ...userInfo } ) );
}

function getMinecraftRoles( username: string ) {
	return getRoles( addHash( { type: 'minecraft', username } ) );
}

function getRoles( commandIssuer: Omit<CommandIssuer, 'roles'> ) {
	const roleInfo = ( Object.entries( roleMap ) as readonly [ string, Query<CommandIssuer> ][] );
	return ( function getRolesImpl( u: Omit<CommandIssuer, 'roles'>, maxDepth: number ) {
		if( maxDepth <= 0 ) return [];
		const commandIssuer = { ...u,
			get roles() {
				return getRolesImpl( u, maxDepth - 1 );
			}
		} as CommandIssuer;
		return roleInfo
		.filter( ( [ , query ] ) => evalQuery( commandIssuer, query ) )
		.map( ( [ roleName ] ) => roleName )
		.sort();
	}( commandIssuer, 5 ) ).sort();
}

function generateChompRegexp( ...str: readonly string[] ) {
	// TODO: permute
	const substitutions = {
		automatic: [ 'auto' ],
		delete: [ 'remove', 'del', 'rm' ],
		and: [ '\\&\\&', '\\&', 'n' ],
		or: [ '\\|\\|', '\\|' ],
		to: [ '2' ],
		too: [ '2' ],
		2: [ 'too', 'to', 'two' ],
		the: [ 'teh' ],
		integer: [ 'int' ],
		random: [ 'ra?nd' ],
		string: [ 'str' ]
	} as Record<string, string[]>;

	function getSubstitutions( word: string ) {
		return [ word, ...( substitutions[ word ] ?? [] ) ];
	}

	return new RegExp(
	'^[:!\\[]*(' +
	str.map( s =>
		s
		.split( ' ' )
		.map( w => '(?:' + getSubstitutions( w ).join( '|' ) + ')' )
		.join( '[-_.:\\s\'\\|\\,\\|]*' )
	).join( '|' )
	+ ')[:!\\]]*(?:\\s+|$)', 'i' );
}

function chomp( str: string, ...prefixes: readonly string[] ) {
	const re = generateChompRegexp( ...prefixes );
	const match = re.exec( str );
	if( !match ) return [];
	return [ match[ 1 ], str.slice( match[ 0 ].length ) ];
}

function isValidParameterName( str: string ) {
	if( /^(?:https?|ftp|javascript|mailto)(?::|$)/i.test( str ) ) return false;
	str = str.replace( /^--?/, '' );
	if( !/^[a-z][-_a-z0-9]*$/i.test( str ) ) return false;
	return true;
}

export default async function( { moduleName, session, lifecycle, bus, socket, logger }: Params ) {
	if( module.hot ) {
		module.hot.addDisposeHandler( () => {
			lifecycle.shutdown();
		} );
	}
	const userSlugCacheUid = new AsyncCache( ( uid: number ) => api.user.getUserslug( { session, uid } ), { maxAge: TimeSpan.fromDays( 1 ), retryCount: 5 } );

	const userInfoCacheUserslug = new AsyncCache( async ( _userslug: string ) => {
		const { uid, username, userslug, groups } = await api.user.get( { session, userslug: _userslug } );
		userSlugCacheUid.set( uid, userslug );
		return { uid, username, userslug, groups: groups.map( ( { slug } ) => slug ).sort() };
	}, { maxAge: TimeSpan.fromDays( 1 ), retryCount: 5 } );

	async function parseCommand( after: string, rawCommand: RawCommand, definition: CommandDefinition<any, any> ) {
		dump( logger, { after, rawCommand } );
		const closeTag = {
			'<': '>',
			'[': ']',
			'{': '}',
			'(': ')'
		};

		interface ParseContext {
			readonly name: readonly string[];
			readonly separator: readonly string[];
			readonly value: readonly string[];
			readonly isEscape: boolean;
			readonly openTag: readonly ( keyof typeof closeTag )[];
			readonly openQuote: ''|'`'|'"'|"'";
		}

		const defaultParseContext = {
			name: [],
			separator: [],
			value: [],
			isEscape: false,
			openTag: [],
			openQuote: ''
		} as ParseContext;

		let parseContext = defaultParseContext;
		type ParsedToken = [ string, string, string ];
		let parsedTokens = [] as readonly ParsedToken[];

		function processToken( parseContext: ParseContext ) {
			if( parseContext.name.length > 0 && parseContext.separator.length > 0 && parseContext.value.length === 0 ) {
				parseContext = { ...parseContext, name: [], separator: [], value: [ ...parseContext.name, ...parseContext.separator ] };
			}
			if( parseContext.name.length === 0 && parseContext.value.length === 0 ) return;
			const parsedToken = [ parseContext.name.join( '' ), parseContext.separator.join( '' ), parseContext.value.join( '' ) ] as ParsedToken;
			parsedTokens = [ ...parsedTokens, parsedToken ];
			return parsedToken;
		}

		for( let i = 0; i < after.length; ++i ) {
			const [ c, next ] = after.slice( i, i + 2 );
			if( parseContext.isEscape ) {
				parseContext = { ...parseContext, isEscape: false };
			} else if( parseContext.openQuote ) {
				if( c === parseContext.openQuote ) { // close quote
					parseContext = { ...parseContext, openQuote: '' };
					continue;
				}
				if( c === '\\' ) { // escape
					parseContext = { ...parseContext, isEscape: true };
					continue;
				}
			} else {
				if( parseContext.openTag.length > 0 ) {
					const openTag = parseContext.openTag[ parseContext.openTag.length - 1 ];
					if( c === closeTag[ openTag ] ) {
						parseContext = { ...parseContext, openTag: parseContext.openTag.slice( 0, parseContext.openTag.length - 1 ) };
					}
				}
				if(
					/[[{(]/.test( c )
				||	( /[<]/.test( c ) && /[a-z!]/i.test( next ) )
				) {
					parseContext = { ...parseContext, openTag: [ ...parseContext.openTag, c as keyof typeof closeTag ] };
				}
				if( parseContext.openTag.length === 0 ) {
					if( parseContext.value.length === 0 && /[`'"]/.test( c ) ) { // open quote
						parseContext = { ...parseContext, openQuote: c as '`'|"'"|'"' };
						continue;
					}
					if( /\s/.test( c ) ) { // next token
						processToken( parseContext );
						parseContext = defaultParseContext;
						continue;
					}
					if( parseContext.name.length === 0 && parseContext.value.length > 0 ) {
						if( /[:=]/.test( c ) ) {
							if( isValidParameterName( parseContext.value.join( '' ) ) ) {
								parseContext = { ...parseContext, name: [ ...parseContext.value ], separator: [ c ], value: [] };
								continue;
							}
						}
					}
				}
			}
			parseContext = { ...parseContext, value: [ ...parseContext.value, c ] };
		}

		if( parseContext.isEscape ) throw new FriendlyError( 'Invalid escape' );
		if( parseContext.openQuote ) throw new FriendlyError( `Expected: ${parseContext.openQuote}` );
		processToken( parseContext );

		interface UnparsedParameter {
			readonly name: string;
			readonly raw: string;
		}

		async function parseParameterValue( def: CommandParameterDefinition, { name, raw }: UnparsedParameter ) {
			switch( def.type ) {
				case 'bigint': {
					if( raw === '' ) throw new FriendlyError( `Parameter ${name}: Invalid ${def.type}: ${raw}` );
					const value = BigInt( raw );
					return value;
				}
				case 'integer': {
					if( raw === '' ) throw new FriendlyError( `Parameter ${name}: Invalid ${def.type}: ${raw}` );
					const value = parseInt( raw ); // eslint-disable-line radix
					return value;
				}
				case 'number': {
					if( raw === '' ) throw new FriendlyError( `Parameter ${name}: Invalid ${def.type}: ${raw}` );
					const value = parseFloat( raw );
					return value;
				}
				case 'onehot':
				case 'boolean': {
					switch( raw.toLowerCase() ) {
					case 'on':
					case 'y':
					case 'yes':
					case 't':
					case 'true':
					case '1':
						return true;
					case 'off':
					case 'n':
					case 'no':
					case 'f':
					case 'false':
					case '0':
						return false;
					default:
						throw new FriendlyError( `Parameter ${name}: Invalid ${def.type}: ${raw}` );
					}
				}
				case 'rest':
				case 'string': return raw;
				case 'vector2': {
					const match = /\(?\s*([-+]?\s*[0-9]+(?:\.[0-9]+))\s*[-x:,]?\s*([-+]?\s*[0-9]+(?:\.[0-9]+))\s*\)?/.exec( raw );
					if( !match ) throw new FriendlyError( `Parameter ${name}: Vector not in correct format: ${raw}` );
					const value = [ +match[ 1 ], +match[ 2 ] ];
					if( value.some( v => !isFinite( v ) ) ) throw new FriendlyError( `Parameter ${name}: Vector out of bounds` );
					return value;
				}
				case 'url': {
					let href = /!?\[.*?\]\((.*?)\)/.exec( raw )?.[ 1 ];
					if( !href ) {
						const { window: { document: body } } = new JSDOM( raw );
						if( !href ) {
							href = body.querySelector( 'img' )?.src;
						}
						if( !href ) {
							href = body.querySelector( 'a' )?.href;
						}
					}
					return new URL( href || raw, baseUrl );
				}
				case 'user': {
					let userslug: string;
					if( /^[0-9]+$/.test( raw ) ) {
						userslug = await userSlugCacheUid.get( parseInt( raw, 10 ) );
					} else {
						userslug = api.slugify( raw.replace( /^:?@|:?$/g, '' ) );
					}
					const userInfo = await userInfoCacheUserslug.get( userslug );
					return { ...userInfo, roles: getUserRoles( userInfo ) };
				}
				default: throw new FriendlyError( `Parameter ${name}: Unknown type: ${def.type}` );
			}
		}

		let command = { ...rawCommand } as CommandBase<any, any>;
		if( definition.parameters === 'ignore' ) {
			command = { ...command, parameterInfo: {}, parameters: {} };
		} else {
			try {
				let unmatchedParameters = Object.entries( definition.parameters || {} ) as readonly ( readonly [ string, CommandParameterDefinition ] )[];

				interface ParsedParameterPending extends UnparsedParameter {
					readonly type: CommandParameterType;
					readonly value: Promise<any>;
					readonly supplied: boolean;
				}

				interface ParsedParameter extends UnparsedParameter {
					readonly type: CommandParameterType;
					readonly value: any;
					readonly supplied: boolean;
				}
				let matchedParameters = [] as readonly ( readonly [ CommandParameterDefinition, ParsedParameterPending ] )[];

				const hotValues = new Map<string|number, Promise<[ string, boolean ]>>();
				function resolveOneHot( name: string, definition: CommandParameterDefinitionOneHot, value: Eventually<boolean> ) {
					assert.equal( definition.type, 'onehot' );
					const p =
						Promise.resolve( hotValues.get( definition.groupKey ) )
						.then( async n => {
							if( n == null ) n = [ null, definition.allowNone === true ];
							if( await value ) {
								if( n[ 0 ] != null && n[ 0 ] !== name ) throw new FriendlyError( `Conflicting parameters ${n[ 0 ]} & ${name}` );
								if( n[ 1 ] !== ( definition.allowNone === true ) ) throw new FriendlyError( `Conflicting parameters ${n[ 0 ]} & ${name}` );
								return [ name, n[ 1 ] ] as [ string, boolean ];
							} else {
								return n;
							}
						} );
					hotValues.set( definition.groupKey, p );
					return p.then( ( [ n ] ) => name === n );
				}

				( function matchNamedParameters() {
					for( const unmatchedParameter of unmatchedParameters.filter( ( [ name ] ) => !!name ) ) {
						const [ name, definition ] = unmatchedParameter;
						const possibleNames = [ name, ...( definition.alias || [] ) ];
						let index: number;
						let raw: string;
						let value: Promise<any>;

						if( [ 'boolean', 'onehot' ].includes( definition.type ) ) {
							index = parsedTokens.findIndex( ( [ name, , value ] ) => !name && [ ...possibleNames.map( n => `--${n}` ), ...possibleNames.map( n => `--no-${n}` ) ].includes( value.toLowerCase() ) );
							if( index >= 0 ) {
								raw = parsedTokens[ index ][ 2 ];
								value = Promise.resolve( !/^--no-/i.test( raw ) );
							}
						}
						if( value == null ) {
							index = parsedTokens.findIndex( ( [ name ] ) => possibleNames.includes( name.toLowerCase().replace( /^--?/, '' ) ) );
							if( index < 0 ) continue;
							raw = parsedTokens[ index ][ 2 ];
							value = parseParameterValue( unmatchedParameter[ 1 ], { name, raw } );
						}
						if( definition.type === 'onehot' ) value = resolveOneHot( name, definition, value as Promise<boolean> );
						matchedParameters = [ ...matchedParameters, [ unmatchedParameter[ 1 ], { name, type: definition.type, value, raw, supplied: true } ] ];
						parsedTokens = allExceptIndex( parsedTokens, index );
						unmatchedParameters = allExceptValue( unmatchedParameters, unmatchedParameter );
					}
				}() );
				( function matchPositionalParameters() {
					let gaps = matchedParameters.map( ( [ { position } ] ) => position ).filter( p => typeof p === 'number' ).sort();
					for( const unmatchedParameter of
						unmatchedParameters
						.filter( ( [ , { position } ] ) => typeof position === 'number' )
						.sort( ( [ _1, { position: p1 } ], [ _2, { position: p2 } ] ) => p1 - p2 )
					) {
						const [ name, definition ] = unmatchedParameter;
						let { position } = definition;
						// offset position by the number of positional arguments already resolved by name
						position = position - gaps.filter( p => p <= position ).length;
						if( parsedTokens.length <= position ) continue;
						gaps = [ ...gaps, position ].sort();
						const raw = parsedTokens[ position ].join( '' );
						let value = parseParameterValue( unmatchedParameter[ 1 ], { name, raw } );
						if( definition.type === 'onehot' ) value = resolveOneHot( name, definition, value as Promise<boolean> );
						matchedParameters = [ ...matchedParameters, [ unmatchedParameter[ 1 ], { name, type: definition.type, value, raw, supplied: true } ] ];
						parsedTokens = allExceptIndex( parsedTokens, position );
						unmatchedParameters = allExceptValue( unmatchedParameters, unmatchedParameter );
					}
				}() );
				( function matchRestParameters() {
					const restParameters = unmatchedParameters.filter( ( [ , { type } ] ) => type === 'rest' );
					assert.ok( restParameters.length <= 1 );
					const [ restParameter ] = restParameters;
					if( !restParameter ) return;
					const definition = restParameter[ 1 ];
					const unnamedTokens = parsedTokens.filter( ( [ name ] ) => !name );
					if( unnamedTokens.length === 0 ) return;
					const [ name, { type } ] = restParameter;
					const raw = unnamedTokens.map( v => v.join( '' ) ).join( ' ' );
					const value = parseParameterValue( definition, { name, raw } );
					matchedParameters = [ ...matchedParameters, [ definition, { name, type, value, raw, supplied: true } ] ];
					parsedTokens = allExceptValue( parsedTokens, ...unnamedTokens );
					unmatchedParameters = allExceptValue( unmatchedParameters, restParameter );
				}() );
				if( parsedTokens.length > 0 ) throw new FriendlyError( `Unexpected parameter: ${parsedTokens[ 0 ][ 0 ]}` );
				( function matchUnsuppliedParameters() {
					for( const unmatchedParameter of [ ...unmatchedParameters ] ) {
						const [ name, definition ] = unmatchedParameter;
						let def = definition.default;
						if( typeof def === 'function' ) def = def() as any;
						let value: Promise<any>;
						if( definition.type === 'onehot' ) {
							if( hotValues.has( definition.groupKey ) && !definition.allowNone ) value = hotValues.get( definition.groupKey ).then( ( [ n ] ) => n == null );
							else value = Promise.resolve( def as Eventually<boolean> );
							value = resolveOneHot( name, definition, value );
						} else if( definition.required ) {
							throw new FriendlyError( `Parameter ${name}: Missing` );
						} else {
							value = Promise.resolve( def );
						}
						if( definition.type === 'onehot' ) value = resolveOneHot( name, definition, value as Promise<boolean> );
						matchedParameters = [ ...matchedParameters, [ definition, { name, type: definition.type, value, raw: null, supplied: false } ] ];
						unmatchedParameters = allExceptValue( unmatchedParameters, unmatchedParameter );
					}
				}() );
				assert.equal( unmatchedParameters.length, 0 );
				for( const [ def, { supplied, name } ] of matchedParameters ) {
					if( !supplied ) continue;
					const conflicts =
						matchedParameters
						.filter( ( [ d, { supplied } ] ) => d !== def && supplied )
						.filter( ( [ { conflicts } ] ) => ( conflicts || [] ).includes( name ) );
					if( conflicts.length > 0 ) {
						throw new FriendlyError( conflicts.map( ( [ , { name: name2 } ] ) => `${name2} conflicts with ${name}` ).join( '\n' ) );
					}
				}

				const matchedParameterMap = new Map(
					await Promise.all(
						matchedParameters
						.map( async ( [ def, { value, ...p } ] ) =>
							( [ def, { ...p, value: await value } ] as [ CommandParameterDefinition, ParsedParameter ] )
						)
					)
				);
				await Promise.all( [ ...hotValues.values() ] ).then( v => {
					if( v.some( ( [ v, allowNone ] ) => v == null && !allowNone ) ) throw new FriendlyError( 'Onehot requires at least one true value' );
				} );

				const definedParameters = Object.values( definition.parameters || {} );
				assert.ok( definedParameters.every( def => matchedParameterMap.has( def ) ) );
				const parameters =
					definedParameters
					.map( def => matchedParameterMap.get( def ) );

				const parameterInfo = Object.fromEntries(
					parameters.map( parameter => ( [ parameter.name, parameter ] ) )
				);

				for( const [ name, def ] of Object.entries( definition.parameters || {} ) ) {
					if( def.query && !evalQuery( matchedParameterMap.get( def ), def.query ) ) {
						throw new FriendlyError( `Validation failed for parameter ${name}` );
					}
				}

				command = {
					...command,
					parameterInfo,
					parameters:
						Object.fromEntries(
							parameters.map( parameter => ( [ parameter.name, parameter.value ] ) )
						)
				} as Command<any, any>;
			} catch( ex ) {
				if( ex && ex.message ) {
					ex.message = `${rawCommand.raw}: ${ex.message}`;
				}
				throw ex;
			}
		}
		if( definition.name ) command = { ...command, name: definition.name };
		return command;
	}

	if( module.hot ) {
		module.hot.addDisposeHandler( data => {
			data.userInfoCacheUserslug = userInfoCacheUserslug.save();
			userInfoCacheUserslug.clear();
			data.userSlugCacheUid = userSlugCacheUid.save();
			userSlugCacheUid.clear();
		} );
		if( module.hot.data?.userInfoCacheUserslug ) {
			userInfoCacheUserslug.load( module.hot.data.userInfoCacheUserslug );
		}
		if( module.hot.data?.userSlugCacheUid ) {
			userSlugCacheUid.load( module.hot.data.userSlugCacheUid );
		}
	}

	async function getUserInfo( _uid: number ) {
		const _userslug = await userSlugCacheUid.get( _uid );
		return await userInfoCacheUserslug.get( _userslug );
	}

	async function getIssuerUser( _uid: number ) {
		const userInfo = await getUserInfo( _uid );
		return addHash( {
			type: 'user',
			...userInfo,
			roles: getUserRoles( userInfo )
		} ) as CommandIssuerUser;
	}

	async function getIssuerMinecraft( username: string ) {
		return addHash( {
			type: 'minecraft',
			username,
			roles: getMinecraftRoles( username )
		} ) as CommandIssuerMinecraft;
	}

	const commandSources = {
		console: never(),
		chat:
			socket.getEvent( 'event:chats.receive' ).pipe(
				filter( ( { self } ) => self !== 1 ),
				mergeMap( async ( { roomId, message, ...p } ) => {
					const timestamp = +new Date;
					const raw = await api.modules.chats.getRaw( { socket, mid: message.mid, roomId } );
					return { timestamp, raw, roomId, message, ...p };
				} ),
				mergeMap( ( { raw, ...p } ) => getLines( raw ).map( raw => ( { ...p, raw } ) ) ),
				mergeMap( async ( { raw, timestamp, fromUid: uid, roomId } ) => ( {
					timestamp,
					issuer: await getIssuerUser( uid ),
					source: addHash( { type: 'chat', roomId } ) as CommandSourceChat,
					responseRoute: { type: 'chat', roomId } as ResponseRouteChat,
					raw
				} as RawCommand ) )
			),
		mention:
			socket.getEvent( 'event:new_notification' ).pipe(
				filter( ( { type } ) => type === 'mention' ),
				mergeMap( ( { bodyLong, ...p } ) => {
					const timestamp = +new Date;
					return getLines( bodyLong ).map( raw => ( {
						timestamp,
						bodyLong,
						raw,
						...p
					} ) );
				} ),
				mergeMap( async ( { cid, tid, pid, from: uid, raw, timestamp } ) => ( {
					timestamp,
					issuer: await getIssuerUser( uid ),
					source: addHash( { type: 'mention', cid, tid, pid } ) as CommandSourceMention,
					responseRoute: { type: 'thread', cid, tid, pid } as ResponseRouteThread,
					raw
				} as RawCommand ) )
			),
		minecraft:
			bus.pipe(
				filter( ( { type } ) => type === 'minecraft-chat-receive' ),
				mergeMap( async ( { timestamp, username, message }: BusMessageMinecraftChatReceive ) => ( {
					timestamp,
					issuer: await getIssuerMinecraft( username ),
					source: addHash( { type: 'minecraft' } ) as CommandSourceMinecraft,
					responseRoute: { type: 'minecraft' } as ResponseRouteMinecraft,
					raw: message
				} as RawCommand ) )
			)
	} as { readonly [ key: string ]: Observable<RawCommand>; };

	merge( ...Object.values( commandSources ) ).pipe(
		handleErrors( { logger } ),
		filter( ( { issuer: { roles } } ) => !roles.includes( 'persona_non_grata' ) ),
		mergeMap( rawCommand =>
			from( registeredCommands )
			.pipe(
				filter( ( { options: { filter } } ) => !filter || evalQuery( rawCommand, filter ) ),
				map( registeredCommand => {
					const options = registeredCommand.options;
					const definition = options.command;
					if( definition.query && !evalQuery( rawCommand, definition.query ) ) {
						return null;
					}
					let after = normalize( rawCommand.raw, options.normalize );
					if( definition.prefix && definition.prefix.length > 0 ) {
						after = chomp( after, ...definition.prefix )[ 1 ];
					}
					if( after == null ) return null;
					return {
						registeredCommand,
						after
					};
				} ),
				filter( c => !!c ),
				toArray(),
				map( r => r.sort(
					( { registeredCommand: { options: { priority: priority1 = 0 } } },
					  { registeredCommand: { options: { priority: priority2 = 0 } } } ) =>
						priority2 - priority1
				)[ 0 ] ),
				filter( c => !!c ),
				map( ( { registeredCommand, after } ) => ( {
					rawCommand,
					registeredCommand,
					after
				} ) )
			)
		),
		takeUntil( lifecycle.shutdown$ ),
		handleErrors( { logger } )
	).subscribe( async ( { rawCommand, registeredCommand, after } ) => {
		if( registeredCommands.indexOf( registeredCommand ) < 0 ) return;
		try {
			if( registeredCommand.options.processParameters ) {
				after = registeredCommand.options.processParameters( after );
			}
			const command = await parseCommand( after, rawCommand, registeredCommand.options.command );
			if( registeredCommands.indexOf( registeredCommand ) < 0 ) return;
			registeredCommand.dispatch( command );
		} catch( ex ) {
			logger.error( ex );
			registeredCommand.dispatch( null, ex );
			if( ex instanceof FriendlyError ) {
				const message = renderToStaticMarkup( <pre>{ex.message}</pre> );
				bus.next( { type: 'response', message, route: rawCommand.responseRoute } );
			}
		}
	} );

	lifecycle.shutdown$
	.subscribe( () => {
		lifecycle.done();
	} );
}
