import React from 'react';
import { renderToStaticMarkup } from 'react-dom/server';

import path from 'path';

import * as api from '~nodebb/api';

import { createCanvas, createImageData } from 'canvas';

import puppeteer from 'puppeteer';

import { parseCommands } from '~command';

import data from '~data/magic-8-ball.yaml';
import { handleErrors } from '~rx';
import { mergeMap, takeUntil, take } from 'rxjs/operators';
import { shuffle } from '~random';

import express from 'express';
import { Socket } from 'net';
import { fromEvent } from 'rxjs';

if( module.hot ) {
	module.hot.accept( [ '../components/index.ts' ] );
}

type ModuleName = 'magic-8-ball';
type Params = ModuleParamsMap[ ModuleName ];

export default async function( { moduleName, session, socket, logger, lifecycle, bus, commandFilter }: Params ) {
	if( module.hot ) {
		module.hot.addDisposeHandler( () => {
			lifecycle.shutdown();
		} );
	}

	const app = express();
	app.use( express.static( path.resolve( __dirname, '..', '..', 'dist', 'wwwtemp' ) ) );
	const port = 8888;
	const server = app.listen( port, () => {
		logger.info( `${moduleName} listening on port ${port}...` );
	} );

	interface ScreenshotCommandParameters {
		readonly url: 'url';
	}
	parseCommands<ScreenshotCommandParameters>( {
		command: {
			name: '!screenshot',
			prefix: [ 'screen shot' ],
			parameters: {
				url: {
					type: 'url',
					position: 0
				}
			}
		},
		lifecycle,
		filter: commandFilter
	} )
	.pipe(
		mergeMap( async ( { parameters: { url }, responseRoute } ) => {
			let browser: puppeteer.Browser;
			try {
				browser = await puppeteer.launch( {
					args: [],
					dumpio: true,
					ignoreHTTPSErrors: true
				} );
				const page = ( await browser.pages() )[ 0 ];
				await page.goto( url.href, { waitUntil: 'networkidle2' } );
				const buffer = await page.screenshot( {
					type: 'jpeg',
					quality: 50,
					fullPage: true
					// clip: { x: 0, y: 0, width: 960, height: 768 }
				} ) as Buffer;
				const filename = `screenshot-${url.hostname}.jpg`;
				const src = await api.posts.upload( { session, filename, buffer, contentType: 'image/jpeg' } );
				const message = renderToStaticMarkup( <img src={src} /> );
				bus.next( { type: 'response', route: responseRoute, message } );
			} finally {
				browser?.close();
			}
		} ),
		handleErrors( { logger } )
	)
	.subscribe();

	parseCommands( {
		command: {
			name: '!magic-8-ball',
			prefix: [ 'magic 8 ball', '8 ball' ]
		},
		lifecycle,
		filter: commandFilter
	} )
	.pipe(
		mergeMap( async ( { responseRoute } ) => {
			let imageData: ImageData;
			let browser: puppeteer.Browser;
			try {
				browser = await puppeteer.launch( {
					args: [
						'--no-sandbox',
						'--use-angle=gl'
					],
					dumpio: true,
					timeout: 0
				} );
				const page = ( await browser.pages() )[ 0 ];
				page.exposeFunction( 'getOptions', () => ( {
					width: 1024,
					height: 1024,
					fov: 70,
					messages: shuffle( [ ...data.messages.standard ] ).slice( 0, 12 )
				} ) );
				imageData = await new Promise<ImageData>( ( resolve, reject ) => {
					let success = false;
					page.exposeFunction( 'response', ( imageData: ImageData ) => {
						try {
							const cid = createImageData( imageData.width, imageData.height );
							cid.data.set( imageData.data );
							resolve( cid );
						} catch( ex ) {
							reject( ex );
							return;
						}
						success = true;
					} );
					( async () => {
						try {
							await page.goto( `http://localhost:${port}/`, { waitUntil: 'networkidle0' } );
							await page.waitFor( 2 * 60 * 1000 );
							if( !success ) throw new Error( 'timeout' );
						} catch( ex ) {
							reject( ex );
						}
					} )();
				} );
				await page.close();
			} finally {
				await browser?.close();
			}
			const canvas = createCanvas( imageData.width, imageData.height );
			const c2d = canvas.getContext( '2d' );
			c2d.putImageData( imageData, 0, 0 );
			const contentType = 'image/png';
			const buffer = canvas.toBuffer( contentType, {
				compressionLevel: 9
			} );
			const filename = '8ball.png';
			const url = await api.posts.upload( { session, filename, buffer, contentType } );
			const message = renderToStaticMarkup( <img src={url} /> );
			bus.next( { type: 'response', route: responseRoute, message } );
		} ),
		handleErrors( { logger } )
	)
	.subscribe();

	const connections = new Set<Socket>();
	fromEvent<Socket>( server, 'connection' )
	.pipe( takeUntil( lifecycle.shutdown$ ) )
	.subscribe( socket => {
		connections.add( socket );
		fromEvent( socket, 'close' )
		.pipe( takeUntil( lifecycle.shutdown$ ), take( 1 ) )
		.subscribe( () => {
			connections.delete( socket );
		} );
	} );

	lifecycle.shutdown$
	.subscribe( () => {
		server.close();
		for( const socket of connections.values() ) {
			socket.end();
		}
		lifecycle.done();
	} );
}
