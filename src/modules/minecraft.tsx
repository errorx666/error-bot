/// <reference path="./minecraft.d.ts"/>
import { apiKey, url } from '~data/minecraft.yaml';

import { filter, concatMap, takeUntil, startWith } from 'rxjs/operators';
import { dump } from '~util';
import { handleErrors } from '~rx';
import { renderToStaticMarkup } from 'react-dom/server';
import React from 'react';
import { parseCommands } from '~command';
import { connectWs } from '~ws';
import { interval } from 'rxjs';

if( module.hot ) {
	module.hot.accept( [ '../components/index.ts' ] );
}

type ModuleName = 'minecraft';
type Params = ModuleParamsMap[ ModuleName ];


export default async function( { lifecycle, bus, tid, logger }: Params ) {
	if( module.hot ) {
		module.hot.addDisposeHandler( () => {
			lifecycle.shutdown();
		} );
	}

	const { wsIn, wsOut } = connectWs<MinecraftMessage>( {
		url,
		lifecycle,
		logger,
		retryDelay: 15 * 1000
	} );

	// wsIn.pipe( tapLog( logger, 'rx' ) ).subscribe();
	// wsOut.pipe( tapLog( logger, 'tx' ) ).subscribe();

	wsIn.pipe(
		filter( v => v.type === 'serverChat' )
	).subscribe( ( { username, message }: MinecraftServerChatMessage ) => {
		bus.next( {
			type: 'minecraft-chat-receive',
			timestamp: +new Date,
			username,
			message
		} as BusMessageMinecraftChatReceive );
	} );

	type MinecraftChatCommandParameters = { text: 'rest'; };
	parseCommands<MinecraftChatCommandParameters>( {
		command: {
			prefix: [ 'wtdwtf', 'tdwtf', 'wtf', 'chat', 'say' ],
			parameters: {
				text: {
					type: 'rest',
					required: true
				}
			}
		},
		lifecycle,
		filter: {
			issuer: {
				type: {
					$eq: 'minecraft'
				}
			}
		}
	} ).pipe(
		concatMap( async ( {
			issuer,
			parameters: { text }
		} ) => {
			const name = ( issuer as CommandIssuerMinecraft ).username;
			const message = renderToStaticMarkup( <p>
				{name} said: <blockquote>{text}</blockquote>
			</p> );
			bus.next( {
				type: 'response',
				route: { type: 'thread', tid },
				message
			} );
		} )
	).subscribe();

	parseCommands<MinecraftChatCommandParameters>( {
		command: {
			name: '!minecraft',
			prefix: [ 'mine craft', 'm c' ],
			parameters: {
				text: {
					type: 'rest',
					required: true
				}
			}
		},
		lifecycle,
		filter: {
			issuer: {
				type: {
					$eq: 'user'
				}
			},
			source: {
				$or: [ {
					type: {
						$eq: 'chat'
					}
				}, {
					type: {
						$eq: 'mention'
					}
				} ]
			}
		}
	} ).pipe(
		concatMap( async ( {
			issuer,
			parameters: { text }
		} ) => {
			const name = ( issuer as CommandIssuerUser ).username;

			bus.next( {
				type: 'response',
				route: { type: 'minecraft' },
				message: `${name}> ${text}`
			} );
		} ),
		handleErrors( { logger } )
	).subscribe();


	wsIn.pipe(
		filter( v => v.type === 'livingDeath' )
	).subscribe( ( { deathMessage }: MinecraftLivingDeathMessage ) => {
		const message = renderToStaticMarkup( <p>{deathMessage}</p> );
		bus.next( {
			type: 'response',
			route: { type: 'thread', tid },
			message
		} );
	} );

	wsIn.pipe(
		filter( v => v.type === 'advancement' )
	).subscribe( ( { name, advancement }: MinecraftAdvancementMessage ) => {
		const message = renderToStaticMarkup( <p>
			{name} unlocked the <strong>{advancement}</strong> advancement.
		</p> );
		bus.next( {
			type: 'response',
			route: { type: 'thread', tid },
			message
		} );
	} );

	wsIn.pipe(
		filter( v => v.type === 'playerLoggedIn' )
	).subscribe( ( { name }: MinecraftPlayerLoggedInMessage ) => {
		const message = renderToStaticMarkup( <>
			{name} logged in
		</> );
		bus.next( {
			type: 'response',
			route: { type: 'thread', tid },
			message
		} );
	} );

	wsIn.pipe(
		filter( v => v.type === 'ping' )
	).subscribe( () => {
		wsOut.next( { type: 'pong' } );
	} );

	interval( 60 * 1000 )
	.pipe( takeUntil( lifecycle.shutdown$ ) )
	.subscribe( () => {
		wsOut.next( { type: 'ping' } );
	} );

	wsIn.pipe(
		filter( v => v.type === 'authRequest' ),
		startWith( {} )
	).subscribe( () => {
		wsOut.next( { type: 'auth', key: apiKey } );
	} );

	wsIn.pipe(
		filter( v => v.type === 'playerLoggedOut' )
	).subscribe( ( { name }: MinecraftPlayerLoggedOutMessage ) => {
		const message = renderToStaticMarkup( <p>
			{name} logged out
		</p> );
		bus.next( {
			type: 'response',
			route: { type: 'thread', tid },
			message
		} );
	} );

	bus.pipe(
		filter( ( { type } ) => type === 'minecraft-chat-send' )
	).subscribe( async ( { message }: BusMessageMinecraftChatSend ) => {
		wsOut.next( { type: 'response', message } );
		// bus.next( {
		// 	type: 'response',
		// 	route: {
		// 		type: 'thread', tid
		// 	},
		// 	message
		// } );
	} );

	dump( logger, { url } );

	lifecycle.shutdown$
	.subscribe( async () => {
		wsOut.complete();
		lifecycle.done();
	} );
}

