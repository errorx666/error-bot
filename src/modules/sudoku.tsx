import React, { PureComponent } from 'react';
import type { ReactNode } from 'react';
import { takeUntil, take, concatMap } from 'rxjs/operators';
import { renderToStaticMarkup } from 'react-dom/server';
import { parseCommands } from '~command';
import { shrinkImg, shrinkSvg } from '~dom';
import { handleErrors } from '~rx';
import { Map } from 'immutable';
import { dump } from '~util';
import _ from 'lodash';

if( module.hot ) {
	module.hot.accept( [ '../components/index.ts' ] );
}

type ModuleName = 'sudoku';
type Params = ModuleParamsMap[ ModuleName ];

type GameStates = 'new-game'|'gameplay'|'game-over';
type GameTransitions = 'ready'|'load'|'win'|'lose'|'restart';

interface SudokuProps {
	readonly gridImg: JSX.Element;
	readonly children?: ReactNode;
}
class Sudoku extends PureComponent<SudokuProps> {}

export default async function( { moduleName, lifecycle, bus, session, socket, tid, logger }: Params ) {
	if( module.hot ) {
		module.hot.addDisposeHandler( () => {
			lifecycle.shutdown();
		} );
	}

	lifecycle.shutdown$
	.subscribe( async () => {
		lifecycle.done();
	} );
}
