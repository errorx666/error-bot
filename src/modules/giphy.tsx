import { concatMap } from 'rxjs/operators';
import React, { PureComponent } from 'react';
import { renderToStaticMarkup } from 'react-dom/server';
import { parseCommands } from '~command';
import { URL } from 'url';

import * as http from '~http';

import { apiKey } from '~data/giphy.yaml';

import { ExternalContent } from '~components';
import { dump } from '~util';
import { shuffle } from '~random';
import { handleErrors } from '~rx';

if( module.hot ) {
	module.hot.accept( [ '../components/index.ts', '../http.ts' ] );
}

interface GiphyProps {
	readonly title: string;
	readonly url: string;
	readonly image: GiphyImage;
}
class Giphy extends PureComponent<GiphyProps> {
	public constructor( props: GiphyProps ) {
		super( props );
	}

	public render() {
		const { title, url, image } = this.props;
		const src = image.webp ?? image.url ?? image.mp4;
		return <ExternalContent name="Giphy" url={url}>
			<img src={src} title={title}/>
		</ExternalContent>;
	}
}

type ModuleName = 'giphy';
type Params = ModuleParamsMap[ ModuleName ];

interface GiphyImage {
	readonly url: string;
	readonly width: string;
	readonly height: string;
	readonly size: string;
	readonly frames: string;
	readonly mp4: string;
	readonly mp4_size: string;
	readonly webp: string;
	readonly webp_size: string;
}

interface GiphySearchResponse {
	readonly data: readonly {
		readonly type: string;
		readonly id: string;
		readonly slug: string;
		readonly url: string;
		readonly bitly_url: string;
		readonly embed_url: string;
		readonly username: string;
		readonly source: string;
		readonly rating: string;
		readonly images: { readonly [ key: string ]: GiphyImage; };
		readonly title: string;
	}[];
}

export default async function( { moduleName, lifecycle, bus, logger }: Params ) {
	if( module.hot ) {
		module.hot.addDisposeHandler( () => {
			lifecycle.shutdown();
		} );
	}

	interface GiphyCommandParameters {
		readonly query: 'rest';
		readonly limit: 'integer';
		readonly offset: 'integer';
		readonly rating: 'string';
		readonly language: 'string';
		readonly rendition: 'string';
		readonly shuffle: 'boolean';
	}
	parseCommands<GiphyCommandParameters>( {
		command: {
			name: '!giphy',
			prefix: [ 'giphy', 'giffy', 'gif' ],
			parameters: {
				query: {
					type: 'rest'
				},
				limit: {
					alias: [ 'take' ],
					type: 'integer',
					required: false,
					default: 1,
					query: {
						value: {
							$gte: 1,
							$lte: 15
						}
					}
				},
				offset: {
					alias: [ 'o', 'skip' ],
					type: 'integer',
					required: false,
					default: 0,
					query: {
						value: {
							$gte: 0
						}
					}
				},
				rating: {
					alias: [ 'r' ],
					type: 'string',
					required: false,
					default: 'r',
					query: {
						value: {
							$in: [ 'g', 'pg', 'pg-13', 'r' ]
						}
					}
				},
				language: {
					alias: [ 'lang', 'l' ],
					type: 'string',
					required: false,
					default: 'en',
					query: {
						value: {
							$in: [ 'en', 'es', 'pt', 'id', 'fr', 'ar', 'tr', 'th', 'vi', 'de', 'it', 'ja', 'zh-CN', 'zh-TW', 'ru', 'ko', 'pl', 'nl', 'ro', 'hu', 'sv', 'cs', 'hi', 'bn', 'da', 'fa', 'tl', 'fi', 'iw', 'ms', 'no', 'uk' ]
						}
					}
				},
				shuffle: {
					type: 'boolean',
					required: false,
					default: true
				},
				rendition: {
					type: 'string',
					required: false,
					default: 'original'
				}
			}
		},
		lifecycle
	} )
	.pipe(
		concatMap( async ( { parameters: { query, rating, language, offset, limit, rendition, shuffle: shuf }, issuer, responseRoute } ) => {
			const url = new URL( 'https://api.giphy.com/v1/gifs/search' );
			url.searchParams.set( 'api_key', apiKey );
			url.searchParams.set( 'q', query );
			url.searchParams.set( 'limit', String( limit ) );
			url.searchParams.set( 'lang', language );
			url.searchParams.set( 'rating', rating );
			url.searchParams.set( 'offset', String( offset ) );
			url.searchParams.set( 'random_id', issuer.hash );
			const response = await http.get<GiphySearchResponse>( { url, json: true } );
			dump( logger, { url: url.href, response } );
			let data = response.data;
			if( shuf ) data = shuffle( data );
			const [ gif ] = data;
			const image = gif.images[ rendition ];
			if( !image ) return;
			const message = renderToStaticMarkup( <Giphy title={gif.title} url={gif.url} image={image}/> );
			bus.next( {	type: 'response', message, route: responseRoute } );
		} ),
		handleErrors( { logger } )
	)
	.subscribe();

	lifecycle.shutdown$
	.subscribe( () => {
		lifecycle.done();
	} );
}
