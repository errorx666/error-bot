declare interface HangmanLockout {
	readonly uid: number;
	readonly username: string;
	readonly userslug: string;
	readonly expires: number;
	readonly turns?: number;
}

declare interface HangmanGameContext {
	readonly lastGuess: number;
	readonly roundId: string;
	readonly word: string;
	readonly guessed: readonly string[];
	readonly lockouts: readonly HangmanLockout[];
}

declare interface HangmanState {
	readonly gameContext: HangmanGameContext;
	readonly nextWord?: string;
}

declare interface HangmanResponse {
	readonly element: JSX.Element;
	readonly route: ResponseRoute;
}
