import { concatMap } from 'rxjs/operators';
import { parseCommands } from '~command';

import React, { PureComponent } from 'react';
import { renderToStaticMarkup } from 'react-dom/server';

import { URL } from 'url';
import { ExternalContent } from '~components';

import * as http from '~http';
import { handleErrors } from '~rx';

if( module.hot ) {
	module.hot.accept( [ '../components/index.ts', '../http.ts' ] );
}

type ModuleName = 'penny-arcade';
type Params = ModuleParamsMap[ ModuleName ];

interface PennyArcadeProps {
	name: string;
	url: string;
	via?: string;
	src: string;
}

class PennyArcade extends PureComponent<PennyArcadeProps> {
	public constructor( props: PennyArcadeProps ) {
		super( props );
	}

	public render() {
		const { props } = this;
		return (
			<ExternalContent name="Penny Arcade" url={props.url} title={props.name} via={props.via}>
				<img src={props.src} alt={props.name}/>
			</ExternalContent>
		);
	}
}

export default async function( { moduleName, lifecycle, bus, commandFilter, logger }: Params ) {
	if( module.hot ) {
		module.hot.addDisposeHandler( () => {
			lifecycle.shutdown();
		} );
	}

	type PennyArcadeCommandParameters = { query: 'rest'; };
	parseCommands<PennyArcadeCommandParameters>( {
		command: {
			name: '!penny-arcade',
			prefix: [ 'penny arcade', 'p a' ],
			parameters: {
				query: {
					type: 'rest',
					required: true
				}
			}
		},
		lifecycle,
		filter: commandFilter
	} ).pipe(
		concatMap( async ( {
			responseRoute,
			parameters: { query }
		} ) => {
			let url: string;

			const searchUrl = new URL( 'https://www.penny-arcade.com/archive/results/search' );
			searchUrl.searchParams.set( 'keywords', query as string );
			const via = url = searchUrl.href;
			const { window: { document } } = await http.get( { url, html: true } );
			const img = document.querySelector( '#results .img img' ) as HTMLImageElement;
			if( !img ) return;
			const name = document.querySelector( '#results h3' ).textContent;
			const src = new URL( img.src, url ).href;
			url = new URL( ( document.querySelector( '#results a[href]' ) as HTMLAnchorElement ).href, url ).href;
			const message = renderToStaticMarkup( <PennyArcade name={name} url={url} src={src} via={via}/> );
			bus.next( { type: 'response', message, route: responseRoute } );
		} ),
		handleErrors( { logger } )
	).subscribe();

	lifecycle.shutdown$
	.subscribe( () => {
		lifecycle.done();
	} );
}
