import { takeUntil, exhaustMap, filter, take, debounceTime, map, mergeMap, tap } from 'rxjs/operators';
import { getConfig } from '~nodebb/api';
import * as rest from '~nodebb/rest';
import { interval, merge, fromEvent, of } from 'rxjs';
import { parseCommands, query } from '~command';
import { handleErrors } from '~rx';

type ModuleName = 'health';
type Params = ModuleParamsMap[ ModuleName ];

export default function( { moduleName, lifecycle, bus, socket, session, logger }: Params ) {
	if( module.hot ) {
		module.hot.addDisposeHandler( () => {
			lifecycle.shutdown();
		} );
	}

	const uid = session.config.value.uid;

	let lastPong = +new Date;

	socket.getEvent( 'pong' )
	.pipe(
		takeUntil( lifecycle.shutdown$ )
	).subscribe( () => {
		logger.debug( 'pong' );
		lastPong = +new Date;
	} );

	merge(
		interval( 1000 )
		.pipe(
			filter( () => ( ( +new Date ) - lastPong ) > 60 * 1000 ),
			tap( () => { logger.fatal( 'late pong' ); } )
		),
		of( 'connect_error', 'connect_timeout', 'reconnect_error', 'reconnect_failed', 'disconnect', 'error' )
		.pipe(
			mergeMap( e =>
				fromEvent( socket.socket, e )
				.pipe( tap( ex => { logger.fatal( e, ex ); } ) )
			)
		),
		socket.error$
		.pipe(
			filter( ( { message } = { message: null } as any ) => [
				undefined, null,
				'[[error:account-locked]]',
				'[[error:blacklisted-ip]]',
				'[[error:csrf-invalid]]',
				'[[error:invalid-session]]',
				'[[error:not-logged-in]]',
				'[[error:user-banned]]'
			].includes( message ) ),
			tap( err => { logger.fatal( err ); } )
		),
		rest.error$,
		socket.getEvent( 'checkSession' )
		.pipe(
			filter( ( { value: id } ) => id !== uid ),
			tap( () => { logger.fatal( 'checkSession failed' ); } )
		),
		merge(
			interval( 5 * 60 * 1000 ),
			interval( 10 * 1000 )
			.pipe(
				map( () => socket.socket.connected && !socket.socket.disconnected ),
				filter( value => value === false )
			),
			interval( 60 * 1000 )
			.pipe(
				exhaustMap( async () => {
					try {
						const response = await rest.get( { session, path: '/ping' } );
						return response === '200';
					} catch { return false; }
				} ),
				filter( value => value === false ),
				tap( err => { logger.fatal( 'ping failed' ); } )
			)
		).pipe(
			debounceTime( 1000 ),
			exhaustMap( async () => {
				const s = socket.socket;
				if( !s.connected || s.disconnected ) {
					logger.fatal( 'socket disconnected' );
					return true;
				}
				try {
					const config = await getConfig( { session } );
					if( config.uid !== uid || !config.loggedIn ) {
						logger.fatal( 'session invalid' );
						return true;
					} else {
						return false;
					}
				} catch( ex ) {
					logger.fatal( ex );
					return true;
				}
			} ),
			filter( s => s )
		)
	).pipe(
		handleErrors( { logger } ),
		take( 1 ),
		takeUntil( lifecycle.shutdown$ )
	).subscribe( () => {
		logger.info( 'Reload all' );
		bus.next( { type: 'reload-all' } );
	} );

	interface ReloadModulesParams {
		moduleNames: 'rest';
	}
	parseCommands<ReloadModulesParams>( {
		command: {
			prefix: [ 'reload modules', 'reload module', 'reload' ],
			query: query.fromRole( 'admin' ),
			parameters: {
				moduleNames: {
					type: 'rest',
					required: false
				}
			}
		},
		lifecycle
	} )
	.subscribe( ( { parameters: { moduleNames } } ) => {
		if( moduleNames === 'all' ) {
			bus.next( { type: 'reload-all' } );
		} else {
			bus.next( { type: 'reload-modules', ...(
				moduleNames ? {
					moduleNames: moduleNames.split( /\s+/ ) as ModuleName[]
				} : {}
			) } );
		}
	} );

	lifecycle.shutdown$
	.subscribe( () => {
		lifecycle.done();
	} );
}
