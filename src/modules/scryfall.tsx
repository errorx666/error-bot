import { concatMap } from 'rxjs/operators';
import { parseCommands } from '~command';

import { URL } from 'url';

import React, { PureComponent } from 'react';
import { renderToStaticMarkup } from 'react-dom/server';

import { Card, Cards } from 'scryfall-sdk';
import { getTimeout } from '~util';
import { handleErrors } from '~rx';

if( module.hot ) {
	module.hot.accept( [ '../components/index.ts' ] );
}

type ModuleName = 'scryfall';
type Params = ModuleParamsMap[ ModuleName ];

interface GathererProps {
	card: Card;
}

class Scryfall extends PureComponent<GathererProps> {
	public constructor( props: GathererProps ) {
		super( props );
	}

	public render() {
		const { props: { card } } = this;
		let href = '#';
		if( card.multiverse_ids ) {
			const multiverseId = card.multiverse_ids[ 0 ];
			if( multiverseId && isFinite( multiverseId ) ) {
				const url = new URL( 'https://gatherer.wizards.com/Pages/Card/Details.aspx' );
				url.searchParams.set( 'multiverseid', String( multiverseId ) );
				href = url.href;
			}
		}
		const src = card.image_uris.large || card.image_uris.normal || card.image_uris.png || card.image_uris.small;
		const alt = card.name;
		const title = [ card.name, card.type_line, card.oracle_text, card.flavor_text ].map( s => s?.trim() ).filter( s => !!s ).join( '\r\n' );

		return (
			<div>
				<a href={href} target="_blank" rel="noopener noreferrer">
					<img
						src={src}
						alt={alt}
						title={title}
					/>
				</a>
			</div>
		);
	}
}

export default async function( { moduleName, lifecycle, bus, commandFilter, logger }: Params ) {
	if( module.hot ) {
		module.hot.addDisposeHandler( () => {
			lifecycle.shutdown();
		} );
	}

	function isSuitableCard( card: Card ) {
		if( !card ) return false;
		if( !card.multiverse_ids ) return false;
		if( !card.name ) return false;
		if( !card.image_uris ) return false;
		return true;
	}

	async function *getCardById( id: number ) {
		try {
			yield await Cards.byMultiverseId( id );
		} catch( ex ) {
			logger.error( ex );
		}
	}

	async function *getCard( query: string ) {
		if( /^[0-9]+$/.test( query ) ) {
			yield *getCardById( parseInt( query, 10 ) );
		}
		try {
			yield await Cards.byName( query );
		} catch( ex ) {
			logger.error( ex );
		}
		try {
			const results = await getTimeout( Cards.search( query ).cancelAfterPage().waitForAll(), 5000 );
			for( const result of results ) {
				yield result;
			}
		} catch( ex ) {
			logger.error( ex );
		}
	}

	type ScryfallCommandParameters = { query: 'rest'; };
	parseCommands<ScryfallCommandParameters>( {
		command: {
			name: '!magic-the-gathering',
			prefix: [
				'magic the gathering',
				'magic',
				'mtg',
				'scryfall'
			],
			parameters: {
				query: {
					type: 'rest',
					required: true
				}
			}
		},
		lifecycle,
		filter: commandFilter
	} )
	.pipe(
		concatMap( async ( {
			responseRoute,
			parameters: { query }
		} ) => {
			let card: Card;
			try {
				for await( const result of getCard( query as string ) ) {
					if( isSuitableCard( result ) ) {
						card = result;
						break;
					}
				}
			} catch( ex ) {
				logger.error( ex );
				return;
			}
			if( !isSuitableCard( card ) ) return;
			const message = renderToStaticMarkup( <Scryfall card={card}/> );
			bus.next( { type: 'response', message, route: responseRoute } );
		} ),
		handleErrors( { logger } )
	).subscribe();

	lifecycle.shutdown$
	.subscribe( () => {
		lifecycle.done();
	} );
}
