import assert from 'assert';
import { Lifecycle } from '~lifecycle';
import { Logger, getLogger } from 'log4js';
import { fromEvent, throwError, merge, BehaviorSubject, timer, Subject, from, firstValueFrom } from 'rxjs';
import { switchMap, take, takeUntil, mergeMap, exhaustMap, map, filter } from 'rxjs/operators';
import os from 'os';
import fs from 'fs';
import path from 'path';
import upath from 'upath';
import { MessageChannel, Worker } from 'worker_threads';
import { handleErrors } from '~rx';
import { watch } from '~watch';

import LoggingEvent from 'log4js/lib/LoggingEvent';
import clustering from 'log4js/lib/clustering';

interface ThreadPoolOptions {
	readonly src: string;
	readonly lifecycle: Lifecycle;
	readonly logger?: Logger;
	readonly threadCount?: number;
}
export class ThreadPool<TRequest extends object, TResult extends object> {
	public readonly threadCount: number;
	private readonly name: string;
	private readonly fullPath: string;
	private readonly done = new Subject<true>();
	private readonly enqueue$ = new Subject<true>();
	private readonly lifecycle: Lifecycle;
	private readonly logger: Logger;
	private readonly workers = new BehaviorSubject<Worker[]>( [] );
	private workQueue = [] as ReadonlyArray<( worker: Worker ) => Promise<void>>;

	public constructor( options: ThreadPoolOptions ) {
		const baseDir = path.resolve( process.cwd(), 'dist', 'workers' );
		const name = this.name = upath.trimExt( upath.normalize( options.src ), [ 'worker' ] );
		const fullPath = this.fullPath = path.join( baseDir, ...upath.addExt( name, '.js' ).split( '/' ) );
		const lifecycle = this.lifecycle = options.lifecycle;
		const logger = this.logger = options.logger ?? getLogger( `worker/${name}` );
		const done = merge( this.done, lifecycle.shutdown$ );
		this.threadCount = options.threadCount ?? os.cpus().length;

		assert.ok( !lifecycle.isShutdown );
		lifecycle.shutdown$.pipe(
			take( 1 ),
			takeUntil( done )
		).subscribe( () => {
			this.shutdown();
		} );

		this.workers
		.pipe(
			switchMap( workers =>
				from( workers ).pipe(
					mergeMap( worker => {
						const error$ =
							fromEvent( worker, 'error' )
							.pipe( switchMap( ( e: string ) => throwError( () => new Error( e ) ) ) );
						const exit$ =
							fromEvent( worker, 'exit' )
							.pipe( switchMap( () => throwError( () => new Error( 'exited' ) ) ) );
						return merge(
							fromEvent<any>( worker, 'message' ),
							error$,
							exit$
						)
						.pipe(
							filter( m => m?.type === 'log' ),
							handleErrors( { logger } ),
							takeUntil( done )
						);
					} ),
					takeUntil( done )
				)
			),
			takeUntil( done )
		)
		.subscribe( ( { event, eventData } ) => {
			const e = LoggingEvent.deserialise( eventData );
			for( const [ key, value ] of Object.entries( event ) ) {
				if( !( key in e ) ) {
					e[ key ] = value;
				}
			}
			clustering.send( e );
		} );

		this.workers
		.pipe(
			switchMap( workers =>
				from( workers ).pipe(
					mergeMap( worker =>
						timer( 0, 0 )
						.pipe(
							exhaustMap( () => {
								if( this.workQueue.length <= 0 ) return this.enqueue$.pipe( take( 1 ), takeUntil( done ) );
								const task = this.workQueue[ 0 ];
								assert.ok( typeof task === 'function' );
								this.workQueue = this.workQueue.slice( 1 );
								return task( worker );
							} ),
							handleErrors( { logger } ),
							takeUntil( done )
						)
					),
					takeUntil( done )
				)
			),
			takeUntil( done )
		)
		.subscribe();

		this.reload();

		if( module.hot ) {
			module.hot.addDisposeHandler( () => {
				this.shutdown();
			} );

			watch( fullPath )
			.pipe(
				filter( ( [ type ] ) => type === 'change' ),
				handleErrors( { logger } ),
				takeUntil( done )
			)
			.subscribe( () => {
				this.reload();
			} );
		}
	}

	public async reload() {
		const { lifecycle, name, fullPath } = this;
		assert.ok( !lifecycle.isShutdown );
		assert.ok( fs.existsSync( fullPath ) );
		const workers = this.workers.value;
		this.workers.next(
			Array.from( { length: this.threadCount } )
			.map( ( _, index ) => {
				const worker = new Worker( fullPath, {
					stderr: true,
					stdout: true,
					workerData: {
						name,
						worker: index
					}
				} );
				worker.stdout.pipe( process.stdout );
				worker.stderr.pipe( process.stderr );
				return worker;
			} )
		);
		for( const worker of workers ) {
			worker.postMessage( { type: 'shutdown' } );
		}
	}

	public async request( req: TRequest ): Promise<TResult> {
		const { lifecycle, done, logger } = this;
		assert.ok( !lifecycle.isShutdown );
		return new Promise( ( resolve, reject ) => {
			this.workQueue = [ ...this.workQueue, async ( worker ) => {
				const { port1: portIn, port2: portOut } = new MessageChannel;
				let result: any;
				try {
					const error$ =
						fromEvent( worker, 'error' )
						.pipe( switchMap( ( e: string ) => throwError( () => new Error( e ) ) ) );
					const result$ =
						fromEvent<{ readonly result: TResult; readonly type?: never; }>( portOut, 'message' )
						.pipe(
							filter( e => e?.result != null && ( typeof e.result ) === 'object' ),
							map( e => e.result )
						);
					const closed$ =
						fromEvent( portOut, 'close' )
						.pipe( switchMap( () => throwError( () => new Error( 'closed' ) ) ) );
					const exit$ =
						fromEvent( worker, 'exit' )
						.pipe( switchMap( () => throwError( () => new Error( 'exited' ) ) ) );
					const promise =
						firstValueFrom(
							merge( result$, error$, closed$, exit$ )
							.pipe(
								takeUntil( done ),
								takeUntil( lifecycle.shutdown$ )
							) );
					worker.postMessage( { ...req, type: 'request', port: portIn }, [ portIn ] );
					result = await promise;
				} catch( ex ) {
					( logger ?? console ).error( ex );
					reject( ex );
					return;
				} finally {
					portIn.close();
					portOut.close();
				}
				if( lifecycle.isShutdown ) reject( new Error( 'interrupted' ) );
				else resolve( result );
			} ];
			this.enqueue$.next( true );
		} );
	}

	public shutdown() {
		this.done.next( true );
		this.done.complete();
		this.enqueue$.complete();
		const workers = this.workers.value;
		this.workers.next( [] );
		this.workers.complete();
		for( const worker of workers ) {
			worker.postMessage( { type: 'shutdown' } );
		}
	}

	public toString() {
		return `[Worker ${this.name}]`;
	}
}
