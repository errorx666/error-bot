declare module 'striptags' {
	export default function striptags( str: string, allowedTags?: readonly string[], tagReplacement?: string ): string;
}

declare namespace __WebpackModuleApi {
	interface RequireContext {
		id: string;
	}
}

declare interface ObjectConstructor {
	fromEntries<T extends keyof any, K>( entries: [ T, K ][] ): Record<T, K>;
}

declare interface PromiseConstructor {
	allSettled( p: readonly Promise<any>[] ): Promise<readonly { readonly status: 'fulfilled'|'rejected'; }[]>;
}

declare interface URLSearchParams {
	entries(): [ string, string ][];
}

declare module 'moment-duration-format' {
	import moment from 'moment';
	export default function momentDurationFormatSetup( m: typeof moment ): void;
}

declare module 'fast-levenshtein' {
	export interface LevenshteinOptions {
		useCollator?: boolean;
	}
	export function get( s1: string, s2: string, options?: LevenshteinOptions ): number;
}

declare module 'sowpods' {
	const sowpods: readonly string[];
	export default sowpods;
}
