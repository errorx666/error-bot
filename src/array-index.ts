import { fromJS, Map } from 'immutable';

export class ArrayIndex<T extends object, U extends keyof T> {
	private index: Map<Readonly<Pick<T, U>>, number>;
	private readonly indexProps: readonly U[];
	public constructor( private readonly array: ArrayLike<T>, ...indexProps: readonly U[] ) {
		this.indexProps = [ ...indexProps ];
	}

	public getIndex( key: Readonly<Pick<T, U>> ) {
		if( this.index == null ) this.reindex();
		return this.index.get( fromJS( key ) as unknown as typeof key );
	}

	public get( key: Readonly<Pick<T, U>> ) {
		const i = this.getIndex( key );
		if( i < 0 ) return undefined;
		return this.array[ i ];
	}

	public has( key: Readonly<Pick<T, U>> ) {
		if( this.index == null ) this.reindex();
		return this.index.has( fromJS( key ) as unknown as typeof key );
	}

	public reindex() {
		let index = Map<Readonly<Pick<T, U>>, number>();
		const { array, indexProps } = this;
		for( let i = 0; i < array.length; ++i ) {
			const value = array[ i ];
			const key = Object.fromEntries( indexProps.map( prop => ( [ prop, value[ prop ] ] ) ) ) as Pick<T, U>;
			index = index.set( fromJS( key ) as unknown as typeof key, i );
		}
		this.index = index;
	}
}
