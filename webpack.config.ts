import fs from 'fs';
import path from 'path';
import upath from 'upath';
import jsYaml from 'js-yaml';
import _ from 'lodash';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import TsconfigPathsWebpackPlugin from 'tsconfig-paths-webpack-plugin';
import webpackNodeExternals from 'webpack-node-externals';
import { Configuration, HotModuleReplacementPlugin, ProvidePlugin, IgnorePlugin, RuleSetUseItem } from 'webpack';
import ForkTsCheckerWebpackPlugin from 'fork-ts-checker-webpack-plugin';
import WebpackCleanObsoleteChunksPlugin from 'webpack-clean-obsolete-chunks';

import glob from 'fast-glob';
import { AutostartWebpackPlugin } from '@errorx666/autostart-webpack-plugin';

export default async function getConfiguration( env = {} as { readonly production?: boolean; }, flags = {} as { readonly hot?: boolean; watch?: boolean; } ) {
	const isProduction = !!env?.production;
	const isDevelopment = !isProduction;

	const isWatch = !!flags?.watch;

	const config = jsYaml.load( await fs.promises.readFile( path.resolve( __dirname, 'webpack', 'webpack.config.yaml' ), 'utf8' ) ) as {
		readonly loaders: Record<string, RuleSetUseItem>;
		readonly configuration: Configuration;
	};

	const { loaders, configuration } = config;

	const mode = isProduction ? 'production' : 'development';
	const hmrPath = 'webpack/hot/poll?100';

	const esVersions = [ 'next', ...( function *() {
		for( let year = ( new Date ).getFullYear(); year >= 2015; --year ) {
			yield String( year );
		}
	}() ), '6' ];
	const esTypes = [ 'fesm', 'fes', 'esm', 'es', 'jsm', 'js' ];

	const experiments = {
		topLevelAwait: true
	} as Configuration[ 'experiments' ];

	const mainFields = [ ...esTypes.flatMap( esType => esVersions.map( esVersion => `${esType}${esVersion}` ) ), 'module', 'main' ];

	const nodeTsconfig = path.resolve( __dirname, 'tsconfig.json' );

	const nodeConfig = _.merge( {}, configuration, { mode, resolve: { mainFields } } as Configuration, ( {
		// cache: false,
		entry: {
			...Object.fromEntries(
				( await glob( [ 'src/workers/**/*.worker.[jt]s{,x}' ], {
					cwd: __dirname
				} ) ).map( p => ( [
					upath.trimExt( upath.relative( upath.join( __dirname, 'src' ), p ), [ '.worker' ] ),
					[
						path.resolve( __dirname, 'src', 'workers', 'bootstrap' ),
						path.resolve( __dirname, p )
					]
				] ) )
			),
			index: [
				...( isWatch ? [ hmrPath ] : [] ),
				'source-map-support/register',
				path.resolve( __dirname, 'src', 'index' )
			]
		},
		experiments,
		externals: [ webpackNodeExternals( { allowlist: [
			hmrPath
		] } ) ],
		module: {
			rules: [
				{ test: /\.(?:xml|xslt)$/, use: [ loaders.raw ] },
				{ test: /\.worker\.js$/, use: [ loaders.text ] },
				{ test: /\.[jt]sx?$/, use: [ loaders.babel ], include: [
					path.resolve( __dirname, 'src' )
				] },
				{ test: /\.tsx?$/, use: [
					_.merge( {}, loaders.typescript, {
						options: {
							configFile: nodeTsconfig
						}
					} )
				] },
				{ test: /\.ya?ml$/, use: [ loaders.yaml ] }
			]
		},
		output: {
			devtoolModuleFilenameTemplate: '[absolute-resource-path]',
			hashFunction: 'xxhash64'
		},
		optimization: {
			minimize: false
		},
		plugins: [
			new ProvidePlugin( {
				DOMParser: [ 'xmldom', 'DOMParser' ]
			} ),
			new IgnorePlugin( {
				resourceRegExp: /canvas/,
				contextRegExp: /jsdom/
			} ),
			// new ForkTsCheckerWebpackPlugin( {
			// 	async: true,
			// 	formatter: 'codeframe',
			// 	typescript: {
			// 		configFile: nodeTsconfig
			// 	}
			// } ),
			new WebpackCleanObsoleteChunksPlugin,
			...( isWatch ? [
				new HotModuleReplacementPlugin,
				new AutostartWebpackPlugin( {
					modulePath: './dist',
					args: [],
					cwd: __dirname,
					env: {
						NODE_ENV: mode,
						NODE_TLS_REJECT_UNAUTHORIZED: '0',
						NEW_RELIC_NO_CONFIG_FILE: 'true',
						NEW_RELIC_HOME: path.resolve( __dirname, 'dist' ),
						NODE_OPTIONS: '--inspect=9229'
					}
				} )
			] : [] )
		],
		resolve: {
			alias: {
				'~workers': path.resolve( __dirname, 'src', 'workers' )
			},
			plugins: [
				// debugResolvePlugin,
				new TsconfigPathsWebpackPlugin( {
					configFile: nodeTsconfig
				} ) as any
			]
		}
	} as Configuration ) );

	const wwwtempTsconfig = path.resolve( __dirname, 'src', 'wwwtemp', 'tsconfig.json' );

	const wwwtempConfig = _.merge( {}, { mode } as Configuration, ( {
		// cache: false,
		devtool: 'inline-source-map',
		entry: {
			index: path.resolve( __dirname, 'src', 'wwwtemp', 'index.js' )
		},
		experiments,
		module: {
			rules: [
				// { test: /\.[jt]sx?$/, use: [ loaders.babel ], include: [ path.resolve( __dirname, 'src' ) ] },
				{ test: /\.tsx?$/, use: [
					_.merge( {}, loaders.typescript, {
						options: {
							configFile: wwwtempTsconfig
						}
					} )
				] },
				// { test: /\.[jt]sx?$/, include: [ path.resolve( __dirname, 'src' ) ], use: [ loaders.babel ] },
				{ test: /\.tsx?$/, use: [ { loader: 'ts-loader', options: {
					configFile: path.resolve( __dirname, 'src', 'wwwtemp', 'tsconfig.json' ),
					transpileOnly: true
				} } ] },
				{ test: /\.ya?ml$/, use: [ loaders.yaml ] }

			]
		},
		output: {
			hashFunction: 'xxhash64',
			path: path.resolve( __dirname, 'dist', 'wwwtemp' ),
			filename: '[name].js'
		},
		performance: {
			hints: false
		},
		plugins: [
			new HtmlWebpackPlugin( {} ) //,
			// new ForkTsCheckerWebpackPlugin( {
			// 	async: true,
			// 	formatter: 'codeframe',
			// 	typescript: {
			// 		configFile: wwwtempTsconfig
			// 	}
			// } )
		],
		resolve: {
			extensions: [ '.tsx', '.ts', '.jsx', '.js', '.json' ],
			plugins: [ new TsconfigPathsWebpackPlugin( {
				configFile: wwwtempTsconfig
			} ) as any ]
		}
	} as Configuration ) );

	return [ nodeConfig, wwwtempConfig ] as readonly Configuration[];
}
